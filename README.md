**Polygon Hunter**
====

**Overview**
====

*2018/9月に完成した作品です。<br>制作期間はフレームワークの制作を含めて3ヶ月です。*
*  [Application Framework](Application%20Framework) - Polygon Hunterを制作時に作成したフレームワークです。
*  [Execution Data](Execution%20Data) - Polygon Hunterの実行データです。
*  [Develop](Develop) - Polygon Hunterの開発データです。
*  [DirectX SDK Installer](DirectX%20SDK%20Installer) - DirectX SDK の インストールプログラムです。

**Demo**
====
*  [YouTube](https://youtu.be/sd26Z4WLxw4)

**Requirement**
====

*  [IDE (Integrated Development Environment)](https://my.visualstudio.com/Downloads?q=Visual%20Studio%202017&pgroup=) - Visual Studio 2017
*  [End-User Runtime](https://www.microsoft.com/ja-jp/download/details.aspx?id=34429) - DirectX 9.0c End-User Runtime

##

![VisualStudio2017](Asset/VisualStudio2017.png)![c++](Asset/C__.png)![DirectX9SDK](Asset/DirectX9SDK.png)