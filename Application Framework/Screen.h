//==========================================================================
// スクリーン[Screen.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"
#include "LoadScreen.h"
#include "Fade.h"

//==========================================================================
//
// class  : CScreen
// Content: スクリーン
//
//==========================================================================
class CScreen : private mslib::DX9_SetRender, private mslib::DX9_SetSampler
{
public:
    CScreen();
    ~CScreen();
    // 初期化
    bool Init(HINSTANCE hInstance, HWND hWnd);
    // 解放
    void Uninit(void);
    // 更新
    bool Update(void);
    // 描画
    void Draw(void);
    // シーン変更キー
    static void screenchange(SceneName Name);
private:
    // スクリーンの切り替え
    void change(SceneName Name);
    // 最初の初期化
    bool startinit(void);
    // 最初の初期化時の描画
    void startdraw(void);
    // 最初の初期化データの破棄
    void startdatarelease(void);
    // スクリーンの初期化
    bool initializer(void);
    // フェード
    void fade(void);

    // ワイヤーフレームに切り替える
    void SetFillmode(void);

    // 描画モード
    void Fillmode(LPDIRECT3DDEVICE9 pDevice);

    // 描画モード
    void EndFillmode(LPDIRECT3DDEVICE9 pDevice);

    // フィルタリングのセット
    void SetFittering(void);

    // フィルタリング
    void Fittering(LPDIRECT3DDEVICE9 pDevice);

    // 更新の停止
    bool Updatestop(void);

    // デバッグ用シーンchange
    void DebugSceneChange(void);
private:
    CFade * m_fade; // フェード
    CLoadScreen *m_load; // ロード画面
    CSceneManager m_scene; // スクリーンセット
    mslib::DX9_ImGui m_ImGui;
    bool m_debugkey; // デバッグキー
    bool m_start; // 初期化フラグ
    bool m_change; // 切り替えフラグ
    int m_count; // カウンタ
    int m_end_count; // カウンタ
    bool m_bFillmode;
    bool m_bFittering;
    bool m_bUpdatestop;
    bool m_bstartinitdata; // 初期時格納データを設定する場合このフラグをtrueに
    bool m_bstartdraw;
    bool m_initializer;

    static bool m_screenchange;
    static SceneName m_name; // name

    const char *m_pchar[2] = { "false","true", };
};
