//=============================================================================
// resource file path
// 2019/02/11
// 18:18:20
//=============================================================================
#pragma once

//=============================================================================
// filename extension [.DDS]
//=============================================================================

#ifndef RESOURCE_LoadTex_DDS
#define RESOURCE_LoadTex_DDS "resource/texture/Load/LoadTex.DDS"
#endif // !RESOURCE_LoadTex_DDS
#ifndef RESOURCE_NowLoading_DDS
#define RESOURCE_NowLoading_DDS "resource/texture/Load/NowLoading.DDS"
#endif // !RESOURCE_NowLoading_DDS

//=============================================================================
// filename extension [.bmp]
//=============================================================================

#ifndef RESOURCE_画像表示枠サイズ_bmp
#define RESOURCE_画像表示枠サイズ_bmp "画像表示枠サイズ.bmp"
#endif // !RESOURCE_画像表示枠サイズ_bmp
