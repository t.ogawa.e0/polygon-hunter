//==========================================================================
// WindowsAPI[WindowsAPI.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "WindowsAPI.h"

CWindowsAPI::CWindowsAPI(const std::string class_name, const std::string window_name)
{
    this->m_class_name = class_name;
    this->m_window_name = window_name;
    this->m_hWnd = nullptr;
    this->m_msg = MSG();
    this->m_wcex = WNDCLASSEX();
    this->m_rect = RECT();
}

CWindowsAPI::~CWindowsAPI()
{
    this->m_class_name.clear();
    this->m_window_name.clear();

    UnregisterClass(this->m_wcex.lpszClassName, this->m_wcex.hInstance);
}

//==========================================================================
// ウィンドウクラスの情報セット
// hInstance インスタンスハンドル
// __WndProc WndProc
// hIcon アイコンのハンドル
// hIconSm 16×16の小さいサイズのアイコンのハンドル
// lpszMenuName デフォルトメニュー名
ATOM CWindowsAPI::MyClass(UINT style, WNDPROC __WndProc, LPCSTR hIcon, LPCSTR hIconSm, LPCSTR lpszMenuName, HINSTANCE hInstance)
{
    this->m_wcex.cbSize = sizeof(this->m_wcex);                 // 構造体のサイズ
    this->m_wcex.style = style;                                 // ウインドウスタイル
    this->m_wcex.lpfnWndProc = __WndProc;                       // そのウインドウのメッセージを処理するコールバック関数へのポインタ
    this->m_wcex.cbClsExtra = 0L;                               // ウインドウクラス構造体の後ろに割り当てる補足バイト数．普通0．
    this->m_wcex.cbWndExtra = 0L;                               // ウインドウインスタンスの後ろに割り当てる補足バイト数．普通0．
    this->m_wcex.hInstance = hInstance;                         // このクラスのためのウインドウプロシージャがあるインスタンスハンドル．
    this->m_wcex.hIcon = LoadIcon(hInstance, hIcon);            // アイコンのハンドル
    this->m_wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);		// マウスカーソルのハンドル．LoadCursor(nullptr, IDC_ARROW )とか．
    this->m_wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);    // ウインドウ背景色(背景描画用ブラシのハンドル)．
    this->m_wcex.lpszMenuName = lpszMenuName;					// デフォルトメニュー名(MAKEINTRESOURCE(メニューID))
    this->m_wcex.lpszClassName = this->m_class_name.c_str();	// このウインドウクラスにつける名前
    this->m_wcex.hIconSm = LoadIcon(hInstance, hIconSm);        // 16×16の小さいサイズのアイコン

    // ウィンドウの情報
    AdjustWindowRect(&this->m_rect, this->m_wcex.style, TRUE);

    return RegisterClassEx(&this->m_wcex);
}

//==========================================================================
// ウィンドウの生成
// dwStyle ウィンドウスタイル
// rect ウィンドウサイズ
// hWndParent 親ウィンドウまたはオーナーウィンドウのハンドル
// hMenu メニューハンドルまたは子ウィンドウ ID
// lpParam ウィンドウ作成データ
bool CWindowsAPI::Create(DWORD dwStyle, RECT rect, HWND hWndParent, HMENU hMenu, LPVOID lpParam, int nCmdShow)
{
    this->m_hWnd = CreateWindow(this->m_class_name.c_str(), this->m_window_name.c_str(), dwStyle, rect.left, rect.top, rect.right, rect.bottom, hWndParent, hMenu, this->m_wcex.hInstance, lpParam);
    this->m_rect = rect;

    if (this->m_hWnd == nullptr)
    {
        MessageBox(this->m_hWnd, "ウィンドウが生成されませんでした。", "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
        return true;
    }

    ShowWindow(this->m_hWnd, nCmdShow);
    UpdateWindow(this->m_hWnd);

    return false;
}

//==========================================================================
// メッセージのゲッター
MSG & CWindowsAPI::GetMSG(void)
{
    return this->m_msg;
}

//==========================================================================
// ウィンドウRECT
RECT CWindowsAPI::GetWindowRECT(void)
{
    return this->m_rect;
}

//==========================================================================
// ウィンドウハンドル 
HWND CWindowsAPI::GetHWND(void)
{
    return this->m_hWnd;
}
