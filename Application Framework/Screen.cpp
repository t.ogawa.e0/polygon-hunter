//==========================================================================
// スクリーン[Screen.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Screen.h"

//==========================================================================
// 実体
//==========================================================================
bool CScreen::m_screenchange;
SceneName CScreen::m_name; // name

CScreen::CScreen()
{
    this->m_fade = nullptr;
    this->m_load = nullptr;
    this->m_change = false;
    this->m_count = 0;
    this->m_end_count = 0;
    this->m_start = false;
    this->m_debugkey = false;
    this->m_bFillmode = false;
    this->m_bFittering = false;
    this->m_bUpdatestop = false;
    this->m_bstartinitdata = false; // 初期時のみ読み込もデータがあるときtrueに
    this->m_bstartdraw = false;
    this->m_initializer = false;
}

CScreen::~CScreen()
{
}

//==========================================================================
// 初期化
bool CScreen::Init(HINSTANCE hInstance, HWND hWnd)
{
    srand((unsigned)time(nullptr));
    if (mslib::DX9_DeviceManager::GetDXDevice()->CreateDevice()) { return true; }
    if (mslib::DX9_DeviceManager::Init(mslib::DX9_DeviceInitList::keyboard, hInstance, hWnd)) { return true; }
    if (mslib::DX9_DeviceManager::Init(mslib::DX9_DeviceInitList::Mouse, hInstance, hWnd)) { return true; }
    if (mslib::DX9_DeviceManager::Init(mslib::DX9_DeviceInitList::XAudio2, hInstance, hWnd)) { return true; }

    this->m_change = false;
    this->m_count = 0;
    this->m_start = false;
    this->m_debugkey = false;
    this->m_name = SceneName::begin;
    this->m_screenchange = false;

    mslib::_new(this->m_fade);
    mslib::_new(this->m_load);

    if (this->m_fade->Init()) { return true; }

    this->m_ImGui.Init(hWnd, mslib::DX9_DeviceManager::GetDXDevice()->GetD3DDevice());

    return this->m_load->Init();
}

//==========================================================================
// 解放
void CScreen::Uninit(void)
{
    this->m_scene.Uninit();
    this->startdatarelease();
    this->m_ImGui.Uninit();
    mslib::_delete(this->m_fade);
    mslib::_delete(this->m_load);

    mslib::DX9_DeviceManager::Release();
}

//==========================================================================
// 更新処理
bool CScreen::Update(void)
{
    this->m_ImGui.NewFrame();
    this->m_ImGui.Update();
    this->m_ImGui.NewWindow("Operation window", true);

    this->DebugSceneChange();

    if (this->startinit()) { return true; }

    if (this->m_bstartinitdata == false)
    {
        if (this->initializer()) { return true; }

        mslib::DX9_DeviceManager::Update();
        this->m_fade->Update();
        this->fade();
        this->SetFillmode();
        this->SetFittering();

        // ロードが終わっているとき
        if (this->m_change == true)
        {
            if (!this->Updatestop())
            {
                this->m_scene.Update();
            }
        }
        else
        {
            this->m_load->Update();
        }
    }

    this->m_ImGui.EndWindow();
    this->m_ImGui.EndFrame();

    return false;
}

//==========================================================================
// 描画処理
void CScreen::Draw(void)
{
    auto pDevice = mslib::DX9_DeviceManager::GetDXDevice()->GetD3DDevice();	//デバイス渡し	
    auto pd3dpp = mslib::DX9_DeviceManager::GetDXDevice()->Getd3dpp();

    //Direct3Dによる描画の開始
    if (mslib::DX9_DeviceManager::GetDXDevice()->DrawBegin())
    {
        this->Fillmode(pDevice);
        this->Fittering(pDevice);

        // 画面切り替えが終わっているとき
        if (this->m_change == true)
        {
            this->m_scene.Draw();
            this->m_fade->Draw();
        }
        else
        {
            this->m_load->Draw();
            this->startdraw();
        }
        this->EndFillmode(pDevice);
        this->m_ImGui.Draw(pDevice);
    }
    this->m_ImGui.DeviceReset(mslib::DX9_DeviceManager::GetDXDevice()->DrawEnd(), pDevice, &pd3dpp);
}

//==========================================================================
// シーン変更キー
void CScreen::screenchange(SceneName Name)
{
    m_screenchange = true;
    m_name = Name;
}

//==========================================================================
// スクリーンの切り替え
void CScreen::change(SceneName Name)
{
    // すべての初期化
    this->m_scene.ChangeScene(Name);
}

//==========================================================================
// 最初の初期化
bool CScreen::startinit(void)
{
    // 最初に読み込むデータがある場合のみ有効
    if (this->m_bstartdraw)
    {
        // この間にセット

        // この間にセット
        this->m_bstartinitdata = false;
        this->m_bstartdraw = false;
    }

    return false;
}

//==========================================================================
// 最初の初期化時の描画
void CScreen::startdraw(void)
{
    // 最初に読み込むデータがある場合のみ有効
    if (this->m_bstartinitdata)
    {
        this->m_bstartdraw = true;
    }
}

//==========================================================================
// 最初の初期化データの破棄
void CScreen::startdatarelease(void)
{
}

//==========================================================================
// スクリーンの初期化
bool CScreen::initializer(void)
{
    //==========================================================================
    // デバッグ用
#if defined(_DEBUG) || defined(DEBUG)
    if (this->m_count == 30 && this->m_change == false && this->m_start == true && this->m_debugkey == true)
    {
        this->change(SceneName::Default);
        this->m_debugkey = false;
        this->m_initializer = false;
    }
#endif
    //==========================================================================
    // 最初の画面
    if (this->m_count == 30 && this->m_change == false && this->m_start == false)
    {
        this->m_start = true;
        this->change(SceneName::Default);
        this->m_initializer = false;
    }

    //==========================================================================
    // シーン変更
    if (this->m_count == 30 && this->m_change == false && this->m_name != SceneName::begin)
    {
        this->m_start = true;
        this->change(this->m_name);
        this->m_name = SceneName::begin;
        this->m_initializer = false;
    }

    //==========================================================================
    // 初期化中
    if (30 <= this->m_count && this->m_change == false && this->m_initializer != true)
    {
        // 全インスタンスの初期化終了判定
        this->m_initializer = this->m_scene.Init();
        // 全てのインスタンスの初期化が終了した際に、ロードを終えるための情報を与える
        if (this->m_initializer == true)
        {
            this->m_end_count = this->m_count;
            this->m_end_count += 30;
        }
    }

    //==========================================================================
    // フェード終了判定
    if (this->m_count == this->m_end_count && this->m_change == false && this->m_initializer == true)
    {
        this->m_fade->Out();
        this->m_change = true;
    }

    // ロードカウンタ
    if (this->m_change == false)
    {
        this->m_count++;
    }

    return false;
}

//==========================================================================
// フェード
void CScreen::fade(void)
{
    // フェードイン
    if (this->m_screenchange)
    {
        if (!this->m_fade->GetDraw())
        {
            this->m_fade->In();
        }
    }

    // フェードが終わりスクリーンがロード画面ではない場合
    if (this->m_fade->FeadInEnd() && this->m_change == true)
    {
        this->m_screenchange = false;
        this->m_change = false;
        this->m_count = 0;
    }
}

//==========================================================================
// ワイヤーフレームに切り替える
void CScreen::SetFillmode(void)
{
    if (this->m_ImGui.MenuItem(this->m_ImGui.CreateText("Switch to wire frame : %s", m_pchar[this->m_bFillmode]).c_str()))
    {
        mslib::bool_change(this->m_bFillmode);
    }
}

//==========================================================================
// 描画モード
void CScreen::Fillmode(LPDIRECT3DDEVICE9 pDevice)
{
    if (this->m_bFillmode)
    {
        this->SetRenderWIREFRAME(pDevice);
    }
    else
    {
        this->SetRenderSOLID(pDevice);
    }
}

//==========================================================================
// 描画モード
void CScreen::EndFillmode(LPDIRECT3DDEVICE9 pDevice)
{
    if (this->m_bFillmode)
    {
        this->SetRenderSOLID(pDevice);
    }
}

//==========================================================================
// フィルタリングのセット
void CScreen::SetFittering(void)
{
    if (this->m_ImGui.MenuItem(this->m_ImGui.CreateText("Change filtering : %s", m_pchar[this->m_bFittering]).c_str()))
    {
        mslib::bool_change(this->m_bFittering);
    }
}

//==========================================================================
// フィルタリング
void CScreen::Fittering(LPDIRECT3DDEVICE9 pDevice)
{
    if (this->m_bFittering)
    {
        this->SamplerFitteringGraphical(pDevice);
    }
    else
    {
        this->SamplerFitteringLINEAR(pDevice);
    }
}

//==========================================================================
// 更新の停止
bool CScreen::Updatestop(void)
{
    if (this->m_ImGui.MenuItem(this->m_ImGui.CreateText("Stop updating : %s", m_pchar[this->m_bUpdatestop]).c_str()))
    {
        mslib::bool_change(this->m_bUpdatestop);
    }
    return this->m_bUpdatestop;
}

//==========================================================================
// デバッグ用シーンchange
void CScreen::DebugSceneChange(void)
{
    this->m_ImGui.NewWindow("Scene manager", true);
    if (this->m_ImGui.MenuItem("Default"))
    {
        this->screenchange(SceneName::Default);
    }
    if (this->m_ImGui.MenuItem("End"))
    {
        DestroyWindow(mslib::DX9_DeviceManager::GetDXDevice()->GetHwnd());
    }
    this->m_ImGui.EndWindow();
}