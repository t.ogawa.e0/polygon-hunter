//==========================================================================
// キーボード入力処理[dinput_keyboard.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "dinput_keyboard.h"

_MSLIB_BEGIN

DirectInputKeyboard::DirectInputKeyboard()
{
}

DirectInputKeyboard::~DirectInputKeyboard()
{
}

//==========================================================================
/**
@brief 初期化
@param hInstance [in] インスタンスハンドル
@param hWnd [in] ウィンドウハンドル
@return 失敗時に true が返ります
*/
bool DirectInputKeyboard::Init(HINSTANCE hInstance, HWND hWnd)
{
	// DirectInputオブジェクトの作成
	if (FAILED(DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&this->m_DInput, nullptr)))
	{
		MessageBox(hWnd, "DirectInputオブジェクトが作れませんでした", "警告", MB_ICONWARNING);
		return true;
	}

	// デバイスの作成
	if (FAILED(this->m_DInput->CreateDevice(GUID_SysKeyboard, &this->m_DIDevice, nullptr)))
	{
		MessageBox(hWnd, "キーボードがありませんでした", "警告", MB_ICONWARNING);
		return true;
	}

	if (this->m_DIDevice != nullptr)
	{
		// データフォーマットを設定
		if (FAILED(this->m_DIDevice->SetDataFormat(&c_dfDIKeyboard)))
		{
			MessageBox(hWnd, "キーボードのデータフォーマットを設定できませんでした。", "警告", MB_ICONWARNING);
			return true;
		}

		// 協調モードを設定（フォアグラウンド＆非排他モード）
		if (FAILED(this->m_DIDevice->SetCooperativeLevel(hWnd, (DISCL_FOREGROUND/*DISCL_BACKGROUND*/ | DISCL_NONEXCLUSIVE))))
		{
			MessageBox(hWnd, "キーボードの協調モードを設定できませんでした。", "警告", MB_ICONWARNING);
			return true;
		}

		// キーボードへのアクセス権を獲得(入力制御開始)
        this->m_DIDevice->Acquire();
	}

	return false;
}

//==========================================================================
/**
@brief 更新
*/
void DirectInputKeyboard::Update(void)
{
	BYTE aState[256];

    if (this->m_DIDevice == nullptr)
    {
        return;
    }

    // デバイスからデータを取得
    if (SUCCEEDED(this->m_DIDevice->GetDeviceState(sizeof(aState), aState)))
    {
        // aKeyState[m_KeyMax]&0x80
        for (int i = 0; i < 256; i++)
        {
            // キートリガー・リリース情報を生成
            this->m_StateTrigger[i] = (this->m_State[i] ^ aState[i]) & aState[i];
            this->m_StateRelease[i] = (this->m_State[i] ^ aState[i]) & this->m_State[i];

            // キーリピート情報を生成
            if (aState[i])
            {
                if (this->m_StateRepeatCnt[i] < 20)
                {
                    this->m_StateRepeatCnt[i]++;
                    if (this->m_StateRepeatCnt[i] == 1 || this->m_StateRepeatCnt[i] >= 20)
                    {// キーを押し始めた最初のフレーム、または一定時間経過したらキーリピート情報ON
                        this->m_StateRepeat[i] = aState[i];
                    }
                    else
                    {
                        this->m_StateRepeat[i] = 0;
                    }
                }
            }
            else
            {
                this->m_StateRepeatCnt[i] = 0;
                this->m_StateRepeat[i] = 0;
            }

            // キープレス情報を保存
            this->m_State[i] = aState[i];
        }
    }
    else
    {
        // キーボードへのアクセス権を取得
        this->m_DIDevice->Acquire();
    }
}

//==========================================================================
/**
@brief Key Cast(int)
@param cast [in] キャスト対象
@return キャストされた値が返ります
*/
DirectInputKeyboardButton DirectInputKeyboard::KeyCast(int cast)
{
    return (DirectInputKeyboardButton)cast;
};

//==========================================================================
/**
@brief プレス
@param key [in] 入力キー
@return 押されている場合 true が返ります
*/
bool DirectInputKeyboard::Press(DirectInputKeyboardButton key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_State[(int)key] & 0x80) ? true : false;
}

//==========================================================================
/**
@brief トリガー
@param key [in] 入力キー
@return 押されている場合 true が返ります
*/
bool DirectInputKeyboard::Trigger(DirectInputKeyboardButton key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_StateTrigger[(int)key] & 0x80) ? true : false;
}

//==========================================================================
/**
@brief リピート
@param key [in] 入力キー
@return 押されている場合 true が返ります
*/
bool DirectInputKeyboard::Repeat(DirectInputKeyboardButton key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_StateRelease[(int)key] & 0x80) ? true : false;
}

//==========================================================================
/**
@brief リリース
@param key [in] 入力キー
@return 押されている場合 true が返ります
*/
bool DirectInputKeyboard::Release(DirectInputKeyboardButton key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_StateRelease[(int)key] & 0x80) ? true : false;
}

_MSLIB_END