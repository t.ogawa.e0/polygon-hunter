//==========================================================================
// テキスト表示[DX9_Text.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Text.h"

_MSLIB_BEGIN

DX9_Text::DX9_Text(LPDIRECT3DDEVICE9 pDevice)
{
    this->m_Device = pDevice;
    this->m_color = 255;
    this->m_pos = 0;
    this->m_font = nullptr;
    this->m_fontheight = 0;
    this->m_fontheight_master = 20;
    this->m_Lock = false;
}

DX9_Text::~DX9_Text()
{
    this->Release();
}

//==========================================================================
/**
@brief 初期化 フォントの種類はデフォルトで Terminal になってます。
@param w [in] 画面の横幅
@param h [in] 画面の高さ
*/
void DX9_Text::Init(int w, int h)
{
    this->Init(w, h, "Terminal");
}

//==========================================================================
/**
@brief 初期化 フォントの種類はデフォルトで Terminal になってます。
@param w [in] 画面の横幅
@param h [in] 画面の高さ
@param font [in] フォント
*/
void DX9_Text::Init(int w, int h, const char * font)
{
	this->m_color = CColor<int>(255, 255, 255, 255);
	this->m_pos = CTexvec<int>(this->m_pos.x, this->m_pos.y, w, h);
	this->m_fontheight = 0;
	this->m_Lock = false;

	D3DXCreateFont
	(
        this->m_Device,				// デバイス
		this->m_fontheight_master,	// 文字の高さ
		0,							// 文字の幅
		FW_REGULAR,					// フォントの太さ
		0,							// MIPMAPのレベル
		FALSE,						// イタリックか？
		SHIFTJIS_CHARSET,			// 文字セット
		OUT_DEFAULT_PRECIS,			// 出力精度
		DEFAULT_QUALITY,			// 出力品質
		FIXED_PITCH | FF_SCRIPT,	// フォントピッチとファミリ
		TEXT(font),					// フォント名
		&this->m_font				// Direct3Dフォントへのポインタへのアドレス
	);
}

//==========================================================================
/**
@brief 解放
*/
void DX9_Text::Release(void)
{
    if (this->m_font != nullptr)
    {
        this->m_font->Release();
        this->m_font = nullptr;
    }
}

//==========================================================================
/**
@brief 色
@param r [in] 色
@param g [in] 色
@param b [in] 色
@param a [in] 色
*/
void DX9_Text::color(int r, int g, int b, int a)
{
    this->m_color = CColor<int>(r, g, b, a);
}

//==========================================================================
/**
@brief 色
@param input [in] {r,g,b,a}
*/
void DX9_Text::color(const CColor<int>& input)
{
    this->m_color = input;
}

//==========================================================================
/**
@brief 座標
@param x [in] X座標
@param y [in] Y座標
*/
void DX9_Text::pos(int x, int y)
{
    this->m_pos = CTexvec<int>(x, y, this->m_pos.w, this->m_pos.h);
}

//==========================================================================
/**
@brief テキスト表示
@param input [in] 表示内容
*/
void DX9_Text::text(const char * input, ...)
{
	if (this->m_Lock)
	{
		va_list argp;
		RECT rect = { this->m_pos.x, this->m_pos.y + this->m_fontheight, this->m_pos.w, this->m_pos.h };
		char strBuf[1024] = { 0 };

        va_start(argp, input);
        vsprintf_s(strBuf, 1024, input, argp);
        va_end(argp);
        this->m_font->DrawText(nullptr, strBuf, -1, &rect, DT_LEFT, this->m_color.get());
		this->m_fontheight += this->m_fontheight_master;
	}
}

//==========================================================================
/**
@brief テキストの始まりに必ずセットしよう。
*/
void DX9_Text::textnew(void)
{
    this->m_Lock = true; this->m_fontheight = 0;
}

//==========================================================================
/**
@brief テキストの終わりに必ずセットしよう。
*/
void DX9_Text::textend(void)
{
    this->m_Lock = false;
}

//==========================================================================
/**
@brief 文字のサイズ
@param input [in] フォントサイズ
*/
void DX9_Text::size(int input)
{
    this->m_fontheight_master = input;
}

_MSLIB_END