//==========================================================================
// クリエイトバッファ[DX9_CreateBuffer.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_CreateBuffer.h"

_MSLIB_BEGIN

//==========================================================================
/**
@brief バーテックスバッファの生成
@param pDevice [in] デバイス
@param hWid [in] ウィンドウハンドル
@param Length [in] データ型のサイズ
@param Usage [in] Usages for Vertex/Index buffers
@param FVF [in] FVF
@param Pool [in] Pool types
@param ppVertexBuffer [in/out] バーテックスバッファ
@param pSharedHandle [in] Handle to an Object
@return 失敗時 true が返ります
*/
bool DX9_CreateBuffer::CreateVertexBuffer(LPDIRECT3DDEVICE9 pDevice, HWND hWid, UINT Length, DWORD Usage, DWORD FVF, D3DPOOL Pool, IDirect3DVertexBuffer9** ppVertexBuffer, HANDLE* pSharedHandle)
{
	if (FAILED(pDevice->CreateVertexBuffer(Length, Usage, FVF, Pool, ppVertexBuffer, pSharedHandle)))
	{
        MessageBox(hWid, "頂点バッファが作れませんでした。", "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
		return true;
	}

	return false;
}

//==========================================================================
/**
@brief インデックスバッファの生成
@param pDevice [in] デバイス
@param hWid [in] ウィンドウハンドル
@param Length [in] データ型のサイズ
@param Usage [in] Usages for Vertex/Index buffers
@param Format [in] D3D9Ex only
@param Pool [in] Pool types
@param ppIndexBuffer [in/out] インデックスバッファ
@param pSharedHandle [in] Handle to an Object
@return 失敗時 true が返ります
*/
bool DX9_CreateBuffer::CreateIndexBuffer(LPDIRECT3DDEVICE9 pDevice, HWND hWid, UINT Length, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DIndexBuffer9** ppIndexBuffer, HANDLE* pSharedHandle)
{
    if (FAILED(pDevice->CreateIndexBuffer(Length, Usage, Format, Pool, ppIndexBuffer, pSharedHandle)))
    {
        MessageBox(hWid, "インデックスバッファが作れませんでした。", "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
        return true;
    }
    return false;
}

_MSLIB_END