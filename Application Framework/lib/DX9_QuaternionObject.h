//==========================================================================
// クオータニオンオブジェクト[DX9_QuaternionObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_QuaternionObject
// Content: クオータニオンオブジェクト
//
//==========================================================================
class DX9_QuaternionObject
{
public:
    DX9_QuaternionObject() {
        // クオータニオンの初期化
        D3DXQuaternionIdentity(&this->m_Quaternion);
    }
    virtual ~DX9_QuaternionObject() {}

    // 回転角度の指定 = rot
    // 回転量を指定 = power
    void QuaternionRot(const D3DXVECTOR3 & rot, float power) {
        D3DXQUATERNION quater = D3DXQUATERNION(0, 0, 0, 1);

        // 指定方向に回転
        D3DXQuaternionRotationAxis(&quater, &rot, power);

        // 合成で結果を反映
        D3DXQuaternionMultiply(&this->m_Quaternion, &quater, &this->m_Quaternion);
    }

    // クオータニオンの取得
    const D3DXQUATERNION * GetQuaternion(void) {
        return &this->m_Quaternion;
     }
protected:
    // 行列に合成
    D3DXMATRIX* QuaternionMatrix(D3DXMATRIX *pOut) {
        return D3DXMatrixRotationQuaternion(pOut, &this->m_Quaternion);
    }
protected:
    D3DXQUATERNION m_Quaternion; // クオータニオン
};

_MSLIB_END