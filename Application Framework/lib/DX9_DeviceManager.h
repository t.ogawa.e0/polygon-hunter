//==========================================================================
// デバイスマネージャー[DX9_DeviceManager.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_DirectXDevice.h"
#include "vector_wrapper.h"
#include "dinput_manager.h"
#include "dinput_mouse.h"
#include "dinput_keyboard.h"
#include "dinput_controller.h"
#include "XAudio2.h"

_MSLIB_BEGIN

enum class DX9_DeviceInitList
{
    Mouse, // マウス
    keyboard, // キーボード
    Controller, // コントローラー
    DXDevice, // DirectX9デバイス
    XAudio2, // XAudio2Device
    MAXDevice, // デバイス数
};

//==========================================================================
//
// class  : DX9_DeviceManager
// Content: DX9_DeviceManager
//
//==========================================================================
class DX9_DeviceManager
{
public:
    DX9_DeviceManager();
    ~DX9_DeviceManager();

    /**
    @brief 初期化
    @param Serect [in] 初期化デバイス
    @param hInstance [in] インスタンスハンドル
    @param hWnd [in] ウィンドウハンドル
    @return 失敗時に true が返ります
    */
    static bool Init(DX9_DeviceInitList Serect, HINSTANCE hInstance, HWND hWnd);

    /**
    @brief 解放
    */
    static void Release(void);

    /**
    @brief 更新
    */
    static void Update(void);

    /**
    @brief DirectInputマウスの取得
    @return DirectInputマウス
    */
    static DirectInputMouse * GetMouse(void);

    /**
    @brief DirectInputキーボードの取得
    @return DirectInputキーボード
    */
    static DirectInputKeyboard * GetKeyboard(void);

    /**
    @brief DirectInputコントローラーの取得
    @return DirectInputコントローラー
    */
    static DirectInputController * GetController(void);

    /**
    @brief DirectX9デバイスの取得
    @return DirectX9デバイス
    */
    static DX9_DirectXDevice * GetDXDevice(void);
private:
    static vector_wrapper<DX9_DeviceInitList> m_InitList; // 初期化終了情報の格納
    static DirectInputMouse * m_Mouse; // DirectInputマウス
    static DirectInputKeyboard * m_Keyboard; // DirectInputキーボード
    static DirectInputController * m_Controller; // DirectInputコントローラー
    static DX9_DirectXDevice * m_DXDevice; // DirectX9デバイス
    static XAudio2Device * m_XAudio2; // XAudio2Device
};

_MSLIB_END