//==========================================================================
// 3Dオブジェクトモーション[DX9_3DObjectMotion.h]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_3DObjectMotion.h"

_MSLIB_BEGIN

constexpr float BUF_NONE = 0.0f;

DX9_3DObjectMotionData::DX9_3DObjectMotionData()
{
    this->m_max_rot = D3DXVECTOR3(0, 0, 0);
    this->m_max_pos = D3DXVECTOR3(0, 0, 0);
    this->m_rot = D3DXVECTOR3(0, 0, 0);
    this->m_pos = D3DXVECTOR3(0, 0, 0);
    this->m_buf_rot = D3DXVECTOR3(0, 0, 0);
    this->m_buf_pos = D3DXVECTOR3(0, 0, 0);
    this->m_comparison_result_pos = D3DXVECTOR3(0, 0, 0);
    this->m_comparison_result_rot = D3DXVECTOR3(0, 0, 0);
    this->m_powor = 0.0f;
    this->m_key1 = false;
    this->m_key2 = false;
    this->m_setkey1 = false;
    this->m_setkey2 = false;
}

DX9_3DObjectMotionData::DX9_3DObjectMotionData(const D3DXVECTOR3 max_pos, const D3DXVECTOR3 max_rot, float fpowor)
{
    this->m_max_rot = max_rot;
    this->m_max_pos = max_pos;
    this->m_rot = D3DXVECTOR3(0, 0, 0);
    this->m_pos = D3DXVECTOR3(0, 0, 0);
    this->m_buf_rot = D3DXVECTOR3(0, 0, 0);
    this->m_buf_pos = D3DXVECTOR3(0, 0, 0);
    this->m_powor = fpowor;
    this->m_key1 = false;
    this->m_key2 = false;
    this->m_setkey1 = false;
    this->m_setkey2 = false;
    this->m_comparison_result_pos.x = max(BUF_NONE, this->m_max_pos.x);
    this->m_comparison_result_pos.y = max(BUF_NONE, this->m_max_pos.y);
    this->m_comparison_result_pos.z = max(BUF_NONE, this->m_max_pos.z);
    this->m_comparison_result_rot.x = max(BUF_NONE, this->m_max_rot.x);
    this->m_comparison_result_rot.y = max(BUF_NONE, this->m_max_rot.y);
    this->m_comparison_result_rot.z = max(BUF_NONE, this->m_max_rot.z);
}

DX9_3DObjectMotionData::~DX9_3DObjectMotionData()
{
}

//==========================================================================
/**
@brief 更新
@param Out [in/out] アニメーション対象オブジェクト
*/
void DX9_3DObjectMotionData::Update(void)
{
    // 移動量の処理
    if ((this->m_key1.x == false) || (this->m_key1.y == false) || (this->m_key1.z == false))
    {
        this->m_buf_pos += this->m_max_pos*this->m_powor;
        this->m_pos += this->m_max_pos*this->m_powor;

        this->SetData(this->m_key1.x, BUF_NONE, this->m_buf_pos.x, this->m_comparison_result_pos.x, this->m_max_pos.x);
        this->SetData(this->m_key1.y, BUF_NONE, this->m_buf_pos.y, this->m_comparison_result_pos.y, this->m_max_pos.y);
        this->SetData(this->m_key1.z, BUF_NONE, this->m_buf_pos.z, this->m_comparison_result_pos.z, this->m_max_pos.z);
    }

    // 回転量の処理
    if ((this->m_key2.x == false) || (this->m_key2.y == false) || (this->m_key2.z == false))
    {
        this->m_buf_rot += this->m_max_rot*this->m_powor;
        this->m_rot += this->m_max_rot*this->m_powor;

        this->SetData(this->m_key2.x, BUF_NONE, this->m_buf_rot.x, this->m_comparison_result_rot.x, this->m_max_rot.x);
        this->SetData(this->m_key2.y, BUF_NONE, this->m_buf_rot.y, this->m_comparison_result_rot.y, this->m_max_rot.y);
        this->SetData(this->m_key2.z, BUF_NONE, this->m_buf_rot.z, this->m_comparison_result_rot.z, this->m_max_rot.z);
    }
}

//==========================================================================
/**
@brief データの反映
@param Out [in/out] アニメーション対象オブジェクト
*/
void DX9_3DObjectMotionData::ReflectData(DX9_3DObject * Out)
{
    // 処理データの反映
    Out->MoveX(this->m_pos.x);
    Out->MoveY(this->m_pos.y);
    Out->MoveZ(this->m_pos.z);
    Out->RotX(this->m_rot.x);
    Out->RotY(this->m_rot.y);
    Out->RotZ(this->m_rot.z);
}

//==========================================================================
/**
@brief リセット
*/
void DX9_3DObjectMotionData::Reset(void)
{
    this->m_rot = D3DXVECTOR3(0, 0, 0);
    this->m_pos = D3DXVECTOR3(0, 0, 0);
    this->m_buf_rot = D3DXVECTOR3(0, 0, 0);
    this->m_buf_pos = D3DXVECTOR3(0, 0, 0);
    this->m_key1 = false;
    this->m_key2 = false;
    this->m_setkey1 = false;
    this->m_setkey2 = false;
}

//==========================================================================
/**
@brief 処理終了判定チェック
@return 処理終了時に true が返ります
*/
bool DX9_3DObjectMotionData::Check(void)
{
    // 全ての処理判定が終了のとき
    if ((this->m_key1 == true) && (this->m_key2 == true))
    {
        return true;
    }
    return false;
}

//==========================================================================
/**
@brief 移動量の取得
@return 移動量が返ります
*/
const D3DXVECTOR3 & DX9_3DObjectMotionData::GetPos(void)
{
    return this->m_pos;
}

//==========================================================================
/**
@brief 移動量の登録
@param Set [in] 移動量
*/
void DX9_3DObjectMotionData::SetPos(const D3DXVECTOR3 & Set)
{
    // 未登録のとき
    if (this->m_setkey1 == false)
    {
        this->m_setkey1 = true;
        this->m_pos = Set;
    }
}

//==========================================================================
/**
@brief 回転量の取得
@return 回転量の返ります
*/
const D3DXVECTOR3 & DX9_3DObjectMotionData::GetRot(void)
{
    return this->m_rot;
}

//==========================================================================
/**
@brief 回転量の登録
@param Set [in] 回転量
*/
void DX9_3DObjectMotionData::SetRot(const D3DXVECTOR3 & Set)
{
    // 未登録のとき
    if (this->m_setkey2 == false)
    {
        this->m_setkey2 = true;
        this->m_rot = Set;
    }
}

//==========================================================================
/**
@brief 処理量に登録
@param Set [in] 処理量
*/
void DX9_3DObjectMotionData::SetPowor(float fpowor)
{
    this->m_powor = fpowor;
}

//==========================================================================
/**
@brief 処理量の取得
@return 処理量
*/
float DX9_3DObjectMotionData::GetPowor(void)
{
    return this->m_powor;
}

//==========================================================================
/**
@brief データの更新
@param key [in/out] 処理判定キー
@param none [in] 中間の値
@param buff [in] 移動バフ
@param comparison_result [in] min/max
@param data [in] 加算量
*/
void DX9_3DObjectMotionData::SetData(bool &key, float none, float buff, float comparison_result, float data)
{
    if (none <= comparison_result)
    {
        key = false;
        if (data <= buff)
        {
            key = true;
        }
    }
    if (comparison_result <= none)
    {
        key = false;
        if (buff <= data)
        {
            key = true;
        }
    }
}

DX9_3DObjectMotionLinearData::DX9_3DObjectMotionLinearData()
{
    this->Reset();
}

DX9_3DObjectMotionLinearData::DX9_3DObjectMotionLinearData(const std::string & MotionName, int ID, const D3DXVECTOR3 & vpos, const D3DXVECTOR3 & vrot, bool check)
{
    this->m_vpos = vpos;
    this->m_vrot = vrot;
    this->m_MotionName = MotionName;
    this->m_ID = ID;
    this->m_check = check;
}

DX9_3DObjectMotionLinearData::~DX9_3DObjectMotionLinearData()
{
}

//==========================================================================
/**
@brief 初期化
*/
void DX9_3DObjectMotionLinearData::Reset(void)
{
    this->m_vpos = D3DXVECTOR3(0, 0, 0);
    this->m_vrot = D3DXVECTOR3(0, 0, 0);
    this->m_MotionName = "";
    this->m_ID = 0;
    this->m_check = false;
}

//==========================================================================
/**
@brief 補間データ 移動量の取得
@return 移動量
*/
const D3DXVECTOR3 & DX9_3DObjectMotionLinearData::GetPos(void)
{
    return this->m_vpos;
}

//==========================================================================
/**
@brief 補間データ 回転量の取得
@return 回転量
*/
const D3DXVECTOR3 & DX9_3DObjectMotionLinearData::GetRot(void)
{
    return this->m_vrot;
}

//==========================================================================
/**
@brief 補間データの保有データ名の取得
@return データ名
*/
const std::string & DX9_3DObjectMotionLinearData::GetMotionName(void)
{
    return this->m_MotionName;
}

//==========================================================================
/**
@brief 補間データの保有グループID
@return グループID
*/
int DX9_3DObjectMotionLinearData::GetID(void)
{
    return this->m_ID;
}

//==========================================================================
/**
@brief 処理判定
@return 処理終了時に true が返ります
*/
bool DX9_3DObjectMotionLinearData::Check(void)
{
    return this->m_check;
}

DX9_3DObjectMotion::DX9_3DObjectMotion()
{
}

DX9_3DObjectMotion::~DX9_3DObjectMotion()
{
    this->m_motion_data.clear();
}

//==========================================================================
/**
@brief モーションの生成
@param MotionName [in] モーション名
@param ID [in] グループID
@param param [in] モーションデータ
*/
void DX9_3DObjectMotion::Create(const std::string & MotionName, int ID, DX9_3DObjectMotionData param)
{
    this->m_motion_data[MotionName][ID].push_back(param);
}

//==========================================================================
/**
@brief モーションの生成
@param MotionName [in] モーション名
@param ID [in] グループID
@param Object [in/out] 対象オブジェクト
*/
void DX9_3DObjectMotion::Play(const std::string & MotionName, int ID, DX9_3DObject * Object)
{
    // アニメーション名でデータを検索
    auto itr1 = this->m_motion_data.find(MotionName);

    // 探索に失敗
    if (itr1 == this->m_motion_data.end())
    {
        return;
    }

    // 再生IDでデータを検索
    auto itr2 = itr1->second.find(ID);

    // 探索に失敗
    if (itr2 == itr1->second.end())
    {
        return;
    }

    bool check = false;
    D3DXVECTOR3 vpos = D3DXVECTOR3(0, 0, 0);
    D3DXVECTOR3 vrot = D3DXVECTOR3(0, 0, 0);

    // 補間データの引継ぎ
    if (this->m_linear.GetMotionName() != "")
    {
        if ((this->m_linear.GetMotionName() != MotionName) && (this->m_linear.GetID() != ID))
        {
            vpos = this->m_linear.GetPos();
            vrot = this->m_linear.GetRot();
        }
    }

    // メイン処理
    for (auto itr3 = itr2->second.begin(); itr3 != itr2->second.end(); ++itr3)
    {
        check = itr3->Check();
        if (check == false)
        {
            itr3->SetPos(vpos);
            itr3->SetRot(vrot);
            itr3->Update();
            itr3->ReflectData(Object);
            break;
        }
        else if (check == true)
        {
            vpos = itr3->GetPos();
            vrot = itr3->GetRot();
        }
    }

    if (check == true)
    {
        if (itr2->second.size() != 0)
        {
            auto itr3 = --itr2->second.end();
            itr3->ReflectData(Object);
        }
    }

    // 補間データを更新
    this->m_linear = DX9_3DObjectMotionLinearData(MotionName, ID, vpos, vrot, check);
}

//==========================================================================
/**
@brief 補間データのセット
@param linear [in] 補間データ
*/
void DX9_3DObjectMotion::SetLinearData(const LinearData & linear)
{
    this->m_linear = linear;
}

//==========================================================================
/**
@brief 補間データの取得
@return 補間データ
*/
DX9_3DObjectMotionLinearData * DX9_3DObjectMotion::GetLinearData(void)
{
    return &this->m_linear;
}

//==========================================================================
/**
@brief リセット
*/
void DX9_3DObjectMotion::Reset(void)
{
    for (auto itr1 = this->m_motion_data.begin(); itr1 != this->m_motion_data.end(); ++itr1)
    {
        for (auto itr2 = itr1->second.begin(); itr2 != itr1->second.end(); ++itr2)
        {
            for (auto itr3 = itr2->second.begin(); itr3 != itr2->second.end(); ++itr3)
            {
                itr3->Reset();
            }
        }
    }
}

//==========================================================================
/**
@brief リセット
@param MotionName [in] モーション名
*/
void DX9_3DObjectMotion::Reset(const std::string & MotionName)
{
    // アニメーション名でデータを検索
    auto itr1 = this->m_motion_data.find(MotionName);

    // 探索に失敗
    if (itr1 == this->m_motion_data.end())
    {
        return;
    }

    for (auto itr2 = itr1->second.begin(); itr2 != itr1->second.end(); ++itr2)
    {
        for (auto itr3 = itr2->second.begin(); itr3 != itr2->second.end(); ++itr3)
        {
            itr3->Reset();
        }
    }
}

//==========================================================================
/**
@brief リセット
@param MotionName [in] モーション名
@param ID [in] グループID
*/
void DX9_3DObjectMotion::Reset(const std::string & MotionName, int ID)
{
    // アニメーション名でデータを検索
    auto itr1 = this->m_motion_data.find(MotionName);

    // 探索に失敗
    if (itr1 == this->m_motion_data.end())
    {
        return;
    }

    // 再生IDでデータを検索
    auto itr2 = itr1->second.find(ID);

    // 探索に失敗
    if (itr2 == itr1->second.end())
    {
        return;
    }

    for (auto itr3 = itr2->second.begin(); itr3 != itr2->second.end(); ++itr3)
    {
        itr3->Reset();
    }
}

//==========================================================================
/**
@brief モーション名の取得
@return モーション名
*/
std::list<std::string> DX9_3DObjectMotion::GetMotionName(void)
{
    std::list<std::string> motion_name;

    for (auto itr1 = this->m_motion_data.begin(); itr1 != this->m_motion_data.end(); ++itr1)
    {
        motion_name.push_back(itr1->first);
    }

    return motion_name;
}

//==========================================================================
/**
@brief サイズの取得
@return サイズ
*/
int DX9_3DObjectMotion::size(void)
{
    return this->m_motion_data.size();
}

//==========================================================================
/**
@brief グループサイズ
@param MotionName [in] モーション名
@return サイズ
*/
int DX9_3DObjectMotion::size(const std::string & MotionName)
{
    // アニメーション名でデータを検索
    auto itr1 = this->m_motion_data.find(MotionName);

    // 探索に成功
    if (itr1 != this->m_motion_data.end())
    {
        return itr1->second.size();
    }

    return 0;
}

//==========================================================================
/**
@brief グループサイズ
@param MotionName [in] モーション名
@param ID [in] グループID
@return サイズ
*/
int DX9_3DObjectMotion::size(const std::string & MotionName, int ID)
{
    // アニメーション名でデータを検索
    auto itr1 = this->m_motion_data.find(MotionName);

    // 探索に失敗
    if (itr1 == this->m_motion_data.end())
    {
        return 0;
    }

    // 再生IDでデータを検索
    auto itr2 = itr1->second.find(ID);

    // 探索に失敗
    if (itr2 == itr1->second.end())
    {
        return 0;
    }

    return itr2->second.size();
}

//==========================================================================
/**
@brief 指定アニメーションの処理速度変更
@param MotionName [in] モーション名
@param ID [in] グループID
@param value [in] 処理速度
*/
void DX9_3DObjectMotion::SetPowor(const std::string & MotionName, int ID, float value)
{
    // アニメーション名でデータを検索
    auto itr1 = this->m_motion_data.find(MotionName);

    // 探索に失敗
    if (itr1 == this->m_motion_data.end())
    {
        return;
    }

    // 再生IDでデータを検索
    auto itr2 = itr1->second.find(ID);

    // 探索に失敗
    if (itr2 == itr1->second.end())
    {
        return;
    }

    // アニメーションの処理速度を変更
    for (auto itr3 = itr2->second.begin(); itr3 != itr2->second.end(); ++itr3)
    {
        itr3->SetPowor(value);
    }
}

//==========================================================================
/**
@brief 指定アニメーションの処理速度変更
@param MotionName [in] モーション名
@param ID [in] グループID
@param value [in] 処理速度グループ
*/
void DX9_3DObjectMotion::SetPowor(const std::string & MotionName, int ID, const std::vector<float>& value)
{
    // アニメーション名でデータを検索
    auto itr1 = this->m_motion_data.find(MotionName);

    // 探索に失敗
    if (itr1 == this->m_motion_data.end())
    {
        return;
    }

    // 再生IDでデータを検索
    auto itr2 = itr1->second.find(ID);

    // 探索に失敗
    if (itr2 == itr1->second.end())
    {
        return;
    }

    // アニメーションの処理速度を変更
    int ncount = 0;
    for (auto itr3 = itr2->second.begin(); itr3 != itr2->second.end(); ++itr3)
    {
        if ((int)value.size() == ncount)
        {
            break;
        }
        itr3->SetPowor(value[ncount]);
        ncount++;
    }
}

//==========================================================================
/**
@brief 指定アニメーションの処理速度取得
@param MotionName [in] モーション名
@param ID [in] グループID
@return 処理速度
*/
float DX9_3DObjectMotion::GetPowor(const std::string & MotionName, int ID)
{
    // アニメーション名でデータを検索
    auto itr1 = this->m_motion_data.find(MotionName);

    // 探索に失敗
    if (itr1 == this->m_motion_data.end())
    {
        return 0.0f;
    }

    // 再生IDでデータを検索
    auto itr2 = itr1->second.find(ID);

    // 探索に失敗
    if (itr2 == itr1->second.end())
    {
        return 0.0f;
    }

    // アニメーションが未登録
    if (itr2->second.size() == 0)
    {
        return 0.0f;
    }

    return itr2->second.begin()->GetPowor();
}

_MSLIB_END