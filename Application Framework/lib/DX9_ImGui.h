//==========================================================================
// DX9_ImGui[DX9_ImGui.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// #define _SETImGui_Dx9_
// release時にImguiの有効化
//==========================================================================

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <string>
#include "ImGUI_DirectX9\imgui.h"
#include "ImGUI_DirectX9\imgui_impl_dx9.h"
#include <d3d9.h>
#define DIRECTINPUT_VERSION (0x0800) // DirectInputのバージョン指定
#include <dinput.h>
#include <tchar.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"
#include "char_convert.hpp"

_MSLIB_BEGIN
//#define _SETImGui_Dx9_

//==========================================================================
//
// class  : DX9_ImGui
// Content: ImGui
//
//==========================================================================
class DX9_ImGui
{
public:
    DX9_ImGui();
	~DX9_ImGui();

    /**
    @brief 初期化
    @param hWnd [in] ウィンドウハンドル
    @param pDevice [in] デバイス
    @return 失敗時に true が返ります
    */
    bool Init(HWND hWnd, LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief 解放
    */
	void Uninit(void);

    /**
    @brief 更新
    */
	void Update(void);

    /**
    @brief 描画
    */
    void Draw(LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief 描画 (Handle loss of D3D9 device)
    @param result [in] Component Object Model defines, and macros
    @param pDevice [in] デバイス
    @param pd3dpp [in] デバイス
    */
	void DeviceReset(HRESULT result, LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS * pd3dpp);

    /**
    @brief WndProc (Process Win32 mouse/keyboard inputs)
    @param hWnd [in] ウィンドウハンドル
    @param uMsg [in] メッセージ
    @param wParam [in] ダブルパラム
    @param lParam [in] エルパラム
    @return Types use for passing & returning polymorphic values
    */
	IMGUI_API LRESULT ImGui_WndProcHandler(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

    /**
    @brief メッセージ
    @param hWnd [in] ウィンドウハンドル
    @param uMsg [in] メッセージ
    @param wParam [in] ダブルパラム
    @param lParam [in] エルパラム
    @param pDevice [in] デバイス
    @param pd3dpp [in] デバイス
    @return Types use for passing & returning polymorphic values
    */
	LRESULT SetMenu(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS * pd3dpp);

    /**
    @brief 新しいウィンドウ開始
    @param name [in] ウィンドウ名
    @param key [in] falseで開けない/trueで開ける
    @param flags [in] ImGuiWindowFlags_::??
    */
	void NewWindow(const char * name, bool key, int flags = 0);

    /**
    @brief ウィンドウ終了
    */
	void EndWindow(void);

    /**
    @brief テキスト生成
    @param text [in] 入力文字列
    @param text [in] 表示させたい値
    @return string テキスト
    */
	template <typename ... Args>
    const std::string CreateText(const char * text, Args const & ... args);

    /**
    @brief スクロールバー int値操作
    @param label [in] スクロールバー名
    @param v [in/out] 操作対象
    @param v_min [in] 操作範囲の最小
    @param v_max [in] 操作範囲の最大
    @param display_format [in] 表示形式
    @return 処理時に true が返ります
    */
    bool SliderInt(const char* label, int* v, int v_min, int v_max, const char* display_format = "%f");

    /**
    @brief スクロールバー float値操作
    @param label [in] スクロールバー名
    @param v [in/out] 操作対象
    @param v_min [in] 操作範囲の最小
    @param v_max [in] 操作範囲の最大
    @param power [in] 操作倍率
    @param display_format [in] 表示形式
    @return 処理時に true が返ります
    */
    bool SliderFloat(const char* label, float* v, float v_min, float v_max, float power = (1.0f), const char* display_format = "%f");

    /**
    @brief ボタン
    @param text [in] ボタン名
    @param size_arg [in] ボタンサイズ
    @return 処理時に true が返ります
    */
    bool Button(const char * text, const CVector4<float>& size_arg = CVector4<float>(0.0f, 0.0f));

    /**
    @brief ボタン
    @param text [in] ボタン名
    @param args [in] 表示したいデータ
    @return 処理時に true が返ります
    */
    template<typename ... Args>
    bool Button(const char * text, Args const & ...args);

    /**
    @brief 線を引きます
    */
    void Separator(void);

    /**
    @brief 階層構造開始
    @param text [in] 構造名
    @param is_open [in] false デフォルトで閉じています/true デフォルトで開いています
    @param args [in] 表示したいデータ
    @return 処理時に true が返ります
    */
	template <typename ... Args>
	bool NewTreeNode(const char * text, bool is_open, Args const & ... args);

    /**
    @brief 階層構造終わり
    */
    void EndTreeNode(void); 

    /**
    @brief テキスト表示
    @param text [in] 表示内容
    @param args [in] 表示したいデータ
    */
    template <typename ... Args>
	void Text(const char * text, Args const & ... args);

    /**
    @brief 入力ボックス
    @param label [in] デフォルトの内容
    @return 入力内容
    */
    const std::string InputText(const std::string label = "");

    /**
    @brief 表示フレーム開始
    */
    void NewFrame(void);

    /**
    @brief 表示フレーム開始終わり
    */
	void EndFrame(void);

    /**
    @brief チェックボックス
    @param label [in] チェックボックス名
    @param v [in/out] 操作対象
    @return 処理時に true が返ります
    */
    bool Checkbox(const char* label, bool& v);

    /**
    @brief デコイ
    @param text [in] デコイ化
    */
    void decoy(const char * text, ...);

    /**
    @brief メニューバーの開始
    @return 処理時に true が返ります
    */
    bool NewMenuBar(void);

    /**
    @brief メニューバーの終了
    */
    void EndMenuBar(void);

    /**
    @brief メニュー内アイテムの登録
    @param label [in] アイテム名
    @param shortcut [in]
    @param selected [in]
    @param enabled [in]
    @return 処理時に true が返ります
    */
    bool MenuItem(const char* label, const char* shortcut = nullptr, bool selected = false, bool enabled = true);

    /**
    @brief メニュー開始
    @param label [in] メニュー名
    @param enabled [in] falseでメニュー無効/trueで有効
    @return 処理時に true が返ります
    */
    bool NewMenu(const char* label, bool enabled = true);

    /**
    @brief メニュー終わり
    */
    void EndMenu(void);

    /**
    @brief カラーエディタ
    @param _color [in] 操作対象
    @return 操作した色が返ります
    */
    ImVec4 ColorEdit(const ImVec4 & _color);

    /**
    @brief テクスチャ描画
    @param user_texture_id [in] テクスチャポインタ
    @param size [in] テクスチャサイズ
    @param master_size [in] 基準となる描画サイズ(w,h共通)
    */
    void Image(LPDIRECT3DTEXTURE9 user_texture_id, const CTexvec<int>& size, float master_size = 100.0f);
private:
};

//==========================================================================
/**
@brief テキスト生成
@param text [in] 入力文字列
@param text [in] 表示させたい値
@return string テキスト
*/
template<typename ...Args>
inline const std::string DX9_ImGui::CreateText(const char * text, Args const & ...args)
{
#if defined(_SETImGui_Dx9_) || defined(_DEBUG) || defined(DEBUG)
    char pfon[1024] = { 0 };

    sprintf(pfon, text, args ...);
    return pfon;
#else
    char pfon[1] = { 0 };
    this->decoy(text, args ...);
	return pfon;
#endif
}

//==========================================================================
/**
@brief ボタン
@param text [in] ボタン名
@param args [in] 表示したいデータ
@return 処理時に true が返ります
*/
template<typename ...Args>
inline bool DX9_ImGui::Button(const char * text, Args const & ...args)
{
#if defined(_SETImGui_Dx9_) || defined(_DEBUG) || defined(DEBUG)
    std::string stdlabel = _MSLIB char_convert::multi_to_utf8_winapi(this->CreateText(text, args ...));
    return ImGui::Button(stdlabel.c_str(), ImVec2(0, 0));
#else
    this->decoy(text, args ...);
    return false;
#endif
}

//==========================================================================
/**
@brief 階層構造開始
@param text [in] 構造名
@param is_open [in] false デフォルトで閉じています/true デフォルトで開いています
@param args [in] 表示したいデータ
@return 処理時に true が返ります
*/
template<typename ...Args>
inline bool DX9_ImGui::NewTreeNode(const char * text, bool is_open, Args const & ...args)
{
#if defined(_SETImGui_Dx9_) || defined(_DEBUG) || defined(DEBUG)
    ImGui::SetNextTreeNodeOpen(is_open, ImGuiSetCond_Once);
    std::string stdlabel = _MSLIB char_convert::multi_to_utf8_winapi(this->CreateText(text, args ...));
    return ImGui::TreeNode(stdlabel.c_str());
#else
    this->decoy(text, args ...);
	is_open;
	return false;
#endif
}

//==========================================================================
/**
@brief テキスト表示
@param text [in] 表示内容
@param args [in] 表示したいデータ
*/
template<typename ... Args>
inline void DX9_ImGui::Text(const char * text, Args const & ...args)
{
#if defined(_SETImGui_Dx9_) || defined(_DEBUG) || defined(DEBUG)
    std::string stdlabel = _MSLIB char_convert::multi_to_utf8_winapi(this->CreateText(text, args ...));
	ImGui::Text(stdlabel.c_str());
#else
	this->decoy(text, args ...);
#endif
}

_MSLIB_END

/*

struct  ImGuiStyle
{
float        Alpha ;                       //グローバルアルファはImGui
ImVec2       WindowPaddingのすべてに適用されます。              //ウィンドウ内のパディング
ImVec2       WindowMinSize ;               //最小ウィンドウサイズ
float        WindowRounding ;              //ウィンドウコーナーの丸めの半径。0.0fに設定すると、長方形のウィンドウが表示されます
。ImGuiAlign   WindowTitleAlign ;            //タイトルバーのテキストの整列
float        ChildWindowRounding ;         //子ウィンドウの四隅の丸めの半径。長方形のウィンドウを持つように0.0fに設定
ImVec2       フレームパッディング;                //フレーム化された矩形内の
余白（ほとんどのウィジェットで使用される）float        FrameRounding ;               //フレームコーナーの丸めの半径。ほとんどのウィジェットで使用される長方形のフレームを持つには、0.0fに設定します。
ImVec2       ItemSpacing ;                 //ウィジェット/ライン間の水平方向と垂直方向の間隔
ImVec2       ItemInnerSpacing ;            //合成されたウィジェットの要素内での水平方向と垂直方向の間隔（スライダとそのラベルなど）
ImVec2       TouchExtraPadding ;          //タッチ位置が正確ではないタッチベースのシステムのリアクションバウンディングボックスを拡張する。残念ながら、我々はウィジェットをソートしないので、オーバーラップの優先順位は常に最初のウィジェットに与えられます。だからこれをあまり大きくしないでください！
float        IndentSpacing ;               //例えば、ツリーノードに入るときの横インデント。一般的に==（FontSize + FramePadding.x * 2）。
float        ColumnsMinSpacing ;           // 2つの列の間の最小の水平スペーシング
float        ScrollbarSize ;               //垂直スクロールバーの幅、水平スクロールバーの高さ
float        ScrollbarRounding ;           //スクロールバーのグラブの角の半径
float        GrabMinSize ;                //スライダ/スクロールバーのグラブボックスの最小幅/高さ
float        GrabRounding ;                //グラブ角の丸めの半径。長方形のスライダーグラブを持つには0.0fに設定します。
ImVec2       DisplayWindowPadding ;        //ウィンドウの位置は、少なくともこの量だけ表示領域内に表示されるように固定されます。通常のウィンドウのみをカバーします。
ImVec2       DisplaySafeAreaPadding ;      //画面の端を見ることができない場合（例えばTVで）、安全領域のパディングを増やしてください。通常のウィンドウと同様にポップアップ/ツールチップをカバーします。
bool         AntiAliasedLines ;            //ライン/ボーダーでアンチエイリアスを有効にする。あなたが本当にCPU / GPUでタイトな場合は無効にしてください。
bool         AntiAliasedShapes;           //塗りつぶされた図形（丸い四角形、円など）でアンチエイリアスを有効にする
float        CurveTessellationTol ;        //テッセレーション許容値。高度にテッセレーションされたカーブ（高品質、多角形）を減らし、品質を低下させます。
ImVec4の      色[ ImGuiCol_COUNT ];

IMGUI_API  ImGuiStyle （）;
};



// Cinder-ImGui
const  auto  opts  =  ui :: Options （）の例
です。antiAliasedLines （真）
。antiAliasedShapes （true ）
です。windowRounding （0.0 F ）
。frameRounding （0.0 f ）;

ui :: initialize （opts ）;


// https://github.com/ocornut/imgui/blob/v1.49/imgui.h#L592-L638
enum  ImGuiCol_
{
ImGuiCol_Text 、
ImGuiCol_TextDisabled 、
ImGuiCol_WindowBg 、              //通常のウィンドウの
背景ImGuiCol_ChildWindowBg 、         //子ウィンドウの背景
ImGuiCol_PopupBg 、               //ポップアップ、メニュー、ツールチップウィンドウの
背景ImGuiCol_Border 、
ImGuiCol_BorderShadow 、
ImGuiCol_FrameBg 、               //チェックボックス、ラジオボタン、プロット、スライダ、テキスト入力の
背景ImGuiCol_FrameBgHovered 、
ImGuiCol_FrameBgActive 、
ImGuiCol_TitleBg 、
ImGuiCol_TitleBgCollapsed 、
ImGuiCol_TitleBgActive 、
ImGuiCol_MenuBarBg 、
ImGuiCol_ScrollbarBg 、
ImGuiCol_ScrollbarGrab 、
ImGuiCol_ScrollbarGrabHovered 、
ImGuiCol_ScrollbarGrabActive 、
ImGuiCol_ComboBg 、
ImGuiCol_CheckMark 、
ImGuiCol_SliderGrab 、
ImGuiCol_SliderGrabActive 、
ImGuiCol_Button 、
ImGuiCol_ButtonHovered 、
ImGuiCol_ButtonActive 、
ImGuiCol_Header 、
ImGuiCol_HeaderHovered 、
ImGuiCol_HeaderActive 、
ImGuiCol_Column 、
ImGuiCol_ColumnHovered 、
ImGuiCol_ColumnActive 、
ImGuiCol_ResizeGrip 、
ImGuiCol_ResizeGripHovered 、
ImGuiCol_ResizeGripActive 、
ImGuiCol_CloseButton 、
ImGuiCol_CloseButtonHovered 、
ImGuiCol_CloseButtonActive 、
ImGuiCol_PlotLines 、
ImGuiCol_PlotLinesHovered 、
ImGuiCol_PlotHistogram 、
ImGuiCol_PlotHistogramHovered 、
ImGuiCol_TextSelectedBg 、
ImGuiCol_ModalWindowDarkening 、  //モーダルウィンドウがアクティブである場合、画面全体が暗く
ImGuiCol_COUNT
}。

*/