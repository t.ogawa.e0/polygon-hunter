//==========================================================================
// 2Dオブジェクト[DX9_2DObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_2DObject.h"

_MSLIB_BEGIN

DX9_2DObject::DX9_2DObject()
{
    this->m_Affine = DX9_2DObjectAffine(0.0f, false);
    this->m_Anim = DX9_2DObjectAnim(-1, 0, 0, 0, false);
    this->m_Color = 255;
    this->m_Cut = 0;
    this->m_Pos = 0;
    this->m_PercentPos = 0;
    this->m_index = -1;
    this->m_Scale = 1.0f;
    this->m_Key = false;
    this->m_CentralCoordinates = false;
}

DX9_2DObject::~DX9_2DObject()
{
}

//==========================================================================
/**
@brief 初期化
@param index [in] 使用するテクスチャ番号
*/
void DX9_2DObject::Init(int index)
{
	this->m_Affine = DX9_2DObjectAffine(0.0f, false);
	this->m_Anim = DX9_2DObjectAnim(-1, 0, 0, 0, false);
	this->m_Color = 255;
	this->m_Cut = 0;
	this->m_Pos = 0;
	this->m_index = index;
	this->m_Scale = 1.0f;
	this->m_Key = false;
	this->m_CentralCoordinates = false;
}

//==========================================================================
/**
@brief アニメーション用初期化
@param index [in] テクスチャ番号
@param Frame [in] 更新フレーム
@param Pattern [in] アニメーション数
@param Direction [in] 横一列のアニメーション数
*/
void DX9_2DObject::Init(int index, int Frame, int Pattern, int Direction)
{
	this->m_Affine = DX9_2DObjectAffine(0.0f, false);
	this->m_Anim = DX9_2DObjectAnim(-1, Frame, Pattern, Direction, true);
	this->m_Color = 255;
	this->m_Cut = 0.0f;
	this->m_Pos = 0.0f;
	this->m_index = index;
	this->m_Scale = 1.0f;
	this->m_Key = false;
	this->m_CentralCoordinates = false;
}

//==========================================================================
/**
@brief アニメーションだけの情報セット
@param index [in] テクスチャ番号
@param Frame [in] 更新フレーム
@param Pattern [in] アニメーション数
@param Direction [in] 横一列のアニメーション数
*/
void DX9_2DObject::SetAnim(int index, int Frame, int Pattern, int Direction)
{
	this->m_index = index;
	this->m_Anim = DX9_2DObjectAnim(-1, Frame, Pattern, Direction, true);
}

//==========================================================================
/**
@brief 座標のセット
@param x [in] X座標
@param y [in] Y座標
*/
void DX9_2DObject::SetPos(float x, float y)
{
	this->m_Pos = CVector2<float>(x, y);
}

//==========================================================================
/**
@brief 座標のセット
@param x [in] X座標
*/
void DX9_2DObject::SetX(float x)
{
	this->m_Pos.x = x;
}

//==========================================================================
/**
@brief 座標のセット
@param y [in] Y座標
*/
void DX9_2DObject::SetY(float y)
{
	this->m_Pos.y = y;
}

//==========================================================================
/**
@brief 座標の加算
@param x [in] X座標
*/
void DX9_2DObject::SetXPlus(float x)
{
	this->m_Pos.x += x;
}

//==========================================================================
/**
@brief 座標の加算
@param y [in] Y座標
*/
void DX9_2DObject::SetYPlus(float y)
{
	this->m_Pos.y += y;
}

//==========================================================================
/**
@brief 座標のセット
@param vpos [in] {x,y}座標
*/
void DX9_2DObject::SetPos(const CVector2<float> & vpos)
{
	this->m_Pos = vpos;
}

//==========================================================================
/**
@brief 座標の加算
@param vpos [in] {x,y}座標
*/
void DX9_2DObject::SetPosPlus(const CVector2<float> & vpos)
{
	this->m_Pos += vpos;
}

//==========================================================================
/**
@brief 中心座標モード
@param mood [in] trueで有効化
*/
void DX9_2DObject::SetCentralCoordinatesMood(bool mood)
{
    this->m_CentralCoordinates = mood;
}

//==========================================================================
/**
@brief 座標の取得
@return x,y 座標
*/
const CVector2<float>* DX9_2DObject::GetPos(void) const
{
    return &this->m_Pos;
}

//==========================================================================
/**
@brief サイズ加算
@param Size [in] 入力した値が加算されます
*/
void DX9_2DObject::SetScalePlus(float Size)
{
	this->m_Scale += Size;
}

//==========================================================================
/**
@brief サイズ入力
@param Size [in] サイズ設定です
*/
void DX9_2DObject::SetScale(float Size)
{
    this->m_Scale = Size;
}

//==========================================================================
/**
@brief サイズ取得
@return サイズ
*/
float DX9_2DObject::GetScale(void) const
{
    return this->m_Scale;
}

//==========================================================================
/**
@brief 回転角度入力
@param Angle [in] 角度が加算されます
*/
void DX9_2DObject::SetAnglePlus(float Angle)
{
	this->m_Affine.Key = true;
	this->m_Affine.Angle += Angle;
}

//==========================================================================
/**
@brief 回転角度リセット
*/
void DX9_2DObject::AngleReset(void)
{
	this->m_Affine.Key = false;
	this->m_Affine.Angle = 0.0f;
}

//==========================================================================
/**
@brief 回転角度の取得
@return 回転角度
*/
float DX9_2DObject::GetAngle(void)
{
    return this->m_Affine.Angle;
}

//==========================================================================
/**
@brief 回転角度入力
@param Angle [in] 角度が入ります
*/
void DX9_2DObject::SetAngle(float Angle)
{
    this->m_Affine.Key = true;
    this->m_Affine.Angle = Angle;
}

//==========================================================================
/**
@brief 色セット
@param r [in] R値が入ります
@param g [in] G値が入ります
@param b [in] B値が入ります
@param a [in] A値が入ります
*/
void DX9_2DObject::SetColor(int r, int g, int b, int a)
{
	this->m_Color = CColor<int>(r, g, b, a);
}

//==========================================================================
/**
@brief 色セット
@param color [in] {r,g,b,a} RGBA値が入ります
*/
void DX9_2DObject::SetColor(const CColor<int> & color)
{
	this->m_Color = color;
}

//==========================================================================
/**
@brief 色の取得
@return r,g,b,a値が返ります
*/
const CColor<int>* DX9_2DObject::GetColor(void)
{
    return &this->m_Color;
}

//==========================================================================
/**
@brief 無回転頂点 非推奨です
@param pPseudo [in] 頂点バッファ
@param ptexsize [in] テクスチャのサイズ
@return VERTEX_3 型が返ります
*/
const DX9_2DObject::VERTEX_3 * DX9_2DObject::CreateVertex(VERTEX_3 * pPseudo, const CTexvec<int> & ptexsize)
{
	// 無回転の場合
	if (this->m_Affine.Key == false)
	{
		CUv<float> UV;

		this->SetTexturCut(ptexsize);
		this->AnimationPalam();
		this->UV(&UV, ptexsize);

		// 中心座標モード
		if (this->m_CentralCoordinates)
		{
			float fWidht = (this->m_Cut.w / 2)*this->m_Scale;
			float fHeight = (this->m_Cut.h / 2)*this->m_Scale;

			pPseudo[0].pos = D3DXVECTOR4((this->m_Pos.x - fWidht - 0.5f), (this->m_Pos.y - fHeight - 0.5f), 1.0f, 1.0f);
			pPseudo[1].pos = D3DXVECTOR4((this->m_Pos.x + fWidht - 0.5f), (this->m_Pos.y - fHeight - 0.5f), 1.0f, 1.0f);
			pPseudo[2].pos = D3DXVECTOR4((this->m_Pos.x - fWidht - 0.5f), (this->m_Pos.y + fHeight - 0.5f), 1.0f, 1.0f);
			pPseudo[3].pos = D3DXVECTOR4((this->m_Pos.x + fWidht - 0.5f), (this->m_Pos.y + fHeight - 0.5f), 1.0f, 1.0f);
		}
		else // 中心座標モード無効時
		{
			if (this->m_Anim.Key)
			{
				pPseudo[0].pos = D3DXVECTOR4((this->m_Pos.x + 0.0f - 0.5f)*this->m_Scale, (this->m_Pos.y + 0.0f - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[1].pos = D3DXVECTOR4((this->m_Pos.x + this->m_Cut.w - 0.5f)*this->m_Scale, (this->m_Pos.y + 0.0f - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[2].pos = D3DXVECTOR4((this->m_Pos.x + 0.0f - 0.5f)*this->m_Scale, (this->m_Pos.y + this->m_Cut.h - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[3].pos = D3DXVECTOR4((this->m_Pos.x + this->m_Cut.w - 0.5f)*this->m_Scale, (this->m_Pos.y + this->m_Cut.h - 0.5f)*this->m_Scale, 1.0f, 1.0f);
			}
			else
			{
				pPseudo[0].pos = D3DXVECTOR4((this->m_Pos.x - 0.0f - 0.5f)*this->m_Scale, (this->m_Pos.y - 0.0f - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[1].pos = D3DXVECTOR4((this->m_Pos.x + ptexsize.w - 0.5f)*this->m_Scale, (this->m_Pos.y - 0.0f - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[2].pos = D3DXVECTOR4((this->m_Pos.x - 0.0f - 0.5f)*this->m_Scale, (this->m_Pos.y + ptexsize.h - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[3].pos = D3DXVECTOR4((this->m_Pos.x + ptexsize.w - 0.5f)*this->m_Scale, (this->m_Pos.y + ptexsize.h - 0.5f)*this->m_Scale, 1.0f, 1.0f);
			}
		}

		pPseudo[0].color = pPseudo[1].color = pPseudo[2].color = pPseudo[3].color = D3DCOLOR_RGBA(this->m_Color.r, this->m_Color.g, this->m_Color.b, this->m_Color.a);

		pPseudo[0].Tex = D3DXVECTOR2(UV.u0, UV.v0);
		pPseudo[1].Tex = D3DXVECTOR2(UV.u1, UV.v0);
		pPseudo[2].Tex = D3DXVECTOR2(UV.u0, UV.v1);
		pPseudo[3].Tex = D3DXVECTOR2(UV.u1, UV.v1);
	}

	return pPseudo;
}

//==========================================================================
/**
@brief 回転頂点 非推奨です
@param pPseudo [in] 頂点バッファ
@param ptexsize [in] テクスチャのサイズ
@return VERTEX_3 型が返ります
*/
const DX9_2DObject::VERTEX_3 * DX9_2DObject::CreateVertexAngle(VERTEX_3 * pPseudo, const CTexvec<int> & ptexsize)
{
	// 回転時
	if (this->m_Affine.Key == true)
	{
		CUv<float> UV;

		this->SetTexturCut(ptexsize);
		this->AnimationPalam();
		this->UV(&UV, ptexsize);

		float PosX = this->m_Pos.x;
		float PosY = this->m_Pos.y;

		// 中心座標割り出し
		float APolyX = 0.0f;
		float APolyX_W = 0.0f;
		float APolyY = 0.0f;
		float APolyY_H = 0.0f;

        if (this->m_Anim.Key)
        {
            APolyX = (-(this->m_Cut.w / 2)* this->m_Scale) + (this->m_Cut.w / 2);
            APolyX_W = ((this->m_Cut.w - (this->m_Cut.w / 2))*  this->m_Scale) + (this->m_Cut.w / 2);
            APolyY = (-(this->m_Cut.h / 2)* this->m_Scale) + (this->m_Cut.h / 2);
            APolyY_H = ((this->m_Cut.h - (this->m_Cut.h / 2))*  this->m_Scale) + (this->m_Cut.h / 2);

            // ずらした分修正
            APolyX -= (this->m_Cut.w / 2);
            APolyX_W -= (this->m_Cut.w / 2);
            APolyY -= (this->m_Cut.h / 2);
            APolyY_H -= (this->m_Cut.h / 2);

            // 中心座標モード無効時
            if (!this->m_CentralCoordinates)
            {
                PosX += (this->m_Cut.w / 2);
                PosY += (this->m_Cut.h / 2);
                PosX *= this->m_Scale;
                PosY *= this->m_Scale;
            }
        }
        else
        {
            APolyX = (-(ptexsize.w / 2)* this->m_Scale) + (ptexsize.w / 2);
            APolyX_W = ((ptexsize.w - (ptexsize.w / 2))*  this->m_Scale) + (ptexsize.w / 2);
            APolyY = (-(ptexsize.h / 2)* this->m_Scale) + (ptexsize.h / 2);
            APolyY_H = ((ptexsize.h - (ptexsize.h / 2))*  this->m_Scale) + (ptexsize.h / 2);

            // ずらした分修正
            APolyX -= (ptexsize.w / 2);
            APolyX_W -= (ptexsize.w / 2);
            APolyY -= (ptexsize.h / 2);
            APolyY_H -= (ptexsize.h / 2);

            // 中心座標モード無効時
            if (!this->m_CentralCoordinates)
            {
                PosX += (ptexsize.w / 2);
                PosY += (ptexsize.h / 2);
                PosX *= this->m_Scale;
                PosY *= this->m_Scale;
            }
        }

		pPseudo[0].pos = D3DXVECTOR4((PosX + APolyX*  cosf(this->m_Affine.Angle) - APolyY* sinf(this->m_Affine.Angle) - 0.5f), PosY + APolyX* sinf(this->m_Affine.Angle) + APolyY* cosf(this->m_Affine.Angle) - 0.5f, 1.0f, 1.0f);
		pPseudo[1].pos = D3DXVECTOR4((PosX + APolyX_W* cosf(this->m_Affine.Angle) - APolyY*  sinf(this->m_Affine.Angle) - 0.5f), PosY + APolyX_W* sinf(this->m_Affine.Angle) + APolyY* cosf(this->m_Affine.Angle) - 0.5f, 1.0f, 1.0f);
		pPseudo[2].pos = D3DXVECTOR4((PosX + APolyX*  cosf(this->m_Affine.Angle) - APolyY_H* sinf(this->m_Affine.Angle) - 0.5f), PosY + APolyX*	sinf(this->m_Affine.Angle) + APolyY_H* cosf(this->m_Affine.Angle) - 0.5f, 1.0f, 1.0f);
		pPseudo[3].pos = D3DXVECTOR4((PosX + APolyX_W* cosf(this->m_Affine.Angle) - APolyY_H* sinf(this->m_Affine.Angle) - 0.5f), PosY + APolyX_W* sinf(this->m_Affine.Angle) + APolyY_H* cosf(this->m_Affine.Angle) - 0.5f, 1.0f, 1.0f);

		pPseudo[0].color = pPseudo[1].color = pPseudo[2].color = pPseudo[3].color = D3DCOLOR_RGBA(this->m_Color.r, this->m_Color.g, this->m_Color.b, this->m_Color.a);

		pPseudo[0].Tex = D3DXVECTOR2(UV.u0, UV.v0);
		pPseudo[1].Tex = D3DXVECTOR2(UV.u1, UV.v0);
		pPseudo[2].Tex = D3DXVECTOR2(UV.u0, UV.v1);
		pPseudo[3].Tex = D3DXVECTOR2(UV.u1, UV.v1);
	}

	return pPseudo;
}

//==========================================================================
/**
@brief アニメーション切り替わり時の判定
@return アニメーション終了時に true が返ります
*/
bool DX9_2DObject::GetPattanNum(void)
{
	if (this->m_Anim.Count == (this->m_Anim.Frame*this->m_Anim.Pattern) - 1)
	{
		this->m_Anim.Count = -1;
		return true;
	}
	return false;
}

//==========================================================================
/**
@brief アニメーションカウンタのセット
@param nCount [in] アニメーションカウント
*/
void DX9_2DObject::AnimationCount(int nCount)
{
    // アニメーションが有効時
    if (this->m_Anim.Key == true)
    {
        this->m_Anim.Count = nCount;
    }
}

//==========================================================================
/**
@brief アニメーションカウンタ
*/
void DX9_2DObject::AnimationCount(void)
{
    // アニメーションが有効時
    if (this->m_Anim.Key == true)
    {
        this->m_Anim.Count++;
    }
}

//==========================================================================
/**
@brief アニメーション情報のゲッター
@return アニメーション情報
*/
DX9_2DObjectAnim * DX9_2DObject::GetAnimParam(void)
{
    return &this->m_Anim;
}

//==========================================================================
/**
@brief アニメーション切り替わり時の判定
@param AnimationCount [in] アニメーションカウンタ
@return アニメーション終了時に true が返ります
*/
bool DX9_2DObject::GetPattanNum(int & AnimationCount)
{
    if (AnimationCount == (this->m_Anim.Frame*this->m_Anim.Pattern) - 1)
    {
        AnimationCount = -1;
        return true;
    }
    return false;
}

//==========================================================================
/**
@brief アニメーションカウンタの初期化
@param AnimationCount [in] アニメーションカウンタ
*/
void DX9_2DObject::InitAnimationCount(int & AnimationCount)
{
    AnimationCount = -1;
}

//==========================================================================
/**
@brief アニメーションカウンタの初期化
*/
void DX9_2DObject::InitAnimationCount(void)
{
    this->m_Anim.Count = -1;
}

//==========================================================================
/**
@brief 使用テクスチャ番号の取得
@return テクスチャ番号
*/
int DX9_2DObject::GetIndex(void) const
{
    return this->m_index;
}

//==========================================================================
/**
@brief 使用テクスチャ番号のセット
@param Input [in] テクスチャ番号
*/
void DX9_2DObject::SetIndex(int Input)
{
    this->m_index = Input;
}

//==========================================================================
/**
@brief 現在使用しているテクスチャのサイズを取得
@return テクスチャのサイズ
*/
const CTexvec<float>* DX9_2DObject::GetTexSize(void) const
{
    return &this->m_Cut;
}

//==========================================================================
/**
@brief 割合座標 ウィンドウサイズの割合座標の設定ができます
@param Proportion [in] {x,y}軸割合
@param WinSize [in] {x,y}ウィンドウサイズ
*/
void DX9_2DObject::SetPercentPos(const CVector2<float> & Proportion, CVector2<float>& WinSize)
{
    this->m_PercentPos = Proportion;
    this->m_Pos = WinSize * this->m_PercentPos;
}

//==========================================================================
/**
@brief 座標調節に使用している割合の取得
@return 座標調節に使用している割合
*/
const CVector2<float>* DX9_2DObject::GetPercentPos(void)
{
    return &this->m_PercentPos;
}

//==========================================================================
/**
@brief 頂点情報を登録
@param label [in] アクセスラベル
@param vertex [in] 頂点情報
*/
void DX9_2DObject::SetVertexPath(int label, VERTEX_3 * vertex)
{
    this->m_vertex_3[label] = vertex;
}

//==========================================================================
/**
@brief 頂点情報を取得
@param label [in] アクセスラベル
@return 頂点情報
*/
const DX9_VERTEX_3D::VERTEX_3 * DX9_2DObject::GetVertexPath(int label)
{
    return this->m_vertex_3[label];
}

//==========================================================================
/**
@brief UVの生成
@param UV [in/out] {u0,v0,u1,v1}UV座標
@param ptexsize [in] テクスチャサイズ
*/
void DX9_2DObject::UV(CUv<float> * UV, const CTexvec<int> & ptexsize)
{
	// 左上
	UV->u0 = (float)this->m_Cut.x / ptexsize.w;
	UV->v0 = (float)this->m_Cut.y / ptexsize.h;

	// 左下
	UV->u1 = (float)(this->m_Cut.x + this->m_Cut.w) / ptexsize.w;
	UV->v1 = (float)(this->m_Cut.y + this->m_Cut.h) / ptexsize.h;
}

//==========================================================================
/**
@brief アニメーションの更新
*/
void DX9_2DObject::AnimationPalam(void)
{
	// アニメーションが有効時
	if (this->m_Anim.Key == true)
	{
        int PattanNum = (this->m_Anim.Count / this->m_Anim.Frame) % this->m_Anim.Pattern;	// フレームに１回	パターン数
        int patternV = PattanNum % this->m_Anim.Direction; // 横方向のパターン
        int patternH = PattanNum / this->m_Anim.Direction; // 縦方向のパターン

        this->m_Cut.x = patternV * this->m_Cut.w; // 切り取り座標X
        this->m_Cut.y = patternH * this->m_Cut.h; // 切り取り座標Y
    }
}

//==========================================================================
/**
@brief テクスチャの切り取り ウィンドウサイズの割合座標の設定ができます
@param ptexsize [in] デフォルトのテクスチャサイズ
*/
void DX9_2DObject::SetTexturCut(const CTexvec<int> & ptexsize)
{
	if (this->m_Cut.w == 0.0f)
	{
		this->m_Cut.w = (float)ptexsize.w;
	}
	if (this->m_Cut.h == 0.0f)
	{
		this->m_Cut.h = (float)ptexsize.h;
	}

	if (this->m_Key == false && this->m_Anim.Key == true)
	{
		int nCount = 0;
		this->m_Cut.w = (float)ptexsize.w / this->m_Anim.Direction;

		// 縦の枚数を検索
		for (int i = 0;; i += this->m_Anim.Direction)
		{
			if (this->m_Anim.Pattern <= i) { break; }
			nCount++;
		}
		this->m_Cut.h = (float)ptexsize.h / nCount;
		this->m_Cut.x = this->m_Cut.y = 0.0f;
		this->m_Key = true;
	}
}

DX9_2DObjectAnim::DX9_2DObjectAnim()
{
    this->Count = 0;
    this->Frame = 0;
    this->Pattern = 0;
    this->Direction = 0;
    this->Key = false;
}

DX9_2DObjectAnim::DX9_2DObjectAnim(int count, int frame, int pattern, int direction, bool key)
{
    this->Count = count;
    this->Frame = frame;
    this->Pattern = pattern;
    this->Direction = direction;
    this->Key = key;
}

DX9_2DObjectAnim::~DX9_2DObjectAnim()
{
}

DX9_2DObjectAffine::DX9_2DObjectAffine()
{
    this->Angle = 0.0f;
    this->Key = false;
}

DX9_2DObjectAffine::DX9_2DObjectAffine(float ang, bool key)
{
    this->Angle = ang;
    this->Key = key;
}

DX9_2DObjectAffine::~DX9_2DObjectAffine()
{
}

_MSLIB_END