//==========================================================================
// 2Dポリゴン[DX9_2DPolygon.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_Renderer.h"
#include "DX9_2DObject.h"
#include "mslib_struct.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_2DPolygon
// Content: 2Dポリゴン 
//
//==========================================================================
class DX9_2DPolygon : public DX9_Renderer<DX9_2DObject>
{
private:
    // コピー禁止 (C++11)
    DX9_2DPolygon(const DX9_2DPolygon &) = delete;
    DX9_2DPolygon &operator=(const DX9_2DPolygon &) = delete;
public:
    DX9_2DPolygon(LPDIRECT3DDEVICE9 pDevice, HWND hWnd, float scale);
    ~DX9_2DPolygon();

    /**
    @brief 初期化
    @param Input [in] 使用するテクスチャのパス ダブルポインタに対応
    @param NumData [in] データ数
    @param AutoTextureResize [in] テクスチャのサイズ自動調節機能 デフォルトは無効/有効化は true
    @return 初期化失敗時 true
    */
	bool Init(const char ** Input, int NumData, bool AutoTextureResize = false);

    /**
    @brief 初期化
    @param Input [in] 使用するテクスチャのパス
    @param AutoTextureResize [in] テクスチャのサイズ自動調節機能 デフォルトは無効/有効化は true
    @return 初期化失敗時 true
    */
	bool Init(const char * Input, bool AutoTextureResize = false);

    /**
    @brief 初期化
    @return 初期化失敗時 true(基本成功)
    */
    bool Init(void);

    /**
    @brief 描画リソースの更新
    */
    void Update(void);

    /**
    @brief リソースの解放
    */
	void Release(void);

    /**
    @brief 描画
    @param ADD [in] 加算合成の有効/無効 デフォルトは無効/有効は true
    */
    void Draw(bool ADD = false);

    /**
    @brief テクスチャの情報の取得
    @param index [in] テクスチャ番号
    @return 成功時にテクスチャの情報が返ります/失敗時は nullptr が返ります
    */
    CTexvec<int> * GetTexSize(int index);

    /**
    @brief テクスチャの枚数取得
    @return 管理しているテクスチャ数が返ります
    */
    int GetNumTex(void);

    /**
    @brief テクスチャのサイズ指定
    @param index [in] テクスチャの番号
    @param Widht [in] テクスチャの幅
    @param Height [in] テクスチャの高さ
    */
    void SetTexSize(int index, int Widht, int Height);

    /**
    @brief テクスチャのサイズ変更
    @param index [in] テクスチャの番号
    @param scale [in] テクスチャのサイズ変更に使う倍率
    */
	void SetTexScale(int index, float scale);

    /**
    @brief テクスチャのサイズリセット
    @param index [in] テクスチャの番号
    */
    void ResetTexSize(int index);
private:
	bool m_Lock; // バッファ登録判定
    float m_scale; // 画面の割合
};

_MSLIB_END