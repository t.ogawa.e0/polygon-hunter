//==========================================================================
// テクスチャローダー[DX9_TextureLoader.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <string>
#include <list>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"
#include "vector_wrapper.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_TextureData
// Content: テクスチャの管理
//
//==========================================================================
class DX9_TextureData
{
public:
    DX9_TextureData();
    ~DX9_TextureData();

    /**
    @brief サイズのリセット
    */
    void resetsize(void);

    /**
    @brief サイズのリセット
    @param Input [in] サイズ指定
    */
    void setsize(CTexvec<int> Input);

    /**
    @brief サイズの取得
    @return テクスチャのサイズ
    */
    CTexvec<int> *getsize(void);

    /**
    @brief 読み込み
    @return Component Object Model defines, and macros
    */
    HRESULT load(LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief 解放
    */
    void Release(void);

    /**
    @brief tagのセット
    @param ptag [in] タグのセット
    */
    void settag(const char * ptag);

    /**
    @brief tagの取得
    @return タグ名
    */
    const char * gettag(void);

    /**
    @brief 同一データチェック
    @return 同一データが存在する場合 true
    */
    bool check(const char * Input);

    /**
    @brief リソースの取得
    @return テクスチャのリソース
    */
    const LPDIRECT3DTEXTURE9 gettex(void);

    /**
    @brief データの複製
    @param pinp [in] 複製対象
    */
    void path(const DX9_TextureData * pinp);

    /**
    テクスチャの情報取得
    @return テクスチャの情報
    */
    const D3DXIMAGE_INFO GetImageInfo(void);
private:
    LPDIRECT3DTEXTURE9 m_texture; // テクスチャポインタ
    D3DXIMAGE_INFO m_ImageInfo; // 画像データの管理
    CTexvec<int> m_texparam; // テクスチャのサイズ
    CTexvec<int> m_mastersize; // マスターサイズ
    std::string m_strName; // タグ
    bool m_original; // オリジナル
};

//==========================================================================
//
// class  : DX9_TextureList
// Content: リソースへのアクセス
//
//==========================================================================
class DX9_TextureList
{
public:
    DX9_TextureList();
    ~DX9_TextureList();
    
    /**
    @brief デバイスのセット
    @param pDevice [in] デバイス
    */
    void SetDevice(LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief リソースの登録
    @param data [in] データ
    */
    void SetTextureData(const DX9_TextureData& data);

    /**
    @brief リソースの取得
    @return アクセス用データ
    */
    std::list<DX9_TextureData> * GetTextureList(void);

    /**
    @brief リソース数の取得
    @return リソース数
    */
    int Size(void);

    /**
    @brief リソース名の登録
    @param name [in] リソース名
    */
    void SetTextureName(const std::string & name);

    /**
    @brief リソース名の取得
    @param name [in] リソース名
    */
    const std::string * GetTextureName(void);

    /**
    @brief テクスチャFVFの生成
    */
    void SetTextureFVF(void);

    /**
    @brief テクスチャFVFの取得
    @return テクスチャFVF
    */
    const DWORD & GetTextureFVF(void);

    /**
    @brief 描画の開始
    */
    void DrawBegin(void);

    /**
    @brief 描画の終了
    */
    void DrawEnd(void);
private:
    std::string m_strName; // タグ
    std::list<DX9_TextureData> m_data;
    DWORD m_texture_fvf; // FVF
    LPDIRECT3DDEVICE9 m_Device; // デバイス
};

//==========================================================================
//
// class  : DX9_TextureLoader
// Content: 2Dテクスチャローダー
//
//==========================================================================
class DX9_TextureLoader
{
private:
    // コピー禁止 (C++11)
    DX9_TextureLoader(const DX9_TextureLoader &) = delete;
    DX9_TextureLoader &operator=(const DX9_TextureLoader &) = delete;
public:
    DX9_TextureLoader(LPDIRECT3DDEVICE9 pDevice, HWND hWnd);
    ~DX9_TextureLoader();

    /**
    @brief 読み込み
    @param Input [in] テクスチャ名
    @param Num [in] 格納数
    @return 失敗時に true が返ります
    */
	bool init(const char ** Input, int Num);

    /**
    @brief 読み込み
    @param Input [in] テクスチャ名
    @return 失敗時に true が返ります
    */
	bool init(const char * Input);

    /**
    @brief 解放
    */
    void Release(void);

    /**
    @brief データの取得
    @param nID [in] アクセスID
    */
    DX9_TextureList * get(int nID);

    /**
    @brief データ数
    @return 数
    */
    int size(void);

    /**
    @brief マスターデータの取得
    @return マスターデーター
    */
    const vector_wrapper<DX9_TextureData> * get_master(void);
private:
    /**
    @brief 生成
    @param pinp [in/out] データ受け取り変数
    @param Input [in] タグ
    @return 生成データ
    */
    DX9_TextureData * create(DX9_TextureData *& pinp, const char * Input);

    /**
    @brief リソース読み取り
    @param InOut [in/out] データ受け取り変数
    @param strName [in] リソース名
    */
    void load(DX9_TextureList * InOut, const std::string & strName);
private:
    vector_wrapper<DX9_TextureData> m_texture; // 生データの管理
    vector_wrapper<DX9_TextureList> m_path; // アクセス用データ
    LPDIRECT3DDEVICE9 m_Device; // デバイス
    HWND m_hWnd; // ウィンドウハンドル
};

_MSLIB_END