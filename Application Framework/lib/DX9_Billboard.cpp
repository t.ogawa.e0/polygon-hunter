//==========================================================================
// ビルボード[DX9_Billboard.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Billboard.h"

_MSLIB_BEGIN

//==========================================================================
// 定数定義
//==========================================================================
constexpr float VCRCT = 0.5f;

DX9_Billboard::DX9_Billboard(LPDIRECT3DDEVICE9 pDevice, HWND hWnd)
{
    this->m_texture = new DX9_TextureLoader(pDevice, hWnd);
    this->m_Device = pDevice;
    this->m_hWnd = hWnd;
}

DX9_Billboard::~DX9_Billboard()
{
    this->Release();
}

//==========================================================================
/**
@brief 初期化
@param Input [in] 使うテクスチャのパス
@param plate [in] 板ポリの種類
@param bCenter [in] true で中心座標を下の真ん中にします/デフォルトはfalse
@return 失敗時 true
*/
bool DX9_Billboard::Init(const char* Input, DX9_BillboardPlateList plate, bool bCenter)
{
	// テクスチャの格納
	if (this->m_texture->init(Input)) { return true; }

    // バッファの生成
	if (this->CreateBuffer(plate, bCenter)) { return true; }

	return false;
}

//==========================================================================
/**
@brief アニメーション用初期化
@param Input [in] 使うテクスチャのパス
@param UpdateFrame [in] 更新フレーム
@param Pattern [in] アニメーション数
@param Direction [in] 横一列のアニメーション数
@param plate [in] 板ポリの種類
@param bCenter [in] true で中心座標を下の真ん中にします/デフォルトはfalse
@return 失敗時 true
*/
bool DX9_Billboard::Init(const char* Input, int UpdateFrame, int Pattern, int Direction, DX9_BillboardPlateList plate, bool bCenter)
{
	// テクスチャの格納
	if (this->m_texture->init(Input)) { return true; }

    // バッファの生成
	if (this->CreateBuffer(plate, bCenter)) { return true; }

	// アニメーションの生成
	this->CreateAnimation(UpdateFrame, Pattern, Direction);

	return false;
}

//==========================================================================
/**
@brief 初期化
@param Input [in] 使うテクスチャのパス ダブルポインタ用
@param numdata [in] データ数
@param plate [in] 板ポリの種類
@param bCenter [in] true で中心座標を下の真ん中にします/デフォルトはfalse
@return 失敗時 true
*/
bool DX9_Billboard::Init(const char ** Input, int numdata, DX9_BillboardPlateList plate, bool bCenter)
{
	for (int i = 0; i < numdata; i++)
	{
		if (this->Init(Input[i], plate, bCenter))
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
/**
@brief アニメーション用初期化
@param Input [in] 使うテクスチャのパス
@param numdata [in] データ数
@param UpdateFrame [in] 更新フレーム
@param Pattern [in] アニメーション数
@param Direction [in] 横一列のアニメーション数
@param plate [in] 板ポリの種類
@param bCenter [in] true で中心座標を下の真ん中にします/デフォルトはfalse
@return 失敗時 true
*/
bool DX9_Billboard::Init(const char ** Input, int numdata, int UpdateFrame, int Pattern, int Direction, DX9_BillboardPlateList plate, bool bCenter)
{
	for (int i = 0; i < numdata; i++)
	{
		if (this->Init(Input[i], UpdateFrame, Pattern, Direction, plate, bCenter))
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
/**
@brief 更新
*/
void DX9_Billboard::Update(void)
{
}

//==========================================================================
/**
@brief 解放
*/
void DX9_Billboard::Release(void)
{
    // テクスチャの解放
    if (this->m_texture != nullptr)
    {
        this->m_texture->Release();
        delete this->m_texture;
        this->m_texture = nullptr;
    }
    this->m_buffer_origin.Release();
    this->m_buffer_path.Release();
    this->ObjectRelease();
}

//==========================================================================
/**
@brief 描画
@param MtxView [in] ビュー行列
*/
void DX9_Billboard::Draw(D3DXMATRIX * MtxView)
{
    this->BaseDraw(false, MtxView);
}

//==========================================================================
/**
@brief 描画
@param bADD [in] 加算合成するかの判定
@param MtxView [in] ビュー行列
*/
void DX9_Billboard::Draw(bool bADD, D3DXMATRIX * MtxView)
{
    this->BaseDraw(bADD, MtxView);
}

//==========================================================================
/**
@brief ベースとなる描画
@param bADD [in] 加算合成
@param MtxView [in] ビュー行列
*/
void DX9_Billboard::BaseDraw(bool bADD, D3DXMATRIX * MtxView)
{
    if (this->m_buffer_path.Size() != 0 && this->m_ObjectData.size() != 0)
    {
        // FVFの設定
        this->m_Device->SetFVF(this->FVF_VERTEX_4);

        // 表示モード操作がある場合
        if (bADD)
        {
            this->SetRenderALPHAREF_END(this->m_Device);

            // 全加算合成
            this->SetRenderSUB(this->m_Device);
            this->SetRenderZWRITEENABLE_START(this->m_Device);
        }

        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            auto *buf = this->m_buffer_path.Get(itr->m_obj->GetIndex());
            auto * pTexList = this->m_texture->get(itr->m_obj->GetIndex());
            if (pTexList != nullptr)
            {
                // アニメーション用カウンタとアニメーション情報が存在するとき
                if (buf->m_pAnimation != nullptr)
                {
                    this->AnimationPalam(buf, itr->m_obj->GetAnimationCount());
                    this->UV(buf);
                }

                // 板ポリの種類
                this->BillboardType(buf, itr->m_obj, itr->MtxWorld, MtxView);

                // バッファ
                this->VertexUpdate(buf);

                // パイプライン
                this->m_Device->SetStreamSource(0, buf->m_buffer.m_pVertexBuffer, 0, sizeof(VERTEX_4));

                // インデックス登録
                this->m_Device->SetIndices(buf->m_buffer.m_pIndexBuffer);

                // トランスフォーム
                this->m_Device->SetTransform(D3DTS_WORLD, &itr->MtxWorld);

                pTexList->DrawBegin();
                this->m_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
                pTexList->DrawEnd();
            }
        }

        // 元に戻す
        if (bADD)
        {
            this->SetRenderADD(this->m_Device);
            this->SetRenderZWRITEENABLE_END(this->m_Device);
            this->SetRenderALPHAREF_START(this->m_Device, 100);
        }
    }
}

//==========================================================================
/**
@brief 頂点の更新
@param ptexture [in] ビルボードアニメーションクラス
*/
void DX9_Billboard::VertexUpdate(DX9_BillboardAnimationParam *ptexture)
{
    ptexture->m_buffer.m_pVertexBuffer->Lock(0, 0, (void**)&ptexture->m_buffer.m_pPseudo, D3DLOCK_DISCARD);
    ptexture->m_buffer.m_pPseudo[0].Tex = D3DXVECTOR2(ptexture->m_uv.u0, ptexture->m_uv.v0);
    ptexture->m_buffer.m_pPseudo[1].Tex = D3DXVECTOR2(ptexture->m_uv.u1, ptexture->m_uv.v0);
    ptexture->m_buffer.m_pPseudo[2].Tex = D3DXVECTOR2(ptexture->m_uv.u1, ptexture->m_uv.v1);
    ptexture->m_buffer.m_pPseudo[3].Tex = D3DXVECTOR2(ptexture->m_uv.u0, ptexture->m_uv.v1);
    ptexture->m_buffer.m_pVertexBuffer->Unlock();	// ロック解除
}

//==========================================================================
/**
@brief ビルボードの種類
@param ptexture [in] ビルボードアニメーションクラス
@param pInput [in] 対象オブジェクト
@param MtxWorld [in/out] ワールド行列
@param MtxView [in] ビュー行列
*/
void DX9_Billboard::BillboardType(DX9_BillboardAnimationParam *ptexture, DX9_3DObject * pInput, D3DXMATRIX & MtxWorld, D3DXMATRIX * MtxView)
{
    // 板ポリの種類
    switch (ptexture->m_buffer.m_plate)
    {
    case DX9_BillboardPlateList::Vertical:
        if (MtxView != nullptr)
        {
            pInput->SetMtxView(MtxView);
            pInput->SetMatrixType(DX9_3DObjectMatrixType::MirrorVector);
            pInput->CreateMtxWorld(MtxWorld);
        }
        else if (MtxView == nullptr)
        {
            pInput->SetMatrixType(DX9_3DObjectMatrixType::Vector1);
            pInput->CreateMtxWorld(MtxWorld);
        }
        break;
    case DX9_BillboardPlateList::Up:
        if (MtxView != nullptr)
        {
            pInput->SetMtxView(MtxView);
            pInput->SetMatrixType(DX9_3DObjectMatrixType::MirrorNotVector);
            pInput->CreateMtxWorld(MtxWorld);
        }
        else if (MtxView == nullptr)
        {
            pInput->SetMatrixType(DX9_3DObjectMatrixType::NotVector);
            pInput->CreateMtxWorld(MtxWorld);
        }
        break;
    default:
        break;
    }
}

//==========================================================================
/**
@brief 頂点バッファの生成
@param ptexture [in] ビルボードアニメーションクラス
@param bCenter [in] 中心座標フラグ
*/
void DX9_Billboard::CreateVertex(DX9_BillboardAnimationParam *ptexture, bool bCenter)
{
	// 板ポリの種類
	switch (ptexture->m_buffer.m_plate)
	{
	case DX9_BillboardPlateList::Vertical:
	{
		if (bCenter == true)
		{
			VERTEX_4 vVertex[] =
			{
				// 手前
				{ D3DXVECTOR3(-VCRCT, VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u0,ptexture->m_uv.v0) }, // 左上
				{ D3DXVECTOR3(VCRCT, VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u1,ptexture->m_uv.v0) }, // 右上
				{ D3DXVECTOR3(VCRCT,-VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u1,ptexture->m_uv.v1) }, // 右下
				{ D3DXVECTOR3(-VCRCT,-VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u0,ptexture->m_uv.v1) }, // 左下
			};

            for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++)
			{
                ptexture->m_buffer.m_pPseudo[i] = vVertex[i];
			}
		}
		else if (bCenter == false)
		{
			VERTEX_4 vVertex[] =
			{
				// 手前
				{ D3DXVECTOR3(-VCRCT, 1.0f,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u0,ptexture->m_uv.v0) }, // 左上
				{ D3DXVECTOR3(VCRCT, 1.0f,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u1,ptexture->m_uv.v0) }, // 右上
				{ D3DXVECTOR3(VCRCT,0.0f,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u1,ptexture->m_uv.v1) }, // 右下
				{ D3DXVECTOR3(-VCRCT,0.0f,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u0,ptexture->m_uv.v1) }, // 左下
			};

			for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++)
			{
                ptexture->m_buffer.m_pPseudo[i] = vVertex[i];
			}
		}
	}
	break;
	case DX9_BillboardPlateList::Up:
	{
		VERTEX_4 vVertex[] =
		{
			// 上
			{ D3DXVECTOR3(-VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv->u0,ptexture->m_uv->v0) }, // 左上
			{ D3DXVECTOR3(VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv->u1,ptexture->m_uv->v0) }, // 右上
			{ D3DXVECTOR3(VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv->u1,ptexture->m_uv->v1) }, // 右下
			{ D3DXVECTOR3(-VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv->u0,ptexture->m_uv->v1) }, // 左下
		};
		for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++)
		{
            ptexture->m_buffer.m_pPseudo[i] = vVertex[i];
		}
	}
	break;
	default:
		break;
	}
}

//==========================================================================
/**
@brief アニメーション情報のセット
@param ptexture [in] ビルボードアニメーションクラス
@param AnimationCount [in] アニメーションカウンタ
*/
void DX9_Billboard::AnimationPalam(DX9_BillboardAnimationParam *ptexture, int & AnimationCount)
{
	int PattanNum = (AnimationCount / ptexture->m_pAnimation->Frame) % ptexture->m_pAnimation->Pattern;	//パターン数
	int patternV = PattanNum % ptexture->m_pAnimation->Direction; // 横方向のパターン
	int patternH = PattanNum / ptexture->m_pAnimation->Direction; // 縦方向のパターン

	ptexture->m_pAnimation->m_Cut.x = patternV * ptexture->m_pAnimation->m_Cut.w; // 切り取り座標X
	ptexture->m_pAnimation->m_Cut.y = patternH * ptexture->m_pAnimation->m_Cut.h; // 切り取り座標Y
}

//==========================================================================
/**
@brief アニメーション切り替わり時の判定
@param index [in] リソース番号
@param AnimationCount [in] アニメーションカウンタを入れるところ
@return アニメーション終了時 true
*/
bool DX9_Billboard::GetPattanNum(int index, int & AnimationCount)
{
    auto *buf = this->m_buffer_path.Get(index);

    // セットされているとき
    // フレームが一致したとき
    if (AnimationCount == (buf->m_pAnimation->Frame*buf->m_pAnimation->Pattern) - 1)
    {
        this->InitAnimationCount(AnimationCount);
        return true;
    }
    return false;
}

//==========================================================================
/**
@brief アニメーションカウンタの初期化
@param AnimationCount [in] アニメーションカウンタを入れるところ
*/
void DX9_Billboard::InitAnimationCount(int & AnimationCount)
{
    AnimationCount = -1;
}

//==========================================================================
/**
@brief UVの生成
@param ptexture [in] ビルボードアニメーションクラス
*/
void DX9_Billboard::UV(DX9_BillboardAnimationParam *ptexture)
{
	// 左上
	ptexture->m_uv.u0 = (float)ptexture->m_pAnimation->m_Cut.x / ptexture->m_pAnimation->m_TexSize.w;
	ptexture->m_uv.v0 = (float)ptexture->m_pAnimation->m_Cut.y / ptexture->m_pAnimation->m_TexSize.h;

	// 左下
	ptexture->m_uv.u1 = (float)(ptexture->m_pAnimation->m_Cut.x + ptexture->m_pAnimation->m_Cut.w) / ptexture->m_pAnimation->m_TexSize.w;
	ptexture->m_uv.v1 = (float)(ptexture->m_pAnimation->m_Cut.y + ptexture->m_pAnimation->m_Cut.h) / ptexture->m_pAnimation->m_TexSize.h;
}

//==========================================================================
/**
@brief 重複検索
@param plate [in] 板ポリの種類
@return 重複時 true
*/
bool DX9_Billboard::OverlapSearch(DX9_BillboardPlateList plate)
{
    for (int i = 0; i < this->m_buffer_origin.Size(); i++)
    {
        auto * buf = this->m_buffer_origin.Get(i);

        // 板ポリの種類が同じとき
        if (buf->m_buffer.m_plate == plate)
        {
            auto * bufsub = this->m_buffer_path.Create();
            bufsub->m_buffer.m_pVertexBuffer = buf->m_buffer.m_pVertexBuffer;
            bufsub->m_buffer.m_pIndexBuffer = buf->m_buffer.m_pIndexBuffer;
            bufsub->m_buffer.m_plate = buf->m_buffer.m_plate;
            bufsub->m_buffer.m_pPseudo = buf->m_buffer.m_pPseudo;
            bufsub->m_buffer.m_origin = false;
            return true;
        }
    }

	return false;
}

//==========================================================================
/**
@brief バッファの生成
@param plate [in] 板ポリの種類
@param bCenter [in] 中心座標モード
@return 重複時 true
*/
bool DX9_Billboard::CreateBuffer(DX9_BillboardPlateList plate, bool bCenter)
{
	WORD* pIndex = nullptr;

	// 重複判定が出なかったとき
	if (!this->OverlapSearch(plate))
	{
		// メモリ確保
        auto * buf = this->m_buffer_origin.Create();

        buf->m_buffer.m_plate = plate;

		if (this->CreateVertexBuffer(this->m_Device, this->m_hWnd, sizeof(VERTEX_4) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &buf->m_buffer.m_pVertexBuffer, nullptr))
		{
			return true;
		}

		if (this->CreateIndexBuffer(this->m_Device, this->m_hWnd, sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &buf->m_buffer.m_pIndexBuffer, nullptr))
		{
			return true;
		}

        buf->m_buffer.m_pVertexBuffer->Lock(0, 0, (void**)&buf->m_buffer.m_pPseudo, D3DLOCK_DISCARD);
		this->CreateVertex(buf, bCenter);
        buf->m_buffer.m_pVertexBuffer->Unlock();	// ロック解除

        buf->m_buffer.m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
		this->RectangleIndex(pIndex, 1);
        buf->m_buffer.m_pIndexBuffer->Unlock();	// ロック解除

        buf->m_buffer.m_origin = true;

        auto * bufsub = this->m_buffer_path.Create();
        bufsub->m_buffer.m_pVertexBuffer = buf->m_buffer.m_pVertexBuffer;
        bufsub->m_buffer.m_pIndexBuffer = buf->m_buffer.m_pIndexBuffer;
        bufsub->m_buffer.m_plate = buf->m_buffer.m_plate;
        bufsub->m_buffer.m_pPseudo = buf->m_buffer.m_pPseudo;
        bufsub->m_buffer.m_origin = false;
	}

	return false;
}

//==========================================================================
/**
@brief アニメーション情報の生成
@param UpdateFrame [in] 更新フレーム
@param Pattern [in] アニメーション数
@param Direction [in] 一行のアニメーション数
*/
void DX9_Billboard::CreateAnimation(int UpdateFrame, int Pattern, int Direction)
{
	int nCount = 0;

	// メモリ確保
    auto * buf = this->m_buffer_path.Get(this->m_buffer_path.Size() - 1);
    buf->m_pAnimation = new DX9_BillboardAnimation;

	*buf->m_pAnimation = DX9_BillboardAnimation(UpdateFrame, Pattern, Direction, CTexvec<float>(0.0f, 0.0f, 0.0f, 0.0f), *this->m_texture->get(this->m_texture->size() - 1)->GetTextureList()->begin()->getsize());

    buf->m_pAnimation->m_Cut.w = (float)buf->m_pAnimation->m_TexSize.w / buf->m_pAnimation->Direction;

	// 切り取る縦幅を計算
	for (int i = 0;; i += buf->m_pAnimation->Direction)
	{
		if (buf->m_pAnimation->Pattern <= i) { break; }
		nCount++;
	}

    buf->m_pAnimation->m_Cut.h = (float)buf->m_pAnimation->m_TexSize.h / nCount;
    buf->m_pAnimation->m_Cut.x = buf->m_pAnimation->m_Cut.y = 0.0f;
}

DX9_BillboardAnimation::DX9_BillboardAnimation()
{
    this->Frame = 0;
    this->Pattern = 0;
    this->Direction = 0;
    this->m_Cut = 0;
    this->m_TexSize = 0;
}

//==========================================================================
/**
@brief ビルボードアニメーション
@param frame [in] 更新タイミング
@param pattern [in] アニメーションのパターン数
@param direction [in] 一行のアニメーション数
@param cut [in] 切り取り情報
@param texsize [in] テクスチャのサイズ
*/
DX9_BillboardAnimation::DX9_BillboardAnimation(int frame, int pattern, int direction, CTexvec<float> cut, CTexvec<int> texsize)
{
    this->Frame = frame;
    this->Pattern = pattern;
    this->Direction = direction;
    this->m_Cut = cut;
    this->m_TexSize = texsize;
}

DX9_BillboardAnimation::~DX9_BillboardAnimation()
{
}

DX9_BillboardBuffer::DX9_BillboardBuffer()
{
    this->m_plate = (DX9_BillboardPlateList)-1;
    this->m_pIndexBuffer = nullptr;
    this->m_pVertexBuffer = nullptr;
    this->m_pPseudo = nullptr;
    this->m_origin = false;
}

DX9_BillboardBuffer::~DX9_BillboardBuffer()
{
    this->Release();
}

//==========================================================================
/**
@brief 解放
*/
void DX9_BillboardBuffer::Release(void)
{
    if (this->m_origin)
    {
        // バッファ解放
        if (this->m_pVertexBuffer != nullptr)
        {
            this->m_pVertexBuffer->Release();
            this->m_pVertexBuffer = nullptr;
        }

        // インデックスバッファ解放
        if (this->m_pIndexBuffer != nullptr)
        {
            this->m_pIndexBuffer->Release();
            this->m_pIndexBuffer = nullptr;
        }
    }

    this->m_pPseudo = nullptr;
}

DX9_BillboardAnimationParam::DX9_BillboardAnimationParam()
{
    this->m_pAnimation = nullptr;
    this->m_uv = CUv<float>(0.0f, 0.0f, 1.0f, 1.0f);
}

DX9_BillboardAnimationParam::~DX9_BillboardAnimationParam()
{
    this->Release();
}

//==========================================================================
/**
@brief 解放
*/
void DX9_BillboardAnimationParam::Release(void)
{
    if (this->m_pAnimation!=nullptr)
    {
        delete this->m_pAnimation;
        this->m_pAnimation = nullptr;
    }
}

DX9_BillboardAnimationDataCase::DX9_BillboardAnimationDataCase()
{
    this->m_strName[0] = this->m_strName[1] = this->m_strName[2] = this->m_strName[3] = "";
    this->m_numdata = 0;
    this->m_frame = 0;
    this->m_pattern = 0;
    this->m_direction = 0;
    this->m_plate = DX9_BillboardPlateList::Up;
    this->m_centermood = false;
}

DX9_BillboardAnimationDataCase::~DX9_BillboardAnimationDataCase()
{
    this->m_strName[0].clear();
    this->m_strName[1].clear();
    this->m_strName[2].clear();
    this->m_strName[3].clear();
}

_MSLIB_END