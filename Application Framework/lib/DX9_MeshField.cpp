//==========================================================================
// メッシュフィールド[MeshField.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_MeshField.h"

_MSLIB_BEGIN

//==========================================================================
// 定数定義
//==========================================================================
constexpr int x_2 = 2;
constexpr float CorrectionValue = 0.5f;
constexpr int DefaultSize = 8; // バフ

DX9_MeshFieldData::DX9_MeshFieldData()
{
    this->m_NameID = "";
    this->m_Color = 255;
}

DX9_MeshFieldData::~DX9_MeshFieldData()
{
    this->m_NameID.clear();
}

DX9_MeshFieldDataParam::DX9_MeshFieldDataParam()
{
    this->NumMeshX = 0;
    this->NumMeshZ = 0;
    this->VertexOverlap = 0;
    this->NumXVertexWey = 0;
    this->NumZVertex = 0;
    this->NumXVertex = 0;
    this->NumMeshVertex = 0;
    this->MaxPrimitive = 0;
    this->MaxIndex = 0;
    this->MaxVertex = 0;
}

DX9_MeshFieldDataParam::~DX9_MeshFieldDataParam()
{
}

DX9_MeshFieldParam::DX9_MeshFieldParam()
{
    this->pVertexBuffer = nullptr;
    this->pIndexBuffer = nullptr;
    this->m_FieldParameter.Release();
    this->m_MismatchSubData.clear();
    this->m_MismatchMainData.clear();
}

DX9_MeshFieldParam::~DX9_MeshFieldParam()
{
    this->Release();
}

void DX9_MeshFieldParam::Release(void)
{
    // バッファ解放
    if (this->pVertexBuffer != nullptr)
    {
        this->pVertexBuffer->Release();
        this->pVertexBuffer = nullptr;
    }

    // インデックスバッファ解放
    if (this->pIndexBuffer != nullptr)
    {
        this->pIndexBuffer->Release();
        this->pIndexBuffer = nullptr;
    }

    this->m_MismatchSubData.clear();
    this->m_MismatchMainData.clear();
    this->m_FieldParameter.Release();
    this->m_GroupID.clear();
    this->m_ArrayID.Release();
}

DX9_MeshField::DX9_MeshField(LPDIRECT3DDEVICE9 pDevice, HWND hWnd)
{
    this->m_texture = new DX9_TextureLoader(pDevice, hWnd);
    this->m_Device = pDevice;
    this->m_hWnd = hWnd;
}

DX9_MeshField::~DX9_MeshField()
{
    this->Release();
}

//==========================================================================
/**
@brief 初期化
@param texture [in] テクスチャパス
@param x [in] 横幅
@param z [in] 奥行
@return 失敗時に true が返ります
*/
bool DX9_MeshField::Init(const char * texture, int x, int z)
{
    if (this->m_texture->init(texture))
    {
        return true;
    }

    auto * pdepoly = this->m_MeshData.Create();

    return this->Resize(pdepoly, x, z);
}

//==========================================================================
/**
@brief 解放
*/
void DX9_MeshField::Release(void)
{
    // テクスチャの解放
    if (this->m_texture != nullptr)
    {
        this->m_texture->Release();
        delete this->m_texture;
        this->m_texture = nullptr;
    }
    this->m_MeshData.Release();
    this->ObjectRelease();
}

//==========================================================================
/**
@brief リサイズ
@param pdepoly [in] ポリゴン情報
@param x [in] 横幅
@param z [in] 奥行
@return 失敗時 true が返ります
*/
bool DX9_MeshField::Resize(DX9_MeshFieldParam * pdepoly, int x, int z)
{
    VectorV4 createV4;
    // メッシュフィールドの情報算出
    this->MeshFieldInfo(pdepoly->Info, x, z, DefaultSize);

    // 空間分割の生成
    this->create_spatial_partitioning(pdepoly, DefaultSize);

    this->CreateData(createV4, pdepoly);

    this->Consistency(createV4, pdepoly);

    if (this->CreateVertexBuffer(this->m_Device, this->m_hWnd, sizeof(VERTEX_4) * pdepoly->Info.MaxVertex, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &pdepoly->pVertexBuffer, nullptr))
    {
        return true;
    }

    if (this->CreateIndexBuffer(this->m_Device, this->m_hWnd, sizeof(LPWORD) * pdepoly->Info.MaxIndex, D3DUSAGE_WRITEONLY, D3DFMT_INDEX32, D3DPOOL_MANAGED, &pdepoly->pIndexBuffer, nullptr))
    {
        return true;
    }

    return this->RemakeBuffer(pdepoly);
}

//==========================================================================
/**
@brief バッファのリメイク
@param pdepoly [in] ポリゴン情報
@return 失敗時 true が返ります
*/
bool DX9_MeshField::RemakeBuffer(DX9_MeshFieldParam * pdepoly)
{
    VERTEX_4* pPseudo = nullptr;
    LPWORD* pIndex = nullptr;

    this->SetNormal(pdepoly->m_FieldParameter);

    pdepoly->pVertexBuffer->Lock(0, 0, (void**)&pPseudo, D3DLOCK_DISCARD);
    this->CreateVertex(pPseudo, pdepoly->m_FieldParameter);
    pdepoly->pVertexBuffer->Unlock();	// ロック解除

    pdepoly->pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
    this->CreateIndex(pIndex, pdepoly->Info);
    pdepoly->pIndexBuffer->Unlock();	// ロック解除

    return false;
}

//==========================================================================
/**
@brief 更新
*/
void DX9_MeshField::Update(void)
{
    // 登録済みオブジェクトのデータの更新
    for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
    {
        // 行列がデフォルトの指定の時
        if (itr->m_obj->GetMatrixType() == DX9_3DObjectMatrixType::Default)
        {
            itr->m_obj->SetMatrixType(DX9_3DObjectMatrixType::NotVector);
        }

        // 行列の生成
        itr->m_obj->CreateMtxWorld(itr->MtxWorld);
    }
}

//==========================================================================
/**
@brief 描画
*/
void DX9_MeshField::Draw(void)
{
    if (this->m_texture->size() != 0 && this->m_ObjectData.size() != 0)
    {
        // FVFの設定
        this->m_Device->SetFVF(this->FVF_VERTEX_4);

        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            auto * pdepoly = this->m_MeshData.Get(itr->m_obj->GetIndex());
            // テクスチャの取得
            auto * pTexList = this->m_texture->get(itr->m_obj->GetIndex());
            if (pTexList != nullptr)
            {
                // サイズ
                this->m_Device->SetStreamSource(0, pdepoly->pVertexBuffer, 0, sizeof(VERTEX_4));	// パイプライン

                this->m_Device->SetIndices(pdepoly->pIndexBuffer);

                // マテリアル情報をセット
                this->m_Device->SetMaterial(&itr->m_obj->GetD3DMaterial9());

                // 各種行列の設定
                this->m_Device->SetTransform(D3DTS_WORLD, &itr->MtxWorld);

                pTexList->DrawBegin();
                this->m_Device->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, pdepoly->Info.MaxVertex, 0, pdepoly->Info.MaxPrimitive);
                pTexList->DrawEnd();
            }
        }
    }
}

//==========================================================================
/**
@brief バッファに渡すデータの生成
@param SetParam [in] メッシュの情報
@param pdepoly [in] メッシュ情報
*/
void DX9_MeshField::CreateData(VectorV4 & SetParam, DX9_MeshFieldParam * pdepoly)
{
    // アクセスmapの破棄
    pdepoly->m_MismatchMainData.clear();

    bool bZ = false;
    bool bX = false;
    float fPosZ = pdepoly->Info.BoostMeshZ*CorrectionValue;
    float fPosX = 0.0f;
    int nIDZ = (int)(pdepoly->Info.BoostMeshZ*CorrectionValue);
    int nIDX = 0;

    for (int iZ = 0; iZ < pdepoly->Info.NumZVertex; iZ++, fPosZ--, nIDZ--)
    {
        bX = true;
        fPosX = -(pdepoly->Info.BoostMeshX*CorrectionValue);
        nIDX = -(int)(pdepoly->Info.BoostMeshX*CorrectionValue);
        SetParam.Create();
        for (int iX = 0; iX < pdepoly->Info.NumXVertex; iX++, fPosX++, nIDX++)
        {
            auto * vec = SetParam.Get(iZ)->Create();
            vec->m_NameID = std::to_string(nIDZ);
            vec->m_NameID += ":";
            vec->m_NameID += std::to_string(nIDX);
            vec->vertex.pos = D3DXVECTOR3(fPosX, 0.0f, fPosZ);
            vec->vertex.color = vec->m_Color.get();
            vec->vertex.Tex = D3DXVECTOR2((FLOAT)bX, (FLOAT)bZ);
            vec->vertex.Normal = D3DXVECTOR3(0, 0, 0);
            pdepoly->m_MismatchMainData[vec->m_NameID] = vec;
            bX = bX ^ 1;
        }
        bZ = bZ ^ 1;
    }
}

//==========================================================================
/**
@brief データの整合性
@param SetParam [in] メッシュの情報
@param pdepoly [in] メッシュ情報
*/
void DX9_MeshField::Consistency(VectorV4 & Input, DX9_MeshFieldParam * pdepoly)
{
    // 編集されたデータの流し込み
    for (auto itr = pdepoly->m_MismatchSubData.begin(); itr != pdepoly->m_MismatchSubData.end(); ++itr)
    {
        // 要素の探索
        auto itr2 = pdepoly->m_MismatchMainData.find(itr->first);
        if (itr2 != pdepoly->m_MismatchMainData.end())
        {
            auto & vec4_1 = itr->second;
            auto * vec4_2 = itr2->second;

            vec4_2->vertex = vec4_1.vertex;
            vec4_2->m_Color = vec4_1.m_Color;
        }
    }

    pdepoly->m_FieldParameter.Release();

    // アクセス
    for (int iZ = 0; iZ < Input.Size(); iZ++)
    {
        pdepoly->m_FieldParameter.Create();
        for (int iX = 0; iX < Input.Get(iZ)->Size(); iX++)
        {
            auto * vec = pdepoly->m_FieldParameter.Get(iZ)->Create();

            vec->m_Color = Input.Get(iZ)->Get(iX)->m_Color;
            vec->m_NameID = Input.Get(iZ)->Get(iX)->m_NameID;
            vec->vertex.pos = Input.Get(iZ)->Get(iX)->vertex.pos;
            vec->vertex.Normal = Input.Get(iZ)->Get(iX)->vertex.Normal;
            vec->vertex.color = Input.Get(iZ)->Get(iX)->vertex.color;
            vec->vertex.Tex = Input.Get(iZ)->Get(iX)->vertex.Tex;
        }
    }
}

//==========================================================================
/**
@brief インデックス情報の生成
@param Output [out] インデックス情報の取得
@param Input [in] メッシュデータ
*/
void DX9_MeshField::CreateIndex(LPWORD * Output, const DX9_MeshFieldDataParam & Input)
{
    for (int i = 0, Index1 = 0, Index2 = Input.NumXVertex, Overlap = 0; i < Input.MaxIndex; Index1++, Index2++)
    {
        // 通常頂点
        Output[i] = (LPWORD)Index2;
        i++;

        // 重複点
        if (Overlap == Input.NumXVertexWey&&i < Input.MaxIndex)
        {
            Output[i] = (LPWORD)Index2;
            i++;
            Overlap = 0;
        }

        // 通常頂点
        Output[i] = (LPWORD)Index1;
        i++;

        Overlap += x_2;

        // 重複点
        if (Overlap == Input.NumXVertexWey&&i < Input.MaxIndex)
        {
            Output[i] = (LPWORD)Index1;
            i++;
        }
    }
}

//==========================================================================
/**
@brief バーテックス情報の生成
@param Output [out] バーテックス情報
@param SetParam [in] セットするパラメータ
*/
void DX9_MeshField::CreateVertex(VERTEX_4 * Output, VectorV4 & SetParam)
{
    // バッファに流し込む
    for (int iZ = 0; iZ < SetParam.Size(); iZ++)
    {
        for (int iX = 0; iX < SetParam.Get(iZ)->Size(); iX++, Output++)
        {
            auto * vec = SetParam.Get(iZ)->Get(iX);
            Output->pos = vec->vertex.pos;
            Output->color = vec->vertex.color = vec->m_Color.get();
            Output->Tex = vec->vertex.Tex;
            Output->Normal = vec->vertex.Normal;
        }
    }
}

//==========================================================================
/**
@brief メッシュ情報の生成
@param Output [out] インデックス情報の取得
@param numX [in] X軸への幅
@param numZ [in] Z軸への幅
@param buff [in] 何分岐設定
*/
void DX9_MeshField::MeshFieldInfo(DX9_MeshFieldDataParam & Output, const int numX, const int numZ, const int buff)
{
    Output.NumMeshX = numX;
    Output.NumMeshZ = numZ;
    Output.BoostMeshX = (numX * buff);
    Output.BoostMeshZ = (numZ * buff);
    Output.NumXVertex = (numX * buff) + 1; // 基礎頂点数
    Output.NumZVertex = (numZ * buff) + 1; // 基礎頂点数
    Output.MaxVertex = Output.NumXVertex * Output.NumZVertex; // 最大頂点数
    Output.NumXVertexWey = x_2 * Output.NumXVertex; // 視覚化されている1列の頂点数
    Output.VertexOverlap = x_2 * ((numZ * buff) - 1); // 重複する頂点数
    Output.NumMeshVertex = Output.NumXVertexWey * (numZ * buff); // 視覚化されている全体の頂点数
    Output.MaxIndex = Output.NumMeshVertex + Output.VertexOverlap; // 最大Index数
    Output.MaxPrimitive = (((numX * buff) * (numZ * buff)) * x_2) + (Output.VertexOverlap * x_2); // プリミティブ数
}

//==========================================================================
/**
@brief 法線の生成
@param SetParam [in] セットするパラメータ
*/
void DX9_MeshField::SetNormal(VectorV4 & SetParam)
{
    // 法線の設定
    for (int iZ = 1; iZ < SetParam.Size() - 1; iZ++)
    {
        for (int iX = 1; iX < SetParam.Get(iZ)->Size() - 1; iX++)
        {
            auto n = D3DXVECTOR3(0, 0, 0);
            auto nx = D3DXVECTOR3(0, 0, 0);
            auto nz = D3DXVECTOR3(0, 0, 0);
            auto vx = SetParam.Get(iZ)->Get(iX + 1)->vertex.pos - SetParam.Get(iZ)->Get(iX - 1)->vertex.pos;
            nx.x = vx.y; // 垂直なベクトル
            nx.y = vx.x;
            nx.z = 0.0f;

            vx = SetParam.Get(iZ - 1)->Get(iX)->vertex.pos - SetParam.Get(iZ + 1)->Get(iX)->vertex.pos;
            nz.x = 0.0f; // 垂直なベクトル
            nz.y = vx.z;
            nz.z = -vx.y;

            n = nx + nz;
            D3DXVec3Normalize(&n, &n);
            SetParam.Get(iZ)->Get(iX)->vertex.Normal = n;
        }
    }
}

//==========================================================================
/**
@brief 空間分割の生成
@param pdepoly [in] メッシュ情報
@param buff [in] 分割範囲
*/
void DX9_MeshField::create_spatial_partitioning(DX9_MeshFieldParam * pdepoly, const int buff)
{
    pdepoly->m_GroupID.clear();
    pdepoly->m_ArrayID.Release();

    // 空間分割と境目の定義
    int GroupID = 0;
    for (int iZ = buff, iZCount = 0; iZ < pdepoly->Info.NumZVertex; iZ += buff, iZCount += buff)
    {
        for (int iX = buff, iXCount = 0; iX < pdepoly->Info.NumXVertex; iX += buff, iXCount += buff)
        {
            DX9_MeshFieldID * pMapArray = nullptr;
            pdepoly->m_GroupID.emplace_back(GroupID);
            auto * pArray = pdepoly->m_ArrayID.Create();

            // 左上
            pMapArray = pArray->Create();
            pMapArray->x = iXCount;
            pMapArray->z = iZCount;

            // 右上
            pMapArray = pArray->Create();
            pMapArray->x = iX;
            pMapArray->z = iZCount;

            // 左下
            pMapArray = pArray->Create();
            pMapArray->x = iXCount;
            pMapArray->z = iZ;

            // 右下
            pMapArray = pArray->Create();
            pMapArray->x = iX;
            pMapArray->z = iZ;

            GroupID++;
        }
    }
}

//==========================================================================
/**
@brief 空間探索
@param pdepoly [in] メッシュ情報
@param Position [in] 前回の座標
@param HitID [in] エリアID
@return マップデータの取得
*/
vector_wrapper<DX9_MeshFieldID> * DX9_MeshField::SearchArea(DX9_MeshFieldParam * pdepoly, D3DXVECTOR3 Position, int & HitID)
{
    auto & vector = pdepoly->m_FieldParameter;
    D3DXVECTOR3 v01, v12, v20, v0p, v1p, v2p;
    float c0, c1, c2;

    // 探索空間の検出
    auto * pArrayID = pdepoly->m_ArrayID.Get(HitID);
    if (pArrayID != nullptr)
    {
        auto &pdata = *pArrayID;

        //==========================================================================
        // エリア三角形 左下
        v01 = v12 = v20 = v0p = v1p = v2p = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
        c0 = c1 = c2 = 0.0f;

        // 左上 - 左下
        v01 = vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos - vector.Get(pdata.Get(2)->z)->Get(pdata.Get(2)->x)->vertex.pos;
        // 右下 - 左上
        v12 = vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
        // 左下 - 右下
        v20 = vector.Get(pdata.Get(2)->z)->Get(pdata.Get(2)->x)->vertex.pos - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;
        // 左下
        v0p = Position - vector.Get(pdata.Get(2)->z)->Get(pdata.Get(2)->x)->vertex.pos;
        // 左上
        v1p = Position - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
        // 右下
        v2p = Position - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;

        c0 = v01.x*v0p.z - v01.z*v0p.x;
        c1 = v12.x*v1p.z - v12.z*v1p.x;
        c2 = v20.x*v2p.z - v20.z*v2p.x;

        if (c0 <= 0.0f&&c1 <= 0.0f&&c2 <= 0.0f)
        {
            return &pdata;
        }

        //==========================================================================
        // エリア三角形 右上
        v01 = v12 = v20 = v0p = v1p = v2p = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
        c0 = c1 = c2 = 0.0f;

        // 右上 - 左上
        v01 = vector.Get(pdata.Get(1)->z)->Get(pdata.Get(1)->x)->vertex.pos - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
        // 右下 - 右上
        v12 = vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos - vector.Get(pdata.Get(1)->z)->Get(pdata.Get(1)->x)->vertex.pos;
        // 左上 - 右下
        v20 = vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;
        // 左上
        v0p = Position - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
        // 右上
        v1p = Position - vector.Get(pdata.Get(1)->z)->Get(pdata.Get(1)->x)->vertex.pos;
        // 右下
        v2p = Position - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;

        c0 = v01.x*v0p.z - v01.z*v0p.x;
        c1 = v12.x*v1p.z - v12.z*v1p.x;
        c2 = v20.x*v2p.z - v20.z*v2p.x;

        if (c0 <= 0.0f&&c1 <= 0.0f&&c2 <= 0.0f)
        {
            return &pdata;
        }
    }

    // 探索空間の検出
    for (auto itr = pdepoly->m_GroupID.begin(); itr != pdepoly->m_GroupID.end(); ++itr)
    {
        auto * pArrayID_ = pdepoly->m_ArrayID.Get((*itr));
        if (pArrayID_ != nullptr)
        {
            auto &pdata = *pArrayID_;

            //==========================================================================
            // エリア三角形 左下
            v01 = v12 = v20 = v0p = v1p = v2p = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
            c0 = c1 = c2 = 0.0f;

            // 左上 - 左下
            v01 = vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos - vector.Get(pdata.Get(2)->z)->Get(pdata.Get(2)->x)->vertex.pos;
            // 右下 - 左上
            v12 = vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
            // 左下 - 右下
            v20 = vector.Get(pdata.Get(2)->z)->Get(pdata.Get(2)->x)->vertex.pos - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;
            // 左下
            v0p = Position - vector.Get(pdata.Get(2)->z)->Get(pdata.Get(2)->x)->vertex.pos;
            // 左上
            v1p = Position - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
            // 右下
            v2p = Position - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;

            c0 = v01.x*v0p.z - v01.z*v0p.x;
            c1 = v12.x*v1p.z - v12.z*v1p.x;
            c2 = v20.x*v2p.z - v20.z*v2p.x;

            if (c0 <= 0.0f&&c1 <= 0.0f&&c2 <= 0.0f)
            {
                HitID = (*itr);
                return &pdata;
            }

            //==========================================================================
            // エリア三角形 右上
            v01 = v12 = v20 = v0p = v1p = v2p = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
            c0 = c1 = c2 = 0.0f;

            // 右上 - 左上
            v01 = vector.Get(pdata.Get(1)->z)->Get(pdata.Get(1)->x)->vertex.pos - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
            // 右下 - 右上
            v12 = vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos - vector.Get(pdata.Get(1)->z)->Get(pdata.Get(1)->x)->vertex.pos;
            // 左上 - 右下
            v20 = vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;
            // 左上
            v0p = Position - vector.Get(pdata.Get(0)->z)->Get(pdata.Get(0)->x)->vertex.pos;
            // 右上
            v1p = Position - vector.Get(pdata.Get(1)->z)->Get(pdata.Get(1)->x)->vertex.pos;
            // 右下
            v2p = Position - vector.Get(pdata.Get(3)->z)->Get(pdata.Get(3)->x)->vertex.pos;

            c0 = v01.x*v0p.z - v01.z*v0p.x;
            c1 = v12.x*v1p.z - v12.z*v1p.x;
            c2 = v20.x*v2p.z - v20.z*v2p.x;

            if (c0 <= 0.0f&&c1 <= 0.0f&&c2 <= 0.0f)
            {
                HitID = (*itr);
                return &pdata;
            }
        }
    }
    return nullptr;
}

//==========================================================================
/**
@brief 当たり判定
@param pdepoly [in] ポリゴン情報
@param Position [in] 座標
@param HitID [in/out] エリアID
@return 判定時の座標情報
*/
D3DXVECTOR3 DX9_MeshField::GetHeight(DX9_MeshFieldParam * pdepoly, D3DXVECTOR3 Position, int & HitID)
{
    auto & vector = pdepoly->m_FieldParameter;

    // 空間探索
    auto * pArea = this->SearchArea(pdepoly, Position, HitID);
    if (pArea != nullptr)
    {
        // エリア内を探索
        for (int iZ = pArea->Get(0)->z; iZ <= pArea->Get(3)->z - 1; iZ++)
        {
            // 三角形片面
            // 左下
            for (int iX = pArea->Get(0)->x; iX <= pArea->Get(3)->x - 1; iX++)
            {
                // 1 - 0
                auto v01 = vector.Get(iZ)->Get(iX)->vertex.pos - vector.Get(iZ + 1)->Get(iX)->vertex.pos;
                // 3 - 1
                auto v12 = vector.Get(iZ + 1)->Get(iX + 1)->vertex.pos - vector.Get(iZ)->Get(iX)->vertex.pos;
                // 0 - 3
                auto v20 = vector.Get(iZ + 1)->Get(iX)->vertex.pos - vector.Get(iZ + 1)->Get(iX + 1)->vertex.pos;
                // 0
                auto v0p = Position - vector.Get(iZ + 1)->Get(iX)->vertex.pos;
                // 1
                auto v1p = Position - vector.Get(iZ)->Get(iX)->vertex.pos;
                // 3
                auto v2p = Position - vector.Get(iZ + 1)->Get(iX + 1)->vertex.pos;

                float c0 = v01.x*v0p.z - v01.z*v0p.x;
                float c1 = v12.x*v1p.z - v12.z*v1p.x;
                float c2 = v20.x*v2p.z - v20.z*v2p.x;

                if (c0 <= 0.0f&&c1 <= 0.0f&&c2 <= 0.0f)
                {
                    auto N = D3DXVECTOR3(0, 0, 0);
                    auto &vpos = vector.Get(iZ)->Get(iX)->vertex.pos;

                    D3DXVec3Cross(&N, &v01, &v12);
                    Position.y = vpos.y - (N.x * (Position.x - vpos.x) + N.z * (Position.z - vpos.z)) / N.y;
                    return Position;
                }
            }

            // 三角形片面
            // 右上
            for (int iX = pArea->Get(0)->x; iX <= pArea->Get(3)->x - 1; iX++)
            {
                // 2 - 1
                auto v01 = vector.Get(iZ)->Get(iX + 1)->vertex.pos - vector.Get(iZ)->Get(iX)->vertex.pos;
                // 3 - 2
                auto v12 = vector.Get(iZ + 1)->Get(iX + 1)->vertex.pos - vector.Get(iZ)->Get(iX + 1)->vertex.pos;
                // 1 - 3
                auto v20 = vector.Get(iZ)->Get(iX)->vertex.pos - vector.Get(iZ + 1)->Get(iX + 1)->vertex.pos;
                // 1
                auto v0p = Position - vector.Get(iZ)->Get(iX)->vertex.pos;
                // 2
                auto v1p = Position - vector.Get(iZ)->Get(iX + 1)->vertex.pos;
                // 3
                auto v2p = Position - vector.Get(iZ + 1)->Get(iX + 1)->vertex.pos;

                float c0 = v01.x*v0p.z - v01.z*v0p.x;
                float c1 = v12.x*v1p.z - v12.z*v1p.x;
                float c2 = v20.x*v2p.z - v20.z*v2p.x;

                if (c0 <= 0.0f&&c1 <= 0.0f&&c2 <= 0.0f)
                {
                    auto N = D3DXVECTOR3(0, 0, 0);
                    auto vpos = vector.Get(iZ)->Get(iX)->vertex.pos;

                    D3DXVec3Cross(&N, &v01, &v12);
                    Position.y = vpos.y - (N.x * (Position.x - vpos.x) + N.z * (Position.z - vpos.z)) / N.y;
                    return Position;
                }
            }
        }
    }
    return D3DXVECTOR3(0, 0, 0);
}

//==========================================================================
/**
@brief マップの保存
@param strFileName [in] ファイル名
*/
void DX9_MeshField::Save(const std::string strFileName)
{
    FILE *pFile = fopen(strFileName.c_str(), "wb");

    if (pFile)
    {
        // マップデータの取得
        int nMapSize = this->m_MeshData.Size();
        fwrite(&nMapSize, sizeof(nMapSize), 1, pFile);

        // 各データ全て書き込み
        for (int i = 0; i < this->m_MeshData.Size(); i++)
        {
            auto pmesh = this->m_MeshData.Get(i);

            // 各マップ毎の変化データ数
            int nSubMapSize = pmesh->m_MismatchSubData.size();
            fwrite(&nSubMapSize, sizeof(nSubMapSize), 1, pFile);

            // 各マップの広さ
            fwrite(&pmesh->Info.NumMeshX, sizeof(pmesh->Info.NumMeshX), 1, pFile);
            fwrite(&pmesh->Info.NumMeshZ, sizeof(pmesh->Info.NumMeshZ), 1, pFile);

            // map内のデータを全て呼び出し
            for (auto itr = pmesh->m_MismatchSubData.begin(); itr != pmesh->m_MismatchSubData.end(); ++itr)
            {
                // 要素の探索
                char cBuffer[512] = { 0 };

                // バッファに変換
                sprintf(cBuffer, "%s", itr->first.c_str());

                // 変化を与えるのに必要不可欠なデータ
                fwrite(&cBuffer, sizeof(cBuffer), 1, pFile);
                fwrite(&itr->second.m_Color, sizeof(itr->second.m_Color), 1, pFile);
                fwrite(&itr->second.vertex.color, sizeof(itr->second.vertex.color), 1, pFile);
                fwrite(&itr->second.vertex.Normal, sizeof(itr->second.vertex.Normal), 1, pFile);
                fwrite(&itr->second.vertex.pos, sizeof(itr->second.vertex.pos), 1, pFile);
                fwrite(&itr->second.vertex.Tex, sizeof(itr->second.vertex.Tex), 1, pFile);
            }
        }
        fclose(pFile);
    }
}

//==========================================================================
/**
@brief マップの読み込み
@param strFileName [in] ファイル名
*/
void DX9_MeshField::Loat(const std::string strFileName)
{
    FILE *pFile = fopen(strFileName.c_str(), "rb");

    if (pFile)
    {
        //==========================================================================
        // 読み込む前にリセット
        this->m_MeshData.Release();

        //==========================================================================
        // マップデータの取得
        int nMapSize = 0;
        fread(&nMapSize, sizeof(nMapSize), 1, pFile);

        // サイズ分ステージ格納領域の生成
        for (int i = 0; i < nMapSize; i++)
        {
            this->m_MeshData.Create();
        }

        //==========================================================================
        // 各データ全て書き込み
        for (int i = 0; i < this->m_MeshData.Size(); i++)
        {
            auto pmesh = this->m_MeshData.Get(i);

            //==========================================================================
            // 各マップ毎の変化データ数取得
            int nSubMapSize = 0;
            fread(&nSubMapSize, sizeof(nSubMapSize), 1, pFile);

            // 各マップの広さ取得
            fread(&pmesh->Info.NumMeshX, sizeof(pmesh->Info.NumMeshX), 1, pFile);
            fread(&pmesh->Info.NumMeshZ, sizeof(pmesh->Info.NumMeshZ), 1, pFile);

            // 変化データの取得
            for (int s = 0; s < nSubMapSize; s++)
            {
                char cBuffer[512] = { 0 };

                // 識別キーの取得
                fread(&cBuffer, sizeof(cBuffer), 1, pFile);

                // 識別キーを元にデータの登録
                auto & Input = pmesh->m_MismatchSubData[cBuffer];
                fread(&Input.m_Color, sizeof(Input.m_Color), 1, pFile);
                fread(&Input.vertex.color, sizeof(Input.vertex.color), 1, pFile);
                fread(&Input.vertex.Normal, sizeof(Input.vertex.Normal), 1, pFile);
                fread(&Input.vertex.pos, sizeof(Input.vertex.pos), 1, pFile);
                fread(&Input.vertex.Tex, sizeof(Input.vertex.Tex), 1, pFile);
            }
            this->Resize(pmesh, pmesh->Info.NumMeshX, pmesh->Info.NumMeshZ);
        }
        fclose(pFile);
    }
}

//==========================================================================
/**
@brief メッシュデータの取得
@return マップデータの取得
*/
vector_wrapper<DX9_MeshFieldParam>* DX9_MeshField::GetMeshData(void)
{
    return &this->m_MeshData;
}

_MSLIB_END