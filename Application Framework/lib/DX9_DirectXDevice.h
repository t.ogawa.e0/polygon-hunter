//==========================================================================
// デバイス[DX9_DirectXDevice.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_DirectXDevice
// Content: デバイス 
//
//==========================================================================
class DX9_DirectXDevice
{
private:
    // コピー禁止 (C++11)
    DX9_DirectXDevice(const DX9_DirectXDevice &) = delete;
    DX9_DirectXDevice &operator=(const DX9_DirectXDevice &) = delete;
public:
    DX9_DirectXDevice();
    ~DX9_DirectXDevice();

    /**
    @brief 初期化
    @return 失敗時に true が返ります
    */
    bool Init(void);

    /**
    @brief 解放
    */
    void Release(void);

    /**
    @brief ウィンドウモードの生成
    @param data [in] ウィンドウサイズ
    @param Mode [in] ウィンドウモード true でフルスクリーン
    */
    bool CreateWindowMode(const CVector2<int> & data, bool Mode);

    /**
    @brief デバイスの生成
    @return 失敗時に true が返ります
    */
    bool CreateDevice(void);

    /**
    @brief デバイスの習得
    @return デバイス
    */
    LPDIRECT3DDEVICE9 GetD3DDevice(void);

    /**
    @brief デバイスの習得
    @return デバイス
    */
    D3DPRESENT_PARAMETERS Getd3dpp(void);

    /**
    @brief ウィンドウサイズの習得
    @return ウィンドウサイズ
    */
    const CVector2<int> & GetWindowsSize(void);

    /**
    @brief ウィンドウハンドルの入力
    @param hWnd [in] ウィンドウハンドル
    */
    void SetHwnd(HWND hWnd);

    /**
    @brief ウィンドウハンドルの取得
    @return ウィンドウハンドル
    */
    HWND GetHwnd(void);

    /**
    @brief 推奨ウィンドウサイズとの誤差の取得
    @return 誤差割合が返ります
    */
    float screenbuffscale(void);

    /**
    @brief 描画開始
    @return 描画可能な際に true が返ります
    */
    bool DrawBegin(void);

    /**
    @brief 描画終了
    @param SourceRect [in]
    @param DestRect [in]
    @param hDestWindowOverride [in]
    @param DirtyRegion [in]
    @return Component Object Model defines, and macros
    */
    HRESULT STDMETHODCALLTYPE DrawEnd(const RECT* SourceRect, const RECT* DestRect, HWND hDestWindowOverride, const RGNDATA* DirtyRegion);

    /**
    @brief 描画終了
    @return Component Object Model defines, and macros
    */
    HRESULT STDMETHODCALLTYPE DrawEnd(void);

    /**
    @brief エラーメッセージ
    @param text [in] テキスト入力
    @param args [in] 出力文字
    */
    template <typename ... Args>
    void ErrorMessage(const char * text, Args const & ... args);
private:
    LPDIRECT3D9 m_pD3D; //ダイレクト3Dインターフェース
    LPDIRECT3DDEVICE9 m_pD3DDevice;//ダイレクト3Dデバイス
    CVector2<int> m_WindowsSize; // ウィンドウサイズ
    D3DPRESENT_PARAMETERS m_d3dpp;
    D3DDISPLAYMODE m_d3dpm;
    HWND m_hwnd; // ウィンドウハンドル
    std::vector<D3DDISPLAYMODE> m_d3dspMode; // ディスプレイモード管理
    float m_screenbuffscale; // 推奨サイズとの描画サイズの誤差
};

//==========================================================================
/**
@brief エラーメッセージ
@param text [in] テキスト入力
@param args [in] 出力文字
*/
template<typename ...Args>
inline void DX9_DirectXDevice::ErrorMessage(const char * text, Args const & ...args)
{
    char pfon[1024] = { 0 };

    sprintf(pfon, text, args ...);

    MessageBox(this->m_hwnd, pfon, "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
}

_MSLIB_END