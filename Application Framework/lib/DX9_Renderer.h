//==========================================================================
// DX9_Renderer[DX9_Renderer.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_TextureLoader.h"
#include "DX9_Vertex3D.h"
#include "DX9_SetRender.h"
#include "DX9_SetSampler.h"
#include "DX9_CreateBuffer.h"
#include "DX9_ObjectInput.h"
#include "DX9_Rectangle.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_Renderer
// Content : DX9_Renderer
// param _Ty DX9_2DObject or DX9_3DObject
//
//==========================================================================
template<typename _Ty>
class DX9_Renderer :
    protected DX9_VERTEX_3D,
    protected DX9_SetRender,
    protected DX9_SetSampler,
    protected DX9_CreateBuffer,
    protected DX9_Rectangle,
    public DX9_ObjectInput<_Ty>
{
protected:
    // コピー禁止 (C++11)
    DX9_Renderer(const DX9_Renderer &) = delete;
    DX9_Renderer &operator=(const DX9_Renderer &) = delete;
protected:
    DX9_Renderer() {
        this->m_pIndexBuffer = nullptr;
        this->m_pVertexBuffer = nullptr;
        this->m_texture = nullptr;
        this->m_Device = nullptr;
        this->m_hWnd = nullptr;
    }
    virtual ~DX9_Renderer() {
        if (this->m_texture != nullptr)
        {
            this->m_texture->Release();
            delete this->m_texture;
            this->m_texture = nullptr;
        }
        if (this->m_pVertexBuffer != nullptr)
        {
            this->m_pVertexBuffer->Release();
            this->m_pVertexBuffer = nullptr;
        }
        if (this->m_pIndexBuffer != nullptr)
        {
            this->m_pIndexBuffer->Release();
            this->m_pIndexBuffer = nullptr;
        }
        this->m_Device = nullptr;
        this->m_hWnd = nullptr;
    }
public:
    /**
    @brief テクスチャデータへのアクセス
    @return データへのアクセスポインタ
    */
    DX9_TextureLoader * GetTexture(void) const {
        return this->m_texture;
    }
protected:
    LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer; // バッファ
    LPDIRECT3DINDEXBUFFER9 m_pIndexBuffer; // インデックスバッファ
    LPDIRECT3DDEVICE9 m_Device; // デバイス
    HWND m_hWnd; // ウィンドウハンドル
    DX9_TextureLoader * m_texture; // テクスチャの格納
};

_MSLIB_END