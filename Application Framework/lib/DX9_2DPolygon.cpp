//==========================================================================
// 2Dポリゴン[DX9_2DPolygon.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_2DPolygon.h"

_MSLIB_BEGIN

DX9_2DPolygon::DX9_2DPolygon(LPDIRECT3DDEVICE9 pDevice, HWND hWnd, float scale)
{
    this->m_texture = new DX9_TextureLoader(pDevice, hWnd);
    this->m_Device = pDevice;
    this->m_hWnd = hWnd;
    this->m_scale = scale;
    this->m_pVertexBuffer = nullptr;
    this->m_Lock = false;
}

DX9_2DPolygon::~DX9_2DPolygon()
{
    this->Release();
}

//==========================================================================
/**
@brief 初期化
@param Input [in] 使用するテクスチャのパス ダブルポインタに対応
@param NumData [in] データ数
@param AutoTextureResize [in] テクスチャのサイズ自動調節機能 デフォルトは無効/有効化は true
@return 初期化失敗時 true
*/
bool DX9_2DPolygon::Init(const char ** Input, int NumData, bool AutoTextureResize)
{
	for (int i = 0; i < NumData; i++)
	{
		if (this->Init(Input[i], AutoTextureResize))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
/**
@brief 初期化
@param Input [in] 使用するテクスチャのパス
@param AutoTextureResize [in] テクスチャのサイズ自動調節機能 デフォルトは無効/有効化は true
@return 初期化失敗時 true
*/
bool DX9_2DPolygon::Init(const char * Input, bool AutoTextureResize)
{
	// テクスチャの格納
	if (this->m_texture->init(Input))
	{
		return true;
	}

	// バッファが登録されていないとき
	if (!this->m_Lock)
	{
		if (this->CreateVertexBuffer(this->m_Device, this->m_hWnd, sizeof(VERTEX_3) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_3, D3DPOOL_MANAGED, &this->m_pVertexBuffer, nullptr))
		{
			return true;
		}
		this->m_Lock = true;
	}

    // 画像サイズ自動調節
    if (AutoTextureResize)
    {
        CTexvec<int> vtex = CTexvec<int>(0, 0, 0, 0);
        auto * ptex = this->m_texture->get(this->m_texture->size() - 1);
        auto itr = ptex->GetTextureList()->begin();
        itr->resetsize();
        vtex = CTexvec<int>(0, 0, (int)((itr->getsize()->w*this->m_scale) + 0.5f), (int)((itr->getsize()->h*this->m_scale) + 0.5f));
        itr->setsize(vtex);
    }

	return false;
}

//==========================================================================
/**
@brief 初期化
@return 初期化失敗時 true(基本成功)
*/
bool DX9_2DPolygon::Init(void)
{
    return this->Init(nullptr);
}

//==========================================================================
/**
@brief 描画リソースの更新
*/
void DX9_2DPolygon::Update(void)
{
    if (this->m_texture->size() != 0 && this->m_ObjectData.size() != 0)
    {
        // 登録済みオブジェクトのデータの更新
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            auto *pTexList = this->m_texture->get(itr->m_obj->GetIndex());
            if (pTexList != nullptr)
            {
                // バッファのロック
                itr->m_obj->CreateVertexAngle(itr->vertex_3, *pTexList->GetTextureList()->begin()->getsize());
                itr->m_obj->CreateVertex(itr->vertex_3, *pTexList->GetTextureList()->begin()->getsize());
                itr->m_obj->SetVertexPath(0, &itr->vertex_3[0]);
                itr->m_obj->SetVertexPath(1, &itr->vertex_3[1]);
                itr->m_obj->SetVertexPath(2, &itr->vertex_3[2]);
                itr->m_obj->SetVertexPath(3, &itr->vertex_3[3]);
            }
        }
    }
}

//==========================================================================
/**
@brief リソースの解放
*/
void DX9_2DPolygon::Release(void)
{
    // バッファ解放
    if (this->m_pVertexBuffer != nullptr)
    {
        this->m_pVertexBuffer->Release();
        this->m_pVertexBuffer = nullptr;
    }

    // テクスチャの解放
    if (this->m_texture != nullptr)
    {
        this->m_texture->Release();
        delete this->m_texture;
        this->m_texture = nullptr;
    }
    this->m_Lock = false;
    this->ObjectRelease();
}

//==========================================================================
/**
@brief 描画
@param ADD [in] 加算合成の有効/無効 デフォルトは無効/有効は true
*/
void DX9_2DPolygon::Draw(bool ADD)
{
    if (this->m_texture->size() != 0 && this->m_ObjectData.size() != 0)
    {
        // FVFの設定
        this->m_Device->SetFVF(this->FVF_VERTEX_3);

        // 加算合成
        if (ADD == true)
        {
            this->SetRenderZWRITEENABLE_START(this->m_Device);
            this->SetRenderSUB(this->m_Device);
        }

        // バッファのセット
        VERTEX_3* pPseudo = nullptr;

        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            // テクスチャの取得
            auto * pTexList = this->m_texture->get(itr->m_obj->GetIndex());

            if (pTexList != nullptr)
            {
                this->m_pVertexBuffer->Lock(0, sizeof(VERTEX_3) * 4, (void**)&pPseudo, D3DLOCK_DISCARD);
                pPseudo[0].pos = itr->vertex_3[0].pos;
                pPseudo[1].pos = itr->vertex_3[1].pos;
                pPseudo[2].pos = itr->vertex_3[2].pos;
                pPseudo[3].pos = itr->vertex_3[3].pos;

                pPseudo[0].color = itr->vertex_3[0].color;
                pPseudo[1].color = itr->vertex_3[1].color;
                pPseudo[2].color = itr->vertex_3[2].color;
                pPseudo[3].color = itr->vertex_3[3].color;

                pPseudo[0].Tex = itr->vertex_3[0].Tex;
                pPseudo[1].Tex = itr->vertex_3[1].Tex;
                pPseudo[2].Tex = itr->vertex_3[2].Tex;
                pPseudo[3].Tex = itr->vertex_3[3].Tex;
                this->m_pVertexBuffer->Unlock(); // ロック解除

                this->m_Device->SetStreamSource(0, this->m_pVertexBuffer, 0, sizeof(VERTEX_3));

                pTexList->DrawBegin();
                this->m_Device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
                pTexList->DrawEnd();
            }
        }

        if (ADD == true)
        {
            this->SetRenderZWRITEENABLE_END(this->m_Device);
            this->SetRenderADD(this->m_Device);
        }
    }
}

//==========================================================================
/**
@brief テクスチャの情報の取得
@param index [in] テクスチャ番号
@return 成功時にテクスチャの情報が返ります/失敗時は nullptr が返ります
*/
CTexvec<int>* DX9_2DPolygon::GetTexSize(int index)
{
    auto *pTexList = this->m_texture->get(index);
    if (pTexList != nullptr)
    {
        if (pTexList->Size() != 0)
        {
            return pTexList->GetTextureList()->begin()->getsize();
        }
    }
    return nullptr;
}

//==========================================================================
/**
@brief テクスチャの枚数取得
@return 管理しているテクスチャ数が返ります
*/
int DX9_2DPolygon::GetNumTex(void)
{
    return this->m_texture->size();
}

//==========================================================================
/**
@brief テクスチャのサイズ指定
@param index [in] テクスチャの番号
@param Widht [in] テクスチャの幅
@param Height [in] テクスチャの高さ
*/
void DX9_2DPolygon::SetTexSize(int index, int Widht, int Height)
{
    auto *size = GetTexSize(index);
    if (size != nullptr)
    {
        *size = (CTexvec<int>(0, 0, Widht, Height));
    }
}

//==========================================================================
/**
@brief テクスチャのサイズ変更
@param index [in] テクスチャの番号
@param scale [in] テクスチャのサイズ変更に使う倍率
*/
void DX9_2DPolygon::SetTexScale(int index, float scale)
{
    auto *size = GetTexSize(index);
    if (scale != 1.0f&&size != nullptr)
    {
        *size = CTexvec<int>(0, 0, (int)((size->w*scale) + 0.5f), (int)((size->h*scale) + 0.5f));
    }
}

//==========================================================================
/**
@brief テクスチャのサイズリセット
@param index [in] テクスチャの番号
*/
void DX9_2DPolygon::ResetTexSize(int index)
{
    auto *pTexList = this->m_texture->get(index);
    if (pTexList != nullptr)
    {
        if (pTexList->Size() != 0)
        {
            pTexList->GetTextureList()->begin()->resetsize();
        }
    }
}

_MSLIB_END