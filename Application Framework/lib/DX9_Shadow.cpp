//==========================================================================
// 影[DX9_Shadow.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Shadow.h"

_MSLIB_BEGIN

//==========================================================================
// 定数定義
//==========================================================================
constexpr float VCRCT = 0.5f;
constexpr int DefaltColor = 80;
constexpr int ColorBuf = 8;

DX9_Shadow::DX9_Shadow(LPDIRECT3DDEVICE9 pDevice, HWND hWnd)
{
    this->m_texture = new DX9_TextureLoader(pDevice, hWnd);
    this->m_Device = pDevice;
    this->m_hWnd = hWnd;
    this->m_pIndexBuffer = nullptr;
    this->m_pVertexBuffer = nullptr;
    this->m_pPseudo = nullptr;
    this->m_Lock = false;
}

DX9_Shadow::~DX9_Shadow()
{
    this->Release();
}

//==========================================================================
/**
@brief 初期化
@param Input [in] 使用するテクスチャのパス
@return 失敗時に true が返ります
*/
bool DX9_Shadow::Init(const char * Input)
{
	CUv<float> uUV;
	WORD* pIndex;

	if (this->m_Lock == false)
	{
		if (this->m_texture->init(Input))
		{
			return true;
		}

		this->m_pIndexBuffer = nullptr;
		this->m_pVertexBuffer = nullptr;

		uUV = CUv<float>(0.0f, 0.0f, 1.0f, 1.0f);

		if (this->CreateVertexBuffer(this->m_Device, this->m_hWnd, sizeof(VERTEX_4) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &this->m_pVertexBuffer, nullptr))
		{
			return true;
		}

		if (this->CreateIndexBuffer(this->m_Device, this->m_hWnd, sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &this->m_pIndexBuffer, nullptr))
		{
			return true;
		}

		this->m_pVertexBuffer->Lock(0, 0, (void**)&this->m_pPseudo, D3DLOCK_DISCARD);
		this->VertexBuffer(this->m_pPseudo, &uUV);
		this->m_pVertexBuffer->Unlock(); // ロック解除

		this->m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
		this->RectangleIndex(pIndex, 1);
		this->m_pIndexBuffer->Unlock();	// ロック解除
	}

	return false;
}

//==========================================================================
/**
@brief 更新
*/
void DX9_Shadow::Update(void)
{
    // 登録済みオブジェクトのデータの更新
    for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
    {
        auto aObject = *itr->m_obj; // 複製
        const D3DXVECTOR3 *pos = aObject.GetMatInfoPos(); // 座標の取得

        // 距離から色を出す
        int nColor = DefaltColor - (int)((pos->y * ColorBuf) + 0.5f);

        // 回転行列初期化
        aObject.SetMatInfoRot({ 0, 0, 0 });

        // サイズの設定
        aObject.SetMatInfoSca({ pos->x*(pos->y + 1.0f) ,pos->y*(pos->y + 1.0f) ,pos->z*(pos->y + 1.0f) });

        // マイナスの値回避
        if (nColor <= 0) { nColor = 0; }
        if (pos->y <= 0) { nColor = 0; }

        // 色を登録
        itr->color = CColor<int>(255, 255, 255, nColor);

        // 各種行列の設定
        aObject.SetMatInfoPos({ pos->x,0.0f,pos->z });

        // 行列がデフォルトの指定の時
        if (aObject.GetMatrixType() == DX9_3DObjectMatrixType::Default)
        {
            aObject.SetMatrixType(DX9_3DObjectMatrixType::NotVector);
        }

        // 行列を生成
        aObject.CreateMtxWorld(itr->MtxWorld);
    }
}

//==========================================================================
/**
@brief 解放
*/
void DX9_Shadow::Release(void)
{
    // バッファ解放
    if (this->m_pVertexBuffer != nullptr)
    {
        this->m_pVertexBuffer->Release();
        this->m_pVertexBuffer = nullptr;
    }

    // インデックスバッファ解放
    if (this->m_pIndexBuffer != nullptr)
    {
        this->m_pIndexBuffer->Release();
        this->m_pIndexBuffer = nullptr;
    }

    // テクスチャの解放
    if (this->m_texture != nullptr)
    {
        this->m_texture->Release();
        delete this->m_texture;
        this->m_texture = nullptr;
    }

	this->m_Lock = false;

    this->ObjectRelease();
}

//==========================================================================
/**
@brief 描画
*/
void DX9_Shadow::Draw(void)
{
    if (this->m_texture->size() != 0 && this->m_ObjectData.size() != 0)
    {
        // パイプライン
        this->m_Device->SetStreamSource(0, this->m_pVertexBuffer, 0, sizeof(VERTEX_4));

        this->m_Device->SetIndices(this->m_pIndexBuffer);

        // FVFの設定
        this->m_Device->SetFVF(this->FVF_VERTEX_4);

        // バッファを開く
        this->m_pVertexBuffer->Lock(0, 0, (void**)&this->m_pPseudo, D3DLOCK_DISCARD);

        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            // テクスチャの取得
            auto * pTexList = this->m_texture->get(itr->m_obj->GetIndex());
            if (pTexList != nullptr)
            {
                this->m_pPseudo[0].color = itr->color.get();
                this->m_pPseudo[1].color = itr->color.get();
                this->m_pPseudo[2].color = itr->color.get();
                this->m_pPseudo[3].color = itr->color.get();

                // 各種行列の設定
                this->m_Device->SetTransform(D3DTS_WORLD, &itr->MtxWorld);

                pTexList->DrawBegin();
                this->m_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
                pTexList->DrawEnd();
            }
        }
        this->m_pVertexBuffer->Unlock(); // ロック解除
    }
}

//==========================================================================
/**
@brief 頂点バッファの生成
@param Output [in] バッファ
@param uv [in] UV
*/
void DX9_Shadow::VertexBuffer(VERTEX_4 * Output, CUv<float> * uv)
{
	VERTEX_4 vVertex[] =
	{
		// 上
		{ D3DXVECTOR3(-VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u0,uv->v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u1,uv->v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u1,uv->v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u0,uv->v1) }, // 左下
	};

	for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++)
	{
		Output[i] = vVertex[i];
	}
}

_MSLIB_END