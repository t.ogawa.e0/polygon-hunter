//==========================================================================
// テキスト表示[DX9_Text.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_Text
// Content: テキストクラス 
//
//==========================================================================
class DX9_Text
{
private:
    // コピー禁止 (C++11)
    DX9_Text(const DX9_Text &) = delete;
    DX9_Text &operator=(const DX9_Text &) = delete;
public:
    DX9_Text(LPDIRECT3DDEVICE9 pDevice);
    ~DX9_Text();

    /**
    @brief 初期化 フォントの種類はデフォルトで Terminal になってます。
    @param w [in] 画面の横幅
    @param h [in] 画面の高さ
    */
    void Init(int w, int h);

    /**
    @brief 初期化 フォントの種類はデフォルトで Terminal になってます。
    @param w [in] 画面の横幅
    @param h [in] 画面の高さ
    @param font [in] フォント
    */
	void Init(int w, int h, const char * font);

    /**
    @brief 解放
    */
	void Release(void);

    /**
    @brief 色
    @param r [in] 色
    @param g [in] 色
    @param b [in] 色
    @param a [in] 色
    */
    void color(int r, int g, int b, int a);

    /**
    @brief 色
    @param input [in] {r,g,b,a}
    */
    void color(const CColor<int> & input);

    /**
    @brief 座標
    @param x [in] X座標
    @param y [in] Y座標
    */
    void pos(int x, int y);

    /**
    @brief テキスト表示
    @param input [in] 表示内容
    */
	void text(const char* input, ...);

    /**
    @brief テキストの始まりに必ずセットしよう。
    */
    void textnew(void);

    /**
    @brief テキストの終わりに必ずセットしよう。
    */
    void textend(void);

    /**
    @brief 文字のサイズ
    @param input [in] フォントサイズ
    */
    void size(int input);
private:
    LPDIRECT3DDEVICE9 m_Device; // デバイス
	CColor<int> m_color; // 色
	CTexvec<int> m_pos; // 座標
	LPD3DXFONT m_font; // フォント
	int m_fontheight; // 文字の高さ
	int m_fontheight_master; // 文字の高さ
	bool m_Lock; // 描画のロック
};

_MSLIB_END