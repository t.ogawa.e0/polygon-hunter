//==========================================================================
// 画像の品質調整[DX9_SetSampler.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_SetSampler.h"

_MSLIB_BEGIN

DX9_SetSampler::DX9_SetSampler()
{
}

DX9_SetSampler::~DX9_SetSampler()
{
}

//==========================================================================
/**
@brief 初期値
@param pDevice [in] デバイス
*/
void DX9_SetSampler::SamplerFitteringNONE(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_NONE); // 小さくなった時に白枠
	pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_NONE); // 常に白枠
	pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE); // 元のサイズより小さい時綺麗にする
}

//==========================================================================
/**
@brief ぼや
@param pDevice [in] デバイス
*/
void DX9_SetSampler::SamplerFitteringLINEAR(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
	pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
	pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // 元のサイズより小さい時綺麗にする
}

//==========================================================================
/**
@brief グラフィカル
@param pDevice [in] デバイス
*/
void DX9_SetSampler::SamplerFitteringGraphical(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
	pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
	pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE); // 元のサイズより小さい時綺麗にする
}

_MSLIB_END