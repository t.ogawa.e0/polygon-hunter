//==========================================================================
// モデルデータ[DX9_Xmodel.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_3DObject.h"
#include "DX9_TextureLoader.h"
#include "DX9_Renderer.h"
#include "vector_wrapper.h"

_MSLIB_BEGIN

//==========================================================================
// マテリアルライトの設定
//==========================================================================
enum class DX9_XmodelMoad
{
    System, // システム設定
    Material // マテリアル設定
};

//==========================================================================
//
// class  : DX9_XmodelData
// Content: Xデータ管理 
//
//==========================================================================
class DX9_XmodelData
{
private:
    using Vertex_Element = vector_wrapper<D3DVERTEXELEMENT9>;
public:
    DX9_XmodelData();
    ~DX9_XmodelData();

    /**
    @brief tagのセット
    @param ptag [in] タグ
    */
    void settag(const char * ptag);

    /**
    @brief tagの取得
    */
    const char * gettag(void);

    /**
    @brief 複製データ
    @param pinp [in] Xデータ
    */
    void path(const DX9_XmodelData * pinp);

    /**
    @brief 読み込み
    @param mode [in] マテリアルライトの設定
    @param pDevice [in] デバイス
    @return Component Object Model defines, and macros
    */
    HRESULT load(DX9_XmodelMoad mode, LPDIRECT3DDEVICE9 pDevice, HWND hWnd);

    /**
    @brief 解放
    */
    void Release(void);

    /**
    @brief 描画
    @param pDevice [in] デバイス
    */
    void draw(LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief メッシュデータの取得
    @return メッシュの情報
    */
    LPD3DXMESH GetMesh(void);

    /**
    @brief マテリアル数の取得
    @return マテリアル数
    */
    const DWORD & GetNumMaterial(void);

    /**
    @brief 頂点構造体の情報を取得
    @return Xデータ
    */
    Vertex_Element * GetVertexElement(void);

    /**
    @brief テクスチャデータへのアクセス
    @return データへのアクセスポインタ
    */
    DX9_TextureLoader * GetTexture(void) const;
private:
    /**
    @brief 読み込み
    @param pAdjacency [in] ID3DXBuffer*
    @param pMaterialBuffer [in] ID3DXBuffer*
    @param pDevice [in] デバイス
    @return Component Object Model defines, and macros
    */
    HRESULT read(LPD3DXBUFFER * pAdjacency, LPD3DXBUFFER * pMaterialBuffer, LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief 最適化
    @param pAdjacency [in] ID3DXBuffer*
    @return Component Object Model defines, and macros
    */
    HRESULT optimisation(LPD3DXBUFFER pAdjacency);

    /**
    @brief 頂点の宣言
    @param pElements [in] D3DVERTEXELEMENT9
    @return Component Object Model defines, and macros
    */
    HRESULT declaration(D3DVERTEXELEMENT9 *pElements);

    /**
    @brief 複製
    @param pElements [in] D3DVERTEXELEMENT9
    @param pTempMesh [in] ID3DXMesh
    @param pDevice [in] デバイス
    @return Component Object Model defines, and macros
    */
    HRESULT replication(D3DVERTEXELEMENT9 *pElements, LPD3DXMESH * pTempMesh, LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief テクスチャの読み込み
    @param Input [in] マテリアル
    @return ANSI (Multi-byte Character) types
    */
    const char * loadtexture(const LPD3DXMATERIAL Input, LPDIRECT3DDEVICE9 pDevice, HWND hWnd);

    /**
    @brief ファイルパスの生成
    */
    void create_file_pass(void);
private:
    DX9_XmodelMoad m_MaterialMood; // マテリアルのモード
    LPD3DXMESH m_Mesh; // メッシュデータ
    DWORD m_NumMaterial; // マテリアル数
    DX9_TextureLoader *m_texture; // テクスチャの格納
    std::string m_strName; // タグ
    std::string m_strFilePass; // ファイルパス
    bool m_origin; // オリジナルデータ判定
    Vertex_Element m_Elements; // 頂点構造体
};

//==========================================================================
//
// class  : DX9_Xmodel
// Content: Xモデル 
//
//==========================================================================
class DX9_Xmodel : public DX9_Renderer<DX9_3DObject>
{
private:
    using XmodelData = vector_wrapper<DX9_XmodelData>;
private:
    // コピー禁止 (C++11)
    DX9_Xmodel(const DX9_Xmodel &) = delete;
    DX9_Xmodel &operator=(const DX9_Xmodel &) = delete;
public:
    DX9_Xmodel(LPDIRECT3DDEVICE9 pDevice, HWND hWnd);
    ~DX9_Xmodel();

    /**
    @brief 初期化
    @param Input [in] 使用するテクスチャのパス
    @param mode [in] マテリアルのライティングの設定
    @return 失敗時 true が返ります
    */
    bool Init(const char * Input, DX9_XmodelMoad mode = DX9_XmodelMoad::System);

    /**
    @brief 初期化
    @param Input [in] 使用するテクスチャのパス ダブルポインタに対応
    @param mode [in] マテリアルのライティングの設定
    @return 失敗時 true が返ります
    */
	bool Init(const char ** Input, int num, DX9_XmodelMoad mode = DX9_XmodelMoad::System);

    /**
    @brief 更新
    */
    void Update(void);

    /**
    @brief 解放
    */
	void Release(void);

    /**
    @brief 描画
    */
    void Draw(void);

    /**
    @brief モデルデータのパラメーターゲッター
    @param Input [in] 取得対象のオブジェクト
    @return 取得成功時に データが返ります。失敗時 nullptr が返ります。
    */
    DX9_XmodelData* GetMdelDataParam(DX9_3DObject & Input);

    /**
    @brief モデルデータのパラメーターゲッター
    @param id [in] モデルID
    @return 取得成功時に データが返ります。失敗時 nullptr が返ります。
    */
    DX9_XmodelData* GetMdelDataParam(int id);

    /**
    @brief リソース数の取得
    @return リソース数
    */
    int Size(void);
private:
    /**
    @brief 頂点構造体の情報を登録
    @param data [in] データ
    */
    void SetVertexElement(DX9_XmodelData * data);
private:
    XmodelData m_data; // モデルデータ管理(生データ)
    XmodelData m_data_path; // モデルデータ管理(複製データ)
};

_MSLIB_END