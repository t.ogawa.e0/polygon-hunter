//==========================================================================
// オブジェクト管理[DX9_Object.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <direct.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <XInput.h>
#include <XAudio2.h>
#include <chrono> // c++ 時間演算
#include <codecvt> // c++ 文字コード変換
#include <cstdlib> // c++ 数値変換
#include <locale> // c++ 文字の判定
#include <system_error> // c++ OSエラー
#include <vector> // c++ 動的構造体
#include <list> // c++ 双方向list
#include <map> // c++ 平衡二分木 ソート機能付き
#include <unordered_map> // c++ 平衡二分木 ソート機能無し
#include <iomanip> // c++ 時間
#include <string> // c++ char
#include <random> // c++ rand
#include <fstream> // c++ file
#include <type_traits> // c++ メタ関数
#include <algorithm> // c++ アルゴリズム
#define DIRECTINPUT_VERSION (0x0800) // DirectInputのバージョン指定
#include <dinput.h>
#include <tchar.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_SetSampler.h" // サンプラー
#include "DX9_SetRender.h" // レンダー
#include "DX9_2DObject.h" // 2Dオブジェクト
#include "DX9_3DObject.h" // 3Dオブジェクト
#include "DX9_3DObjectMotion.h" // 3Dオブジェクトモーション
#include "DX9_2DPolygon.h" // 2Dポリゴン
#include "DX9_Number.h" // 数字
#include "DX9_Billboard.h" // ビルボード
#include "DX9_Cube.h" // キューブ
#include "DX9_Mesh.h" // メッシュ
#include "DX9_Shadow.h" // 影
#include "DX9_Sphere.h" // 球体
#include "DX9_Xmodel.h" // Xモデル
#include "DX9_Grid.h" // グリッド
#include "DX9_Text.h" // テキスト
#include "DX9_MeshField.h" // メッシュフィールド
#include "DX9_ImGui.h" // ImGui
#include "DX9_Camera.h" // カメラ
#include "DX9_Collision.h" // 当たり判定
#include "DX9_Light.h" // ライト
#include "DX9_DeviceManager.h" // デバイスマネージャー
#include "DX9_TextureLoader.h"
#include "DX9_Fog.h" // フォグ
#include "vector_wrapper.h"
#include "list_wrapper.h"// リスト管理オブジェクト
#include "Timer.h" // タイマー
#include "enum_system.hpp" // enum演算子
#include "XInput.h" // 入力処理

_MSLIB_BEGIN
//#define _MSLIB_DebugKey_

// オブジェクトデバッグ用
class debug_object_vertex_3d 
{
public:
    debug_object_vertex_3d() {
        this->pos = D3DXVECTOR3(0, 0, 0);
        this->color = (DWORD)0;
    }
    ~debug_object_vertex_3d() {}
public:
    D3DXVECTOR3 pos; // 座標変換が必要
    D3DCOLOR color; // ポリゴンの色
};
// オブジェクトデバッグ用
class debug_object_vertex_2d 
{
public:
    debug_object_vertex_2d() {
        this->pos = D3DXVECTOR4(0, 0, 0, 0);
        this->color = (DWORD)0;
    }
    ~debug_object_vertex_2d() {}
public:
    D3DXVECTOR4 pos; // 座標変換が必要
    D3DCOLOR color; // ポリゴンの色
};

// CObjectID
enum class DX9_ObjectID {
    Default, // default
    Camera, // カメラ
    Grid, // グリッド
    Field, // フィールド
    Cube, // キューブ
    Mesh, // メッシュ
    Shadow, // 影
    Sphere, // 球体
    Xmodel, // Xモデル
    Billboard, // ビルボード
    Polygon2D, // 2D
    Text, // テキスト
    SpecialBlock, // オブジェクトに認識されない領域
    MAX, // Object数
};

//==========================================================================
//
// class  : DX9_Object
// Content: オブジェクト管理クラス
//
//==========================================================================
class DX9_Object : private DX9_SetSampler, public DX9_SetRender
{
private:
    enum class ProcessingList
    {
        Begin,
        Initializa,
        update,
        draw,
        limit_time_New,
        limit_time_Old,
        End,
    };
private:
    /**
    @brief コピー禁止 (C++11)
    */
    DX9_Object(const DX9_Object &) = delete;
    DX9_Object &operator=(const DX9_Object &) = delete;
protected:
    /**
    @brief コンストラクタ
    @param ObjectID [in] オブジェクトID
    */
    DX9_Object(DX9_ObjectID ObjectID = DX9_ObjectID::Default);

    /**
    @brief デストラクタ
    */
    virtual ~DX9_Object();

    /**
    @brief 継承初期化
    */
    virtual bool Init(void) = 0;

    /**
    @brief 継承解放
    */
    virtual void Uninit(void) = 0;

    /**
    @brief 継承更新
    */
    virtual void Update(void) = 0;

    /**
    @brief 継承描画
    */
    virtual void Draw(void) = 0;

    /**
    @brief 継承デバッグ
    */
    virtual void Debug(void) = 0;
public:
    /**
    @brief オブジェクトの取得
    @param ObjectID [in] オブジェクトID
    @param ObjectName [in] オブジェクト名
    @return 取得対象のオブジェクト
    */
    DX9_Object * GetObjects(const DX9_ObjectID ObjectID, const std::string & ObjectName);

    /**
    @brief 3Dオブジェクトインスタンス管理
    @return 3Dオブジェクトのインスタンス
    */
    vector_wrapper<DX9_3DObject>*_3DObject(void);

    /**
    @brief 2Dオブジェクトインスタンス管理
    @return 2Dオブジェクトのインスタンス
    */
    vector_wrapper<DX9_2DObject>*_2DObject(void);

    /**
    @brief カメラの取得
    @param ID [in] カメラID
    @return カメラインスタンス
    */
    DX9_Camera * GetCamera(int ID);

    /**
    @brief カメラへ追従情報を渡す
    @param ID [in] カメラID
    @param vpos [in] 追従座標
    */
    void SetCameraLockOn(int ID, const D3DXVECTOR3 & vpos);

    /**
    @brief オブジェクト名の取得
    @return オブジェクト名
    */
    const std::string * GetObjectName(void);

    /**
    @brief 特定オブジェクトの安全な破棄(外部アクセス用)(C3DObject or C2DObject)
    @param Input [in] 3Dオブジェクト
    @return オブジェクト名
    */
    void ObjectRelease(DX9_3DObject * Input);

    /**
    @brief 特定オブジェクトの安全な破棄(外部アクセス用)(C3DObject or C2DObject)
    @param Input [in] 2Dオブジェクト
    @return オブジェクト名
    */
    void ObjectRelease(DX9_2DObject * Input);

    /**
    @brief フィールド数
    @return 数
    */
    int FieldSize(void);

    /**
    @brief 指定フィールドとの当たり判定
    @param FieldID [in] フィールドID
    @param vpos [in] 現在の座標
    @param HitID [in] エリアID
    @return 判定座標
    */
    D3DXVECTOR3 FieldHeight(int FieldID, const D3DXVECTOR3 & vpos, int & HitID);
protected:
    /**
    @brief オブジェクト名のセット
    @param ObjectName [in] オブジェクト名の登録
    */
    void SetObjectName(const std::string & ObjectName);

    /**
    @brief 2Dポリゴン
    @return 2Dポリゴンのインスタンス
    */
    DX9_2DPolygon * _2DPolygon(void);

    /**
    @brief 数字
    @return 数字のインスタンス
    */
    DX9_Number * _2DNumber(void);

    /**
    @brief ビルボード
    @return ビルボードのインスタンス
    */
    DX9_Billboard *_3DBillboard(void);

    /**
    @brief キューブ
    @return キューブのインスタンス
    */
    DX9_Cube *_3DCube(void);

    /**
    @brief メッシュ
    @return メッシュのインスタンス
    */
    DX9_Mesh *_3DMesh(void);

    /**
    @brief フィールド
    @return フィールドのインスタンス
    */
    DX9_MeshField *_3DField(void);

    /**
    @brief 丸影
    @return 丸影のインスタンス
    */
    DX9_Shadow *_3DA_CircleShadow(void);

    /**
    @brief Xモデル
    @return Xモデルのインスタンス
    */
    DX9_Xmodel *_3DXmodel(void);

    /**
    @brief 球体
    @return 球体のインスタンス
    */
    DX9_Sphere *_3DSphere(void);

    /**
    @brief グリッド
    @return グリッドのインスタンス
    */
    DX9_Grid *_3DGrid(void);

    /**
    @brief テキスト
    @return テキストのインスタンス
    */
    DX9_Text *_2DText(void);

    /**
    @brief ImGui
    @return ImGuiのインスタンス
    */
    DX9_ImGui * ImGui(void);

    /**
    @brief XInput
    @return XInputのインスタンス
    */
    XInput *_XInput(void);

    /**
    @brief XAudio
    @return XAudioのインスタンス
    */
    XAudio2 *XAudio(void);

    /**
    @brief カメラ
    @return カメラのインスタンス
    */
    vector_wrapper<DX9_Camera> *Camera(void);

    /**
    @brief タイマー
    @return タイマーのインスタンス
    */
    vector_wrapper<Timer> *_Timer(void);

    /**
    @brief ライト
    @return ライトのインスタンス
    */
    DX9_Light * Light(void);

    /**
    @brief 単純な判定処理
    @return 単純な判定処理のインスタンス
    */
    DX9_Collision * Collision(void);

    /**
    @brief メルセンヌ・ツイスタの32ビット版
    @return 初期シード値の取得
    */
    std::mt19937 & GetMt19937(void);

    /**
    @brief 範囲の一様乱数管理関数(int)
    @return 範囲の一様乱数
    */
    vector_wrapper<rand_int> * _rand_int(void);

    /**
    @brief 範囲の一様乱数管理関数(float)
    @return 範囲の一様乱数
    */
    vector_wrapper<rand_float> * _rand_float(void);

    /**
    @brief 3Dオブジェクトモーション
    @return モーションインスタンス
    */
    vector_wrapper<DX9_3DObjectMotion> * _3DObjectMotion(void);

    /**
    @brief フォグ
    @return フォグイスタンス
    */
    DX9_Fog * _Fog(void);

    /**
    @brief デバッグON
    */
    void DebugON(void);

    /**
    @brief デバッグOFF
    */
    void DebugOFF(void);

    /**
    @brief デバッグオブジェクトの登録(毎フレーム必要)
    @param Input [in] オブジェクト
    */
    void Debug_3DObject(DX9_3DObject* Input);

    /**
    @brief オブジェクト内部データの読み取り
    @brief Object名が登録されていない場合処理が発生しません
    @brief ファイル名は自動設定されます
    */
    void ObjectLoad(void);

    /**
    @brief 現在の時間<型指定>
    @return 時間
    */
    template<typename _Ty>
    _Ty GetTimeSec(void);

    /**
    @brief ダイレクトインプットkeyboard
    @return 存在しない場合は nullptr が返ります
    */
    static DirectInputKeyboard * Dinput_Keyboard(void);

    /**
    @brief ダイレクトインプットマウス
    @return 存在しない場合は nullptr が返ります
    */
    static DirectInputMouse * Dinput_Mouse(void);

    /**
    @brief ダイレクトインプットコントローラー(PS4)
    @return 存在しない場合は nullptr が返ります
    */
    static  DirectInputController * Dinput_Controller(void);

    /**
    @brief ウィンドウサイズの取得
    @return ウィンドウサイズ[x.y]
    */
    static const CVector2<int> GetWinSize(void);

    /**
    @brief DirectX9デバイスの取得
    @return デバイス
    */
    static LPDIRECT3DDEVICE9 GetDirectX9Device(void);
public:
    /**
    @brief オブジェクトの生成 (new _Ty)
    @param p [in] 生成対象
    @return インスタンス
    */
    template <typename _Ty> static _Ty* NewObject(_Ty*& p);

    /**
    @brief オブジェクトの生成 (new _Ty(...))
    @param p [in] 生成対象
    @return インスタンス
    */
    template<typename _Ty, typename ... _Valty> static _Ty* NewObject(_Ty *& p, _Valty && ..._Val);

    /**
    @brief 登録済みオブジェクトの初期化
    */
    static bool InitAll(void);

    /**
    @brief 登録済みオブジェクトの解放
    */
    static void ReleaseAll(void);

    /**
    @brief 登録済みオブジェクトの更新
    */
    static void UpdateAll(void);

    /**
    @brief 登録済みオブジェクトの描画
    */
    static void DrawAll(void);

    /**
    @brief ファイル指定
    @param file_pass [in] ファイルパス
    */
    static void SetFilePass(const std::string file_pass);
private:
    /**
    @brief 描画の準備
    @param ObjectID [in] オブジェクトID
    */
    static void DrawPreparation(DX9_ObjectID ObjectID);

    /**
    @brief 描画の後処理
    @param ObjectID [in] オブジェクトID
    */
    static void DrawPostProcessing(DX9_ObjectID ObjectID);

    /**
    @brief デバッグの描画
    @param NumLine [in] 線の本数
    @param pMatrix [in] マトリックス
    @param pDevice [in] デバイス
    */
    static void DebugDraw3D(int NumLine, const D3DXMATRIX *pMatrix, LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief デバッグの描画
    @param SetPos [in] 座標
    @param SetTexSize [in] テクスチャのサイズ
    @param pDevice [in] デバイス
    */
    static void DebugDraw2D(const D3DXVECTOR4 *pos1, const D3DXVECTOR4 *pos2, const D3DXVECTOR4 *pos3, const D3DXVECTOR4 *pos4, LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief デバッグオブジェクト3D
    @param ObjectID [in] オブジェクトID
    */
    static void Debug3DObjectDraw(DX9_ObjectID ObjectID);

    /**
    @brief デバッグオブジェクト2D
    @param ObjectID [in] オブジェクトID
    */
    static void Debug2DObjectDraw(DX9_ObjectID ObjectID);

    /**
    @brief 全て書き込み
    */
    static void SaveAll(void);

    /**
    @brief 全て読み取り
    @param str_object [in] オブジェクト名
    */
    static void LoadAll(const std::string * str_object);

    /**
    @brief 機能
    */
    static void System(void);

    /**
    @brief 2Dオブジェクトのエディタ
    */
    void Editor2DObject(void);

    /**
    @brief 3Dオブジェクトのパラメーターチェック
    */
    void _3DObjectParameterDebug(void);

    /**
    @brief カメラオブジェクトのパラメーターチェック
    */
    void _CameraParameterDebug(void);

    /**
    @brief XModelのパラメーターチェック
    */
    void _XModelParameterDebug(void);

    /**
    @brief 2Dオブジェクトのパラメーターチェック
    */
    void _2DObjectParameterDebug(void);

    /**
    @brief XInputのパラメーターデバッグ
    */
    void _XInputParameterDebug(void);

    /**
    @brief 処理時間表示
    @param ProcessTime [in] 処理時間
    */
    void _ProcessingTime(float ProcessTime);

    /**
    @brief 処理時間計算
    */
    float _ProcessingTimeCount(void);

    /**
    @brief 登録されているリソースを表示する
    */
    void _ResourceDebug(void);

    /**
    @brief リソース登録
    @param pRenderer [in] テクスチャのロダ
    */
    void _InputResourceDebug(DX9_TextureLoader * pRenderer);

    /**
    @brief delete
    @param p [in] 破棄対象
    */
    template <typename _Ty> void Delete(_Ty*& p);

    /**
    @brief new
    @param p [in] インスタンス受け取り
    @return インスタンス
    */
    template <typename _Ty> _Ty* New(_Ty*& p);

    /**
    @brief new
    @param p [in] インスタンス受け取り
    @return インスタンス
    */
    template <typename _Ty, typename ... _Valty> _Ty* New(_Ty*& p, _Valty && ..._Val);

    /**
    @brief 描画オブジェクトへの検索
    */
    template <typename _Ty, typename _Oy> void ObjectDelete(_Ty *pt, _Oy *po);
private:
    static std::list<DX9_Object*> m_Object[(int)DX9_ObjectID::MAX]; // オブジェクト格納
    static std::string m_file_name; // file pass
private:
    vector_wrapper<DX9_3DObject>*m_3DObject; // 3Dオブジェクト座標インスタンス管理
    vector_wrapper<DX9_2DObject>*m_2DObject; // 2Dオブジェクト座標インスタンス管理
    vector_wrapper<DX9_Camera>*m_Camera; // カメラ座標インスタンス管理
    vector_wrapper<Timer>*m_Timer; // タイマー
    vector_wrapper<rand_int>*m_rand_int; // 範囲の一様乱数
    vector_wrapper<rand_float>*m_rand_float; // 範囲の一様乱数
    vector_wrapper<DX9_3DObjectMotion>*m_3DObjectMotion; // 3Dオブジェクトモーション
    DX9_2DPolygon *m_2DPolygon; // 2Dポリゴン
    DX9_Billboard *m_Billboard; // ビルボード
    DX9_Cube *m_Cube; // キューブ
    DX9_Mesh *m_Mesh; // メッシュ
    DX9_Shadow *m_A_CircleShadow; // 丸影
    DX9_Xmodel *m_Xmodel; // Xモデル
    DX9_Sphere *m_Sphere; // 球体
    DX9_Grid *m_Grid; // グリッド
    DX9_Text *m_Text; // テキスト
    DX9_Number *m_Number; // 数字
    DX9_MeshField * m_MeshField; // メッシュフィールド
    DX9_ImGui m_ImGui; // ImGui
    DX9_Collision m_Collision; // 当たり判定
    DX9_Fog * m_Fog; // フォグ
    DX9_Light *m_Light; // ライト
    XInput *m_XInput; // XInput
    XAudio2 *m_XAudio; // XAudio

    std::vector<float> m_processing_time; // 処理時間
    std::string *m_ObjectName; // オブジェクト名
    std::list<DX9_3DObject*> *m_debug_object3d; // デバッグオブジェクトの格納
    std::mt19937 *m_mt; // メルセンヌ・ツイスタの32ビット版、引数は初期シード値
    bool m_debug_key; // デバッグを有効にする
    bool m_initializer; // 初期化フラグ
};

//==========================================================================
/**
@brief 現在の時間<型指定>
@return 時間
*/
template<typename _Ty>
inline _Ty DX9_Object::GetTimeSec(void)
{
    return static_cast<_Ty>(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()) / (_Ty)1000000000;
}

//==========================================================================
/**
@brief オブジェクトの生成 (new _Ty)
@param p [in] 生成対象
@return インスタンス
*/
template<typename _Ty>
inline _Ty * DX9_Object::NewObject(_Ty*& p)
{
    p = nullptr;
    p = new _Ty;

    return p;
}

//==========================================================================
/**
@brief オブジェクトの生成 (new _Ty(...))
@param p [in] 生成対象
@return インスタンス
*/
template<typename _Ty, typename ..._Valty>
inline _Ty * DX9_Object::NewObject(_Ty *& p, _Valty && ..._Val)
{
    p = nullptr;
    p = new _Ty((_Val)...);

    return p;
}

//==========================================================================
/**
@brief new
@param p [in] インスタンス受け取り
@return インスタンス
*/
template<typename _Ty>
inline _Ty * DX9_Object::New(_Ty *& p)
{
    // メモリが未確保時
    if (p == nullptr) {
        p = new _Ty;
    }

    return p;
}

//==========================================================================
/**
@brief new
@param p [in] インスタンス受け取り
@return インスタンス
*/
template<typename _Ty, typename ..._Valty>
inline _Ty * DX9_Object::New(_Ty *& p, _Valty && ..._Val)
{
    // メモリが未確保時
    if (p == nullptr) {
        p = new _Ty((_Val)...);
    }

    return p;
}

//==========================================================================
/**
@brief 描画オブジェクトへの検索
*/
template<typename _Ty, typename _Oy>
inline void DX9_Object::ObjectDelete(_Ty *pt, _Oy *po)
{
    if (pt != nullptr&&po != nullptr) {
        pt->ObjectDelete(po);
    }
}

//==========================================================================
/**
@brief delete
@param p [in] 破棄対象
*/
template<typename _Ty>
inline void DX9_Object::Delete(_Ty *& p)
{
    if (p != nullptr) {
        delete p;
        p = nullptr;
    }
}

_MSLIB_END