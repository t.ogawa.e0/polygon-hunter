//==========================================================================
// キューブ[DX9_Cube.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"
#include "DX9_Renderer.h"
#include "DX9_3DObject.h"

_MSLIB_BEGIN

// テクスチャ情報格納
class DX9_CubeTexture
{
public:
    DX9_CubeTexture();
    ~DX9_CubeTexture();
public:
    int Direction1; // 一行の画像数
    int Direction2; // 一列の画像数
    int Pattern; // 面の数
    CTexvec<float> texsize; // テクスチャのサイズ
    CTexvec<float> texcutsize; // テクスチャの切り取りサイズ
};

//==========================================================================
//
// class  : DX9_Cube
// Content: キューブ 
//
//==========================================================================
class DX9_Cube : public DX9_Renderer<DX9_3DObject>
{
private:
    // コピー禁止 (C++11)
    DX9_Cube(const DX9_Cube &) = delete;
    DX9_Cube &operator=(const DX9_Cube &) = delete;
private:
public:
    DX9_Cube(LPDIRECT3DDEVICE9 pDevice, HWND hWnd);
    ~DX9_Cube();

    /**
    @brief 初期化
    @param Input [in] 使用するテクスチャのパス ダブルポインタに対応
    @param Input [in] 要素数
    @return 失敗時 true
    */
	bool Init(const char** Input, int size);

    /**
    @brief 初期化
    @param Input [in] 使用するテクスチャのパス
    @param Input [in] 要素数
    @return 失敗時 true
    */
	bool Init(const char* Input);

    /**
    @brief 更新
    */
    void Update(void);

    /**
    @brief 解放
    */
	void Release(void);

    /**
    @brief 描画
    */
    void Draw(void);
private:
    /**
    @brief キューブ生成
    @param Output [out] 頂点情報
    @param _this [in] UV座標
    */
	void SetCube(VERTEX_4 * Output, const CUv<float> * _this);

    /**
    @brief UV生成
    @param Input [in] 面情報
    @param Output [in] UV情報
    @param nNum [in] 現在の面
    */
	void SetUV(const DX9_CubeTexture * Input, CUv<float> * Output, const int nNum);
private: // 格納
	bool m_Lock; // バッファロック
};

_MSLIB_END