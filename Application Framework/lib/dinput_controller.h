//==========================================================================
// コントローラー[dinput_controller.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "dinput_manager.h"

_MSLIB_BEGIN

// 方向キー
enum class DirectInputControllerDirectionKey
{
    LeftButtonUp,
    LeftButtonUpRight,
    LeftButtonRight,
    LeftButtonUnderRight,
    LeftButtonUnder,
    LeftButtonUnderLeft,
    LeftButtonLeft,
    LeftButtonUpLeft,
    MAX
};

// ボタン
enum class DirectInputControllerButton
{
    RightButton1,	// □ シカク
    RightButton2,	// Ｘ バツ
    RightButton3,	// 〇 マル
    RightButton4,	// △ サンカク
    L1Button,		// L1
    R1Button,		// R1
    L2Button,		// L2
    R2Button,		// R2
    SHAREButton,	// SHARE
    SOPTIONSButton,	// OPTIONS
    L3Button,		// L3
    R3Button,		// R3
    PSButton,		// PS
    TouchPad,		// TouchPad
    MAX
};

//==========================================================================
//
// class  : CPS4
// Content: PS4コントローラー
//
//==========================================================================
class DirectInputControllerStick
{
public:
    DirectInputControllerStick();
    ~DirectInputControllerStick();
public:
    LONG m_LeftRight;
    LONG m_UpUnder;
};

//==========================================================================
//
// class  : DirectInputController
// Content: コントローラー
//
//==========================================================================
class DirectInputController : public DirectInput, public DirectInputManager
{
public:

private:
    // コピー禁止 (C++11)
    DirectInputController(const DirectInputController &) = delete;
    DirectInputController &operator=(const DirectInputController &) = delete;
public:
    DirectInputController();
    ~DirectInputController();

    /**
    @brief 初期化
    @param hInstance [in] インスタンスハンドル
    @param hWnd [in] ウィンドウハンドル
    @return 失敗時に true が返ります
    */
    bool Init(HINSTANCE hInstance, HWND hWnd);

    /**
    @brief 更新
    */
    void Update(void)override;

    /**
    @brief 左スティックの状態取得
    @return ステックの情報
    */
    DirectInputControllerStick LeftStick(void);

    /**
    @brief 右スティックの状態取得
    @return ステックの情報
    */
    DirectInputControllerStick RightStick(void);

    /**
    @brief L2ボタンの状態取得
    @return L2ボタンの情報
    */
    LONG L2(void);

    /**
    @brief R2ボタンの状態取得
    @return R2ボタンの情報
    */
    LONG R2(void);

    /**
    @brief PS4 方向キー
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool DirectionKey(DirectInputControllerDirectionKey key);

    /**
    @brief PS4 方向キー プレス
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Press(DirectInputControllerDirectionKey key);

    /**
    @brief PS4 方向キー トリガー
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Trigger(DirectInputControllerDirectionKey key);

    /**
    @brief PS4 方向キー リピート
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Repeat(DirectInputControllerDirectionKey key);

    /**
    @brief PS4 方向キー リリ−ス
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Release(DirectInputControllerDirectionKey key);

    /**
    @brief PS4 ボタン プレス
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Press(DirectInputControllerButton key);

    /**
    @brief PS4 ボタン トリガー
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Trigger(DirectInputControllerButton key);

    /**
    @brief PS4 ボタン リピート
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Repeat(DirectInputControllerButton key);

    /**
    @brief PS4 ボタン リリ−ス
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Release(DirectInputControllerButton key);
private:
    /**
    @brief PS4 入力誤差修正
    @param Set [in] 入力
    @return 値が返ります
    */
    LONG Adjustment(LONG Set);

    /**
    @brief PS4 ステック入力制御
    @param Stick1 [in] 入力
    @param Stick2 [in] 入力
    @return 値が返ります
    */
    DirectInputControllerStick Stick(LONG Stick1, LONG Stick2);
private:
    static BOOL CALLBACK EnumJoysticksCallback(const DIDEVICEINSTANCE *pdidInstance, void *pContext);
    static BOOL CALLBACK EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, void *pContext);
private:
    DIJOYSTATE m_State; // 入力情報ワーク
    BYTE m_StateTrigger[(int)sizeof(DIJOYSTATE::rgbButtons)]; // トリガー情報ワーク
    BYTE m_StateRelease[(int)sizeof(DIJOYSTATE::rgbButtons)]; // リリース情報ワーク
    BYTE m_StateRepeat[(int)sizeof(DIJOYSTATE::rgbButtons)]; // リピート情報ワーク
    int m_StateRepeatCnt[(int)sizeof(DIJOYSTATE::rgbButtons)]; // リピートカウンタ

    BYTE m_POVState[(int)DirectInputControllerDirectionKey::MAX]; // POV入力情報ワーク
    BYTE m_POVTrigger[(int)DirectInputControllerDirectionKey::MAX]; // トリガー情報ワーク
    BYTE m_POVRelease[(int)DirectInputControllerDirectionKey::MAX]; // リリース情報ワーク
    BYTE m_POVRepeat[(int)DirectInputControllerDirectionKey::MAX]; // リピート情報ワーク
    int m_POVRepeatCnt[(int)DirectInputControllerDirectionKey::MAX]; // リピートカウンタ
};

_MSLIB_END