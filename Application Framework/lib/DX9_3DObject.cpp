//==========================================================================
// オブジェクト[DX9_3DObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_3DObject.h"

_MSLIB_BEGIN

DX9_3DObject::DX9_3DObject()
{
    this->m_MatType = DX9_3DObjectMatrixType::Default;
    this->m_MtxView = nullptr;
    this->Init(0);
    this->m_AnimationCount = -1;
}

DX9_3DObject::~DX9_3DObject()
{
}

//==========================================================================
/**
@brief 初期化
@param index [in] 使用するリソース番号
*/
void DX9_3DObject::Init(int index)
{
	this->m_index = index;

	this->Info.pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	this->Info.rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	this->Info.sca = D3DXVECTOR3(1.0f, 1.0f, 1.0f);

	this->Look.Eye = D3DXVECTOR3(0.0f, 0.0f, -2.0f); // 視点
	this->Look.At = D3DXVECTOR3(0.0f, 0.0f, 0.0f); // 座標
	this->Look.Up = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // ベクター

	this->Vec.Up = D3DXVECTOR3(0, 1, 0); // 上ベクトル
	this->Vec.Front = D3DXVECTOR3(0, 0, 1); // 前ベクトル
	this->Vec.Right = D3DXVECTOR3(1, 0, 0); //  右ベクトル

	// ベクトルの正規化
	D3DXVec3Normalize(&this->Vec.Front, &this->Vec.Front);
	D3DXVec3Normalize(&this->Vec.Up, &this->Vec.Up);
	D3DXVec3Normalize(&this->Vec.Right, &this->Vec.Right);
}

//==========================================================================
/**
@brief X軸回転
@param value [in] 入力した値が加算されます
*/
void DX9_3DObject::RotX(float value)
{
	D3DXMATRIX RotX; //回転行列
	D3DXVECTOR3 Direction;

	D3DXMatrixRotationY(&RotX, value); // 回転
    D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
	Direction = this->Look.Eye - this->Look.At; // 向きベクトル	
	D3DXVec3TransformNormal(&Direction, &Direction, &RotX);
	D3DXVec3TransformNormal(&this->Vec.Front, &this->Vec.Front, &RotX);
	D3DXVec3TransformNormal(&this->Vec.Right, &this->Vec.Right, &RotX);
    D3DXVec3TransformNormal(&this->Vec.Up, &this->Vec.Up, &RotX);
	this->Look.Eye = this->Look.At + Direction;
}

//==========================================================================
/**
@brief X軸回転
@param rot [in] 回したい方向
@param power [in] 回転にかけるパワー
@return 回転した角度
*/
float DX9_3DObject::RotX(D3DXVECTOR3 & rot, float power)
{
	D3DXVec3Normalize(&rot, &rot);
	D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
	// 角度を求める
	float fVec3Dot = atanf(D3DXVec3Dot(&this->Vec.Right, &rot));
	this->RotX(fVec3Dot*power);

    return fVec3Dot*power;
}

//==========================================================================
/**
@brief Y軸回転
@param value [in] 入力した値が加算されます
*/
void DX9_3DObject::RotY(float value)
{
	//D3DXMATRIX RotY; //回転行列
	//D3DXVECTOR3 Direction;

	//if (this->Restriction(RotY, &value))
	//{
	//	D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
	//	D3DXMatrixRotationAxis(&RotY, &this->Vec.Right, value); // 回転
	//	Direction = this->Look.Eye - this->Look.At; // 向きベクトル
	//	D3DXVec3TransformNormal(&Direction, &Direction, &RotY);
	//	D3DXVec3TransformNormal(&this->Vec.Front, &this->Vec.Front, &RotY);
	//	D3DXVec3TransformNormal(&this->Vec.Right, &this->Vec.Right, &RotY);
	//	this->Look.Eye = this->Look.At + Direction;
	//}

    D3DXMATRIX RotY; //回転行列
    D3DXVECTOR3 Direction1;
    D3DXVECTOR3 Direction2;

    D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
    D3DXMatrixRotationAxis(&RotY, &this->Vec.Right, value); // 回転
    Direction1 = this->Look.Eye - this->Look.At; // 向きベクトル
    Direction2 = this->Look.Up - (this->Look.Eye - this->Look.At); // 向きベクトル
    D3DXVec3TransformNormal(&Direction1, &Direction1, &RotY);
    D3DXVec3TransformNormal(&Direction2, &Direction2, &RotY);
    D3DXVec3TransformNormal(&this->Vec.Front, &this->Vec.Front, &RotY);
    D3DXVec3TransformNormal(&this->Vec.Right, &this->Vec.Right, &RotY);
    D3DXVec3TransformNormal(&this->Vec.Up, &this->Vec.Up, &RotY);
    this->Look.Up = (this->Look.Eye - this->Look.At) + Direction2 + Direction2;
    this->Look.Eye = this->Look.At + Direction1;
}

//==========================================================================
/**
@brief Y軸回転(制限あり)
@param value [in] 入力した値が加算されます
*/
void DX9_3DObject::RotYLock(float value)
{
    D3DXMATRIX RotY; //回転行列
    D3DXVECTOR3 Direction;

    if (this->Restriction(RotY, &value))
    {
    	D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
    	D3DXMatrixRotationAxis(&RotY, &this->Vec.Right, value); // 回転
    	Direction = this->Look.Eye - this->Look.At; // 向きベクトル
    	D3DXVec3TransformNormal(&Direction, &Direction, &RotY);
    	D3DXVec3TransformNormal(&this->Vec.Front, &this->Vec.Front, &RotY);
    	D3DXVec3TransformNormal(&this->Vec.Right, &this->Vec.Right, &RotY);
    	this->Look.Eye = this->Look.At + Direction;
    }
}

//==========================================================================
/**
@brief Y軸回転
@param rot [in] 回したい方向
@param power [in] 回転にかけるパワー
@return 回転した角度
*/
float DX9_3DObject::RotY(D3DXVECTOR3 & rot, float power)
{
    D3DXVec3Normalize(&rot, &rot);
    D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
                                                                      // 角度を求める
    float fVec3Dot = atanf(D3DXVec3Dot(&this->Vec.Right, &rot));
    this->RotY(fVec3Dot*power);

    return fVec3Dot * power;
}

//==========================================================================
/**
@brief Z軸回転
@param value [in] 入力した値が加算されます
*/
void DX9_3DObject::RotZ(float value)
{
    D3DXMATRIX RotZ; //回転行列
    D3DXVECTOR3 Direction;

    D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
    D3DXMatrixRotationAxis(&RotZ, &this->Vec.Front, value); // 回転
    Direction = this->Look.Up - (this->Look.Eye - this->Look.At); // 向きベクトル
    D3DXVec3TransformNormal(&Direction, &Direction, &RotZ);
    D3DXVec3TransformNormal(&this->Vec.Front, &this->Vec.Front, &RotZ);
    D3DXVec3TransformNormal(&this->Vec.Right, &this->Vec.Right, &RotZ);
    D3DXVec3TransformNormal(&this->Vec.Up, &this->Vec.Up, &RotZ);
    this->Look.Up = (this->Look.Eye - this->Look.At) + Direction;
}

//==========================================================================
/**
@brief Z軸回転
@param rot [in] 回したい方向
@param power [in] 回転にかけるパワー
@return 回転した角度
*/
float DX9_3DObject::RotZ(D3DXVECTOR3 & rot, float power)
{
    D3DXVec3Normalize(&rot, &rot);
    D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
                                                                      // 角度を求める
    float fVec3Dot = atanf(D3DXVec3Dot(&this->Vec.Front, &rot));
    this->RotZ(fVec3Dot*power);

    return fVec3Dot * power;
}

//==========================================================================
/**
@brief Z軸移動
@param value [in] 入力した値が加算されます
*/
void DX9_3DObject::MoveZ(float value)
{
	D3DXVec3Normalize(&this->Vec.Front, &this->Vec.Front);
	this->Info.pos += this->Vec.Front*value;
}

//==========================================================================
/**
@brief X軸移動
@param value [in] 入力した値が加算されます
*/
void DX9_3DObject::MoveX(float value)
{
	D3DXVec3Normalize(&this->Vec.Right, &this->Vec.Right);
	this->Info.pos += this->Vec.Right*value;
}

//==========================================================================
/**
@brief Y軸移動
@param value [in] 入力した値が加算されます
*/
void DX9_3DObject::MoveY(float value)
{
	D3DXVec3Normalize(&this->Vec.Up, &this->Vec.Up);
	this->Info.pos += this->Vec.Up*value;
}

//==========================================================================
/**
@brief スケール
@param value [in] 入力した値が加算されます
*/
void DX9_3DObject::ScalePlus(float value)
{
	this->Info.sca += D3DXVECTOR3(value, value, value);
}

//==========================================================================
/**
@brief スケール
@param value [in] 入力した値が加算されます
*/
void DX9_3DObject::ScalePlus(const D3DXVECTOR3 & value)
{
    this->Info.sca += value;
}

//==========================================================================
/**
@brief スケール
@param value [in] 入力した値入ります
*/
void DX9_3DObject::ScaleSet(float value)
{
    this->Info.sca = D3DXVECTOR3(value, value, value);
}

//==========================================================================
/**
@brief スケール
@param value [in] 入力した値入ります
*/
void DX9_3DObject::ScaleSet(const D3DXVECTOR3 & value)
{
    this->Info.sca = value;
}

//==========================================================================
/**
@brief ワールド行列の生成
@param InOut [in/out] ワールド行列を受け取る場所です
@return 成功時にワールド行列が返ります/失敗時に nullptr が返ります
*/
const D3DXMATRIX * DX9_3DObject::CreateMtxWorld(D3DXMATRIX & InOut)
{
	switch (this->m_MatType)
	{
	case DX9_3DObjectMatrixType::Default:
		return this->CreateMtxWorld1(InOut);
		break;
	case DX9_3DObjectMatrixType::Vector1:
        return this->CreateMtxWorld1(InOut);
		break;
    case DX9_3DObjectMatrixType::NotVector:
        return this->CreateMtxWorld2(InOut);
        break;
	case DX9_3DObjectMatrixType::MirrorVector:
		if (this->m_MtxView != nullptr)
		{
			return this->CreateMtxWorld5(InOut, *this->m_MtxView);
		}
		break;
	case DX9_3DObjectMatrixType::MirrorNotVector:
		if (this->m_MtxView != nullptr)
		{
			return this->CreateMtxWorld4(InOut, *this->m_MtxView);
		}
		break;
    case DX9_3DObjectMatrixType::Quaternion:
        return this->CreateMtxWorld6(InOut);
        break;
	default:
		return nullptr;
		break;
	}

	this->m_MtxView = nullptr;
	return nullptr;
}

//==========================================================================
/**
@brief ベクトルを使用した行列の生成
@param InOut [in/out] ワールド行列を受け取る場所です
@return ワールド行列が返ります
*/
const D3DXMATRIX * DX9_3DObject::CreateMtxWorld1(D3DXMATRIX & InOut)
{
	D3DXMATRIX aMtxRot;
	D3DXMATRIX aMtxTra;
	D3DXMATRIX aMtxSca;

	// 平行
	D3DXMatrixTranslation(&aMtxTra, this->Info.pos.x, this->Info.pos.y, this->Info.pos.z);

	// スケール
	D3DXMatrixScaling(&aMtxSca, this->Info.sca.x, this->Info.sca.y, this->Info.sca.z);

	// 回転
	this->CalcLookAtMatrix(aMtxRot);

	D3DXMatrixIdentity(&InOut);

	// 平行の合成
	D3DXMatrixMultiply(&InOut, &aMtxTra, &InOut);

	// スケールの合成
	D3DXMatrixMultiply(&InOut, &aMtxSca, &InOut);

	// 回転の合成
	D3DXMatrixMultiply(&InOut, &aMtxRot, &InOut);

	return &InOut;
}

//==========================================================================
/**
@brief ベクトルを使用しない行列の生成
@param InOut [in/out] ワールド行列を受け取る場所です
@return ワールド行列が返ります
*/
const D3DXMATRIX * DX9_3DObject::CreateMtxWorld2(D3DXMATRIX & InOut)
{
	D3DXMATRIX aMtxRot;
	D3DXMATRIX aMtxTra;
	D3DXMATRIX aMtxSca;

	// 平行
	D3DXMatrixTranslation(&aMtxTra, this->Info.pos.x, this->Info.pos.y, this->Info.pos.z);

	// スケール
	D3DXMatrixScaling(&aMtxSca, this->Info.sca.x, this->Info.sca.y, this->Info.sca.z);

	// 回転
	D3DXMatrixRotationYawPitchRoll(&aMtxRot, this->Info.rot.y, this->Info.rot.x, this->Info.rot.z);

	D3DXMatrixIdentity(&InOut);

	// 平行の合成
	D3DXMatrixMultiply(&InOut, &aMtxTra, &InOut);

	// スケールの合成
	D3DXMatrixMultiply(&InOut, &aMtxSca, &InOut);

	// 回転の合成
	D3DXMatrixMultiply(&InOut, &aMtxRot, &InOut);

	return &InOut;
}

//==========================================================================
/**
@brief ベクトルを使用したカメラの方向を向かせる行列
@param InOut [in/out] ワールド行列を受け取る場所です
@return ワールド行列が返ります
*/
const D3DXMATRIX * DX9_3DObject::CreateMtxWorld4(D3DXMATRIX & InOut, D3DXMATRIX & pMtxView)
{
	D3DXMATRIX MtxView = pMtxView;
	this->CreateMtxWorld1(InOut);

	// 各種行列の設定
	D3DXMatrixTranspose(&MtxView, &MtxView);
	MtxView._14 = 0.0f;
	MtxView._24 = 0.0f;
	MtxView._34 = 0.0f;

	D3DXMatrixMultiply(&InOut, &MtxView, &InOut); // 行列の合成

	return &InOut;
}

//==========================================================================
/**
@brief 直接値を入れるカメラの方向を向かせる行列
@param InOut [in/out] ワールド行列を受け取る場所です
@return ワールド行列が返ります
*/
const D3DXMATRIX * DX9_3DObject::CreateMtxWorld5(D3DXMATRIX & InOut, D3DXMATRIX & pMtxView)
{
	D3DXMATRIX MtxView = pMtxView;
	this->CreateMtxWorld2(InOut);

	// 各種行列の設定
	D3DXMatrixTranspose(&MtxView, &MtxView);
	MtxView._14 = 0.0f;
	MtxView._24 = 0.0f;
	MtxView._34 = 0.0f;

	D3DXMatrixMultiply(&InOut, &MtxView, &InOut); // 行列の合成

	return &InOut;
}

//==========================================================================
/**
@brief クオータニオンを利用した行列
@param InOut [in/out] ワールド行列を受け取る場所です
@return ワールド行列が返ります
*/
const D3DXMATRIX * DX9_3DObject::CreateMtxWorld6(D3DXMATRIX & InOut)
{
    D3DXMATRIX aMtxRot;
    D3DXMATRIX aMtxTra;
    D3DXMATRIX aMtxSca;

    // 平行
    D3DXMatrixTranslation(&aMtxTra, this->Info.pos.x, this->Info.pos.y, this->Info.pos.z);

    // スケール
    D3DXMatrixScaling(&aMtxSca, this->Info.sca.x, this->Info.sca.y, this->Info.sca.z);

    // 回転
    this->QuaternionMatrix(&aMtxRot);

    D3DXMatrixIdentity(&InOut);

    // 平行の合成
    D3DXMatrixMultiply(&InOut, &aMtxTra, &InOut);

    // スケールの合成
    D3DXMatrixMultiply(&InOut, &aMtxSca, &InOut);

    // 回転の合成
    D3DXMatrixMultiply(&InOut, &aMtxRot, &InOut);

    return &InOut;
}

//==========================================================================
/**
@brief ラジコン回転
@param vecRight [in] 向かせたい方向ベクトル
@param speed [in] 向かせる速度
*/
void DX9_3DObject::RadioControl(const D3DXVECTOR3 & vecRight, float speed)
{
	D3DXVECTOR3 vecCross;

	D3DXVec3Cross(&vecCross, &this->Vec.Front, &vecRight);
	this->RotX((vecCross.y < 0.0f ? -0.01f : 0.01f)*speed);
}

//==========================================================================
/**
@brief 前ベクトルのセット
@param Input [in] 前ベクトル
*/
void DX9_3DObject::SetVecFront(const D3DXVECTOR3 & Input)
{
    this->Vec.Front = Input;
}

//==========================================================================
/**
@brief 前ベクトルの取得
@return 前ベクトル
*/
const D3DXVECTOR3 * DX9_3DObject::GetVecFront(void) const
{
    return &this->Vec.Front;
}

//==========================================================================
/**
@brief 上ベクトルのセット
@param Input [in] 上ベクトル
*/
void DX9_3DObject::SetVecUp(const D3DXVECTOR3 & Input)
{
    this->Vec.Up = Input;
}

//==========================================================================
/**
@brief 上ベクトルの取得
@return 上ベクトル
*/
const D3DXVECTOR3 * DX9_3DObject::GetVecUp(void) const
{
    return &this->Vec.Up;
}

//==========================================================================
/**
@brief 右ベクトルのセット
@param Input [in] 右ベクトル
*/
void DX9_3DObject::SetVecRight(const D3DXVECTOR3 & Input)
{
    this->Vec.Right = Input;
}

//==========================================================================
/**
@brief 右ベクトルの取得
@return 右ベクトル
*/
const D3DXVECTOR3 * DX9_3DObject::GetVecRight(void) const
{
    return &this->Vec.Right;
}

//==========================================================================
/**
@brief 上のどの方向を見ているかのセット
@param Input [in] ベクトル
*/
void DX9_3DObject::SetLockUp(const D3DXVECTOR3 & Input)
{
    this->Look.Up = Input;
}

//==========================================================================
/**
@brief 上のどの方向を見ているかの取得
@return ベクトル
*/
const D3DXVECTOR3 * DX9_3DObject::GetLockUp(void) const
{
    return &this->Look.Up;
}

//==========================================================================
/**
@brief 視点ベクトルセット
@param Input [in] 視点ベクトル
*/
void DX9_3DObject::SetLockAt(const D3DXVECTOR3 & Input)
{
    this->Look.At = Input;
}

//==========================================================================
/**
@brief 視点ベクトルの取得
@return 視点ベクトル
*/
const D3DXVECTOR3 * DX9_3DObject::GetLockAt(void) const
{
    return &this->Look.At;
}

//==========================================================================
/**
@brief 視線ベクトルセット
@param Input [in] 視線ベクトル
*/
void DX9_3DObject::SetLockEye(const D3DXVECTOR3 & Input)
{
    this->Look.Eye = Input;
}

//==========================================================================
/**
@brief 視線ベクトルの取得
@return 視線ベクトル
*/
const D3DXVECTOR3 * DX9_3DObject::GetLockEye(void) const
{
    return &this->Look.Eye;
}

//==========================================================================
/**
@brief 座標情報のセット
@param Input [in] 座標情報
*/
void DX9_3DObject::SetMatInfoPos(const D3DXVECTOR3 & Input)
{
    this->Info.pos = Input;
}

//==========================================================================
/**
@brief 座標情報の取得
@return 座標情報
*/
const D3DXVECTOR3 * DX9_3DObject::GetMatInfoPos(void) const
{
    return &this->Info.pos;
}

//==========================================================================
/**
@brief 回転情報のセット
@param Input [in] 回転情報
*/
void DX9_3DObject::SetMatInfoRot(const D3DXVECTOR3 & Input)
{
    this->Info.rot = Input;
}

//==========================================================================
/**
@brief 回転情報の取得
@return 回転情報
*/
const D3DXVECTOR3 * DX9_3DObject::GetMatInfoRot(void) const
{
    return &this->Info.rot;
}

//==========================================================================
/**
@brief スケールのセット
@param Input [in] スケール
*/
void DX9_3DObject::SetMatInfoSca(const D3DXVECTOR3 & Input)
{
    this->Info.sca = Input;
}

//==========================================================================
/**
@brief スケールの取得
@return スケール
*/
const D3DXVECTOR3 * DX9_3DObject::GetMatInfoSca(void) const
{
    return &this->Info.sca;
}

//==========================================================================
/**
@brief 対象の方向に向かせる(ベクトル計算を行い、処理を行います)
@param Input [in] ターゲット
@param power 向かせる力
*/
void DX9_3DObject::LockOn(const DX9_3DObject & Input, float power)
{
    auto VecTor = Input.Info.pos - this->Info.pos;

    this->RotX(VecTor, power);
}

//==========================================================================
/**
@brief 対象の方向に向かせる(直接ベクトルを操作します)
@param Input [in] ターゲット
*/
void DX9_3DObject::LockOn(const DX9_3DObject & Input)
{
    auto VecTor = Input.Info.pos - this->Info.pos;

    D3DXVec3Normalize(&VecTor, &VecTor);

    this->Look.Eye = -VecTor;
    this->Vec.Front = VecTor;
    D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
}

//==========================================================================
/**
@brief 使用リソース番号の取得
@return リソース番号
*/
int DX9_3DObject::GetIndex(void) const
{
    return m_index;
}

//==========================================================================
/**
@brief 使うベクトルのタイプ指定
@param type [in] ベクトルのタイプ
*/
void DX9_3DObject::SetMatrixType(const DX9_3DObjectMatrixType & type)
{
    this->m_MatType = type;
}

//==========================================================================
/**
@brief 使用中のベクトルの種類の取得
@return ベクトルの種類
*/
DX9_3DObjectMatrixType DX9_3DObject::GetMatrixType(void) const
{
    return this->m_MatType;
}

//==========================================================================
/**
@brief ビュー行列の登録
@param Input [in] ビュー行列
*/
void DX9_3DObject::SetMtxView(D3DXMATRIX * Input)
{
    this->m_MtxView = Input;
}

//==========================================================================
/**
@brief 使用リソース番号のセット
@param label [in] リソース番号のセット
*/
void DX9_3DObject::SetIndex(int label)
{
    this->m_index = label;
}

//==========================================================================
/**
@brief サードパーソン用の座標取得
@param move_pos [in] 対象オブジェクトを始点とした座標を入力してください
@return サードパーソン用の座標
*/
D3DXVECTOR3 DX9_3DObject::GetThirdPerson(D3DXVECTOR3 move_pos)
{
    // オブジェクトの複製
    DX9_3DObject obj = *this;

    obj.MoveX(move_pos.x);
    obj.MoveY(move_pos.y);
    obj.MoveZ(move_pos.z);

    return *obj.GetMatInfoPos();
}

//==========================================================================
/**
@brief 内部ベクトルの正規化
*/
void DX9_3DObject::VectorNormalize(void)
{
    D3DXVec3Normalize(&this->Vec.Front, &this->Vec.Front);
    D3DXVec3Normalize(&this->Vec.Up, &this->Vec.Up);
    D3DXVec3Normalize(&this->Vec.Right, &this->Vec.Right);
    D3DXVec3Normalize(&this->Look.At, &this->Look.At);
    D3DXVec3Normalize(&this->Look.Eye, &this->Look.Eye);
    D3DXVec3Normalize(&this->Look.Up, &this->Look.Up);
}

//==========================================================================
/**
@brief アニメーションの更新
*/
void DX9_3DObject::UpdateAnimationCount(void)
{
    this->m_AnimationCount++;
}

//==========================================================================
/**
@brief アニメーションの取得
@return カウンタ
*/
int & DX9_3DObject::GetAnimationCount(void)
{
    return this->m_AnimationCount;
}

//==========================================================================
/**
@brief アニメーションの登録
@param count [in] カウンタ
*/
void DX9_3DObject::SetAnimationCount(int count)
{
    this->m_AnimationCount = count;
}

//==========================================================================
/**
@brief アニメーションの初期化
*/
void DX9_3DObject::InitAnimationCount(void)
{
    this->m_AnimationCount = -1;
}

//==========================================================================
/**
@brief 回転の制限
@param Rot [in] 回転行列
@param pRang [in] 回転角度
@return 回転可能な際に true が返ります
*/
bool DX9_3DObject::Restriction(D3DXMATRIX Rot, const float * pRang)
{
	D3DXVECTOR3 dir = D3DXVECTOR3(0, 1.0f, 0); // 単位ベクトル
	float Limit = 0.75f;
	D3DXVECTOR3 pVecRight, pVecUp, pVecFront;

	pVecRight = this->Vec.Right;
	pVecUp = this->Vec.Up;
	pVecFront = this->Vec.Front;

	// ベクトルの座標変換
	D3DXVec3Cross(&pVecRight, &pVecUp, &pVecFront); // 外積
	D3DXMatrixRotationAxis(&Rot, &pVecRight, *pRang); // 回転
	D3DXVec3TransformNormal(&pVecFront, &pVecFront, &Rot);
	float fVec3Dot = atanf(D3DXVec3Dot(&pVecFront, &dir));

	// 内積
	if (-Limit<fVec3Dot && Limit>fVec3Dot) { return true; }

	return false;
}

//==========================================================================
/**
@brief 回転行列生成
@param InOut [in/out] ワールド行列を受け取る場所です
@return ワールド行列が返ります
*/
const D3DXMATRIX* DX9_3DObject::CalcLookAtMatrix(D3DXMATRIX & InOut)
{
	D3DXVECTOR3 X, Y, Z;

	Z = this->Look.Eye - this->Look.At;
	D3DXVec3Normalize(&Z, &Z);
	D3DXVec3Cross(&X, D3DXVec3Normalize(&Y, &this->Look.Up), &Z);
	D3DXVec3Normalize(&X, &X);
	D3DXVec3Normalize(&Y, D3DXVec3Cross(&Y, &Z, &X));

    InOut._11 = X.x; InOut._12 = X.y; InOut._13 = X.z; InOut._14 = 0;
    InOut._21 = Y.x; InOut._22 = Y.y; InOut._23 = Y.z; InOut._24 = 0;
    InOut._31 = Z.x; InOut._32 = Z.y; InOut._33 = Z.z; InOut._34 = 0;
    InOut._41 = 0.0f; InOut._42 = 0.0f; InOut._43 = 0.0f; InOut._44 = 1.0f;

	return &InOut;
}

//==========================================================================
/**
@brief 回転行列生成
@param InOut [in/out] ワールド行列を受け取る場所です
@return ワールド行列が返ります
*/
const D3DXMATRIX* DX9_3DObject::CalcLookAtMatrixAxisFix(D3DXMATRIX & InOut)
{
	D3DXVECTOR3 X, Y, Z, D;
	D = this->Look.Eye - this->Look.At;
	D3DXVec3Normalize(&D, &D);
	D3DXVec3Cross(&X, D3DXVec3Normalize(&Y, &this->Look.Up), &D);
	D3DXVec3Normalize(&X, &X);
	D3DXVec3Normalize(&Z, D3DXVec3Cross(&Z, &X, &Y));

    InOut._11 = X.x; InOut._12 = X.y; InOut._13 = X.z; InOut._14 = 0;
    InOut._21 = Y.x; InOut._22 = Y.y; InOut._23 = Y.z; InOut._24 = 0;
    InOut._31 = Z.x; InOut._32 = Z.y; InOut._33 = Z.z; InOut._34 = 0;
    InOut._41 = 0.0f; InOut._42 = 0.0f; InOut._43 = 0.0f; InOut._44 = 1.0f;

	return &InOut;
}

_MSLIB_END