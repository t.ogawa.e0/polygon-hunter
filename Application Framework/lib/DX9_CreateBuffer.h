//==========================================================================
// クリエイトバッファ[DX9_CreateBuffer.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_CreateBuffer
// Content: クリエイトバッファ
//
//==========================================================================
class DX9_CreateBuffer
{
protected:
    /**
    @brief バーテックスバッファの生成
    @param pDevice [in] デバイス
    @param hWid [in] ウィンドウハンドル
    @param Length [in] データ型のサイズ
    @param Usage [in] Usages for Vertex/Index buffers
    @param FVF [in] FVF
    @param Pool [in] Pool types
    @param ppVertexBuffer [in/out] バーテックスバッファ
    @param pSharedHandle [in] Handle to an Object
    @return 失敗時 true が返ります
    */
    bool CreateVertexBuffer(LPDIRECT3DDEVICE9 pDevice, HWND hWid, UINT Length, DWORD Usage, DWORD FVF, D3DPOOL Pool, IDirect3DVertexBuffer9** ppVertexBuffer, HANDLE* pSharedHandle);

    /**
    @brief インデックスバッファの生成
    @param pDevice [in] デバイス
    @param hWid [in] ウィンドウハンドル
    @param Length [in] データ型のサイズ
    @param Usage [in] Usages for Vertex/Index buffers
    @param Format [in] D3D9Ex only
    @param Pool [in] Pool types
    @param ppIndexBuffer [in/out] インデックスバッファ
    @param pSharedHandle [in] Handle to an Object
    @return 失敗時 true が返ります
    */
	bool CreateIndexBuffer(LPDIRECT3DDEVICE9 pDevice, HWND hWid, UINT Length, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DIndexBuffer9** ppIndexBuffer, HANDLE* pSharedHandle);
};

_MSLIB_END