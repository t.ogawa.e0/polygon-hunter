//==========================================================================
// 当たり判定[DX9_Collision.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_3DObject.h"
#include "DX9_2DObject.h"
#include "mslib_struct.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_Collision
// Content: 当たり判定専用クラス
//
//==========================================================================
class DX9_Collision
{
public:
    DX9_Collision();
    ~DX9_Collision();

    /**
    @brief 球のコリジョン
    @param TargetA [in] 対象A
    @param TargetB [in] 対象B
    @param Scale [in] コリジョンの有効範囲
    @return 指定範囲内に入った場合 true が返ります
    */
    bool Ball(const DX9_3DObject & TargetA, const DX9_3DObject & TargetB, float Scale);

    /**
    @brief Boxコリジョン
    @param TargetA [in] 対象A
    @param TargetB [in] 対象B
    @param Scale [in] コリジョンの有効範囲
    @return 指定範囲内に入った場合 true が返ります
    */
    bool Simple(const DX9_3DObject & TargetA, const DX9_3DObject & TargetB, float Scale);

    /**
    @brief 距離の計算 (C3DObjectorC2DObject)
    @param TargetA [in] 対象A
    @param TargetB [in] 対象B
    @return 距離が返ります
    */
    float Distance(const DX9_3DObject & TargetA, const DX9_3DObject & TargetB);

    /**
    @brief 距離の計算 (C3DObjectorC2DObject)
    @param TargetA [in] 対象A
    @param TargetB [in] 対象B
    @return 距離が返ります
    */
    float Distance(const DX9_2DObject & TargetA, const DX9_2DObject & TargetB);

    /**
    @brief 距離の計算 (CVector4<float>)
    @param TargetA [in] 対象A
    @param TargetB [in] 対象B
    @return 距離が返ります
    */
    float Distance(const CVector4<float> & TargetA, const CVector4<float> & TargetB);

    /**
    @brief 距離の計算 (CVector3<float>)
    @param TargetA [in] 対象A
    @param TargetB [in] 対象B
    @return 距離が返ります
    */
    float Distance(const CVector3<float> & TargetA, const CVector3<float> & TargetB);

    /**
    @brief 距離の計算 (CVector2<float>)
    @param TargetA [in] 対象A
    @param TargetB [in] 対象B
    @return 距離が返ります
    */
    float Distance(const CVector2<float> & TargetA, const CVector2<float> & TargetB);
private:
    /**
    @brief Boxコリジョン処理
    @param TargetA [in] 対象A
    @param TargetB [in] 対象B
    @param TargetC [in] 対象C
    @param Scale [in] コリジョンの有効範囲
    @return 指定範囲内に入った場合 true が返ります
    */
    bool Sinple2(const float & TargetA, const float & TargetB, const float & TargetC, float Scale);
};

_MSLIB_END