//==========================================================================
// カメラ[DX9_Camera.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"

_MSLIB_BEGIN

enum class DX9_CameraVectorList
{
    VEYE = 0, // 注視点
    VAT, // カメラ座標
    VEYE2, // 注視点
    VAT2, // カメラ座標
    VUP, // ベクター
    VECUP, // 上ベクトル
    VECFRONT, // 前ベクトル
    VECRIGHT, //  右ベクトル
};

//==========================================================================
//
// class  : DX9_Camera
// Content: カメラ
//
//==========================================================================
class DX9_Camera // カメラ
{
public:
    DX9_Camera();
    ~DX9_Camera();

    /**
    @brief 初期化
    */
	void Init(void);

    /**
    @brief 初期化
    @param pEye [in] 注視点
    @param pAt [in] 座標
    */
	void Init(const D3DXVECTOR3 & pEye, const D3DXVECTOR3 & pAt);

    /**
    @brief 解放
    */
	void Release(void);

    /**
    @brief 更新
    @param win_size [in] {x,y}ウィンドウサイズ
    @param pDevice [in] デバイス
    */
    void Update(const CVector2<int> & win_size, LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief 更新 ビューポート mode
    @param MtxView [in] ワールド行列
    @param ViewPort [in] ビューポート
    @param fWidth [in] 幅
    @param fHeight [in] 高さ
    @param pDevice [in] デバイス
    */
    void UpdateViewPort(const D3DXMATRIX * MtxView, const D3DVIEWPORT9 * ViewPort, int fWidth, int fHeight, LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief ビュー行列生成
    @return ビュー行列
    */
    D3DXMATRIX *CreateView(void);

    /**
    @brief 視点中心にX軸回転
    @param Rang [in] 入れた値が加算されます
    */
	void RotViewX(float Rang);

    /**
    @brief 視点中心にY軸回転
    @param Rang [in] 入れた値が加算されます
    */
	void RotViewY(float Rang);

    /**
    @brief カメラ中心にX軸回転
    @param Rang [in] 入れた値が加算されます
    */
	void RotCameraX(float Rang);

    /**
    @brief カメラ中心にY軸回転
    @param Rang [in] 入れた値が加算されます
    */
    void RotCameraY(float Rang);

    /**
    @brief X軸軸移動
    @param Speed [in] 入れた値が加算されます
    */
	void MoveX(float Speed);

    /**
    @brief Y軸軸移動
    @param Speed [in] 入れた値が加算されます
    */
	void MoveY(float Speed);

    /**
    @brief Z軸軸移動
    @param Speed [in] 入れた値が加算されます
    */
    void MoveZ(float Speed);

    /**
    @brief [非推奨]Z軸平行移動(Y軸有効)
    @param Speed [in] 入れた値が加算されます
    */
    void MoveZ_2(float Speed);

    /**
    @brief 視点変更
    @param Distance [in] 入れた値が加算されます
    @return 視点とカメラの距離
    */
	float DistanceFromView(float Distance);

    /**
    @brief 各ベクトルの取得
    @param List [in] 取得するベクトルの種類
    @return 取得したいベクトル
    */
	D3DXVECTOR3 GetVECTOR(DX9_CameraVectorList List);

    /**
    @brief カメラY軸回転情報
    @return 内積
    */
    float GetRestriction_Y(void);

    /**
    @brief カメラX軸回転情報
    @return 内積
    */
    float GetRestriction_X(void);

    /**
    @brief カメラZ軸回転情報
    @return 内積
    */
    float GetRestriction_Z(void);

    /**
    @brief カメラ座標をセット
    @param Eye [in] 注視点
    @param At [in] カメラ座標
    @param Up [in] ベクター
    */
    void SetCameraPos(const D3DXVECTOR3 & Eye, const D3DXVECTOR3 & At, const D3DXVECTOR3 & Up);

    /**
    @brief カメラ座標
    @param At [in] カメラ座標
    */
    void SetAt(const D3DXVECTOR3 & At);

    /**
    @brief 注視点
    @param Eye [in] 注視点
    */
    void SetEye(const D3DXVECTOR3 & Eye);
private:
    /**
    @brief 平行処理
    @param pVec [in] ベクトル
    @param pOut1 [out] 出力1
    @param pOut2 [out] 出力2
    @param pSpeed [in] 移動速度
    */
    void CameraMoveXYZ(D3DXVECTOR3 * pVec, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pSpeed);

    /**
    @brief X軸回転処理
    @param pDirection [in] ベクトル
    @param pRot [in] 回転情報
    @param pOut1 [out] 出力1
    @param pOut2 [out] 出力2
    @param pRang [in] 回転角度
    */
    void CameraRangX(D3DXVECTOR3 *pDirection, D3DXMATRIX *pRot, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pRang);

    /**
    @brief Y軸回転処理
    @param pDirection [in] ベクトル
    @param pRot [in] 回転情報
    @param pOut1 [out] 出力1
    @param pOut2 [out] 出力2
    @param pRang [in] 回転角度
    */
    void CameraRangY(D3DXVECTOR3 *pDirection, D3DXMATRIX *pRot, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pRang);

    /**
    @brief 視点変更
    @param pVec [in] ベクトル
    @param pOut1 [out] 出力1
    @param pOut2 [out] 出力2
    @param pSpeed [in] 移動速度
    @return 戻り値は 距離
    */
    float ViewPos(D3DXVECTOR3 *pVec, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pSpeed);

    /**
    @brief 内積
    @param pRot [in] 回転情報
    @param pRang [in] 回転速度
    @return 移動可能範囲なら true
    */
    bool Restriction(D3DXMATRIX pRot, const float* pRang);
public:
    D3DVIEWPORT9 * m_ViewPort; // ビューポート
private:
    D3DXMATRIX m_aMtxView; // ビュー行列
    D3DXVECTOR3 m_Eye; // 注視点
    D3DXVECTOR3 m_At; // カメラ座標
	D3DXVECTOR3 m_Eye2; // 注視点
	D3DXVECTOR3 m_At2; // カメラ座標
	D3DXVECTOR3 m_Up; // ベクター
	D3DXVECTOR3 m_VecUp; // 上ベクトル
	D3DXVECTOR3 m_VecFront; // 前ベクトル
	D3DXVECTOR3 m_VecRight; //  右ベクトル
};

_MSLIB_END