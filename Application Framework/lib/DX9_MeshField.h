//==========================================================================
// メッシュフィールド[DX9_MeshField.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <unordered_map> // c++ 平衡二分木 ソート機能無し
#include <string> // c++ char
#include <vector> // c++ 動的構造体

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_Renderer.h"
#include "DX9_3DObject.h"
#include "vector_wrapper.h"

_MSLIB_BEGIN

struct DX9_MeshFieldID {
    int x = 0, z = 0; // 配列番号
};

class DX9_MeshFieldData : private  DX9_VERTEX_3D
{
public:
    DX9_MeshFieldData();
    ~DX9_MeshFieldData();
public:
    std::string m_NameID; // IDネーム
    VERTEX_4 vertex;
    CColor<int> m_Color; // 色
};

//==========================================================================
//
// class  : DX9_MeshFieldDataParam
// Content: 縮退ポリゴンのパラメーター
//
//==========================================================================
class DX9_MeshFieldDataParam
{
public:
    DX9_MeshFieldDataParam();
    ~DX9_MeshFieldDataParam();
public:
    int NumMeshX; // 面の数
    int NumMeshZ; // 面の数
    int BoostMeshX; // 面の数に*4した値
    int BoostMeshZ; // 面の数に*4した値
    int VertexOverlap; // 重複する頂点数
    int	NumXVertexWey; // 視覚化されている1列の頂点数
    int	NumZVertex; // 基礎頂点数
    int	NumXVertex; // 基礎頂点数
    int	NumMeshVertex; // 視覚化されている全体の頂点数
    int	MaxPrimitive; // プリミティブ数
    int	MaxIndex; // 最大Index数
    int	MaxVertex; // 最大頂点数
};

//==========================================================================
//
// class  : DX9_MeshFieldParam
// Content: 縮退ポリゴンの情報
//
//==========================================================================
class DX9_MeshFieldParam
{
private:
    using VectorV4 = vector_wrapper<vector_wrapper<DX9_MeshFieldData>>;
public:
    DX9_MeshFieldParam();
    ~DX9_MeshFieldParam();
    /**
    @brief 解放
    */
    void Release(void);
public:
    LPDIRECT3DVERTEXBUFFER9 pVertexBuffer; // 頂点バッファ
    LPDIRECT3DINDEXBUFFER9 pIndexBuffer; // インデックスバッファ
    DX9_MeshFieldDataParam Info; // 縮退ポリゴンの情報
    VectorV4 m_FieldParameter; // フィールドのパラメータ
    std::unordered_map<std::string, DX9_MeshFieldData*> m_MismatchMainData; // 基礎データのmap
    std::unordered_map<std::string, DX9_MeshFieldData> m_MismatchSubData; // y軸に変更の入ったmap
    std::vector<int> m_GroupID; // GroupID
    vector_wrapper<vector_wrapper<DX9_MeshFieldID>> m_ArrayID; // 空間分割ID
};

//==========================================================================
//
// class  : DX9_MeshField
// Content: フィールド
//
//==========================================================================
class DX9_MeshField : public DX9_Renderer<DX9_3DObject>
{
private:
    using VectorV4 = vector_wrapper<vector_wrapper<DX9_MeshFieldData>>;
public:
    DX9_MeshField(LPDIRECT3DDEVICE9 pDevice, HWND hWnd);
    ~DX9_MeshField();

    /**
    @brief 初期化
    @param texture [in] テクスチャパス
    @param x [in] 横幅
    @param z [in] 奥行
    @return 失敗時に true が返ります
    */
    bool Init(const char * texture, int x, int z);

    /**
    @brief 解放
    */
    void Release(void);

    /**
    @brief リサイズ
    @param pdepoly [in] ポリゴン情報
    @param x [in] 横幅
    @param z [in] 奥行
    @return 失敗時 true が返ります
    */
    bool Resize(DX9_MeshFieldParam * pdepoly, int x, int z);

    /**
    @brief バッファのリメイク
    @param pdepoly [in] ポリゴン情報
    @return 失敗時 true が返ります
    */
    bool RemakeBuffer(DX9_MeshFieldParam * pdepoly);

    /**
    @brief 更新
    */
    void Update(void);

    /**
    @brief 描画
    */
    void Draw(void);

    /**
    @brief 当たり判定
    @param pdepoly [in] ポリゴン情報
    @param Position [in] 座標
    @param HitID [in/out] エリアID
    @return 判定時の座標情報
    */
    D3DXVECTOR3 GetHeight(DX9_MeshFieldParam * pdepoly, D3DXVECTOR3 Position, int & HitID);

    /**
    @brief マップの保存
    @param strFileName [in] ファイル名
    */
    void Save(const std::string strFileName);

    /**
    @brief マップの読み込み
    @param strFileName [in] ファイル名
    */
    void Loat(const std::string strFileName);

    /**
    @brief メッシュデータの取得
    @return マップデータの取得
    */
    vector_wrapper<DX9_MeshFieldParam> * GetMeshData(void);
private:
    /**
    @brief 空間分割の生成
    @param pdepoly [in] メッシュ情報
    @param buff [in] 分割範囲
    */
    void create_spatial_partitioning(DX9_MeshFieldParam * pdepoly, const int buff);

    /**
    @brief 空間探索
    @param pdepoly [in] メッシュ情報
    @param Position [in] 前回の座標
    @param HitID [in] エリアID
    @return マップデータの取得
    */
    vector_wrapper<DX9_MeshFieldID> * SearchArea(DX9_MeshFieldParam * pdepoly, D3DXVECTOR3 Position, int & HitID);

    /**
    @brief バッファに渡すデータの生成
    @param SetParam [in] メッシュの情報
    @param pdepoly [in] メッシュ情報
    */
    void CreateData(VectorV4 & SetParam, DX9_MeshFieldParam * pdepoly);

    /**
    @brief データの整合性
    @param SetParam [in] メッシュの情報
    @param pdepoly [in] メッシュ情報
    */
    void Consistency(VectorV4 & Input, DX9_MeshFieldParam * pdepoly);

    /**
    @brief インデックス情報の生成
    @param Output [out] インデックス情報の取得
    @param Input [in] メッシュデータ
    */
    void CreateIndex(LPWORD * Output, const DX9_MeshFieldDataParam & Input);

    /**
    @brief バーテックス情報の生成
    @param Output [out] バーテックス情報
    @param SetParam [in] セットするパラメータ
    */
    void CreateVertex(VERTEX_4 * Output, VectorV4 & SetParam);

    /**
    @brief メッシュ情報の生成
    @param Output [out] インデックス情報の取得
    @param numX [in] X軸への幅
    @param numZ [in] Z軸への幅
    @param buff [in] 何分岐設定
    */
    void MeshFieldInfo(DX9_MeshFieldDataParam & Output, const int numX, const int numZ, const int buff);

    /**
    @brief 法線の生成
    @param SetParam [in] セットするパラメータ
    */
    void SetNormal(VectorV4 & SetParam);
private:
    vector_wrapper<DX9_MeshFieldParam> m_MeshData; // メッシュデータ
};

_MSLIB_END