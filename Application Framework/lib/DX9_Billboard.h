//==========================================================================
// ビルボード[DX9_Billboard.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <string>
#include <list>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"
#include "vector_wrapper.h"
#include "DX9_Renderer.h"
#include "DX9_3DObject.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_BillboardPlateList
// Content: 板ポリの種類
//
//==========================================================================
enum class DX9_BillboardPlateList
{
    Up, // 上向き
    Vertical //　縦
};

//==========================================================================
//
// class  : DX9_BillboardAnimation
// Content: テクスチャ情報
//
//==========================================================================
class DX9_BillboardAnimation
{
public:
    DX9_BillboardAnimation();
    /**
    @brief ビルボードアニメーション
    @param frame [in] 更新タイミング
    @param pattern [in] アニメーションのパターン数
    @param direction [in] 一行のアニメーション数
    @param cut [in] 切り取り情報
    @param texsize [in] テクスチャのサイズ
    */
    DX9_BillboardAnimation(int frame, int pattern, int direction, CTexvec<float> cut, CTexvec<int> texsize);
    ~DX9_BillboardAnimation();
public:
    int Frame; // 更新タイミング
    int Pattern; // アニメーションのパターン数
    int Direction; // 一行のアニメーション数
    CTexvec<float> m_Cut; // 切り取り情報
    CTexvec<int> m_TexSize; // テクスチャのサイズ
};

//==========================================================================
//
// class  : DX9_BillboardBuffer
// Content: バッファ
//
//==========================================================================
class DX9_BillboardBuffer : private DX9_VERTEX_3D
{
public:
    DX9_BillboardBuffer();
    ~DX9_BillboardBuffer();
    /**
    @brief 解放
    */
    void Release(void);
public:
    LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer; // バッファ
    LPDIRECT3DINDEXBUFFER9 m_pIndexBuffer; // インデックスバッファ
    DX9_BillboardPlateList m_plate; // 板ポリの種類
    VERTEX_4* m_pPseudo;// 頂点バッファのロック
    bool m_origin; // オリジナルかどうかの判定
};

//==========================================================================
//
// class  : DX9_BillboardAnimationParam
// Content: アニメーション情報
//
//==========================================================================
class DX9_BillboardAnimationParam
{
public:
    DX9_BillboardAnimationParam();
    ~DX9_BillboardAnimationParam();
    /**
    @brief 解放
    */
    void Release(void);
public:
    DX9_BillboardAnimation *m_pAnimation; // アニメーション情報
    DX9_BillboardBuffer m_buffer; // バッファ
    CUv<float> m_uv; // UV格納
};

//==========================================================================
//
// class  : DX9_BillboardAnimationDataCase
// Content: 複数アニメーション情報ボックス
//
//==========================================================================
class DX9_BillboardAnimationDataCase
{
public:
    DX9_BillboardAnimationDataCase();
    ~DX9_BillboardAnimationDataCase();
public:
    std::string m_strName[4]; // 四方向テクスチャ格納
    int m_numdata; // データ数
    int m_frame; // 更新フレーム
    int m_pattern; // アニメーション数
    int m_direction; // 横一列のアニメーション数
    DX9_BillboardPlateList m_plate; // 板ポリの種類
    bool m_centermood; // 中心モード
};

//==========================================================================
//
// class  : DX9_Billboard
// Content: ビルボード
//
//==========================================================================
class DX9_Billboard : public DX9_Renderer<DX9_3DObject>
{
private:
    // コピー禁止 (C++11)
    DX9_Billboard(const DX9_Billboard &) = delete;
    DX9_Billboard &operator=(const DX9_Billboard &) = delete;
public:
	//==========================================================================
	//
	// class  : CAnimationData
	// Content: 複数アニメーション登録用
	//
	//==========================================================================
	class CAnimationData {
    public:
        DX9_BillboardAnimationDataCase m_list;
    };
public:
    DX9_Billboard(LPDIRECT3DDEVICE9 pDevice, HWND hWnd);
    ~DX9_Billboard();

    /**
    @brief 初期化
    @param Input [in] 使うテクスチャのパス
    @param plate [in] 板ポリの種類
    @param bCenter [in] true で中心座標を下の真ん中にします/デフォルトはfalse
    @return 失敗時 true
    */
	bool Init(const char* Input, DX9_BillboardPlateList plate, bool bCenter = true);

    /**
    @brief アニメーション用初期化
    @param Input [in] 使うテクスチャのパス
    @param UpdateFrame [in] 更新フレーム
    @param Pattern [in] アニメーション数
    @param Direction [in] 横一列のアニメーション数
    @param plate [in] 板ポリの種類
    @param bCenter [in] true で中心座標を下の真ん中にします/デフォルトはfalse
    @return 失敗時 true
    */
	bool Init(const char* Input, int UpdateFrame, int Pattern, int Direction, DX9_BillboardPlateList plate, bool bCenter = true);

    /**
    @brief 初期化
    @param Input [in] 使うテクスチャのパス ダブルポインタ用
    @param numdata [in] データ数
    @param plate [in] 板ポリの種類
    @param bCenter [in] true で中心座標を下の真ん中にします/デフォルトはfalse
    @return 失敗時 true
    */
	bool Init(const char** Input, int numdata, DX9_BillboardPlateList plate, bool bCenter = true);

    /**
    @brief アニメーション用初期化
    @param Input [in] 使うテクスチャのパス
    @param numdata [in] データ数
    @param UpdateFrame [in] 更新フレーム
    @param Pattern [in] アニメーション数
    @param Direction [in] 横一列のアニメーション数
    @param plate [in] 板ポリの種類
    @param bCenter [in] true で中心座標を下の真ん中にします/デフォルトはfalse
    @return 失敗時 true
    */
	bool Init(const char** Input, int numdata, int UpdateFrame, int Pattern, int Direction, DX9_BillboardPlateList plate, bool bCenter = true);

    /**
    @brief 更新
    */
    void Update(void);

    /**
    @brief 解放
    */
	void Release(void);

    /**
    @brief 描画
    @param MtxView [in] ビュー行列
    */
    void Draw(D3DXMATRIX * MtxView);

    /**
    @brief 描画
    @param bADD [in] 加算合成するかの判定
    @param MtxView [in] ビュー行列
    */
    void Draw(bool bADD, D3DXMATRIX * MtxView);

    /**
    @brief アニメーション切り替わり時の判定
    @param index [in] リソース番号
    @param AnimationCount [in] アニメーションカウンタを入れるところ
    @return アニメーション終了時 true
    */
	bool GetPattanNum(int index, int & AnimationCount);

    /**
    @brief アニメーションカウンタの初期化
    @param AnimationCount [in] アニメーションカウンタを入れるところ
    */
    void InitAnimationCount(int & AnimationCount);
private:
    /**
    @brief ベースとなる描画
    @param bADD [in] 加算合成
    @param MtxView [in] ビュー行列
    */
    void BaseDraw(bool bADD, D3DXMATRIX * MtxView);

    /**
    @brief 頂点バッファの生成
    @param ptexture [in] ビルボードアニメーションクラス
    @param bCenter [in] 中心座標フラグ
    */
	void CreateVertex(DX9_BillboardAnimationParam *ptexture, bool bCenter);

    /**
    @brief アニメーション情報のセット
    @param ptexture [in] ビルボードアニメーションクラス
    @param AnimationCount [in] アニメーションカウンタ
    */
	void AnimationPalam(DX9_BillboardAnimationParam *ptexture, int & AnimationCount);

    /**
    @brief UVの生成
    @param ptexture [in] ビルボードアニメーションクラス
    */
	void UV(DX9_BillboardAnimationParam *ptexture);

    /**
    @brief 重複検索
    @param plate [in] 板ポリの種類
    @return 重複時 true
    */
	bool OverlapSearch(DX9_BillboardPlateList plate);

    /**
    @brief バッファの生成
    @param plate [in] 板ポリの種類
    @param bCenter [in] 中心座標モード
    @return 重複時 true
    */
	bool CreateBuffer(DX9_BillboardPlateList plate, bool bCenter);

    /**
    @brief アニメーション情報の生成
    @param UpdateFrame [in] 更新フレーム
    @param Pattern [in] アニメーション数
    @param Direction [in] 一行のアニメーション数
    */
	void CreateAnimation(int UpdateFrame, int Pattern, int Direction);

    /**
    @brief 頂点の更新
    @param ptexture [in] ビルボードアニメーションクラス
    */
    void VertexUpdate(DX9_BillboardAnimationParam *ptexture);

    /**
    @brief ビルボードの種類
    @param ptexture [in] ビルボードアニメーションクラス
    @param pInput [in] 対象オブジェクト
    @param MtxWorld [in/out] ワールド行列
    @param MtxView [in] ビュー行列
    */
    void BillboardType(DX9_BillboardAnimationParam *ptexture, DX9_3DObject * pInput, D3DXMATRIX & MtxWorld, D3DXMATRIX * MtxView);
private:
    vector_wrapper<DX9_BillboardAnimationParam> m_buffer_origin; // オリジナルバッファ
    vector_wrapper<DX9_BillboardAnimationParam> m_buffer_path; // アクセス用バッファ
};

_MSLIB_END