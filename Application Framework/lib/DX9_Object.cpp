//==========================================================================
// オブジェクト管理[DX9_Object.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Object.h"

_MSLIB_BEGIN

//==========================================================================
// 実体
//==========================================================================
std::list<DX9_Object*> DX9_Object::m_Object[(int)DX9_ObjectID::MAX]; // オブジェクト格納
std::string DX9_Object::m_file_name; // file pass

//==========================================================================
/**
@brief コンストラクタ
@param ObjectID [in] オブジェクトID
*/
DX9_Object::DX9_Object(DX9_ObjectID ObjectID)
{
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
    this->m_processing_time.reserve((int)ProcessingList::End);
    for (int i = 0; i < (int)ProcessingList::End; i++) {
        this->m_processing_time.push_back(0);
    }
#endif
    this->m_3DObject = nullptr;
    this->m_2DObject = nullptr;
    this->m_2DPolygon = nullptr;
    this->m_Billboard = nullptr;
    this->m_Cube = nullptr;
    this->m_Mesh = nullptr;
    this->m_A_CircleShadow = nullptr;
    this->m_Xmodel = nullptr;
    this->m_Sphere = nullptr;
    this->m_Grid = nullptr;
    this->m_MeshField = nullptr;
    this->m_Text = nullptr;
    this->m_XInput = nullptr;
    this->m_XAudio = nullptr;
    this->m_Camera = nullptr;
    this->m_Number = nullptr;
    this->m_ObjectName = nullptr;
    this->m_Timer = nullptr;
    this->m_Light = nullptr;
    this->m_debug_object3d = nullptr;
    this->m_debug_key = false;
    this->m_initializer = false;
    this->m_mt = nullptr;
    this->m_rand_int = nullptr;
    this->m_rand_float = nullptr;
    this->m_3DObjectMotion = nullptr;
    this->m_Fog = nullptr;
    if (ObjectID != DX9_ObjectID::SpecialBlock) {
        this->m_Object[(int)ObjectID].emplace_back(this);
    }
}

//==========================================================================
/**
@brief デストラクタ
*/
DX9_Object::~DX9_Object()
{
    this->m_processing_time.clear();
    this->Delete(this->m_3DObject);
    this->Delete(this->m_2DObject);
    this->Delete(this->m_2DPolygon);
    this->Delete(this->m_Billboard);
    this->Delete(this->m_Cube);
    this->Delete(this->m_Mesh);
    this->Delete(this->m_A_CircleShadow);
    this->Delete(this->m_Xmodel);
    this->Delete(this->m_Sphere);
    this->Delete(this->m_Grid);
    this->Delete(this->m_Text);
    this->Delete(this->m_XInput);
    this->Delete(this->m_XAudio);
    this->Delete(this->m_Camera);
    this->Delete(this->m_Number);
    this->Delete(this->m_ObjectName);
    this->Delete(this->m_MeshField);
    this->Delete(this->m_Timer);
    this->Delete(this->m_Light);
    this->Delete(this->m_debug_object3d);
    this->Delete(this->m_mt);
    this->Delete(this->m_rand_int);
    this->Delete(this->m_rand_float);
    this->Delete(this->m_3DObjectMotion);
    this->Delete(this->m_Fog);
}

//==========================================================================
/**
@brief オブジェクトの取得
@param ObjectID [in] オブジェクトID
@param ObjectName [in] オブジェクト名
@return 取得対象のオブジェクト
*/
DX9_Object * DX9_Object::GetObjects(const DX9_ObjectID ObjectID, const std::string & ObjectName)
{
    for (auto itr = this->m_Object[(int)ObjectID].begin(); itr != this->m_Object[(int)ObjectID].end(); ++itr)
    {
        if ((*itr)->m_ObjectName != nullptr)
        {
            if (*(*itr)->m_ObjectName == ObjectName)
            {
                return (*itr);
            }
        }
    }
    return nullptr;
}

//==========================================================================
/**
@brief 3Dオブジェクトインスタンス管理
@return 3Dオブジェクトのインスタンス
*/
vector_wrapper<DX9_3DObject>* DX9_Object::_3DObject(void)
{
    return this->New(this->m_3DObject);
}

//==========================================================================
/**
@brief 2Dオブジェクトインスタンス管理
@return 2Dオブジェクトのインスタンス
*/
vector_wrapper<DX9_2DObject>* DX9_Object::_2DObject(void)
{
    return this->New(this->m_2DObject);
}

//==========================================================================
/**
@brief カメラの取得
@param ID [in] カメラID
@return カメラインスタンス
*/
DX9_Camera * DX9_Object::GetCamera(int ID)
{
    return this->New(this->m_Camera)->Get(ID);
}

//==========================================================================
/**
@brief カメラへ追従情報を渡す
@param ID [in] カメラID
@param vpos [in] 追従座標
*/
void DX9_Object::SetCameraLockOn(int ID, const D3DXVECTOR3 & vpos)
{
    auto * p_obj = this->New(this->m_Camera)->Get(ID);
    if (p_obj != nullptr)
    {
        p_obj->SetAt(vpos);
        p_obj->SetEye(vpos);
    }
}

//==========================================================================
/**
@brief オブジェクト名の取得
@return オブジェクト名
*/
const std::string * DX9_Object::GetObjectName(void)
{
    return this->m_ObjectName;
}

//==========================================================================
/**
@brief 特定オブジェクトの安全な破棄(外部アクセス用)(C3DObject or C2DObject)
@param Input [in] 3Dオブジェクト
@return オブジェクト名
*/
void DX9_Object::ObjectRelease(DX9_3DObject * Input)
{
    this->ObjectDelete(this->m_Billboard, Input);
    this->ObjectDelete(this->m_Cube, Input);
    this->ObjectDelete(this->m_Mesh, Input);
    this->ObjectDelete(this->m_A_CircleShadow, Input);
    this->ObjectDelete(this->m_Xmodel, Input);
    this->ObjectDelete(this->m_Sphere, Input);
    this->m_3DObject->PinpointRelease(Input);
}

//==========================================================================
/**
@brief 特定オブジェクトの安全な破棄(外部アクセス用)(C3DObject or C2DObject)
@param Input [in] 2Dオブジェクト
@return オブジェクト名
*/
void DX9_Object::ObjectRelease(DX9_2DObject * Input)
{
    this->ObjectDelete(this->m_2DPolygon, Input);
    this->m_2DObject->PinpointRelease(Input);
}

//==========================================================================
/**
@brief フィールド数
@return 数
*/
int DX9_Object::FieldSize(void)
{
    return this->_3DField()->GetMeshData()->Size();
}

//==========================================================================
/**
@brief 指定フィールドとの当たり判定
@param FieldID [in] フィールドID
@param vpos [in] 現在の座標
@param HitID [in] エリアID
@return 判定座標
*/
D3DXVECTOR3 DX9_Object::FieldHeight(int FieldID, const D3DXVECTOR3 & vpos, int & HitID)
{
    auto * pData = this->_3DField()->GetMeshData()->Get(FieldID);
    if (pData != nullptr)
    {
        return this->m_MeshField->GetHeight(pData, vpos, HitID);
    }
    return vpos;
}

//==========================================================================
/**
@brief オブジェクト名のセット
@param ObjectName [in] オブジェクト名の登録
*/
void DX9_Object::SetObjectName(const std::string & ObjectName)
{
    *this->New(this->m_ObjectName) = ObjectName;
}

//==========================================================================
/**
@brief 2Dポリゴン
@return 2Dポリゴンのインスタンス
*/
DX9_2DPolygon * DX9_Object::_2DPolygon(void)
{
    return this->New(
        this->m_2DPolygon,
        DX9_DeviceManager::GetDXDevice()->GetD3DDevice(),
        DX9_DeviceManager::GetDXDevice()->GetHwnd(),
        DX9_DeviceManager::GetDXDevice()->screenbuffscale());
}

//==========================================================================
/**
@brief 数字
@return 数字のインスタンス
*/
DX9_Number * DX9_Object::_2DNumber(void)
{
    return this->New(
        this->m_Number,
        DX9_DeviceManager::GetDXDevice()->GetD3DDevice(),
        DX9_DeviceManager::GetDXDevice()->GetHwnd(),
        DX9_DeviceManager::GetDXDevice()->screenbuffscale());
}

//==========================================================================
/**
@brief ビルボード
@return ビルボードのインスタンス
*/
DX9_Billboard * DX9_Object::_3DBillboard(void)
{
    return this->New(
        this->m_Billboard,
        DX9_DeviceManager::GetDXDevice()->GetD3DDevice(),
        DX9_DeviceManager::GetDXDevice()->GetHwnd());
}

//==========================================================================
/**
@brief キューブ
@return キューブのインスタンス
*/
DX9_Cube * DX9_Object::_3DCube(void)
{
    return this->New(
        this->m_Cube,
        DX9_DeviceManager::GetDXDevice()->GetD3DDevice(),
        DX9_DeviceManager::GetDXDevice()->GetHwnd());
}

//==========================================================================
/**
@brief メッシュ
@return メッシュのインスタンス
*/
DX9_Mesh * DX9_Object::_3DMesh(void)
{
    return this->New(
        this->m_Mesh,
        DX9_DeviceManager::GetDXDevice()->GetD3DDevice(),
        DX9_DeviceManager::GetDXDevice()->GetHwnd());
}

//==========================================================================
/**
@brief フィールド
@return フィールドのインスタンス
*/
DX9_MeshField * DX9_Object::_3DField(void)
{
    return this->New(
        this->m_MeshField,
        DX9_DeviceManager::GetDXDevice()->GetD3DDevice(),
        DX9_DeviceManager::GetDXDevice()->GetHwnd());
}

//==========================================================================
/**
@brief 丸影
@return 丸影のインスタンス
*/
DX9_Shadow * DX9_Object::_3DA_CircleShadow(void)
{
    return this->New(
        this->m_A_CircleShadow,
        DX9_DeviceManager::GetDXDevice()->GetD3DDevice(),
        DX9_DeviceManager::GetDXDevice()->GetHwnd());
}

//==========================================================================
/**
@brief Xモデル
@return Xモデルのインスタンス
*/
DX9_Xmodel * DX9_Object::_3DXmodel(void)
{
    return this->New(
        this->m_Xmodel,
        DX9_DeviceManager::GetDXDevice()->GetD3DDevice(),
        DX9_DeviceManager::GetDXDevice()->GetHwnd());
}

//==========================================================================
/**
@brief 球体
@return 球体のインスタンス
*/
DX9_Sphere * DX9_Object::_3DSphere(void)
{
    return this->New(
        this->m_Sphere,
        DX9_DeviceManager::GetDXDevice()->GetD3DDevice(),
        DX9_DeviceManager::GetDXDevice()->GetHwnd());
}

//==========================================================================
/**
@brief グリッド
@return グリッドのインスタンス
*/
DX9_Grid * DX9_Object::_3DGrid(void)
{
    return this->New(
        this->m_Grid,
        DX9_DeviceManager::GetDXDevice()->GetD3DDevice());
}

//==========================================================================
/**
@brief テキスト
@return テキストのインスタンス
*/
DX9_Text * DX9_Object::_2DText(void)
{
    return this->New(
        this->m_Text,
        DX9_DeviceManager::GetDXDevice()->GetD3DDevice());
}

//==========================================================================
/**
@brief ImGui
@return ImGuiのインスタンス
*/
DX9_ImGui * DX9_Object::ImGui(void)
{
    return &this->m_ImGui;
}

//==========================================================================
/**
@brief XInput
@return XInputのインスタンス
*/
XInput * DX9_Object::_XInput(void)
{
    return this->New(this->m_XInput);
}

//==========================================================================
/**
@brief XAudio
@return XAudioのインスタンス
*/
XAudio2 * DX9_Object::XAudio(void)
{
    return this->New(this->m_XAudio);
}

//==========================================================================
/**
@brief カメラ
@return カメラのインスタンス
*/
vector_wrapper<DX9_Camera>* DX9_Object::Camera(void)
{
    return this->New(this->m_Camera);
}

//==========================================================================
/**
@brief タイマー
@return タイマーのインスタンス
*/
vector_wrapper<Timer>* DX9_Object::_Timer(void)
{
    return this->New(this->m_Timer);
}

//==========================================================================
/**
@brief ライト
@return ライトのインスタンス
*/
DX9_Light * DX9_Object::Light(void)
{
    return this->New(
        this->m_Light,
        DX9_DeviceManager::GetDXDevice()->GetD3DDevice());
}

//==========================================================================
/**
@brief 単純な判定処理
@return 単純な判定処理のインスタンス
*/
DX9_Collision * DX9_Object::Collision(void)
{
    return &this->m_Collision;
}

//==========================================================================
/**
@brief メルセンヌ・ツイスタの32ビット版
@return 初期シード値の取得
*/
std::mt19937 & DX9_Object::GetMt19937(void)
{
    // 初期シード値がメモリ確保されていないとき
    if (this->m_mt == nullptr)
    {
        std::random_device rnd; // 非決定的な乱数生成器を生成
        std::mt19937 mt(rnd()); // メルセンヌ・ツイスタの32ビット版、引数は初期シード値

        // メモリの確保
        this->New(this->m_mt);

        // 初期シード値の保存
        *this->m_mt = mt;
    }

    return *this->m_mt;
}

//==========================================================================
/**
@brief 範囲の一様乱数管理関数(int)
@return 範囲の一様乱数
*/
vector_wrapper<rand_int>* DX9_Object::_rand_int(void)
{
    return this->New(this->m_rand_int);
}

//==========================================================================
/**
@brief 範囲の一様乱数管理関数(float)
@return 範囲の一様乱数
*/
vector_wrapper<rand_float>* DX9_Object::_rand_float(void)
{
    return this->New(this->m_rand_float);
}

//==========================================================================
/**
@brief 3Dオブジェクトモーション
@return モーションインスタンス
*/
vector_wrapper<DX9_3DObjectMotion>* DX9_Object::_3DObjectMotion(void)
{
    return this->New(this->m_3DObjectMotion);
}

//==========================================================================
/**
@brief フォグ
@return フォグイスタンス
*/
DX9_Fog * DX9_Object::_Fog(void)
{
    return this->New(this->m_Fog, this->GetDirectX9Device());
}

//==========================================================================
/**
@brief デバッグON
*/
void DX9_Object::DebugON(void)
{
    this->m_debug_key = true;
}

//==========================================================================
/**
@brief デバッグOFF
*/
void DX9_Object::DebugOFF(void)
{
    this->m_debug_key = false;
}

//==========================================================================
/**
@brief デバッグオブジェクトの登録(毎フレーム必要)
@param Input [in] オブジェクト
*/
void DX9_Object::Debug_3DObject(DX9_3DObject * Input)
{
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
    this->New(this->m_debug_object3d)->emplace_back(Input);
#else
    Input;
#endif
}

//==========================================================================
/**
@brief オブジェクト内部データの読み取り
@brief Object名が登録されていない場合処理が発生しません
@brief ファイル名は自動設定されます
*/
void DX9_Object::ObjectLoad(void)
{
    this->LoadAll(this->m_ObjectName);
}

//==========================================================================
/**
@brief ダイレクトインプットkeyboard
@return 存在しない場合は nullptr が返ります
*/
DirectInputKeyboard * DX9_Object::Dinput_Keyboard(void)
{
    return DX9_DeviceManager::GetKeyboard();
}

//==========================================================================
/**
@brief ダイレクトインプットマウス
@return 存在しない場合は nullptr が返ります
*/
DirectInputMouse * DX9_Object::Dinput_Mouse(void)
{
    return DX9_DeviceManager::GetMouse();
}

//==========================================================================
/**
@brief ダイレクトインプットコントローラー(PS4)
@return 存在しない場合は nullptr が返ります
*/
DirectInputController * DX9_Object::Dinput_Controller(void)
{
    return DX9_DeviceManager::GetController();
}

//==========================================================================
/**
@brief ウィンドウサイズの取得
@return ウィンドウサイズ[x.y]
*/
const CVector2<int> DX9_Object::GetWinSize(void)
{
    return DX9_DeviceManager::GetDXDevice()->GetWindowsSize();
}

//==========================================================================
/**
@brief DirectX9デバイスの取得
@return デバイス
*/
LPDIRECT3DDEVICE9 DX9_Object::GetDirectX9Device(void)
{
    return DX9_DeviceManager::GetDXDevice()->GetD3DDevice();
}

//==========================================================================
/**
@brief 登録済みオブジェクトの初期化
*/
bool DX9_Object::InitAll(void)
{
    int max_data = 0; // 最大インスタンス数
    int now_data = 0; // 初期化済みデータ数

    // インスタンス数の取得
    for (int i = (int)DX9_ObjectID::Default; i < (int)DX9_ObjectID::MAX; i++)
    {
        max_data += m_Object[i].size();
    }

    // 初期化
    for (int i = (int)DX9_ObjectID::Default; i < (int)DX9_ObjectID::MAX; i++)
    {
        // イテレーターの先頭から最後まで回す
        for (auto itr = m_Object[i].begin(); itr != m_Object[i].end(); ++itr)
        {
            // 初期化カウンタを回す
            now_data++;

            // 初期化済みではないとき
            if ((*itr)->m_initializer != true)
            {
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
                auto start = (*itr)->GetTimeSec<float>();
#endif
                (*itr)->Init();
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
                auto end = (*itr)->GetTimeSec<float>();
                auto time = end - start;
                (*itr)->m_processing_time[(int)ProcessingList::Initializa] = time;
#endif
                (*itr)->m_initializer = true;
                i = (int)DX9_ObjectID::MAX;
                break;
            }
        }
    }

    // 初期化が終了
    if (max_data == now_data)
    {
        return true;
    }

    return false;
}

//==========================================================================
/**
@brief 登録済みオブジェクトの解放
*/
void DX9_Object::ReleaseAll(void)
{
    for (int i = (int)DX9_ObjectID::Default; i < (int)DX9_ObjectID::MAX; i++)
    {
        // イテレーターの先頭から最後まで回す
        for (auto itr = m_Object[i].begin(); itr != m_Object[i].end(); ++itr)
        {
            (*itr)->Uninit();
            delete (*itr);
        }
        m_Object[i].clear();
    }
    m_file_name.clear();
}

//==========================================================================
/**
@brief 登録済みオブジェクトの更新
*/
void DX9_Object::UpdateAll(void)
{
    for (int i = (int)DX9_ObjectID::Default; i < (int)DX9_ObjectID::MAX; i++)
    {
        // イテレーターの先頭から最後まで回す
        for (auto itr = m_Object[i].begin(); itr != m_Object[i].end(); ++itr)
        {
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
            auto start = (*itr)->GetTimeSec<float>();
            (*itr)->m_processing_time[(int)ProcessingList::limit_time_Old] = (*itr)->m_processing_time[(int)ProcessingList::limit_time_New];
            (*itr)->m_processing_time[(int)ProcessingList::limit_time_New] = start;
#endif
            (*itr)->Update();
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
            auto end = (*itr)->GetTimeSec<float>();
            auto time = end - start;
            (*itr)->m_processing_time[(int)ProcessingList::update] = time;
#endif
        }
    }
    System();
}

//==========================================================================
/**
@brief 登録済みオブジェクトの描画
*/
void DX9_Object::DrawAll(void)
{
    for (int i = (int)DX9_ObjectID::Default; i < (int)DX9_ObjectID::MAX; i++)
    {
        Debug3DObjectDraw((DX9_ObjectID)i);
        DrawPreparation((DX9_ObjectID)i);

        // イテレーターの先頭から最後まで回す
        for (auto itr = m_Object[i].begin(); itr != m_Object[i].end(); ++itr)
        {
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
            auto start = (*itr)->GetTimeSec<float>();
#endif
            (*itr)->Draw();
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
            auto end = (*itr)->GetTimeSec<float>();
            auto time = end - start;
            (*itr)->m_processing_time[(int)ProcessingList::draw] = time;
#endif
        }
        Debug2DObjectDraw((DX9_ObjectID)i);
        DrawPostProcessing((DX9_ObjectID)i);
    }
}

//==========================================================================
/**
@brief ファイル指定
@param file_pass [in] ファイルパス
*/
void DX9_Object::SetFilePass(const std::string file_pass)
{
    m_file_name = file_pass;
}

//==========================================================================
/**
@brief 全て書き込み
*/
void DX9_Object::SaveAll(void)
{
    std::ofstream fstream;
    int size = 0;
    int max_data = 0;
    int count = 0;

    // インスタンス数の取得
    for (int id = (int)DX9_ObjectID::Default; id < (int)DX9_ObjectID::MAX; id++)
    {
        // イテレーターの先頭から最後まで回す
        for (auto itr = m_Object[id].begin(); itr != m_Object[id].end(); ++itr)
        {
            if ((*itr)->m_ObjectName != nullptr)
            {
                max_data++;
            }
        }
    }

    // ファイルを開く
    fstream.open(m_file_name, std::ios::out | std::ios::binary);

    // 保存可能なデータのみを全て保存
    for (int id = (int)DX9_ObjectID::Default; id < (int)DX9_ObjectID::MAX; id++)
    {
        // イテレーターの先頭から最後まで回す
        for (auto itr = m_Object[id].begin(); itr != m_Object[id].end(); ++itr)
        {
            if ((*itr)->m_ObjectName != nullptr)
            {
                char tag[512] = { 0 };
                int itr_str_count = 0;
                count++;

                // tagのセット
                for (auto itr_str = (*itr)->m_ObjectName->begin(); itr_str != (*itr)->m_ObjectName->end(); ++itr_str)
                {
                    tag[itr_str_count] = (*itr_str);
                    itr_str_count++;
                }

                // tagの登録
                fstream.write((char *)&tag, sizeof(tag));

                // 3DObjectが存在する場合
                size = 0;
                if ((*itr)->m_3DObject != nullptr)
                {
                    size = (*itr)->m_3DObject->Size();

                    // 書き込み
                    fstream.write((char *)&size, sizeof(size));

                    // 書き込み
                    for (int i = 0; i < size; i++)
                    {
                        auto *pobj = (*itr)->m_3DObject->Get(i);

                        // 3DObject param
                        const auto & Material = pobj->GetD3DMaterial9();
                        const auto Index = pobj->GetIndex();
                        const auto *pLockAt = pobj->GetLockAt();
                        const auto *pLockEye = pobj->GetLockEye();
                        const auto *pLockUp = pobj->GetLockUp();
                        const auto *pMatInfoPos = pobj->GetMatInfoPos();
                        const auto *pMatInfoRot = pobj->GetMatInfoRot();
                        const auto *pMatInfoSca = pobj->GetMatInfoSca();
                        const auto MatrixType = pobj->GetMatrixType();
                        const auto *pVecFront = pobj->GetVecFront();
                        const auto *pVecRight = pobj->GetVecRight();
                        const auto *pVecUp = pobj->GetVecUp();

                        fstream.write((char *)&Material, sizeof(Material));
                        fstream.write((char *)&Index, sizeof(Index));
                        fstream.write((char *)pLockAt, sizeof(*pLockAt));
                        fstream.write((char *)pLockEye, sizeof(*pLockEye));
                        fstream.write((char *)pLockUp, sizeof(*pLockUp));
                        fstream.write((char *)pMatInfoPos, sizeof(*pMatInfoPos));
                        fstream.write((char *)pMatInfoRot, sizeof(*pMatInfoRot));
                        fstream.write((char *)pMatInfoSca, sizeof(*pMatInfoSca));
                        fstream.write((char *)&MatrixType, sizeof(MatrixType));
                        fstream.write((char *)pVecFront, sizeof(*pVecFront));
                        fstream.write((char *)pVecRight, sizeof(*pVecRight));
                        fstream.write((char *)pVecUp, sizeof(*pVecUp));
                    }
                }
                else
                {
                    // 書き込み
                    fstream.write((char *)&size, sizeof(size));
                }

                // 2DObjectが存在する場合
                size = 0;
                if ((*itr)->m_2DObject != nullptr)
                {
                    size = (*itr)->m_2DObject->Size();

                    // 書き込み
                    fstream.write((char *)&size, sizeof(size));

                    // 書き込み
                    for (int i = 0; i < size; i++)
                    {
                        auto *pobj = (*itr)->m_2DObject->Get(i);

                        // 2DObject param
                        const auto Index = pobj->GetIndex();
                        const auto *pPos = pobj->GetPos();
                        const auto Scale = pobj->GetScale();
                        const auto *pPercentPos = pobj->GetPercentPos();
                        const auto *pColor = pobj->GetColor();
                        const auto Angle = pobj->GetAngle();

                        fstream.write((char *)&Index, sizeof(Index));
                        fstream.write((char *)pPos, sizeof(*pPos));
                        fstream.write((char *)&Scale, sizeof(Scale));
                        fstream.write((char *)pPercentPos, sizeof(*pPercentPos));
                        fstream.write((char *)pColor, sizeof(*pColor));
                        fstream.write((char *)&Angle, sizeof(Angle));
                    }
                }
                else
                {
                    // 書き込み
                    fstream.write((char *)&size, sizeof(size));
                }

                // Numberが存在する場合
                size = 0;
                if ((*itr)->m_Number != nullptr)
                {
                    size = (*itr)->m_Number->GetSize();

                    // 書き込み
                    fstream.write((char *)&size, sizeof(size));

                    // 書き込み
                    for (int i = 0; i < size; i++)
                    {
                        const auto *pMasterPos = (*itr)->m_Number->GetMasterPos(i);
                        const auto *pPercentPos = (*itr)->m_Number->GetPercentPos(i);
                        const auto *pcolor = (*itr)->m_Number->GetMasterColor(i);

                        fstream.write((char *)pMasterPos, sizeof(*pMasterPos));
                        fstream.write((char *)pPercentPos, sizeof(*pPercentPos));
                        fstream.write((char *)pcolor, sizeof(*pcolor));
                    }
                }
                else
                {
                    // 書き込み
                    fstream.write((char *)&size, sizeof(size));
                }

                // 終末文字ではない
                if (max_data != count)
                {
                    char endtag[256] = { "NOTEND" };
                    fstream.write((char *)&endtag, sizeof(endtag));
                }
                // 終末文字である
                if (max_data == count)
                {
                    // 終末文字
                    char endtag[256] = { "END" };
                    fstream.write((char *)&endtag, sizeof(endtag));
                }
            }
        }
    }

    // ファイルを閉じる
    fstream.close();
}

//==========================================================================
/**
@brief 全て読み取り
@param str_object [in] オブジェクト名
*/
void DX9_Object::LoadAll(const std::string * str_object)
{
    std::ifstream fstream;
    auto winsize = CVector2<float>((float)GetWinSize().x, (float)GetWinSize().y);
    int size = 0;
    const std::string str_endtag = "END";

    // 保存可能なデータのみを全て保存
    for (int id = (int)DX9_ObjectID::Default; id < (int)DX9_ObjectID::MAX; id++)
    {
        // イテレーターの先頭から最後まで回す
        for (auto itr = m_Object[id].begin(); itr != m_Object[id].end(); ++itr)
        {
            // ファイルを開く
            fstream.open(m_file_name, std::ios::in | std::ios::binary);

            // 読み取り
            for (; ;) 
            {
                char tag[512] = { 0 };
                char endtag[256] = { 0 };

                // tagの読み取り
                fstream.read((char *)&tag, sizeof(tag));

                //==========================================================================
                // データのサイズ取得
                fstream.read((char *)&size, sizeof(size));

                // 読み取り
                for (int i = 0; i < size; i++)
                {
                    // 3DObject param
                    D3DMATERIAL9 Material;
                    int Index = 0;
                    D3DXVECTOR3 LockAt;
                    D3DXVECTOR3 LockEye;
                    D3DXVECTOR3 LockUp;
                    D3DXVECTOR3 MatInfoPos;
                    D3DXVECTOR3 MatInfoRot;
                    D3DXVECTOR3 MatInfoSca;
                    DX9_3DObjectMatrixType MatrixType;
                    D3DXVECTOR3 VecFront;
                    D3DXVECTOR3 VecRight;
                    D3DXVECTOR3 VecUp;

                    fstream.read((char *)&Material, sizeof(Material));
                    fstream.read((char *)&Index, sizeof(Index));
                    fstream.read((char *)&LockAt, sizeof(LockAt));
                    fstream.read((char *)&LockEye, sizeof(LockEye));
                    fstream.read((char *)&LockUp, sizeof(LockUp));
                    fstream.read((char *)&MatInfoPos, sizeof(MatInfoPos));
                    fstream.read((char *)&MatInfoRot, sizeof(MatInfoRot));
                    fstream.read((char *)&MatInfoSca, sizeof(MatInfoSca));
                    fstream.read((char *)&MatrixType, sizeof(MatrixType));
                    fstream.read((char *)&VecFront, sizeof(VecFront));
                    fstream.read((char *)&VecRight, sizeof(VecRight));
                    fstream.read((char *)&VecUp, sizeof(VecUp));

                    if ((*itr)->m_ObjectName != nullptr && (*itr)->m_3DObject != nullptr)
                    {
                        // object
                        auto *pobj = (*itr)->m_3DObject->Get(i);

                        // tagが一致しているとき
                        // objectが存在
                        if (*(*itr)->m_ObjectName == tag && *str_object == *(*itr)->m_ObjectName && pobj != nullptr)
                        {
                            // D3DMATERIAL9
                            pobj->SetAmbient({ Material.Ambient.r,Material.Ambient.g,Material.Ambient.b,Material.Ambient.a });
                            pobj->SetDiffuse({ Material.Diffuse.r,Material.Diffuse.g,Material.Diffuse.b,Material.Diffuse.a });
                            pobj->SetEmissive({ Material.Emissive.r,Material.Emissive.g,Material.Emissive.b,Material.Emissive.a });
                            pobj->SetSpecular({ Material.Specular.r,Material.Specular.g,Material.Specular.b,Material.Specular.a });
                            // index
                            pobj->SetIndex(Index);
                            // LockAt
                            pobj->SetLockAt(LockAt);
                            // LockEye
                            pobj->SetLockEye(LockEye);
                            // LockUp
                            pobj->SetLockUp(LockUp);
                            // MatInfoPos
                            pobj->SetMatInfoPos(MatInfoPos);
                            // MatInfoRot
                            pobj->SetMatInfoRot(MatInfoRot);
                            // MatInfoSca
                            pobj->SetMatInfoSca(MatInfoSca);
                            // MatrixType
                            pobj->SetMatrixType(MatrixType);
                            // VecFront
                            pobj->SetVecFront(VecFront);
                            // VecRight
                            pobj->SetVecRight(VecRight);
                            // VecUp
                            pobj->SetVecUp(VecUp);
                        }
                    }
                }

                //==========================================================================
                // データのサイズ取得
                fstream.read((char *)&size, sizeof(size));

                // 読み取り
                for (int i = 0; i < size; i++)
                {
                    // 2DObject param
                    CVector2<float> Pos;
                    CVector2<float> PercentPos;
                    CColor<int> Color;
                    int Index = 0;
                    float Scale = 0.0f;
                    float Angle = 0.0f;

                    fstream.read((char *)&Index, sizeof(Index));
                    fstream.read((char *)&Pos, sizeof(Pos));
                    fstream.read((char *)&Scale, sizeof(Scale));
                    fstream.read((char *)&PercentPos, sizeof(PercentPos));
                    fstream.read((char *)&Color, sizeof(Color));
                    fstream.read((char *)&Angle, sizeof(Angle));

                    if ((*itr)->m_ObjectName != nullptr && (*itr)->m_2DObject != nullptr)
                    {
                        // object
                        auto *pobj = (*itr)->m_2DObject->Get(i);

                        // tagが一致しているとき
                        // objectが存在
                        if (*(*itr)->m_ObjectName == tag && *str_object == *(*itr)->m_ObjectName && pobj != nullptr)
                        {
                            pobj->SetIndex(Index);
                            pobj->SetPos(Pos);
                            pobj->SetScale(Scale);
                            pobj->SetPercentPos(PercentPos, winsize);
                            pobj->SetColor(Color);
                            pobj->SetAngle(Angle);
                        }
                    }
                }

                //==========================================================================
                // データのサイズ取得
                fstream.read((char *)&size, sizeof(size));

                // 読み取り
                for (int i = 0; i < size; i++)
                {
                    CVector2<float> MasterPos;
                    CVector2<float> PercentPos;
                    const CColor<int> Color;

                    fstream.read((char *)&MasterPos, sizeof(MasterPos));
                    fstream.read((char *)&PercentPos, sizeof(PercentPos));
                    fstream.read((char *)&Color, sizeof(Color));

                    if ((*itr)->m_ObjectName != nullptr && (*itr)->m_Number != nullptr)
                    {
                        // tagが一致しているとき
                        // objectが存在
                        if (*(*itr)->m_ObjectName == tag && *str_object == *(*itr)->m_ObjectName && (*itr)->m_Number->GetMasterPos(i) != nullptr)
                        {
                            (*itr)->m_Number->SetPos(i, MasterPos);
                            (*itr)->m_Number->SetPercentPos(i, PercentPos, winsize);
                            (*itr)->m_Number->SetMasterColor(i, Color);
                        }
                    }
                }

                fstream.read((char *)&endtag, sizeof(endtag));

                // 終末文字の検出
                if (str_endtag == endtag)
                {
                    break;
                }

                if ((*itr)->m_ObjectName != nullptr)
                {
                    // 読み取る対象が見つかっている
                    if (*(*itr)->m_ObjectName == tag && (*itr)->m_ObjectName == str_object)
                    {
                        return;
                    }
                }
            }
            // ファイルを閉じる
            fstream.close();
        }
    }
}

//==========================================================================
/**
@brief 機能
*/
void DX9_Object::System(void)
{
#if defined(_SETImGui_Dx9_) || defined(_DEBUG) || defined(DEBUG)
    DX9_ImGui Imgui;

    float new_time = 0.0f;
    static float oldtime = 0.0f;
    static bool bdebug = false;

    if (Imgui.NewMenu("Object list"))
    {
        // 保存可能なデータのみを全て保存
        for (int id = (int)DX9_ObjectID::Default; id < (int)DX9_ObjectID::MAX; id++)
        {
            // イテレーターの先頭から最後まで回す
            for (auto itr = m_Object[id].begin(); itr != m_Object[id].end(); ++itr)
            {
                if ((*itr)->m_ObjectName != nullptr)
                {
                    new_time += (*itr)->_ProcessingTimeCount();
                    if ((*itr)->ImGui()->NewMenu((*itr)->m_ObjectName->c_str()))
                    {
                        (*itr)->ImGui()->Checkbox("Object system ON/OFF", (*itr)->m_debug_key);
                        if ((*itr)->m_debug_key)
                        {
                            //==========================================================================
                            // デフォルト機能 開始
                            //==========================================================================
                            (*itr)->_3DObjectParameterDebug();
                            (*itr)->_2DObjectParameterDebug();
                            (*itr)->_CameraParameterDebug();
                            (*itr)->_XInputParameterDebug();
                            (*itr)->_XModelParameterDebug();
                            (*itr)->_ResourceDebug();
                            (*itr)->_ProcessingTime(oldtime);
                            (*itr)->Editor2DObject();
                            //==========================================================================
                            // デフォルト機能 終了
                            //==========================================================================
                            (*itr)->Debug();
                        }
                        (*itr)->ImGui()->EndMenu();
                    }
                }
            }
        }
        Imgui.EndMenu();
    }

    if (Imgui.Checkbox("All debugging functions ON/OFF", bdebug))
    {
        for (int id = (int)DX9_ObjectID::Default; id < (int)DX9_ObjectID::MAX; id++)
        {
            // イテレーターの先頭から最後まで回す
            for (auto itr = m_Object[id].begin(); itr != m_Object[id].end(); ++itr)
            {
                if ((*itr)->m_ObjectName != nullptr)
                {
                    (*itr)->m_debug_key = bdebug;
                }
            }
        }
    }
    oldtime = new_time;
#endif
}

//==========================================================================
/**
@brief 描画の準備
@param ObjectID [in] オブジェクトID
*/
void DX9_Object::DrawPreparation(DX9_ObjectID ObjectID)
{
    auto pDevice = GetDirectX9Device();
    switch (ObjectID)
    {
    case DX9_ObjectID::Default:
        // ライト設定
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);
        break;
    case DX9_ObjectID::Grid:
        // ライト設定
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);
        break;
    case DX9_ObjectID::Field:
        // ライト設定
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);
        break;
    case DX9_ObjectID::Cube:
        //アルファ画像によるブレンド
        SetRenderALPHAREF_START(pDevice, 100);
        break;
    case DX9_ObjectID::Mesh:
        // ライト設定
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);

        //アルファ画像によるブレンド
        SetRenderALPHAREF_START(pDevice, 100);
        break;
    case DX9_ObjectID::Shadow:
        // 減算処理
        SetRenderREVSUBTRACT(pDevice);
        break;
    case DX9_ObjectID::Sphere:
        // パンチ抜き
        SetRenderALPHAREF_START(pDevice, 100);
        //pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, D3DZB_FALSE); //アルファブレンディングの無効化
        pDevice->SetRenderState(D3DRS_WRAP0, D3DWRAPCOORD_0);
        pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW); // 反時計回りの面を消去
        pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
        break;
    case DX9_ObjectID::Xmodel:
        // ライト設定
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);
        break;
    case DX9_ObjectID::Billboard:
        // ライト設定
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);

        // パンチ抜き
        SetRenderALPHAREF_START(pDevice, 100);
        break;
    case DX9_ObjectID::Polygon2D:
        // 画質を高画質にする
        SamplerFitteringGraphical(pDevice);

        // Zバッファの無効
        SetRenderZENABLE_START(pDevice);
        break;
    case DX9_ObjectID::Text:
        break;
    case DX9_ObjectID::MAX:
        break;
    default:
        break;
    }
}

//==========================================================================
/**
@brief 描画の後処理
@param ObjectID [in] オブジェクトID
*/
void DX9_Object::DrawPostProcessing(DX9_ObjectID ObjectID)
{
    auto pDevice = GetDirectX9Device();
    switch (ObjectID)
    {
    case DX9_ObjectID::Default:
        break;
    case DX9_ObjectID::Grid:
        break;
    case DX9_ObjectID::Field:
        break;
    case DX9_ObjectID::Cube:
        SetRenderALPHAREF_END(pDevice);
        break;
    case DX9_ObjectID::Mesh:
        SetRenderALPHAREF_END(pDevice);
        break;
    case DX9_ObjectID::Shadow:
        // 戻す
        SetRenderADD(pDevice);
        break;
    case DX9_ObjectID::Sphere:
        SetRenderALPHAREF_END(pDevice);
        //pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, D3DZB_TRUE); // αブレンドを許可
        pDevice->SetRenderState(D3DRS_WRAP0, 0);
        break;
    case DX9_ObjectID::Xmodel:
        break;
    case DX9_ObjectID::Billboard:
        // パンチ抜き終了
        SetRenderALPHAREF_END(pDevice);
        break;
    case DX9_ObjectID::Polygon2D:
        // Zバッファの有効化
        SetRenderZENABLE_END(pDevice);

        // 通常の画質に戻す
        SamplerFitteringLINEAR(pDevice);
        break;
    case DX9_ObjectID::Text:
        break;
    case DX9_ObjectID::MAX:
        break;
    default:
        break;
    }
}

//==========================================================================
/**
@brief デバッグの描画
@param NumLine [in] 線の本数
@param pMatrix [in] マトリックス
@param pDevice [in] デバイス
*/
void DX9_Object::DebugDraw3D(int NumLine, const D3DXMATRIX *pMatrix, LPDIRECT3DDEVICE9 pDevice)
{
    // 向きベクトル情報
    constexpr float fPosZ[] = { 1.0f,-1.0f,-0.8f,-0.8f ,-1.0f };
    constexpr float fPosX[] = { 0.0f,0.0f,0.1f,-0.1f,0.0f };
    constexpr int nNumSize = sizeof(fPosZ) / sizeof(float);

    float C_R = D3DX_PI * 2 / NumLine;	// 円周率
    D3DXMATRIX aMtxWorld; // ワールド行列

    if (pDevice != nullptr&&pMatrix != nullptr)
    {
        auto *RoundX = new debug_object_vertex_3d[NumLine + 1];
        auto *RoundY = new debug_object_vertex_3d[NumLine + 1];
        auto *lineX = new debug_object_vertex_3d[nNumSize];
        auto *lineY = new debug_object_vertex_3d[nNumSize];
        auto *lineZ = new debug_object_vertex_3d[nNumSize];

        // 円の生成
        for (int i = 0; i < (NumLine + 1); i++)
        {
            RoundX[i].pos.x = ((0.0f + cosf(C_R * i)));
            RoundX[i].pos.y = 0.0f;
            RoundX[i].pos.z = ((0.0f + sinf(C_R * i)));
            RoundX[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);

            RoundY[i].pos.x = 0.0f;
            RoundY[i].pos.y = ((0.0f + cosf(C_R * i)));
            RoundY[i].pos.z = ((0.0f + sinf(C_R * i)));
            RoundY[i].color = D3DCOLOR_RGBA(0, 255, 0, 255);
        }

        // 矢印の生成
        for (int i = 0; i < nNumSize; i++)
        {
            lineX[i].pos.z = fPosX[i];
            lineX[i].pos.x = fPosZ[i];
            lineX[i].pos.y = 0.0f;
            lineX[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);

            lineY[i].pos.z = fPosX[i];
            lineY[i].pos.x = 0.0f;
            lineY[i].pos.y = -fPosZ[i];
            lineY[i].color = D3DCOLOR_RGBA(0, 255, 0, 255);

            lineZ[i].pos.z = fPosZ[i];
            lineZ[i].pos.x = fPosX[i];
            lineZ[i].pos.y = 0.0f;
            lineZ[i].color = D3DCOLOR_RGBA(0, 0, 255, 255);
        }

        // FVFの設定
        pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);

        // サイズ
        pDevice->SetTransform(D3DTS_WORLD, pMatrix);
        pDevice->SetTexture(0, nullptr);

        // 描画
        pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, NumLine, RoundX, sizeof(debug_object_vertex_3d));
        pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, NumLine, RoundY, sizeof(debug_object_vertex_3d));
        pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, (nNumSize - 1), lineZ, sizeof(debug_object_vertex_3d));
        pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, (nNumSize - 1), lineX, sizeof(debug_object_vertex_3d));
        pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, (nNumSize - 1), lineY, sizeof(debug_object_vertex_3d));

        // メモリの解放
        delete[]RoundX;
        delete[]RoundY;
        delete[]lineZ;
        delete[]lineX;
        delete[]lineY;
    }
}

//==========================================================================
/**
@brief デバッグの描画
@param SetPos [in] 座標
@param SetTexSize [in] テクスチャのサイズ
@param pDevice [in] デバイス
*/
void DX9_Object::DebugDraw2D(const D3DXVECTOR4 *pos1, const D3DXVECTOR4 *pos2, const D3DXVECTOR4 *pos3, const D3DXVECTOR4 *pos4, LPDIRECT3DDEVICE9 pDevice)
{
    constexpr int num_vertex = 6;
    int ncount = -1;
    auto* pV = new debug_object_vertex_2d[num_vertex + 1];

    ncount++;
    pV[ncount].pos.x = (float)(pos1->x);
    pV[ncount].pos.y = (float)(pos1->y);
    pV[ncount].pos.z = 1.0f;
    pV[ncount].pos.w = 1.0f;
    pV[ncount].color = D3DCOLOR_RGBA(255, 255, 255, 255);

    ncount++;
    pV[ncount].pos.x = (float)(pos3->x);
    pV[ncount].pos.y = (float)(pos3->y);
    pV[ncount].pos.z = 1.0f;
    pV[ncount].pos.w = 1.0f;
    pV[ncount].color = D3DCOLOR_RGBA(255, 255, 255, 255);

    ncount++;
    pV[ncount].pos.x = (float)(pos4->x);
    pV[ncount].pos.y = (float)(pos4->y);
    pV[ncount].pos.z = 1.0f;
    pV[ncount].pos.w = 1.0f;
    pV[ncount].color = D3DCOLOR_RGBA(255, 255, 255, 255);

    ncount++;
    pV[ncount].pos.x = (float)(pos2->x);
    pV[ncount].pos.y = (float)(pos2->y);
    pV[ncount].pos.z = 1.0f;
    pV[ncount].pos.w = 1.0f;
    pV[ncount].color = D3DCOLOR_RGBA(255, 255, 255, 255);

    ncount++;
    pV[ncount].pos.x = (float)(pos1->x);
    pV[ncount].pos.y = (float)(pos1->y);
    pV[ncount].pos.z = 1.0f;
    pV[ncount].pos.w = 1.0f;
    pV[ncount].color = D3DCOLOR_RGBA(255, 255, 255, 255);

    ncount++;
    pV[ncount].pos.x = (float)(pos3->x);
    pV[ncount].pos.y = (float)(pos3->y);
    pV[ncount].pos.z = 1.0f;
    pV[ncount].pos.w = 1.0f;
    pV[ncount].color = D3DCOLOR_RGBA(255, 255, 255, 255);

    ncount++;
    pV[ncount].pos.x = (float)(pos2->x);
    pV[ncount].pos.y = (float)(pos2->y);
    pV[ncount].pos.z = 1.0f;
    pV[ncount].pos.w = 1.0f;
    pV[ncount].color = D3DCOLOR_RGBA(255, 255, 255, 255);

    pDevice->SetTexture(0, nullptr);
    pDevice->SetFVF(D3DFVF_XYZRHW | D3DFVF_DIFFUSE);
    pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, num_vertex, pV, sizeof(debug_object_vertex_2d));

    delete[]pV;
}

//==========================================================================
/**
@brief デバッグオブジェクト3D
@param ObjectID [in] オブジェクトID
*/
void DX9_Object::Debug3DObjectDraw(DX9_ObjectID ObjectID)
{
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
    auto pDevice = GetDirectX9Device();
    D3DXMATRIX mat;

    pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);

    // イテレーターの先頭から最後まで回す
    for (auto itr = m_Object[(int)ObjectID].begin(); itr != m_Object[(int)ObjectID].end(); ++itr)
    {
        // デバッグキーが有効
        if ((*itr)->m_debug_key == true)
        {
            // オブジェクトが存在
            if ((*itr)->m_3DObject != nullptr)
            {
                auto * p_obj = (*itr)->m_3DObject;
                for (int obj = 0; obj < p_obj->Size(); obj++)
                {
                    DebugDraw3D(30, p_obj->Get(obj)->CreateMtxWorld(mat), pDevice);
                }
            }

            // オブジェクトが存在
            if ((*itr)->m_debug_object3d != nullptr)
            {
                for (auto itr2 = (*itr)->m_debug_object3d->begin(); itr2 != (*itr)->m_debug_object3d->end(); ++itr2)
                {
                    DebugDraw3D(30, (*itr2)->CreateMtxWorld(mat), pDevice);
                }
                // 描画後破棄
                (*itr)->m_debug_object3d->clear();
            }
        }
    }
    pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);
#else
    ObjectID;
#endif
}

//==========================================================================
/**
@brief デバッグオブジェクト2D
@param ObjectID [in] オブジェクトID
*/
void DX9_Object::Debug2DObjectDraw(DX9_ObjectID ObjectID)
{
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
    auto pDevice = GetDirectX9Device();

    pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);
    pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);

    // イテレーターの先頭から最後まで回す
    for (auto itr = m_Object[(int)ObjectID].begin(); itr != m_Object[(int)ObjectID].end(); ++itr)
    {
        // デバッグキーが有効
        if ((*itr)->m_debug_key == true)
        {
            // オブジェクトが存在
            if ((*itr)->m_2DObject != nullptr)
            {
                auto * p_obj = (*itr)->m_2DObject;
                for (int obj = 0; obj < p_obj->Size(); obj++)
                {
                    auto *pData = p_obj->Get(obj);
                    if (pData->GetVertexPath(0) != nullptr)
                    {
                        DebugDraw2D(&pData->GetVertexPath(0)->pos, &pData->GetVertexPath(1)->pos, &pData->GetVertexPath(2)->pos, &pData->GetVertexPath(3)->pos, pDevice);
                    }
                }
            }
        }
    }
    pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
    pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);
#else
    ObjectID;
#endif
}

//==========================================================================
/**
@brief 2Dオブジェクトのエディタ
*/
void DX9_Object::Editor2DObject(void)
{
    // オブジェクトタイプ数回す
    if (this->m_2DObject != nullptr || this->m_Number != nullptr)
    {
        if (this->ImGui()->NewMenu("2D Object Editor."))
        {
            if (this->m_2DObject != nullptr)
            {
                for (int obj_id = 0; obj_id < this->m_2DObject->Size(); obj_id++)
                {
                    auto * pobj = this->m_2DObject->Get(obj_id);
                    auto str_name = this->m_ImGui.CreateText("2DObjectID : %d", obj_id);
                    if (this->m_ImGui.NewMenu(str_name.c_str()))
                    {
                        float fRot = pobj->GetAngle();
                        auto vPercent = *pobj->GetPercentPos();
                        auto vWinSize = CVector2<float>((float)this->GetWinSize().x, (float)this->GetWinSize().y);
                        auto Color = *pobj->GetColor();

                        if (vPercent == 0.0f)
                        {
                            vPercent = (*const_cast<CVector2<float>*>(pobj->GetPos())) / vWinSize;
                        }

                        this->m_ImGui.Text("pos [x:%f,y:%f]", pobj->GetPos()->x, pobj->GetPos()->y);
                        this->m_ImGui.Text("proportion [x:%f,y:%f]", vPercent.x, vPercent.y);
                        this->m_ImGui.Text("collar [r:%d,g:%d,b:%d,a:%d]", Color.r, Color.g, Color.b, Color.a);
                        this->m_ImGui.Text("rotation angle [%f]", fRot);

                        this->m_ImGui.SliderFloat("Coordinate adjustment by ratio[X]", &vPercent.x, -1.0f, 1.0f);
                        this->m_ImGui.SliderFloat("Coordinate adjustment by ratio[Y]", &vPercent.y, -1.0f, 1.0f);
                        this->m_ImGui.SliderFloat("Angle change", &fRot, -3.14159274f - 3.14159274f, 3.14159274f + 3.14159274f);
                        pobj->SetAngle(fRot);
                        pobj->SetPercentPos(vPercent, vWinSize);

                        if (this->m_ImGui.NewTreeNode("Collar Editor.", false))
                        {
                            auto imcolor = this->m_ImGui.ColorEdit({ (float)Color.r,(float)Color.g,(float)Color.b,(float)Color.a });
                            Color = CColor<int>((int)(imcolor.x), (int)(imcolor.y), (int)(imcolor.z), (int)(imcolor.w));
                            pobj->SetColor(Color);
                            this->m_ImGui.EndTreeNode();
                        }
                        this->m_ImGui.EndMenu();
                    }
                }
            }
            if (this->m_Number != nullptr)
            {
                for (int obj_id = 0; obj_id < this->m_Number->GetSize(); obj_id++)
                {
                    auto str_name = this->m_ImGui.CreateText("NumberObjectID : %d", obj_id);
                    if (this->m_ImGui.NewMenu(str_name.c_str()))
                    {
                        auto vPercent = *this->m_Number->GetPercentPos(obj_id);
                        auto vWinSize = CVector2<float>((float)this->GetWinSize().x, (float)this->GetWinSize().y);
                        auto Color = *this->m_Number->GetMasterColor(obj_id);

                        this->m_ImGui.Text("pos [x:%f,y:%f]", this->m_Number->GetMasterPos(obj_id)->x, this->m_Number->GetMasterPos(obj_id)->y);
                        this->m_ImGui.Text("proportion [x:%f,y:%f]", vPercent.x, vPercent.y);
                        this->m_ImGui.Text("collar [r:%d,g:%d,b:%d,a:%d]", Color.r, Color.g, Color.b, Color.a);

                        this->m_ImGui.SliderFloat("Coordinate adjustment by ratio[X]", &vPercent.x, -1.0f, 1.0f);
                        this->m_ImGui.SliderFloat("Coordinate adjustment by ratio[Y]", &vPercent.y, -1.0f, 1.0f);
                        this->m_Number->SetPercentPos(obj_id, vPercent, vWinSize);
                        if (this->m_ImGui.NewTreeNode("Collar Editor", false))
                        {
                            auto imcolor = this->m_ImGui.ColorEdit({ (float)Color.r,(float)Color.g,(float)Color.b,(float)Color.a });
                            Color = CColor<int>((int)(imcolor.x), (int)(imcolor.y), (int)(imcolor.z), (int)(imcolor.w));
                            this->m_Number->SetMasterColor(obj_id, Color);
                            this->m_ImGui.EndTreeNode();
                        }
                        this->m_ImGui.EndMenu();
                    }
                }
            }
            this->ImGui()->EndMenu();
        }
        if (this->ImGui()->MenuItem("Save"))
        {
            this->SaveAll();
        }
    }
}

//==========================================================================
/**
@brief 3Dオブジェクトのパラメーターチェック
*/
void DX9_Object::_3DObjectParameterDebug(void)
{
    // オブジェクトが存在する
    if (this->m_3DObject != nullptr || this->m_debug_object3d != nullptr)
    {
        if (this->m_ImGui.NewMenu("Parameters of the 3D object."))
        {
            int i = 0;
            if (this->m_3DObject != nullptr)
            {
                // インスタンスの数だけ回す
                for (i = 0; i < this->m_3DObject->Size(); i++)
                {
                    auto * p_obj = this->m_3DObject->Get(i);
                    const auto str_text = this->m_ImGui.CreateText("ObjectID : %d", i);
                    if (this->m_ImGui.NewMenu(str_text.c_str()))
                    {
                        this->m_ImGui.Text("resource ID : %d", p_obj->GetIndex());
                        this->m_ImGui.Text("matrix ID : %d", (int)p_obj->GetMatrixType());
                        this->m_ImGui.Separator();
                        auto * p_at = p_obj->GetLockAt();
                        auto * p_eye = p_obj->GetLockEye();
                        auto * p_up = p_obj->GetLockUp();
                        auto * p_pos = p_obj->GetMatInfoPos();
                        auto * p_rot = p_obj->GetMatInfoRot();
                        auto * p_sca = p_obj->GetMatInfoSca();
                        auto * p_qua = p_obj->GetQuaternion();
                        auto * p_vec_front = p_obj->GetVecFront();
                        auto * p_vec_rigth = p_obj->GetVecRight();
                        auto * p_vec_up = p_obj->GetVecUp();

                        this->m_ImGui.Text("at : x[%.2f]:y[%.2f]:z[%.2f]", p_at->x, p_at->y, p_at->z);
                        this->m_ImGui.Text("eye : x[%.2f]:y[%.2f]:z[%.2f]", p_eye->x, p_eye->y, p_eye->z);
                        this->m_ImGui.Text("up : x[%.2f]:y[%.2f]:z[%.2f]", p_up->x, p_up->y, p_up->z);
                        this->m_ImGui.Separator();
                        this->m_ImGui.Text("pos : x[%.2f]:y[%.2f]:z[%.2f]", p_pos->x, p_pos->y, p_pos->z);
                        this->m_ImGui.Text("rot : x[%.2f]:y[%.2f]:z[%.2f]", p_rot->x, p_rot->y, p_rot->z);
                        this->m_ImGui.Text("scale : x[%.2f]:y[%.2f]:z[%.2f]", p_sca->x, p_sca->y, p_sca->z);
                        this->m_ImGui.Text("Quaternion : x[%.2f]:y[%.2f]:z[%.2f]", p_qua->x, p_qua->y, p_qua->z);
                        this->m_ImGui.Separator();
                        this->m_ImGui.Text("front vector : x[%.2f]:y[%.2f]:z[%.2f]", p_vec_front->x, p_vec_front->y, p_vec_front->z);
                        this->m_ImGui.Text("rigth vector : x[%.2f]:y[%.2f]:z[%.2f]", p_vec_rigth->x, p_vec_rigth->y, p_vec_rigth->z);
                        this->m_ImGui.Text("up vector : x[%.2f]:y[%.2f]:z[%.2f]", p_vec_up->x, p_vec_up->y, p_vec_up->z);
                        this->m_ImGui.EndMenu();
                    }
                }
            }

            // 特殊なオブジェクトが登録されている場合
            if (this->m_debug_object3d != nullptr)
            {
                // イテレーターの最初から最後まで回す
                for (auto itr = this->m_debug_object3d->begin(); itr != this->m_debug_object3d->end(); ++itr, i++)
                {
                    auto * p_obj = (*itr);
                    const auto str_text = this->m_ImGui.CreateText("ObjectID : %d", i);
                    if (this->m_ImGui.NewMenu(str_text.c_str()))
                    {
                        this->m_ImGui.Text("resource ID : %d", p_obj->GetIndex());
                        this->m_ImGui.Text("matrix ID : %d", (int)p_obj->GetMatrixType());
                        this->m_ImGui.Separator();
                        auto * p_at = p_obj->GetLockAt();
                        auto * p_eye = p_obj->GetLockEye();
                        auto * p_up = p_obj->GetLockUp();
                        auto * p_pos = p_obj->GetMatInfoPos();
                        auto * p_rot = p_obj->GetMatInfoRot();
                        auto * p_sca = p_obj->GetMatInfoSca();
                        auto * p_qua = p_obj->GetQuaternion();
                        auto * p_vec_front = p_obj->GetVecFront();
                        auto * p_vec_rigth = p_obj->GetVecRight();
                        auto * p_vec_up = p_obj->GetVecUp();

                        this->m_ImGui.Text("at : x[%.2f]:y[%.2f]:z[%.2f]", p_at->x, p_at->y, p_at->z);
                        this->m_ImGui.Text("eye : x[%.2f]:y[%.2f]:z[%.2f]", p_eye->x, p_eye->y, p_eye->z);
                        this->m_ImGui.Text("up : x[%.2f]:y[%.2f]:z[%.2f]", p_up->x, p_up->y, p_up->z);
                        this->m_ImGui.Separator();
                        this->m_ImGui.Text("pos : x[%.2f]:y[%.2f]:z[%.2f]", p_pos->x, p_pos->y, p_pos->z);
                        this->m_ImGui.Text("rot : x[%.2f]:y[%.2f]:z[%.2f]", p_rot->x, p_rot->y, p_rot->z);
                        this->m_ImGui.Text("scale : x[%.2f]:y[%.2f]:z[%.2f]", p_sca->x, p_sca->y, p_sca->z);
                        this->m_ImGui.Text("Quaternion : x[%.2f]:y[%.2f]:z[%.2f]", p_qua->x, p_qua->y, p_qua->z);
                        this->m_ImGui.Separator();
                        this->m_ImGui.Text("front vector : x[%.2f]:y[%.2f]:z[%.2f]", p_vec_front->x, p_vec_front->y, p_vec_front->z);
                        this->m_ImGui.Text("rigth vector : x[%.2f]:y[%.2f]:z[%.2f]", p_vec_rigth->x, p_vec_rigth->y, p_vec_rigth->z);
                        this->m_ImGui.Text("up vector : x[%.2f]:y[%.2f]:z[%.2f]", p_vec_up->x, p_vec_up->y, p_vec_up->z);
                        this->m_ImGui.EndMenu();
                    }
                }
            }
            this->m_ImGui.EndMenu();
        }
    }
}

//==========================================================================
/**
@brief カメラオブジェクトのパラメーターチェック
*/
void DX9_Object::_CameraParameterDebug(void)
{
    // カメラオブジェクトが存在する
    if (this->m_Camera != nullptr)
    {
        if (this->m_ImGui.NewMenu("Parameters of the camera object."))
        {
            for (int i = 0; i < this->m_Camera->Size(); i++)
            {
                auto *pObj = this->m_Camera->Get(i);
                const auto str_text = this->m_ImGui.CreateText("ObjectID : %d", i);
                if (this->m_ImGui.NewMenu(str_text.c_str()))
                {
                    auto vector_front = pObj->GetVECTOR(DX9_CameraVectorList::VECFRONT);
                    auto vector_right = pObj->GetVECTOR(DX9_CameraVectorList::VECRIGHT);
                    auto vector_up = pObj->GetVECTOR(DX9_CameraVectorList::VECUP);
                    auto v_at = pObj->GetVECTOR(DX9_CameraVectorList::VAT);
                    auto v_eye = pObj->GetVECTOR(DX9_CameraVectorList::VEYE);
                    auto v_up = pObj->GetVECTOR(DX9_CameraVectorList::VUP);
                    auto f_restrictionX = pObj->GetRestriction_X();
                    auto f_restrictionY = pObj->GetRestriction_Y();
                    auto f_restrictionZ = pObj->GetRestriction_Z();

                    this->m_ImGui.Text("front vector : x[%.2f]:y[%.2f]:z[%.2f]", vector_front.x, vector_front.y, vector_front.z);
                    this->m_ImGui.Text("right vector : x[%.2f]:y[%.2f]:z[%.2f]", vector_right.x, vector_right.y, vector_right.z);
                    this->m_ImGui.Text("up vector : x[%.2f]:y[%.2f]:z[%.2f]", vector_up.x, vector_up.y, vector_up.z);
                    this->m_ImGui.Text("at : x[%.2f]:y[%.2f]:z[%.2f]", v_at.x, v_at.y, v_at.z);
                    this->m_ImGui.Text("eye : x[%.2f]:y[%.2f]:z[%.2f]", v_eye.x, v_eye.y, v_eye.z);
                    this->m_ImGui.Text("up : x[%.2f]:y[%.2f]:z[%.2f]", v_up.x, v_up.y, v_up.z);
                    this->m_ImGui.Text("Restriction X : angle[%.2f]", f_restrictionX);
                    this->m_ImGui.Text("Restriction Y : angle[%.2f]", f_restrictionY);
                    this->m_ImGui.Text("Restriction Z : angle[%.2f]", f_restrictionZ);

                    this->m_ImGui.EndMenu();
                }
            }
            this->m_ImGui.EndMenu();
        }
    }
}

//==========================================================================
/**
@brief XModelのパラメーターチェック
*/
void DX9_Object::_XModelParameterDebug(void)
{
    // オブジェクトが存在する
    if (this->m_Xmodel != nullptr)
    {
        if (this->m_ImGui.NewMenu("Information on the X model in use."))
        {
            for (int i = 0; i < this->m_Xmodel->Size(); i++)
            {
                auto * p_data = this->m_Xmodel->GetMdelDataParam(i);
                auto * p_element = p_data->GetVertexElement();
                const auto & str_name = this->m_ImGui.CreateText("%d : %s", i, p_data->gettag());
                if (this->m_ImGui.NewMenu(str_name.c_str()))
                {
                    for (int i_2 = 0; i_2 < p_element->Size(); i_2++)
                    {
                        const auto & str_id = this->m_ImGui.CreateText("ID : %2d", i_2);
                        auto * p_param = p_element->Get(i_2);
                        if (this->m_ImGui.NewMenu(str_id.c_str()))
                        {
                            this->m_ImGui.Text("Stream index : %d", (int)p_param->Stream);
                            this->m_ImGui.Text("Offset in the stream in bytes : %d", (int)p_param->Offset);
                            this->m_ImGui.Text("Data type : %d", (int)p_param->Type);
                            this->m_ImGui.Text("Processing method : %d", (int)p_param->Method);
                            this->m_ImGui.Text("Semantics : %d", (int)p_param->Usage);
                            this->m_ImGui.Text("Semantic index : %d", (int)p_param->UsageIndex);
                            this->m_ImGui.EndMenu();
                        }
                    }
                    this->m_ImGui.EndMenu();
                }
            }
            this->m_ImGui.EndMenu();
        }
    }
}

//==========================================================================
/**
@brief 2Dオブジェクトのパラメーターチェック
*/
void DX9_Object::_2DObjectParameterDebug(void)
{
    // オブジェクトが存在するとき
    if (this->m_2DObject != nullptr)
    {
        if (this->m_ImGui.NewMenu("Parameters of the 2D object."))
        {
            // インスタンスの数だけ回す
            for (int i = 0; i < this->m_2DObject->Size(); i++)
            {
                auto * p_obj = this->m_2DObject->Get(i);
                const auto str_text = this->m_ImGui.CreateText("ObjectID : %d", i);
                if (this->m_ImGui.NewMenu(str_text.c_str()))
                {
                    this->m_ImGui.Text("resource ID : %d", p_obj->GetIndex());
                    auto * p_tex = p_obj->GetTexSize();
                    this->m_ImGui.Text("texture size : w[%.2f]:h[%.2f]", p_tex->w, p_tex->h);
                    auto * p_pos = p_obj->GetPos();
                    this->m_ImGui.Text("pos : x[%.2f]:y[%.2f]", p_pos->x, p_pos->y);
                    this->m_ImGui.Text("scale : %.2f", p_obj->GetScale());
                    auto  p_anim = p_obj->GetAnimParam();
                    if (p_anim->Key == true)
                    {
                        this->m_ImGui.Text("animation : %2d/%2d", p_anim->Count, p_anim->Frame*p_anim->Pattern);
                    }
                    this->m_ImGui.EndMenu();
                }
            }
            this->m_ImGui.EndMenu();
        }
    }
}

//==========================================================================
/**
@brief XInputのパラメーターデバッグ
*/
void DX9_Object::_XInputParameterDebug(void)
{
    // オブジェクトが存在するとき
    if (this->m_XInput != nullptr)
    {
        if (this->m_ImGui.NewMenu("Parameters of the XInput object."))
        {
            static const char * pcbool[] = { "false","true" };

            // コントローラーの数だけ回す
            for (int i = 0; i < this->m_XInput->Size(); i++)
            {
                const auto str_text = this->m_ImGui.CreateText("XInputID : %d", i);
                if (this->m_ImGui.NewMenu(str_text.c_str()))
                {
                    // コントローラーのチェック
                    if (this->m_XInput->Check(i))
                    {
                        if (this->m_ImGui.NewMenu("Press"))
                        {
                            this->m_ImGui.Text("A Button : %s", pcbool[this->m_XInput->Press(XInputButton::A, i)]);
                            this->m_ImGui.Text("B Button : %s", pcbool[this->m_XInput->Press(XInputButton::B, i)]);
                            this->m_ImGui.Text("X Button : %s", pcbool[this->m_XInput->Press(XInputButton::X, i)]);
                            this->m_ImGui.Text("Y Button : %s", pcbool[this->m_XInput->Press(XInputButton::Y, i)]);
                            this->m_ImGui.Text("START Button : %s", pcbool[this->m_XInput->Press(XInputButton::START, i)]);
                            this->m_ImGui.Text("BACK Button : %s", pcbool[this->m_XInput->Press(XInputButton::BACK, i)]);
                            this->m_ImGui.Text("the cross key Left : %s", pcbool[this->m_XInput->Press(XInputButton::DPAD_LEFT, i)]);
                            this->m_ImGui.Text("the cross key Right : %s", pcbool[this->m_XInput->Press(XInputButton::DPAD_RIGHT, i)]);
                            this->m_ImGui.Text("the cross key Up : %s", pcbool[this->m_XInput->Press(XInputButton::DPAD_UP, i)]);
                            this->m_ImGui.Text("the cross key Down : %s", pcbool[this->m_XInput->Press(XInputButton::DPAD_DOWN, i)]);
                            this->m_ImGui.Text("LB Button : %s", pcbool[this->m_XInput->Press(XInputButton::LEFT_LB, i)]);
                            this->m_ImGui.Text("RB Button : %s", pcbool[this->m_XInput->Press(XInputButton::RIGHT_RB, i)]);
                            this->m_ImGui.Text("Left Analog Stick Button : %s", pcbool[this->m_XInput->Press(XInputButton::LEFT_THUMB, i)]);
                            this->m_ImGui.Text("Right Analog Stick Button : %s", pcbool[this->m_XInput->Press(XInputButton::RIGHT_THUMB, i)]);
                            this->m_ImGui.EndMenu();
                        }
                        if (this->m_ImGui.NewMenu("Trigger"))
                        {
                            this->m_ImGui.Text("A Button : %s", pcbool[this->m_XInput->Trigger(XInputButton::A, i)]);
                            this->m_ImGui.Text("B Button : %s", pcbool[this->m_XInput->Trigger(XInputButton::B, i)]);
                            this->m_ImGui.Text("X Button : %s", pcbool[this->m_XInput->Trigger(XInputButton::X, i)]);
                            this->m_ImGui.Text("Y Button : %s", pcbool[this->m_XInput->Trigger(XInputButton::Y, i)]);
                            this->m_ImGui.Text("START Button : %s", pcbool[this->m_XInput->Trigger(XInputButton::START, i)]);
                            this->m_ImGui.Text("BACK Button : %s", pcbool[this->m_XInput->Trigger(XInputButton::BACK, i)]);
                            this->m_ImGui.Text("the cross key Left : %s", pcbool[this->m_XInput->Trigger(XInputButton::DPAD_LEFT, i)]);
                            this->m_ImGui.Text("the cross key Right : %s", pcbool[this->m_XInput->Trigger(XInputButton::DPAD_RIGHT, i)]);
                            this->m_ImGui.Text("the cross key Up : %s", pcbool[this->m_XInput->Trigger(XInputButton::DPAD_UP, i)]);
                            this->m_ImGui.Text("the cross key Down : %s", pcbool[this->m_XInput->Trigger(XInputButton::DPAD_DOWN, i)]);
                            this->m_ImGui.Text("LB Button : %s", pcbool[this->m_XInput->Trigger(XInputButton::LEFT_LB, i)]);
                            this->m_ImGui.Text("RB Button : %s", pcbool[this->m_XInput->Trigger(XInputButton::RIGHT_RB, i)]);
                            this->m_ImGui.Text("Left Analog Stick Button : %s", pcbool[this->m_XInput->Trigger(XInputButton::LEFT_THUMB, i)]);
                            this->m_ImGui.Text("Right Analog Stick Button : %s", pcbool[this->m_XInput->Trigger(XInputButton::RIGHT_THUMB, i)]);
                            this->m_ImGui.EndMenu();
                        }
                        if (this->m_ImGui.NewMenu("Release"))
                        {
                            this->m_ImGui.Text("A Button : %s", pcbool[this->m_XInput->Release(XInputButton::A, i)]);
                            this->m_ImGui.Text("B Button : %s", pcbool[this->m_XInput->Release(XInputButton::B, i)]);
                            this->m_ImGui.Text("X Button : %s", pcbool[this->m_XInput->Release(XInputButton::X, i)]);
                            this->m_ImGui.Text("Y Button : %s", pcbool[this->m_XInput->Release(XInputButton::Y, i)]);
                            this->m_ImGui.Text("START Button : %s", pcbool[this->m_XInput->Release(XInputButton::START, i)]);
                            this->m_ImGui.Text("BACK Button : %s", pcbool[this->m_XInput->Release(XInputButton::BACK, i)]);
                            this->m_ImGui.Text("the cross key Left : %s", pcbool[this->m_XInput->Release(XInputButton::DPAD_LEFT, i)]);
                            this->m_ImGui.Text("the cross key Right : %s", pcbool[this->m_XInput->Release(XInputButton::DPAD_RIGHT, i)]);
                            this->m_ImGui.Text("the cross key Up : %s", pcbool[this->m_XInput->Release(XInputButton::DPAD_UP, i)]);
                            this->m_ImGui.Text("the cross key Down : %s", pcbool[this->m_XInput->Release(XInputButton::DPAD_DOWN, i)]);
                            this->m_ImGui.Text("LB Button : %s", pcbool[this->m_XInput->Release(XInputButton::LEFT_LB, i)]);
                            this->m_ImGui.Text("RB Button : %s", pcbool[this->m_XInput->Release(XInputButton::RIGHT_RB, i)]);
                            this->m_ImGui.Text("Left Analog Stick Button : %s", pcbool[this->m_XInput->Release(XInputButton::LEFT_THUMB, i)]);
                            this->m_ImGui.Text("Right Analog Stick Button : %s", pcbool[this->m_XInput->Release(XInputButton::RIGHT_THUMB, i)]);
                            this->m_ImGui.EndMenu();
                        }
                        D3DXVECTOR3 AnalogLKey;
                        D3DXVECTOR3 AnalogRKey;
                        this->m_ImGui.Separator();
                        this->m_ImGui.Text("Analog");
                        this->m_ImGui.Text("Left Analog Stick Pushing : %s", pcbool[this->m_XInput->AnalogL(i, AnalogLKey)]);
                        this->m_ImGui.Text(" > vector : x = %.2f , y = %.2f , z = %.2f", AnalogLKey.x, AnalogLKey.y, AnalogLKey.z);
                        this->m_ImGui.Text(" > vector : x = %d , y = %d", this->m_XInput->GetState(i)->Gamepad.sThumbLX, this->m_XInput->GetState(i)->Gamepad.sThumbLY);
                        this->m_ImGui.Text("Right Analog Stick Pushing : %s", pcbool[this->m_XInput->AnalogR(i, AnalogRKey)]);
                        this->m_ImGui.Text(" > vector : x = %.2f , y = %.2f , z = %.2f", AnalogRKey.x, AnalogRKey.y, AnalogRKey.z);
                        this->m_ImGui.Text(" > vector : x = %d , y = %d", this->m_XInput->GetState(i)->Gamepad.sThumbRX, this->m_XInput->GetState(i)->Gamepad.sThumbRY);
                        this->m_ImGui.Text("Left Analog Trigger Up : %s", pcbool[this->m_XInput->AnalogLTrigger(XInputAnalog::UP, i)]);
                        this->m_ImGui.Text("Left Analog Trigger Down : %s", pcbool[this->m_XInput->AnalogLTrigger(XInputAnalog::DOWN, i)]);
                        this->m_ImGui.Text("Left Analog Trigger Left : %s", pcbool[this->m_XInput->AnalogLTrigger(XInputAnalog::LEFT, i)]);
                        this->m_ImGui.Text("Left Analog Trigger Right : %s", pcbool[this->m_XInput->AnalogLTrigger(XInputAnalog::RIGHT, i)]);
                        this->m_ImGui.Text("Right Analog Trigger Up : %s", pcbool[this->m_XInput->AnalogRTrigger(XInputAnalog::UP, i)]);
                        this->m_ImGui.Text("Right Analog Trigger Down : %s", pcbool[this->m_XInput->AnalogRTrigger(XInputAnalog::DOWN, i)]);
                        this->m_ImGui.Text("Right Analog Trigger Left : %s", pcbool[this->m_XInput->AnalogRTrigger(XInputAnalog::LEFT, i)]);
                        this->m_ImGui.Text("Right Analog Trigger Right : %s", pcbool[this->m_XInput->AnalogRTrigger(XInputAnalog::RIGHT, i)]);
                        this->m_ImGui.Text("LT Button : %s", pcbool[this->m_XInput->LT(i)]);
                        this->m_ImGui.Text("RT Button : %s", pcbool[this->m_XInput->RT(i)]);
                    }
                    else
                    {
                        this->m_ImGui.Text("Not XInput");
                    }
                    this->m_ImGui.EndMenu();
                }
            }
            this->m_ImGui.EndMenu();
        }
    }
}

//==========================================================================
/**
@brief 処理時間表示
@param ProcessTime [in] 処理時間
*/
void DX9_Object::_ProcessingTime(float ProcessTime)
{
    if (this->m_ImGui.NewMenu("processing time."))
    {
        float time = 0.0f;

        time = this->m_processing_time[(int)ProcessingList::limit_time_New] - this->m_processing_time[(int)ProcessingList::limit_time_Old];
        this->m_ImGui.Text("application average = %.3f ms / %.3f ms", ProcessTime, time*1000.0f);
        time = this->m_processing_time[(int)ProcessingList::Initializa];
        this->m_ImGui.Text("init time = %.3f ms", time*1000.0f);
        time = this->m_processing_time[(int)ProcessingList::update];
        this->m_ImGui.Text("update time = %.3f ms", time*1000.0f);
        time = this->m_processing_time[(int)ProcessingList::draw];
        this->m_ImGui.Text("draw time = %.3f ms", time*1000.0f);
        this->m_ImGui.EndMenu();
    }
}

//==========================================================================
/**
@brief 処理時間計算
*/
float DX9_Object::_ProcessingTimeCount(void)
{
    float time = 0.0f;

    time += this->m_processing_time[(int)ProcessingList::update];
    time += this->m_processing_time[(int)ProcessingList::draw];

    return time * 1000.0f;
}

//==========================================================================
/**
@brief 登録されているリソースを表示する
*/
void DX9_Object::_ResourceDebug(void)
{
    if (this->m_ImGui.NewMenu("Resources in use"))
    {
        if (this->m_2DPolygon != nullptr)
        {
            this->_InputResourceDebug(this->m_2DPolygon->GetTexture());
        }
        if (this->m_A_CircleShadow != nullptr)
        {
            this->_InputResourceDebug(this->m_A_CircleShadow->GetTexture());
        }
        if (this->m_Billboard != nullptr)
        {
            this->_InputResourceDebug(this->m_Billboard->GetTexture());
        }
        if (this->m_Cube != nullptr)
        {
            this->_InputResourceDebug(this->m_Cube->GetTexture());
        }
        if (this->m_Mesh != nullptr)
        {
            this->_InputResourceDebug(this->m_Mesh->GetTexture());
        }
        if (this->m_MeshField != nullptr)
        {
            this->_InputResourceDebug(this->m_MeshField->GetTexture());
        }
        if (this->m_Number != nullptr)
        {
            if (this->m_Number->Get2DPolygon() != nullptr)
            {
                this->_InputResourceDebug(this->m_Number->Get2DPolygon()->GetTexture());
            }
        }
        if (this->m_Sphere != nullptr)
        {
            this->_InputResourceDebug(this->m_Sphere->GetTexture());
        }
        if (this->m_Xmodel != nullptr)
        {
            for (int i = 0; i < this->m_Xmodel->Size(); i++)
            {
                auto *pData = this->m_Xmodel->GetMdelDataParam(i);
                if (pData != nullptr)
                {
                    this->_InputResourceDebug(this->m_Xmodel->GetMdelDataParam(i)->GetTexture());
                }
            }
        }
        this->m_ImGui.EndMenu();
    }
}

//==========================================================================
/**
@brief リソース登録
@param pRenderer [in] テクスチャのロダ
*/
void DX9_Object::_InputResourceDebug(DX9_TextureLoader * pRenderer)
{
    if (pRenderer != nullptr)
    {
        auto * pMasterData = const_cast<vector_wrapper<DX9_TextureData>*>(pRenderer->get_master());
        
        for (int i = 0; i <  pMasterData->Size(); i++)
        {
            auto *pTexture = pMasterData->Get(i);

            if (pTexture->gettex() != nullptr)
            {
                this->m_ImGui.Separator();
                this->m_ImGui.Text("texture pass : %s", pTexture->gettag());
                this->m_ImGui.Text("Width : %d", (int)pTexture->GetImageInfo().Width);
                this->m_ImGui.Text("Height : %d", (int)pTexture->GetImageInfo().Height);
                this->m_ImGui.Text("Depth : %d", (int)pTexture->GetImageInfo().Depth);
                this->m_ImGui.Text("MipLevels : %d", (int)pTexture->GetImageInfo().MipLevels);
                this->m_ImGui.Text("Format : %d", (int)pTexture->GetImageInfo().Format);
                switch (pTexture->GetImageInfo().ResourceType)
                {
                case D3DRESOURCETYPE::D3DRTYPE_SURFACE:
                    this->m_ImGui.Text("ResourceType : Surface resource");
                    break;
                case D3DRESOURCETYPE::D3DRTYPE_VOLUME:
                    this->m_ImGui.Text("ResourceType : Volume resource");
                    break;
                case D3DRESOURCETYPE::D3DRTYPE_TEXTURE:
                    this->m_ImGui.Text("ResourceType : Texture resource");
                    break;
                case D3DRESOURCETYPE::D3DRTYPE_VOLUMETEXTURE:
                    this->m_ImGui.Text("ResourceType : Volume texture resource");
                    break;
                case D3DRESOURCETYPE::D3DRTYPE_CUBETEXTURE:
                    this->m_ImGui.Text("ResourceType : Cube texture resource");
                    break;
                case D3DRESOURCETYPE::D3DRTYPE_VERTEXBUFFER:
                    this->m_ImGui.Text("ResourceType : Vertex buffer resource");
                    break;
                case D3DRESOURCETYPE::D3DRTYPE_INDEXBUFFER:
                    this->m_ImGui.Text("ResourceType : Index buffer resource");
                    break;
                default:
                    break;
                }
                switch (pTexture->GetImageInfo().ImageFileFormat)
                {
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_BMP:
                    this->m_ImGui.Text("ImageFileFormat : BMP");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_JPG:
                    this->m_ImGui.Text("ImageFileFormat : JPG");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_TGA:
                    this->m_ImGui.Text("ImageFileFormat : TGA");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_PNG:
                    this->m_ImGui.Text("ImageFileFormat : PNG");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_DDS:
                    this->m_ImGui.Text("ImageFileFormat : DDS");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_PPM:
                    this->m_ImGui.Text("ImageFileFormat : PPM");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_DIB:
                    this->m_ImGui.Text("ImageFileFormat : DIB");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_HDR:
                    this->m_ImGui.Text("ImageFileFormat : HDR");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_PFM:
                    this->m_ImGui.Text("ImageFileFormat : PFM");
                    break;
                case D3DXIMAGE_FILEFORMAT::D3DXIFF_FORCE_DWORD:
                    this->m_ImGui.Text("ImageFileFormat : NONE");
                    break;
                default:
                    break;
                }
                this->m_ImGui.Image(pTexture->gettex(), *pTexture->getsize(), 550);
            }
        }
    }
}

_MSLIB_END