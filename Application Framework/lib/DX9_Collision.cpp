//==========================================================================
// 当たり判定[DX9_Collision.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Collision.h"

_MSLIB_BEGIN

//==========================================================================
// 定数定義
////==========================================================================
constexpr float fNull = 0.0f;

DX9_Collision::DX9_Collision()
{
}

DX9_Collision::~DX9_Collision()
{
}

//==========================================================================
/**
@brief 球のコリジョン
@param TargetA [in] 対象A
@param TargetB [in] 対象B
@param Scale [in] コリジョンの有効範囲
@return 指定範囲内に入った場合 true が返ります
*/
bool DX9_Collision::Ball(const DX9_3DObject & TargetA, const DX9_3DObject & TargetB, float Scale)
{
    if (this->Distance(TargetA, TargetB)<Scale) { return true; }

    return false;
}

//==========================================================================
/**
@brief Boxコリジョン
@param TargetA [in] 対象A
@param TargetB [in] 対象B
@param Scale [in] コリジョンの有効範囲
@return 指定範囲内に入った場合 true が返ります
*/
bool DX9_Collision::Simple(const DX9_3DObject & TargetA, const DX9_3DObject & TargetB, float Scale)
{
    const auto * TargetAPos = TargetA.GetMatInfoPos();
    const auto * TargetBPos = TargetB.GetMatInfoPos();

    if (this->Sinple2(TargetAPos->z, TargetBPos->z, fNull, Scale)) { return true; }
    if (this->Sinple2(TargetAPos->x, TargetBPos->x, fNull, Scale)) { return true; }

    return false;
}

//==========================================================================
/**
@brief 距離の計算 (C3DObjectorC2DObject)
@param TargetA [in] 対象A
@param TargetB [in] 対象B
@return 距離が返ります
*/
float DX9_Collision::Distance(const DX9_3DObject & TargetA, const DX9_3DObject & TargetB)
{
    const auto * TargetAPos = TargetA.GetMatInfoPos();
    const auto * TargetBPos = TargetB.GetMatInfoPos();

    return
        ((TargetBPos->x) - (TargetAPos->x))*
        ((TargetBPos->x) - (TargetAPos->x)) +
        ((TargetBPos->y) - (TargetAPos->y))*
        ((TargetBPos->y) - (TargetAPos->y)) +
        ((TargetBPos->z) - (TargetAPos->z))*
        ((TargetBPos->z) - (TargetAPos->z));
}

//==========================================================================
/**
@brief 距離の計算 (C3DObjectorC2DObject)
@param TargetA [in] 対象A
@param TargetB [in] 対象B
@return 距離が返ります
*/
float DX9_Collision::Distance(const DX9_2DObject & TargetA, const DX9_2DObject & TargetB)
{
    const auto * TargetAPos = TargetA.GetPos();
    const auto * TargetBPos = TargetB.GetPos();

    return
        ((TargetBPos->x) - (TargetAPos->x))*
        ((TargetBPos->x) - (TargetAPos->x)) +
        ((TargetBPos->y) - (TargetAPos->y))*
        ((TargetBPos->y) - (TargetAPos->y)) +
        ((0) - (0))*
        ((0) - (0));
}

//==========================================================================
/**
@brief 距離の計算 (CVector4<float>)
@param TargetA [in] 対象A
@param TargetB [in] 対象B
@return 距離が返ります
*/
float DX9_Collision::Distance(const CVector4<float>& TargetA, const CVector4<float>& TargetB)
{
    return
        ((TargetB.x) - (TargetA.x))*
        ((TargetB.x) - (TargetA.x)) +
        ((TargetB.y) - (TargetA.y))*
        ((TargetB.y) - (TargetA.y)) +
        ((TargetB.z) - (TargetA.z))*
        ((TargetB.z) - (TargetA.z));
}

//==========================================================================
/**
@brief 距離の計算 (CVector3<float>)
@param TargetA [in] 対象A
@param TargetB [in] 対象B
@return 距離が返ります
*/
float DX9_Collision::Distance(const CVector3<float>& TargetA, const CVector3<float>& TargetB)
{
    return
        ((TargetB.x) - (TargetA.x))*
        ((TargetB.x) - (TargetA.x)) +
        ((TargetB.y) - (TargetA.y))*
        ((TargetB.y) - (TargetA.y)) +
        ((TargetB.z) - (TargetA.z))*
        ((TargetB.z) - (TargetA.z));
}

//==========================================================================
/**
@brief 距離の計算 (CVector2<float>)
@param TargetA [in] 対象A
@param TargetB [in] 対象B
@return 距離が返ります
*/
float DX9_Collision::Distance(const CVector2<float>& TargetA, const CVector2<float>& TargetB)
{
    return
        ((TargetB.x) - (TargetA.x))*
        ((TargetB.x) - (TargetA.x)) +
        ((TargetB.y) - (TargetA.y))*
        ((TargetB.y) - (TargetA.y));
}

//==========================================================================
/**
@brief Boxコリジョン処理
@param TargetA [in] 対象A
@param TargetB [in] 対象B
@param TargetC [in] 対象C
@param Scale [in] コリジョンの有効範囲
@return 指定範囲内に入った場合 true が返ります
*/
bool DX9_Collision::Sinple2(const float & TargetA, const float & TargetB, const float & TargetC, float Scale)
{
    if (TargetC < TargetA)
    {
        if (TargetC < TargetB)
        {
            if (TargetB < TargetA*Scale)
            {
                return true;
            }
        }
    }
    else if (TargetA < TargetC)
    {
        if (TargetB < TargetC)
        {
            if (TargetA*Scale < TargetB)
            {
                return true;
            }
        }
    }

    return false;
}

_MSLIB_END