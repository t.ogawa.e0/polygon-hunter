//==========================================================================
// Xインプット[XInput.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#pragma comment (lib, "xinput.lib") // ライブラリのインポート
#include <Windows.h>
#include <XInput.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

_MSLIB_BEGIN

//==========================================================================
//
// class  : XInputBool4
// Content: XInputBool4
//
//==========================================================================
class XInputBool4
{
public:
    XInputBool4();
    ~XInputBool4();
public:
    bool m_up, m_down, m_left, m_right;
};

//==========================================================================
//
// class  : CXInputAnalogTrigger
// Content: アナログスティックのトリガー
//
//==========================================================================
class XInputAnalogTrigger
{
public:
    XInputAnalogTrigger();
    ~XInputAnalogTrigger();
public:
    /**
    @brief 更新
    */
    void update(void);

    /**
    @brief 切り替え
    @param input [in] 判定キー
    @param Out [in/out] 対象
    */
    void ifset(bool input, bool * Out);

    /**
    @brief 比較 in1 < in2
    @param in1 [in] 対象1
    @param in2 [in/out] 対象2
    @return in2 が大きい場合 true
    */
    bool ifbool(float in1, float in2);
public:
    XInputBool4 m_Ltrigger; // 左アナログのトリガー
    XInputBool4 m_LtriggerOld; // 左アナログの古いトリガー
    XInputBool4 m_Rtrigger; // 右アナログのトリガー
    XInputBool4 m_RtriggerOld; // 右アナログの古いトリガー
};

// アナログのボタン
enum class XInputAnalog
{
    UP, // 上
    DOWN, // 下
    LEFT, // 左
    RIGHT, // 右
};

// xBoxの押せるボタン
enum class XInputButton
{
    DPAD_UP = XINPUT_GAMEPAD_DPAD_UP, // 十字ボタン 上
    DPAD_DOWN = XINPUT_GAMEPAD_DPAD_DOWN, // 十字ボタン 下
    DPAD_LEFT = XINPUT_GAMEPAD_DPAD_LEFT, // 十字ボタン 左
    DPAD_RIGHT = XINPUT_GAMEPAD_DPAD_RIGHT, // 十字ボタン 右
    START = XINPUT_GAMEPAD_START, // STARTボタン
    BACK = XINPUT_GAMEPAD_BACK, // BACKボタン
    LEFT_THUMB = XINPUT_GAMEPAD_LEFT_THUMB, // 左アナログスティックのボタン
    RIGHT_THUMB = XINPUT_GAMEPAD_RIGHT_THUMB, // 右アナログスティックのボタン
    LEFT_LB = XINPUT_GAMEPAD_LEFT_SHOULDER, // LBボタン
    RIGHT_RB = XINPUT_GAMEPAD_RIGHT_SHOULDER, // RBボタン
    A = XINPUT_GAMEPAD_A, // Aボタン
    B = XINPUT_GAMEPAD_B, // Bボタン
    X = XINPUT_GAMEPAD_X, // Xボタン
    Y = XINPUT_GAMEPAD_Y, // Yボタン
};

//==========================================================================
//
// class  : XInput
// Content: Xインプット
//
//==========================================================================
class XInput
{
private:
    // コピー禁止 (C++11)
    XInput(const XInput &) = delete;
    XInput &operator=(const XInput &) = delete;
public:
    XInput();
    ~XInput();

    /**
    @brief 初期化
    @param Num [in] 取るコントローラーの数
    @return 失敗時 true
    */
    bool Init(int Num);

    /**
    @brief 解放
    */
    void Release(void);

    /**
    @brief 更新
    */
    void Update(void);

    /**
    @brief プレス
    @param button [in] ボタンID
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool Press(XInputButton button, int index);

    /**
    @brief トリガー
    @param button [in] ボタンID
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool Trigger(XInputButton button, int index);

    /**
    @brief リリース
    @param button [in] ボタンID
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool Release(XInputButton button, int index);

    /**
    @brief 左アナログスティック
    @param index [in] 処理コントローラー
    @param Out [in] 傾きベクトル
    @return 押しているとき true
    */
    bool AnalogL(int index, D3DXVECTOR3 & Out);

    /**
    @brief 左アナログスティック
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool AnalogL(int index);

    /**
    @brief 右アナログスティック
    @param index [in] 処理コントローラー
    @param Out [in] 傾きベクトル
    @return 押しているとき true
    */
    bool AnalogR(int index, D3DXVECTOR3 & Out);

    /**
    @brief 右アナログスティック
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool AnalogR(int index);

    /**
    @brief 左アナログスティックのトリガー
    @param key [in] ボタンID
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool AnalogLTrigger(XInputAnalog key, int index);

    /**
    @brief 左アナログスティックのトリガー
    @param index [in] 処理コントローラー
    @param key [in] ボタンID
    @param Out [in] 傾きベクトル
    @return 押しているとき true
    */
    bool AnalogLTrigger(XInputAnalog key, int index, D3DXVECTOR3 * Out);

    /**
    @brief 右アナログスティックのトリガー
    @param index [in] 処理コントローラー
    @param key [in] ボタンID
    @return 押しているとき true
    */
    bool AnalogRTrigger(XInputAnalog key, int index);

    /**
    @brief 右アナログスティックのトリガー
    @param index [in] 処理コントローラー
    @param key [in] ボタンID
    @param Out [in] 傾きベクトル
    @return 押しているとき true
    */
    bool AnalogRTrigger(XInputAnalog key, int index, D3DXVECTOR3 * Out);

    /**
    @brief 左トリガーボタン
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool LT(int index);

    /**
    @brief 右トリガーボタン
    @param index [in] 処理コントローラー
    @return 押しているとき true
    */
    bool RT(int index);

    /**
    @brief コントローラーの存在の確認
    @param index [in] 処理コントローラー
    @return 存在する場合 true
    */
    bool Check(int index);

    /**
    @brief コントローラーの現在の状態を取得
    @param index [in] 処理コントローラー
    @return インスタンス
    */
    XINPUT_STATE* GetState(int index);

    /**
    @brief サイズ
    @return コントローラー数
    */
    int Size(void);
private:
    /**
    @brief コントローラのキートリガー
    @param bNew [in] 新しい判定キー
    @param bOld [in] 古い判定キー
    @return 結果
    */
    bool KeyTrigger(bool bNew, bool bOld);

    /**
    @brief コントローラのキーリリース
    @param bNew [in] 新しい判定キー
    @param bOld [in] 古い判定キー
    @return 結果
    */
    bool KeyRelease(bool bNew, bool bOld);
private:
    XINPUT_STATE * m_state; // コントローラーのステータス
    XINPUT_STATE * m_stateOld; // 古い情報
    bool * m_statedat; //パッドの有無
    XInputAnalogTrigger * m_trigger; // アナログのトリガー
    int m_numdata; // 登録されたデータ数
};

_MSLIB_END