//==========================================================================
// モデルデータ[DX9_Xmodel.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Xmodel.h"

_MSLIB_BEGIN

DX9_XmodelData::DX9_XmodelData()
{
    this->m_MaterialMood = DX9_XmodelMoad::System;
    this->m_Mesh = nullptr;
    this->m_NumMaterial = (DWORD)0;
    this->m_texture = nullptr;
    this->m_strName = "";
    this->m_strFilePass = "";
    this->m_origin = false;
}

DX9_XmodelData::~DX9_XmodelData()
{
    this->Release();
}

DX9_Xmodel::DX9_Xmodel(LPDIRECT3DDEVICE9 pDevice, HWND hWnd)
{
    this->m_Device = pDevice;
    this->m_hWnd = hWnd;
}

DX9_Xmodel::~DX9_Xmodel()
{
    this->Release();
}

//==========================================================================
/**
@brief 初期化
@param Input [in] 使用するテクスチャのパス
@param mode [in] マテリアルのライティングの設定
@return 失敗時 true が返ります
*/
bool DX9_Xmodel::Init(const char * Input, DX9_XmodelMoad mode)
{
    DX9_XmodelData * pxm = nullptr;
	bool bkey = false;

	// データの重複判定
    for (int i = 0; i < this->m_data.Size(); i++)
    {
        pxm = this->m_data.Get(i);

        // 重複しているとき
        if (!strcmp(pxm->gettag(), Input))
        {
            bkey = true;
            break;
        }
    }

	// 重複していないとき
	if (bkey == false)
	{
        pxm = this->m_data.Create();
        pxm->settag(Input);

		// modelの読み込み
        if (FAILED(pxm->load(mode, this->m_Device, this->m_hWnd)))
        {
            return true;
        }
    }

    DX9_XmodelData * pis = this->m_data_path.Create();
    pis->path(pxm);

    this->SetVertexElement(pis);

	return false;
}

//==========================================================================
/**
@brief 初期化
@param Input [in] 使用するテクスチャのパス ダブルポインタに対応
@param mode [in] マテリアルのライティングの設定
@return 失敗時 true が返ります
*/
bool DX9_Xmodel::Init(const char ** Input, int num, DX9_XmodelMoad mode)
{
	for (int i = 0; i < num; i++)
	{
		if (this->Init(Input[i], mode))
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
/**
@brief 更新
*/
void DX9_Xmodel::Update(void)
{
    // 登録済みオブジェクトのデータの更新
    for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
    {
        // 行列がデフォルトの指定の時
        if (itr->m_obj->GetMatrixType() == DX9_3DObjectMatrixType::Default)
        {
            itr->m_obj->SetMatrixType(DX9_3DObjectMatrixType::Vector1);
        }

        // 行列の生成
        itr->m_obj->CreateMtxWorld(itr->MtxWorld);
    }
}

//==========================================================================
/**
@brief 解放
*/
void DX9_Xmodel::Release(void)
{
    this->m_data.Release();
    this->m_data_path.Release();
    this->ObjectRelease();
}

//==========================================================================
// 描画
void DX9_Xmodel::Draw(void)
{
    if (this->m_data_path.Size() != 0 && this->m_ObjectData.size() != 0)
    {
        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            auto * pXdata = this->m_data_path.Get(itr->m_obj->GetIndex());

            // 行列のセット
            this->m_Device->SetTransform(D3DTS_WORLD, &itr->MtxWorld);

            // マテリアル情報をセット
            this->m_Device->SetMaterial(&itr->m_obj->GetD3DMaterial9());

            // 描画
            pXdata->draw(this->m_Device);
        }
    }
}

//==========================================================================
/**
@brief モデルデータのパラメーターゲッター
@param Input [in] 取得対象のオブジェクト
@return 取得成功時に データが返ります。失敗時 nullptr が返ります。
*/
DX9_XmodelData * DX9_Xmodel::GetMdelDataParam(DX9_3DObject & Input)
{
    return this->m_data_path.Get(Input.GetIndex());
}

//==========================================================================
/**
@brief モデルデータのパラメーターゲッター
@param id [in] モデルID
@return 取得成功時に データが返ります。失敗時 nullptr が返ります。
*/
DX9_XmodelData * DX9_Xmodel::GetMdelDataParam(int id)
{
    return this->m_data_path.Get(id);
}

//==========================================================================
/**
@brief リソース数の取得
@return リソース数
*/
int DX9_Xmodel::Size(void)
{
    return this->m_data_path.Size();
}

//==========================================================================
/**
@brief 頂点構造体の情報を登録
@param data [in] データ
*/
void DX9_Xmodel::SetVertexElement(DX9_XmodelData * data)
{
    D3DVERTEXELEMENT9 aElements[MAX_FVF_DECL_SIZE];
    data->GetMesh()->GetDeclaration(aElements);

    for (int i = 0; i < (int)MAX_FVF_DECL_SIZE; i++)
    {
        auto p_elemen = data->GetVertexElement()->Create();

        p_elemen->Stream = aElements[i].Stream;
        p_elemen->Offset = aElements[i].Offset;
        p_elemen->Type = aElements[i].Type;
        p_elemen->Method = aElements[i].Method;
        p_elemen->Usage = aElements[i].Usage;
        p_elemen->UsageIndex = aElements[i].UsageIndex;
    }
}

//==========================================================================
/**
@brief tagのセット
@param ptag [in] タグ
*/
void DX9_XmodelData::settag(const char * ptag)
{
	if (ptag != nullptr)
	{
		this->m_strName = ptag;
	}
	else
	{
		this->m_strName = "";
	}
}

//==========================================================================
/**
@brief tagの取得
*/
const char * DX9_XmodelData::gettag(void)
{
    return this->m_strName.c_str();
}

//==========================================================================
/**
@brief 複製データ
@param pinp [in] Xデータ
*/
void DX9_XmodelData::path(const DX9_XmodelData * pinp)
{
	this->m_MaterialMood = pinp->m_MaterialMood;
	this->m_Mesh = pinp->m_Mesh;
	this->m_NumMaterial = pinp->m_NumMaterial;
    this->m_texture = pinp->m_texture;
    this->m_strName = pinp->m_strName;
    this->m_strFilePass = pinp->m_strFilePass;
	this->m_origin = false;
}

//==========================================================================
/**
@brief 読み込み
@param mode [in] マテリアルライトの設定
@param pDevice [in] デバイス
@return Component Object Model defines, and macros
*/
HRESULT DX9_XmodelData::load(DX9_XmodelMoad mode, LPDIRECT3DDEVICE9 pDevice, HWND hWnd)
{
	LPD3DXBUFFER pAdjacency = nullptr; // 隣接情報
	LPD3DXMESH pTempMesh = nullptr; // テンプレートメッシュ
	LPD3DXBUFFER pMaterialBuffer = nullptr; // マテリアルバッファ
	D3DVERTEXELEMENT9 pElements[MAX_FVF_DECL_SIZE];
	HRESULT hr = (HRESULT)0;

	// マテリアルのモードを記録
	this->m_MaterialMood = mode;

	// モデルの読み込み
	hr = this->read(&pAdjacency, &pMaterialBuffer, pDevice);
	if (FAILED(hr))
	{
        char str[1024] = { 0 };
        sprintf(str, "Xデータが読み込めませんでした。\n %s", this->m_strName.c_str());
        MessageBox(hWnd, str, "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
		return hr;
	}
	
	// 最適化
	hr = this->optimisation(pAdjacency);
	if (FAILED(hr))
	{
        char str[1024] = { 0 };
        sprintf(str, "Xデータの最適化に失敗しました。\n %s", this->m_strName.c_str());
        MessageBox(hWnd, str, "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
		return hr;
	}

	// 頂点の宣言
	hr = this->declaration(pElements);
	if (FAILED(hr))
	{
        char str[1024] = { 0 };
        sprintf(str, "頂点の宣言に失敗しました。\n %s", this->m_strName.c_str());
        MessageBox(hWnd, str, "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
		return hr;
	}

	// 複製
	hr = this->replication(pElements, &pTempMesh, pDevice);
	if (FAILED(hr))
	{
        char str[1024] = { 0 };
        sprintf(str, "複製に失敗しました。\n %s", this->m_strName.c_str());
        MessageBox(hWnd, str, "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
		return hr;
	}

    // ファイルパスの生成
    this->create_file_pass();

	// テクスチャの読み込み
    const char *lhr = this->loadtexture((LPD3DXMATERIAL)pMaterialBuffer->GetBufferPointer(), pDevice, hWnd);
	if (lhr != nullptr)
	{
        char str[1024] = { 0 };
        sprintf(str, "読み込みに失敗しました\n model : %s \n texture : %s", this->m_strName.c_str(), lhr);
        MessageBox(hWnd, str, "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
		return (HRESULT)-1;
	}

	// マテリアルの設定
	//this->materialsetting((LPD3DXMATERIAL)pMaterialBuffer->GetBufferPointer());

    if (pMaterialBuffer != nullptr)
    {
        pMaterialBuffer->Release();
        pMaterialBuffer = nullptr;
    }
    if (this->m_Mesh != nullptr)
    {
        this->m_Mesh->Release();
        this->m_Mesh = nullptr;
    }
    if (pAdjacency != nullptr)
    {
        pAdjacency->Release();
        pAdjacency = nullptr;
    }
	this->m_Mesh = pTempMesh;
	this->m_origin = true;

	return hr;
}

//==========================================================================
/**
@brief 解放
*/
void DX9_XmodelData::Release(void)
{
	if (this->m_origin)
	{
        // テクスチャの解放
        if (this->m_texture != nullptr)
        {
            this->m_texture->Release();
            delete this->m_texture;
            this->m_texture = nullptr;
        }
        if (this->m_Mesh != nullptr)
        {
            this->m_Mesh->Release();
            this->m_Mesh = nullptr;
        }
	}
	this->m_strName.clear();
    this->m_strFilePass.clear();
    this->m_Elements.Release();
}

//==========================================================================
/**
@brief 描画
@param pDevice [in] デバイス
*/
void DX9_XmodelData::draw(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetFVF(this->m_Mesh->GetFVF());
	for (int i = 0; i < (int)this->m_NumMaterial; i++)
	{
        auto * pTexList = this->m_texture->get(i);
        if (pTexList != nullptr)
        {
            pTexList->DrawBegin();
            this->m_Mesh->DrawSubset(i);
            pTexList->DrawEnd();
        }
	}
}

//==========================================================================
/**
@brief メッシュデータの取得
@return メッシュの情報
*/
LPD3DXMESH DX9_XmodelData::GetMesh(void)
{
    return this->m_Mesh;
}

//==========================================================================
/**
@brief マテリアル数の取得
@return マテリアル数
*/
const DWORD & DX9_XmodelData::GetNumMaterial(void)
{
    return this->m_NumMaterial;
}

//==========================================================================
/**
@brief 頂点構造体の情報を取得
@return Xデータ
*/
DX9_XmodelData::Vertex_Element * DX9_XmodelData::GetVertexElement(void)
{
    return &this->m_Elements;
}

//==========================================================================
/**
@brief テクスチャデータへのアクセス
@return データへのアクセスポインタ
*/
DX9_TextureLoader * DX9_XmodelData::GetTexture(void) const
{
    return this->m_texture;
}

//==========================================================================
/**
@brief 読み込み
@param pAdjacency [in] ID3DXBuffer*
@param pMaterialBuffer [in] ID3DXBuffer*
@param pDevice [in] デバイス
@return Component Object Model defines, and macros
*/
HRESULT DX9_XmodelData::read(LPD3DXBUFFER * pAdjacency, LPD3DXBUFFER * pMaterialBuffer, LPDIRECT3DDEVICE9 pDevice)
{
	return D3DXLoadMeshFromX(this->m_strName.c_str(), D3DXMESH_SYSTEMMEM, pDevice, pAdjacency, pMaterialBuffer, nullptr, &this->m_NumMaterial, &this->m_Mesh);
}

//==========================================================================
/**
@brief 最適化
@param pAdjacency [in] ID3DXBuffer*
@return Component Object Model defines, and macros
*/
HRESULT DX9_XmodelData::optimisation(LPD3DXBUFFER pAdjacency)
{
	return this->m_Mesh->OptimizeInplace(D3DXMESHOPT_COMPACT | D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE, (DWORD*)pAdjacency->GetBufferPointer(), nullptr, nullptr, nullptr);
}

//==========================================================================
/**
@brief 頂点の宣言
@param pElements [in] D3DVERTEXELEMENT9
@return Component Object Model defines, and macros
*/
HRESULT DX9_XmodelData::declaration(D3DVERTEXELEMENT9 *pElements)
{
	return this->m_Mesh->GetDeclaration(pElements);
}

//==========================================================================
/**
@brief 複製
@param pElements [in] D3DVERTEXELEMENT9
@param pTempMesh [in] ID3DXMesh
@param pDevice [in] デバイス
@return Component Object Model defines, and macros
*/
HRESULT DX9_XmodelData::replication(D3DVERTEXELEMENT9 *pElements, LPD3DXMESH * pTempMesh, LPDIRECT3DDEVICE9 pDevice)
{
	return this->m_Mesh->CloneMesh(D3DXMESH_MANAGED | D3DXMESH_WRITEONLY, pElements, pDevice, pTempMesh);
}

//==========================================================================
/**
@brief テクスチャの読み込み
@param Input [in] マテリアル
@return ANSI (Multi-byte Character) types
*/
const char * DX9_XmodelData::loadtexture(const LPD3DXMATERIAL Input, LPDIRECT3DDEVICE9 pDevice, HWND hWnd)
{
    if (this->m_texture == nullptr)
    {
        this->m_texture = new DX9_TextureLoader(pDevice, hWnd);
    }
    
    // マテリアルの数だけ
	for (int i = 0; i < (int)this->m_NumMaterial; i++)
	{
		D3DXMATERIAL * pmat = &Input[i];

        // 使用しているテクスチャがあれば読み込む
        if (pmat->pTextureFilename != nullptr &&lstrlen(pmat->pTextureFilename) > 0)
        {
            std::string str_file_pass = pmat->pTextureFilename;
            std::string strFileName;

            // マルチテクスチャであるとき
            if (str_file_pass.find("*") != std::string::npos)
            {
                std::string str_create_pass;

                // ファイルパスを生成
                for (auto itr = str_file_pass.begin(); itr != str_file_pass.end(); ++itr)
                {
                    std::string str_;
                    str_ = (*itr);

                    // マルチテクスチャ判定以外の時に生成
                    if (str_ != "*")
                    {
                        str_create_pass += (*itr);
                    }
                    else if (str_ == "*")
                    {
                        strFileName += (this->m_strFilePass + str_create_pass + "*");
                        str_create_pass.clear();
                    }
                }
                strFileName += this->m_strFilePass + str_create_pass;
                str_create_pass.clear();
            }
            else
            {
                strFileName = this->m_strFilePass + pmat->pTextureFilename;
            }

            if (this->m_texture->init(strFileName.c_str()))
            {
                return strFileName.c_str();
            }
        }
        else
        {
            this->m_texture->init(nullptr);
        }
	}

	// material数が0のとき
	if ((int)this->m_NumMaterial == 0)
	{
		this->m_texture->init(nullptr);
	}

	return nullptr;
}

//==========================================================================
/**
@brief ファイルパスの生成
*/
void DX9_XmodelData::create_file_pass(void)
{
    std::string strFileName = this->m_strName;

    for (auto itr = --strFileName.end(); itr != strFileName.begin(); --itr)
    {
        if (itr == strFileName.begin())
        {
            break;
        }
        if ((strcmp(&(*itr), "/") == 0) || (strcmp(&(*itr), "\0") == 0))
        {
            break;
        }
        strFileName.erase(itr);
    }
    this->m_strFilePass = strFileName;
}

_MSLIB_END