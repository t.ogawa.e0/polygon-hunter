//==========================================================================
// システムウィンドウ[SystemWindow.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <sal.h>
#include <vector>
#include "dxlib.h"
#include "WindowsAPI.h"

//==========================================================================
//
// class  : CSystemWindow
// Content: システムウィンドウ
//
//==========================================================================
class CSystemWindow
{
private:
    class CWinSize
    {
    public:
        CWinSize() {
            this->w = 0;
            this->h = 0;
        };
        CWinSize(int _w, int _h) {
            this->w = _w;
            this->h = _h;
        }
    public:
        int w;
        int h;
    };
private:
    class CData {
    public:
        CData() {
            this->m_serect = 0;
            this->m_key = 0;
        };
        CData(int serect, bool key) {
            this->m_serect = serect;
            this->m_key = key;
        }
    public:
        int m_serect;
        bool m_key;
    };
public:
    CSystemWindow() {
        this->m_class_name = "Default";
        this->m_window_name = "Default";
        this->m_WindowsAPI = nullptr;
    }
    CSystemWindow(const std::string &class_name, const std::string &window_name) {
        this->m_class_name = class_name;
        this->m_window_name = window_name;
        this->m_WindowsAPI = nullptr;
    }
    ~CSystemWindow() {
        this->m_class_name.clear();
        this->m_window_name.clear();
        delete this->m_WindowsAPI;
    }

    // ウィンドウ生成
    bool Window(HINSTANCE hInstance, int nCmdShow);

    // true の時はフルスクリーン
    bool GetWindowMode(void) {
        if (this->m_asp.Size() == this->m_data.m_serect) {
            return true;
        }
        return false;
    }

    // ウィンドウサイズの取得
    mslib::AspectRatio::data_t GetWindowSize(void) {
        if (this->m_asp.Size() != this->m_data.m_serect) {
            return this->m_asp.Get(this->m_data.m_serect).size;
        }
        return mslib::AspectRatio::data_t(0, 0);
    }

    // ウィンドウモードのセット
    void SetAspectRatio(const mslib::AspectRatio::List & Input) {
        this->m_asp.Search(Input.size, Input.asp);
    }
private:
    static void newtex(void);
    // ボタン
    static void Button(HWND hWnd, LPARAM *lParam, int posx, int posy);
    // 設定
    static void Config(HDC * hDC, mslib::CTexvec<int> * vpos);
    // テキスト描画
    static void Text(HWND hWnd, HDC * hDC, LPCTSTR lpszStr, int posx, int posy);
    // イメージデータ表示場所
    static void ImgeData(HDC * hDC, mslib::CTexvec<int> * vpos);
    // テクスチャ読み込み
    static void LoadTex(LPARAM * lp);
    // テクスチャ描画
    static void DrawTex(HWND hWnd, HDC * hDC, mslib::CTexvec<int> * vpos);
    // ウインドウプロシージャ
    static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

    static BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData);
private:
    static HWND m_hWnd;
    static HWND m_combo;
    static HDC m_BackBufferDC;
    static HBITMAP m_BackBufferBMP;
    static HWND m_hWndButton10000;
    static HWND m_hWndButton10001;
    static HWND m_check;

    //ビットマップ
    static HBITMAP m_hBitmap;
    static BITMAP m_Bitmap;

    static CData m_data;

    static mslib::AspectRatio m_asp;
    std::string m_class_name; // クラス名
    std::string m_window_name; // ウィンドウ名
    CWindowsAPI *m_WindowsAPI; // ウィンドウ
};
