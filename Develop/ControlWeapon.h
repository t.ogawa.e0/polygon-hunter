//==========================================================================
// ウェポンコントロール[ControlWeapon.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "WeaponObject.h"
#include "PlayObject.h"

//==========================================================================
//
// class  : ControlWeapon
// Content: ウェポンコントロール
//
//=========================================================================
class ControlWeapon : public mslib::DX9_Object, public PlayObject
{
public:
    ControlWeapon();
    ~ControlWeapon();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:
    // チェンジ
    void Change(void);
    // アクション
    void Action(void);
private:
    std::list<DX9_Object*> m_WeaponCase; // ウェポンケース
    WeaponObject::EList m_serect; // 選択
    WeaponObject::EList m_serect_old; // 選択
    std::string m_MotionID; // モーションID
    std::string m_MotionID_Old;
    void * m_input; // インプットオブジェクトへのアクセス
    void * m_keyboard_object; // キーボードオブジェクト
    bool m_change_key;
    int m_change_count; // チェンジカウント
};

