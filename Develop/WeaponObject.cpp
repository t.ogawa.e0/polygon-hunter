//==========================================================================
// ウェポンオブジェクト[WeaponObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "WeaponObject.h"

WeaponObject::WeaponObject(mslib::DX9_ObjectID ObjectID) : DX9_Object(ObjectID)
{
    this->m_WeaponID = EList::Begen;
    this->m_update_lock = false;
    //this->m_motion_data[MotionID::Attack].clear();
    //this->m_motion_data[MotionID::Damage].clear();
    //this->m_motion_data[MotionID::Moving].clear();
    //this->m_motion_data[MotionID::Wait].clear();
}

WeaponObject::~WeaponObject()
{
    //this->m_motion_data.clear();
}

//==========================================================================
// そのウェポンが処理中かどうかのチェック
const WBoolKey & WeaponObject::Check(void)
{
    return this->m_processing;
}

//==========================================================================
// ウェポンIDの登録
void WeaponObject::SetWeaponID(EList ID)
{
    this->m_WeaponID = ID;
}

const std::string & WeaponObject::GetMotion(void)
{
    return this->m_MotionName;
}

//==========================================================================
// そのウェポンが処理中かどうかのチェック登録(Wキー)
void WeaponObject::SetCheck(bool check1, bool check2)
{
    this->m_processing.key1 = check1;
    this->m_processing.key2 = check2;
}

//==========================================================================
// ウェポンIDの取得
WeaponObject::EList WeaponObject::GetWeaponID(void)
{
    return this->m_WeaponID;
}

void WeaponObject::SetMotion(const std::string & Input)
{
    this->m_MotionName = Input;
}

//==========================================================================
// ロック解除
void WeaponObject::Unlock(void)
{
    this->m_update_lock = true;
}

//==========================================================================
// ロック
void WeaponObject::Lock(void)
{
    this->m_update_lock = false;
}

//==========================================================================
// 更新キー
bool WeaponObject::UpdateKey(void)
{
    return this->m_update_lock;
}

//MotionData::MotionData()
//{
//    this->m_max_rot = D3DXVECTOR3(0, 0, 0);
//    this->m_max_pos = D3DXVECTOR3(0, 0, 0);
//    this->m_rot = D3DXVECTOR3(0, 0, 0);
//    this->m_pos = D3DXVECTOR3(0, 0, 0);
//    this->m_buf_rot = D3DXVECTOR3(0, 0, 0);
//    this->m_buf_pos = D3DXVECTOR3(0, 0, 0);
//    this->m_powor = 0.0f;
//    this->m_key1 = false;
//    this->m_key2 = false;
//    this->m_setkey1 = false;
//    this->m_setkey2 = false;
//}
//
//MotionData::MotionData(D3DXVECTOR3 max_rot, D3DXVECTOR3 max_pos, float fpowor)
//{
//    this->m_max_rot = max_rot;
//    this->m_max_pos = max_pos;
//    this->m_rot = D3DXVECTOR3(0, 0, 0);
//    this->m_pos = D3DXVECTOR3(0, 0, 0);
//    this->m_buf_rot = D3DXVECTOR3(0, 0, 0);
//    this->m_buf_pos = D3DXVECTOR3(0, 0, 0);
//    this->m_powor = fpowor;
//    this->m_key1 = false;
//    this->m_key2 = false;
//    this->m_setkey1 = false;
//    this->m_setkey2 = false;
//}
//
//void MotionData::Update(C3DObject * Out)
//{
//    D3DXVECTOR3 ComparisonResult_Pos, ComparisonResult_Rot;
//    float f_none = 0.0f;
//
//    ComparisonResult_Pos.x = mslib::_max(f_none, this->m_max_pos.x);
//    ComparisonResult_Pos.y = mslib::_max(f_none, this->m_max_pos.y);
//    ComparisonResult_Pos.z = mslib::_max(f_none, this->m_max_pos.z);
//
//    ComparisonResult_Rot.x = mslib::_max(f_none, this->m_max_rot.x);
//    ComparisonResult_Rot.y = mslib::_max(f_none, this->m_max_rot.y);
//    ComparisonResult_Rot.z = mslib::_max(f_none, this->m_max_rot.z);
//
//    // 自分がminである
//    if (this->m_key1 == false)
//    {
//        this->m_buf_pos += this->m_max_pos*this->m_powor;
//        this->m_pos += this->m_max_pos*this->m_powor;
//
//        this->SetData(this->m_key1.x, f_none, this->m_buf_pos.x, ComparisonResult_Pos.x, this->m_max_pos.x);
//        this->SetData(this->m_key1.y, f_none, this->m_buf_pos.y, ComparisonResult_Pos.y, this->m_max_pos.y);
//        this->SetData(this->m_key1.z, f_none, this->m_buf_pos.z, ComparisonResult_Pos.z, this->m_max_pos.z);
//    }
//
//    if (this->m_key2 == false)
//    {
//        this->m_buf_rot += this->m_max_rot*this->m_powor;
//        this->m_rot += this->m_max_rot*this->m_powor;
//
//        // 動作数値がmax以上になったとき
//        this->SetData(this->m_key2.x, f_none, this->m_buf_rot.x, ComparisonResult_Rot.x, this->m_max_rot.x);
//        this->SetData(this->m_key2.y, f_none, this->m_buf_rot.y, ComparisonResult_Rot.y, this->m_max_rot.y);
//        this->SetData(this->m_key2.z, f_none, this->m_buf_rot.z, ComparisonResult_Rot.z, this->m_max_rot.z);
//    }
//
//    Out->MoveX(this->m_pos.x);
//    Out->MoveY(this->m_pos.y);
//    Out->MoveZ(this->m_pos.z);
//    Out->RotX(this->m_rot.x);
//    Out->RotY(this->m_rot.y);
//    Out->RotZ(this->m_rot.z);
//}
//
//void MotionData::Reset(void)
//{
//    this->m_rot = D3DXVECTOR3(0, 0, 0);
//    this->m_pos = D3DXVECTOR3(0, 0, 0);
//    this->m_buf_rot = D3DXVECTOR3(0, 0, 0);
//    this->m_buf_pos = D3DXVECTOR3(0, 0, 0);
//    this->m_key1 = false;
//    this->m_key2 = false;
//    this->m_setkey1 = false;
//    this->m_setkey2 = false;
//}
//
//bool MotionData::Check(void)
//{
//    if ((this->m_key1.x == true) && (this->m_key1.y == true) && (this->m_key1.z == true) && (this->m_key2.x == true) && (this->m_key2.y == true) && (this->m_key2.z == true))
//    {
//        return true;
//    }
//    return false;
//}
//
//const D3DXVECTOR3 & MotionData::GetPos(void)
//{
//    return this->m_pos;
//}
//
//void MotionData::SetPos(const D3DXVECTOR3 & Set)
//{
//    if (this->m_setkey1 == false)
//    {
//        this->m_setkey1 = true;
//        this->m_pos = Set;
//    }
//}
//
//const D3DXVECTOR3 & MotionData::GetRot(void)
//{
//    return this->m_rot;
//}
//
//void MotionData::SetRot(const D3DXVECTOR3 & Set)
//{
//    if (this->m_setkey2 == false)
//    {
//        this->m_setkey2 = true;
//        this->m_rot = Set;
//    }
//}
//
//void MotionData::SetData(bool &key, float none, float buff, float comparison_result, float data)
//{
//    if (none <= comparison_result)
//    {
//        key = false;
//        if (data <= buff)
//        {
//            key = true;
//        }
//    }
//    if (comparison_result <= none)
//    {
//        key = false;
//        if (buff <= data)
//        {
//            key = true;
//        }
//    }
//}
//
//MotionData::~MotionData()
//{
//}
