//==========================================================================
// タイトルの背景[TitleBackground.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "TitleBackground.h"
#include "resource_list.h"

TitleBackground::TitleBackground() : DX9_Object(mslib::DX9_ObjectID::Sphere)
{
    this->SetObjectName("TitleBackground");
}

TitleBackground::~TitleBackground()
{
}

//==========================================================================
// 初期化
bool TitleBackground::Init(void)
{
    auto *pObje = this->_3DObject()->Create();

    pObje->Init(0);
    pObje->ScalePlus(-50);

    this->_3DSphere()->ObjectInput(pObje);
    this->_Fog()->Init({ 255,255,255,255 }, 1.0f, 90.0f);
    this->_Fog()->ON();

    return this->_3DSphere()->Init(RESOURCE_sky_jpg, 20);
}

//==========================================================================
// 解放
void TitleBackground::Uninit(void)
{
}

//==========================================================================
// 更新
void TitleBackground::Update(void)
{
    auto *pObj = this->_3DObject()->Get(0);
    pObj->RotX(0.0005f);

    this->_3DSphere()->Update();
}

//==========================================================================
// 描画
void TitleBackground::Draw(void)
{
    this->_3DSphere()->Draw();
}

//==========================================================================
// デバッグ
void TitleBackground::Debug(void)
{
}
