//==========================================================================
// アクションリスト[Game_Action_List.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CJump
// Content: ジャンプ
//
//=========================================================================
class CJump
{
public:
    CJump() {
        this->m_jump_powor = 0.0f;
        this->m_jump_cont = 0;
    }
    ~CJump() {}

    // ジャンプ(二段ジャンプ可能)
    // 地面 = _y_1
    // 現在のY軸 = _y_2
    // ジャンプ力 = _powor
    // _key = 発動キー
    float _Jump(float _y_1, float _y_2, float _powor, bool &_key);

    // ジャンプパワー
    float _Get_Jump_Powor(void) {
        return this->m_jump_powor;
    }
private:
    float m_jump_powor; // 今働いている力
    int m_jump_cont; // 二段ジャンプ制御
};

//==========================================================================
//
// class  : CStep
// Content: ステップ
//
//=========================================================================
class CStep
{
public:
    CStep() {
        this->m_step_move_powor = 0.0f;
        this->m_step_powor = 0.0f;
        this->m_step_change = false;
        this->m_key = false;
    }
    ~CStep() {}

    // ステップ
    bool _Step(float _powor, bool _key, mslib::DX9_3DObject * pOut, const D3DXVECTOR3 * pVec = nullptr);

    // 移動力
    const mslib::CVector3<float> & _Get_Step_Powor_Vector3(void) {
        return this->m_step_move_powor;
    }

    // 移動力
    float _Get_Step_Powor_float(void) {
        return this->m_step_powor;
    }

    // 鍵
    bool _GetKey(void) {
        return this->m_key;
    }
private:
    mslib::CVector3<float> m_step_move_powor; // 移動力 y軸を空中判定に使用
    float m_step_powor; // 今働いている力
    bool m_step_change; // ベクトルがある場合
    bool m_key;
};
