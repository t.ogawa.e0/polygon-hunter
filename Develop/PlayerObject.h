//==========================================================================
// プレイヤーのオブジェクト[PlayerObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "CharacterObject.h"
#include "PlayObject.h"

//==========================================================================
//
// class  : CPlayerObject
// Content: プレイヤーオブジェクト
//
//=========================================================================
class CPlayerObject : public CharacterObject, public CJump, public CStep, public PlayObject
{
public:
    enum class ObjectType
    {
        Begin,
        Visualization = Begin, // 可視化
        Invisible, // 不可視化
        OldObject, // 古い情報
        End,
    };
public:
    CPlayerObject();
    ~CPlayerObject();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    // 判定時の処理
    void ProcessAtDecision(void);
    // 地面のセット
    void SetGround(const D3DXVECTOR3 & vec);
private:
    // フィールドとのコリジョン
    void FieldCollision(void)override;
    // ジャンプ
    void Jump(void)override;
    // 移動
    void Move(void)override;
    // ステップ
    bool Step(void)override;
    // アクション
    void Action(void)override;
    // 補正
    void Correction(void);
private:
    void * m_Field; // フィールドへのアクセス権
    void * m_gravity; // 重力へのアクセス
    void * m_GameCamera; // カメラへのアクセス権
    void * m_input; // インプットオブジェクトへのアクセス
    void * m_KeyboardObject; // キーボード
    void * m_StatusObj; // ステータスブジェクト
    void * m_GameICODrawObject; // バフアイコンオブジェクト
    void * m_HighGround; // 高台
    int m_HitArea; // 当たったエリア
    D3DXVECTOR3 m_fil_pos; // フィールド上の座標格納
    int m_special_count; // カウント
};
