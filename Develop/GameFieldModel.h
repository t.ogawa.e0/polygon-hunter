//==========================================================================
// フィールドモデル[GameFieldModel.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGameFieldModel
// Content: フィールドモデル
//
//=========================================================================
class CGameFieldModel : public mslib::DX9_Object
{
public:
    CGameFieldModel();
    ~CGameFieldModel();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:

};

