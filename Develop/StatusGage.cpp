//==========================================================================
// ステータスのゲージ[StatusGage.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "StatusGage.h"
#include "GameICODrawObject.h"
#include "resource_list.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int MAX_NUM_MAX_GAGE = 3;
constexpr int MAX_STATUS = 150;
constexpr int RecoveryBuff = 3;

StatusGage::StatusGage() :DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->SetObjectName("StatusGage");
    this->m_ICOObj = nullptr;
}

StatusGage::~StatusGage()
{
}

//=========================================================================
// 初期化
bool StatusGage::Init(void)
{
    //=========================================================================
    // リソースの登録
    //=========================================================================
    this->_2DPolygon()->Init(RESOURCE_status_list_name_HP_png, true);
    this->_2DPolygon()->Init(RESOURCE_status_list_name_MP_png, true);
    this->_2DPolygon()->Init(RESOURCE_status_list_name_SP_png, true);
    this->_2DPolygon()->Init(RESOURCE_status_list_name_gage_Begin_png, true);
    this->_2DPolygon()->Init(RESOURCE_status_list_name_gage_End_png, true);
    this->_2DPolygon()->Init(RESOURCE_status_list_name_gage_Begin_png, true);
    this->_2DPolygon()->Init(RESOURCE_status_list_name_gage_End_png, true);
    this->_2DPolygon()->Init(RESOURCE_status_list_name_gage_Begin_png, true);
    this->_2DPolygon()->Init(RESOURCE_status_list_name_gage_End_png, true);
    this->_2DPolygon()->Init(nullptr, true);
    this->_2DPolygon()->Init(nullptr, true);
    this->_2DPolygon()->Init(nullptr, true);
    this->_2DPolygon()->Init(nullptr, true);
    this->_2DPolygon()->Init(nullptr, true);
    this->_2DPolygon()->Init(nullptr, true);

    //=========================================================================
    // キャパ設定
    //=========================================================================
    this->_2DObject()->Reserve((int)ObjectID::End);

    //=========================================================================
    // オブジェクトの生成
    //=========================================================================
    for (int i = 0; i < (int)ObjectID::End; i++)
    {
        auto * pObj = this->_2DObject()->Create();
        pObj->Init(i);
        this->_2DPolygon()->ObjectInput(pObj);
    }

    this->ObjectLoad();

    auto *p_obj1 = this->_2DObject()->Get((int)ObjectID::label_HP_GageBegin);
    auto *p_obj2 = this->_2DObject()->Get((int)ObjectID::label_HP_GageEnd);
    auto * ptex_master = this->_2DPolygon()->GetTexSize((int)ObjectID::label_HP_GageBegin);

    //=========================================================================
    // ゲージの生成
    //=========================================================================
    for (int i = 0; i < MAX_NUM_MAX_GAGE; i++)
    {
        auto * pGage = this->m_gage.Create();
        pGage->Input({
            p_obj2->GetPos()->x - p_obj1->GetPos()->x,
            (float)ptex_master->h / 2
            });
    }

    //=========================================================================
    // マスターゲージ
    //=========================================================================
    auto * pGegeMas = this->m_gage.Get(0);

    for (int i = (int)ObjectID::label_HP_Gage_Sub; i < (int)ObjectID::Gage_End; i++)
    {
        auto * ptex = this->_2DPolygon()->GetTexSize(i);

        ptex->w = (int)pGegeMas->GetMaxSize().x;
        ptex->h = (int)pGegeMas->GetMaxSize().y;
    }

    //=========================================================================
    // オブジェクト取得
    //=========================================================================
    this->m_ICOObj = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "GameICODrawObject");

    return false;
}

//=========================================================================
// 解放
void StatusGage::Uninit(void)
{
}

//=========================================================================
// 更新
void StatusGage::Update(void)
{
    if (this->GetPlayKey())
    {
        //=========================================================================
        // 変数定義
        //=========================================================================
        auto * pGage1 = this->m_gage.Get(0);
        auto * pGage2 = this->m_gage.Get(1);
        auto * pGage3 = this->m_gage.Get(2);
        auto * ptex1 = this->_2DPolygon()->GetTexSize((int)ObjectID::label_HP_Gage_Main);
        auto * ptex2 = this->_2DPolygon()->GetTexSize((int)ObjectID::label_MP_Gage_Main);
        auto * ptex3 = this->_2DPolygon()->GetTexSize((int)ObjectID::label_SP_Gage_Main);
        auto *pICOObj = (GameICODrawObject*)this->m_ICOObj;

        //=========================================================================
        // ステータスオート回復バフ
        //=========================================================================

        this->HP_Operation(pICOObj->GetBuffData(GameICODrawSystemID::HPRecovery)->m_buff + 0.01f);
        this->MP_Operation(pICOObj->GetBuffData(GameICODrawSystemID::MPRecovery)->m_buff + 0.01f);
        this->SP_Operation(pICOObj->GetBuffData(GameICODrawSystemID::SPRecovery)->m_buff + 0.4f);

        //=========================================================================
        // ゲージ処理
        //=========================================================================
        pGage1->InputMaxParam(MAX_STATUS);
        ptex1->w = (int)pGage1->UpdateW((int)this->m_status.m_HP);
        pGage2->InputMaxParam(MAX_STATUS);
        ptex2->w = (int)pGage2->UpdateW((int)this->m_status.m_MP);
        pGage3->InputMaxParam(MAX_STATUS);
        ptex3->w = (int)pGage3->UpdateW((int)this->m_status.m_SP);
    }

    // 更新
    this->_2DPolygon()->Update();
}

//=========================================================================
// 描画
void StatusGage::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//=========================================================================
// デバッグ
void StatusGage::Debug(void)
{
    this->ImGui()->SliderFloat("HP", &this->m_status.m_HP, 0, MAX_STATUS);
    this->ImGui()->SliderFloat("MP", &this->m_status.m_MP, 0, MAX_STATUS);
    this->ImGui()->SliderFloat("SP", &this->m_status.m_SP, 0, MAX_STATUS);
}

//=========================================================================
// HP操作
// 戻り値 : 減らせない場合false 減らせる場合 true
bool StatusGage::HP_Operation(float value)
{
    return this->StatusOperation(this->m_status.m_HP, value);
}

//=========================================================================
// MP操作
// 戻り値 : 減らせない場合false 減らせる場合 true
bool StatusGage::MP_Operation(float value)
{
    return this->StatusOperation(this->m_status.m_MP, value);
}

//=========================================================================
// SP操作
// 戻り値 : 減らせない場合false 減らせる場合 true
bool StatusGage::SP_Operation(float value)
{
    return this->StatusOperation(this->m_status.m_SP, value);
}

//=========================================================================
// ステータスの取得
const StatusData * StatusGage::GetStatusData(void)
{
    return &this->m_status;
}

//=========================================================================
// ステータス共通処理
bool StatusGage::StatusOperation(float & _this, float value)
{
    _this += value;
    if (_this < 0)
    {
        _this = 0;
        return false;
    }
    else if (MAX_STATUS < _this)
    {
        _this = MAX_STATUS;
        return false;
    }
    return true;
}

StatusData::StatusData()
{
    this->m_HP = MAX_STATUS;
    this->m_MP = MAX_STATUS;
    this->m_SP = MAX_STATUS;
}

StatusData::~StatusData()
{
}
