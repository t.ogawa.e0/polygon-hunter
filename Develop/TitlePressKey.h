//==========================================================================
// ボタンを押してオブジェクト[TitlePressKey.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : TitlePressKey
// Content: ボタンを押してオブジェクト
//
//=========================================================================
class TitlePressKey : public mslib::DX9_Object
{
public:
    TitlePressKey();
    ~TitlePressKey();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
private:
    int m_color_buf; // 色
};

