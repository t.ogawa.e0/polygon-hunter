//==========================================================================
// 線形ポリゴン[LinearPolygon.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "DamagePolygonObject.h"

//==========================================================================
//
// class  : LinearPolygon
// Content: 線形ポリゴン
//
//=========================================================================
class LinearPolygon : public mslib::DX9_Object, public DamagePolygonInputSystem
{
private:
    enum class RandList
    {
        Scale, // サイズ
        Angle, // 回転
    };
public:
    LinearPolygon();
    ~LinearPolygon();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
    // 生成
    void Create(mslib::DX9_3DObject & target, mslib::DX9_3DObject & singular_point) override;
private:
    // 攻撃処理
    void AttackProcessing(void);
    // 消去処理
    void DeleteProcessing(void);
    // 準備
    void Preparation(void);
    // vectorセット
    void SetVector(void);
};

