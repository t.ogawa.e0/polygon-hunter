//==========================================================================
// メイン関数[main.cpp]
// author: tatuya ogawa
//==========================================================================
#define _CRTDBG_MAP_ALLOC
#include <cstdlib>
#include <crtdbg.h>
#define _GLIBCXX_DEBUG
#include "dxlib.h"
#include "GameWindow.h"
#include "SystemWindow.h"

//==========================================================================
//	メイン関数
int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    // CRTメモリリーク箇所検出
    //   _CrtSetBreakAlloc(35556);

    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    CSystemWindow *p_syswin = nullptr;
    CGameWindow *p_gamewin = nullptr;
    mslib::AspectRatio *p_asp = nullptr;
    mslib::CVector2<int> *p_Aspect = nullptr; // アスペクト比
    int end_status = 0; // 終了時のステータス

    p_asp = new mslib::AspectRatio;
    p_Aspect = new mslib::CVector2<int>(16, 9);

    // 指定した数のウィンドウサイズの生成
    for (int i = 0; i < 10; i++)
    {
        if (3 < i)
        {
            // 指定したアスペクト比かどうかのセット&登録
            p_asp->Search((*p_Aspect * 12)*(i + 1), *p_Aspect);
        }
    }

#if defined(_DEBUG) || defined(DEBUG)
    p_gamewin = new CGameWindow("PolygonHunter", "PolygonHunter");

    // ゲームウィンドウ
    end_status = p_gamewin->Window(hInstance, p_asp->Get(3).size, false, nCmdShow);
#else

    p_syswin = new CSystemWindow("Config", "Config");

    // 生成されたウィンドウサイズの情報の入力
    for (int i = 0; i < p_asp->Size(); i++)
    {
        p_syswin->SetAspectRatio(p_asp->Get(i));
    }

    // システムウィンドウ
    bool bPlay = p_syswin->Window(hInstance, nCmdShow);
    bool bWinMode = p_syswin->GetWindowMode();

    *p_Aspect = p_syswin->GetWindowSize();

    // メモリの解放
    if (mslib::nullptr_check(p_syswin) == false)
    {
        delete p_syswin;
        p_syswin = nullptr;
    }

    // ゲーム
    if (bPlay)
    {
        p_gamewin = new CGameWindow("PolygonHunter", "PolygonHunter");

        // ゲームウィンドウ
        end_status = p_gamewin->Window(hInstance, *p_Aspect, bWinMode, nCmdShow);
    }
#endif
    //==========================================================================
    // メモリの解放
    //==========================================================================
    if (mslib::nullptr_check(p_syswin))
    {
        delete p_syswin;
        p_syswin = nullptr;
    }
    if (mslib::nullptr_check(p_gamewin))
    {
        delete p_gamewin;
        p_gamewin = nullptr;
    }
    if (mslib::nullptr_check(p_Aspect))
    {
        delete p_Aspect;
        p_Aspect = nullptr;
    }
    if (mslib::nullptr_check(p_asp))
    {
        delete p_asp;
        p_asp = nullptr;
    }
    return end_status;
}
