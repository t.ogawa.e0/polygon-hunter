//==========================================================================
// インプットライブラリ[InputSystem.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "InputSystem.h"

CInputSystem::CInputSystem() : DX9_Object(mslib::DX9_ObjectID::Default)
{
    this->m_num_xinput = 1;
    this->SetObjectName("InputSystem");
}

CInputSystem::CInputSystem(int NumXinput) : DX9_Object(mslib::DX9_ObjectID::Default)
{
    this->m_num_xinput = NumXinput;
    this->SetObjectName("InputSystem");
}

CInputSystem::~CInputSystem()
{
}

//==========================================================================
// 初期化
bool CInputSystem::Init(void)
{
    // xinputの初期化
    this->_XInput()->Init(this->m_num_xinput);
    return false;
}

//==========================================================================
// 解放
void CInputSystem::Uninit(void)
{
}

//==========================================================================
// 更新
void CInputSystem::Update(void)
{
    // XInput更新
    this->_XInput()->Update();
}

//==========================================================================
// 描画
void CInputSystem::Draw(void)
{
}

//==========================================================================
// デバッグ
void CInputSystem::Debug(void)
{
}

//==========================================================================
// コントローラーの数
int CInputSystem::GetNumControl(void)
{
    return this->m_num_xinput;
}

//==========================================================================
// XInputの取得
mslib::XInput * CInputSystem::GetXInput(void)
{
    return this->_XInput();
}
