//==========================================================================
// バレットノーマルオブジェクト[BulletNormalEffectObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "BulletObject.h"

//==========================================================================
//
// class  : BulletNormalEffectObject
// Content: バレットノーマルオブジェクト
//
//==========================================================================
class BulletNormalEffectObject : public mslib::DX9_Object, public BulletInputSystem
{
public:
    BulletNormalEffectObject();
    ~BulletNormalEffectObject();

    // 初期化
    bool Init(void) override;

    // 解放
    void Uninit(void) override;

    // 更新
    void Update(void) override;

    // 描画
    void Draw(void) override;

    // デバッグ
    void Debug(void) override;

    // オブジェクトの生成
    void Create(const mslib::DX9_3DObject * pobj, const mslib::CVector3<float> & speed, const mslib::CVector3<float> & rot, int limit_time) override;
private:
    void * m_CameraObject;
    void * m_SparkEffectObject;
};

