//==========================================================================
// 消滅エフェクト[ExtinctionEffect.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ExtinctionEffect.h"

ExtinctionEffect::ExtinctionEffect() : DX9_Object(mslib::DX9_ObjectID::Cube)
{
    this->SetObjectName("ExtinctionEffect");
}

ExtinctionEffect::~ExtinctionEffect()
{
}

//=========================================================================
// 初期化
bool ExtinctionEffect::Init(void)
{
    return false;
}

//=========================================================================
// 解放
void ExtinctionEffect::Uninit(void)
{
}

//=========================================================================
// 更新
void ExtinctionEffect::Update(void)
{
}

//=========================================================================
// 描画
void ExtinctionEffect::Draw(void)
{
}

//=========================================================================
// デバッグ
void ExtinctionEffect::Debug(void)
{
}
