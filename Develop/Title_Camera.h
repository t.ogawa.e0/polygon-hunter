//==========================================================================
// タイトルのカメラ[Title_Camera.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : Title_Camera
// Content: タイトルのカメラ
//
//=========================================================================
class Title_Camera : public mslib::DX9_Object
{
public:
    Title_Camera();
    ~Title_Camera();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
};

