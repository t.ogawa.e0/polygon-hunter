//==========================================================================
// タイトル仮置きオブジェクト[Title_TemporaryPlace.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CTitle_TemporaryPlace
// Content: タイトル仮置きオブジェクト
//
//=========================================================================
class CTitle_TemporaryPlace : public mslib::DX9_Object
{
public:
    CTitle_TemporaryPlace();
    ~CTitle_TemporaryPlace();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
};

