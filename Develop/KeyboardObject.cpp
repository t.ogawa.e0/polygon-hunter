//==========================================================================
// キーボードのオブジェクト[KeyboardObject.h]
// author: tatsuya ogawa
//==========================================================================
#include "KeyboardObject.h"

KeyboardObject::KeyboardObject()
{
    this->SetObjectName("KeyboardObject");
}

KeyboardObject::~KeyboardObject()
{
}

//=========================================================================
// 初期化
bool KeyboardObject::Init(void)
{
    return false;
}

//=========================================================================
// 解放
void KeyboardObject::Uninit(void)
{
}

//=========================================================================
// 更新
void KeyboardObject::Update(void)
{
}

//=========================================================================
// 描画
void KeyboardObject::Draw(void)
{
}

//=========================================================================
// デバッグ
void KeyboardObject::Debug(void)
{
    if (this->ImGui()->NewMenu("キー情報"))
    {
        const std::vector<std::string>key_text = { "false","true" };
        KeyboardType key_type = KeyboardType::Press;

        this->ImGui()->Separator();
        if (this->ImGui()->NewTreeNode("Press", false))
        {
            this->ImGui()->Text("Q [%s]", key_text[this->KEY_Q(key_type)].c_str());
            this->ImGui()->Text("W [%s]", key_text[this->KEY_W(key_type)].c_str());
            this->ImGui()->Text("E [%s]", key_text[this->KEY_E(key_type)].c_str());
            this->ImGui()->Text("A [%s]", key_text[this->KEY_A(key_type)].c_str());
            this->ImGui()->Text("S [%s]", key_text[this->KEY_S(key_type)].c_str());
            this->ImGui()->Text("D [%s]", key_text[this->KEY_D(key_type)].c_str());
            this->ImGui()->Text("U [%s]", key_text[this->KEY_U(key_type)].c_str());
            this->ImGui()->Text("I [%s]", key_text[this->KEY_I(key_type)].c_str());
            this->ImGui()->Text("O [%s]", key_text[this->KEY_O(key_type)].c_str());
            this->ImGui()->Text("J [%s]", key_text[this->KEY_J(key_type)].c_str());
            this->ImGui()->Text("K [%s]", key_text[this->KEY_K(key_type)].c_str());
            this->ImGui()->Text("L [%s]", key_text[this->KEY_L(key_type)].c_str());
            this->ImGui()->EndTreeNode();
        }
        this->ImGui()->Separator();
        key_type = KeyboardType::Trigger;
        this->ImGui()->Separator();
        if (this->ImGui()->NewTreeNode("Trigger", false))
        {
            this->ImGui()->Text("Q [%s]", key_text[this->KEY_Q(key_type)].c_str());
            this->ImGui()->Text("W [%s]", key_text[this->KEY_W(key_type)].c_str());
            this->ImGui()->Text("E [%s]", key_text[this->KEY_E(key_type)].c_str());
            this->ImGui()->Text("A [%s]", key_text[this->KEY_A(key_type)].c_str());
            this->ImGui()->Text("S [%s]", key_text[this->KEY_S(key_type)].c_str());
            this->ImGui()->Text("D [%s]", key_text[this->KEY_D(key_type)].c_str());
            this->ImGui()->Text("U [%s]", key_text[this->KEY_U(key_type)].c_str());
            this->ImGui()->Text("I [%s]", key_text[this->KEY_I(key_type)].c_str());
            this->ImGui()->Text("O [%s]", key_text[this->KEY_O(key_type)].c_str());
            this->ImGui()->Text("J [%s]", key_text[this->KEY_J(key_type)].c_str());
            this->ImGui()->Text("K [%s]", key_text[this->KEY_K(key_type)].c_str());
            this->ImGui()->Text("L [%s]", key_text[this->KEY_L(key_type)].c_str());
            this->ImGui()->EndTreeNode();
        }
        this->ImGui()->Separator();
        key_type = KeyboardType::Repeat;
        this->ImGui()->Separator();
        if (this->ImGui()->NewTreeNode("Repeat", false))
        {
            this->ImGui()->Text("Q [%s]", key_text[this->KEY_Q(key_type)].c_str());
            this->ImGui()->Text("W [%s]", key_text[this->KEY_W(key_type)].c_str());
            this->ImGui()->Text("E [%s]", key_text[this->KEY_E(key_type)].c_str());
            this->ImGui()->Text("A [%s]", key_text[this->KEY_A(key_type)].c_str());
            this->ImGui()->Text("S [%s]", key_text[this->KEY_S(key_type)].c_str());
            this->ImGui()->Text("D [%s]", key_text[this->KEY_D(key_type)].c_str());
            this->ImGui()->Text("U [%s]", key_text[this->KEY_U(key_type)].c_str());
            this->ImGui()->Text("I [%s]", key_text[this->KEY_I(key_type)].c_str());
            this->ImGui()->Text("O [%s]", key_text[this->KEY_O(key_type)].c_str());
            this->ImGui()->Text("J [%s]", key_text[this->KEY_J(key_type)].c_str());
            this->ImGui()->Text("K [%s]", key_text[this->KEY_K(key_type)].c_str());
            this->ImGui()->Text("L [%s]", key_text[this->KEY_L(key_type)].c_str());
            this->ImGui()->EndTreeNode();
        }
        this->ImGui()->Separator();

        this->ImGui()->EndMenu();
    }
}

bool KeyboardObject::KEY_Q(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_Q);
}

bool KeyboardObject::KEY_W(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_W);
}

bool KeyboardObject::KEY_S(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_S);
}

bool KeyboardObject::KEY_E(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_E);
}

bool KeyboardObject::KEY_A(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_A);
}

bool KeyboardObject::KEY_D(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_D);
}

bool KeyboardObject::KEY_U(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_U);
}

bool KeyboardObject::KEY_I(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_I);
}

bool KeyboardObject::KEY_O(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_O);
}

bool KeyboardObject::KEY_J(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_J);
}

bool KeyboardObject::KEY_K(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_K);
}

bool KeyboardObject::KEY_L(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_L);
}

bool KeyboardObject::KEY_SPACE(KeyboardType type)
{
    return this->KEY_MASTER(type, mslib::DirectInputKeyboardButton::KEY_SPACE);
}

//=========================================================================
// マスターキー
bool KeyboardObject::KEY_MASTER(KeyboardType type, mslib::DirectInputKeyboardButton key)
{
    switch (type)
    {
    case KeyboardObject::KeyboardType::Press:
        return this->Dinput_Keyboard()->Press(key);
        break;
    case KeyboardObject::KeyboardType::Trigger:
        return this->Dinput_Keyboard()->Trigger(key);
        break;
    case KeyboardObject::KeyboardType::Repeat:
        return this->Dinput_Keyboard()->Repeat(key);
        break;
    default:
        break;
    }
    return false;
}
