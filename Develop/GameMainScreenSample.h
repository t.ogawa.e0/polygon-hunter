//==========================================================================
// ゲーム画面のsample[PlayerObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
class CGameMainScreenSample : public mslib::DX9_Object
{
public:
    CGameMainScreenSample();
    ~CGameMainScreenSample();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:
    bool m_check;
};

