//==========================================================================
// 銃オブジェクト[CGunObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GunObject.h"
#include "PlayerObject.h"
#include "GameCamera.h"
#include "BulletNormalEffectObject.h"
#include "InputSystem.h"
#include "KeyboardObject.h"
#include "resource_list.h"
#include "StatusGage.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int Consumption_MP = 3;

CGunObject::CGunObject() : WeaponObject(mslib::DX9_ObjectID::Xmodel)
{
    this->SetObjectName("GunObject");
    this->m_PlayerObj = nullptr;
    this->m_CameraObj = nullptr;
    this->m_BulletNormal = nullptr;
    this->m_input = nullptr;
    this->m_keyboard_object = nullptr;
    this->m_StatusObj = nullptr;
    this->SetWeaponID(EList::Gun);
}

CGunObject::~CGunObject()
{
}

//=========================================================================
// 初期化
bool CGunObject::Init(void)
{
    auto *pObj = this->_3DObject()->Create();
    pObj->Init((int)EObject::Gun);
    pObj->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);
    this->_3DXmodel()->ObjectInput(pObj);

    pObj = this->_3DObject()->Create();
    pObj->Init((int)EObject::Muzzle);
    pObj->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);

    pObj = this->_3DObject()->Create();
    pObj->Init((int)EObject::Material);
    pObj->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);

    this->m_PlayerObj = this->GetObjects(mslib::DX9_ObjectID::Cube, "PlayerObject");
    this->m_CameraObj = this->GetObjects(mslib::DX9_ObjectID::Camera, "GameCamera");
    this->m_BulletNormal = this->GetObjects(mslib::DX9_ObjectID::Billboard, "BulletNormalEffectObject");
    this->m_input = this->GetObjects(mslib::DX9_ObjectID::Default, "InputSystem");
    this->m_keyboard_object = this->GetObjects(mslib::DX9_ObjectID::Default, "KeyboardObject");
    this->m_StatusObj = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "StatusGage");

    return this->_3DXmodel()->Init(RESOURCE_blade_銃_x, mslib::DX9_XmodelMoad::System);
}

//=========================================================================
// 解放
void CGunObject::Uninit(void)
{
}

//=========================================================================
// 更新
void CGunObject::Update(void)
{
    if (this->UpdateKey())
    {
        auto * p_keyborad = (KeyboardObject*)this->m_keyboard_object;
        auto * p_xinput = (CInputSystem*)this->m_input;
        auto *pPlayer = (CPlayerObject*)this->m_PlayerObj;
        auto *pObj = this->_3DObject()->Get((int)EObject::Gun);
        auto *pMuzzle = this->_3DObject()->Get((int)EObject::Muzzle);
        auto *pMaterial = this->_3DObject()->Get((int)EObject::Material);
        auto *pCamera = (CGameCamera*)this->m_CameraObj;
        auto *pBulletNormal = (BulletNormalEffectObject*)this->m_BulletNormal;
        auto *pStatusGage = (StatusGage*)this->m_StatusObj;

        if (mslib::nullptr_check(pCamera) && mslib::nullptr_check(pMaterial))
        {
            auto *pCameraObj = pCamera->_3DObject()->Get((int)CGameCamera::CameraObjectID::EyeObject);
            if (mslib::nullptr_check(pCameraObj))
            {
                *pMaterial = *pCameraObj;
                pMaterial->MoveZ(50);
            }
        }

        if (mslib::nullptr_check(pPlayer) && mslib::nullptr_check(pMaterial))
        {
            // オブジェクトの準備
            *pObj = *pPlayer->_3DObject()->Get((int)CPlayerObject::ObjectType::Visualization);

            // 銃の更新
            pObj->SetIndex((int)EObject::Gun);
            pObj->MoveY(0.8f);
            pObj->MoveX(0.8f);

            // マテリアルを与える
            pObj->LockOn(*pMaterial);

            // ノズルの更新
            *pMuzzle = *pObj;
            pMuzzle->SetIndex((int)EObject::Muzzle);
            pMuzzle->MoveZ(1.4f);
            pMuzzle->MoveY(0.045f);
        }

        if (mslib::nullptr_check(pBulletNormal) && mslib::nullptr_check(p_keyborad) && mslib::nullptr_check(pStatusGage))
        {
            if (p_keyborad->KEY_U(KeyboardObject::KeyboardType::Press))
            {
                auto oldmp = pStatusGage->GetStatusData()->m_MP;
                if (pStatusGage->MP_Operation(-Consumption_MP))
                {
                    pBulletNormal->Create(pMuzzle, { 0,0,1.0f }, { 0,0,0 }, 100);
                }
                else
                {
                    pStatusGage->MP_Operation(oldmp);
                }
            }
        }
        if (mslib::nullptr_check(pBulletNormal) && mslib::nullptr_check(p_xinput) && mslib::nullptr_check(pStatusGage))
        {
            if (p_xinput->GetXInput()->RT(0))
            {
                auto oldmp = pStatusGage->GetStatusData()->m_MP;
                if (pStatusGage->MP_Operation(-Consumption_MP))
                {
                    pBulletNormal->Create(pMuzzle, { 0,0,1.0f }, { 0,0,0 }, 100);
                }
                else
                {
                    pStatusGage->MP_Operation(oldmp);
                }
            }
        }
        if (this->Dinput_Mouse()->Press(mslib::DirectInputMouseButton::Left))
        {
            auto oldmp = pStatusGage->GetStatusData()->m_MP;
            if (pStatusGage->MP_Operation(-Consumption_MP))
            {
                pBulletNormal->Create(pMuzzle, { 0,0,1.0f }, { 0,0,0 }, 100);
            }
            else
            {
                pStatusGage->MP_Operation(oldmp);
            }
        }

        this->_3DXmodel()->Update();
    }
}

//=========================================================================
// 描画
void CGunObject::Draw(void)
{
    if (this->UpdateKey())
    {
        this->_3DXmodel()->Draw();
    }
}

//=========================================================================
// デバッグ
void CGunObject::Debug(void)
{
}

//=========================================================================
// リセットモーション
void CGunObject::Reset(void)
{
    auto *pAnimation = this->_3DObjectMotion()->Get(0);

    if (mslib::nullptr_check(pAnimation))
    {
        pAnimation->Reset();
        this->SetCheck(false, false);
    }
}
