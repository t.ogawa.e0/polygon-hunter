//==========================================================================
// アクションリスト[Game_Action_List.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Action_List.h"

//==========================================================================
// ジャンプ(二段ジャンプ可能)
// 地面 = _y_1
// 現在のY軸 = _y_2
// ジャンプ力 = _powor
// _key = 発動キー
float CJump::_Jump(float _y_1, float _y_2, float _powor, bool &_key)
{
    // ジャンプ終了判定
    if (_y_1 == _y_2)
    {
        this->m_jump_powor = 0.0f;
        this->m_jump_cont = 0;
    }

    // ジャンプ回数の制御
    if (_key == true && this->m_jump_cont <= 1)
    {
        this->m_jump_cont++;
        this->m_jump_powor = _powor;
    }
    else
    {
        _key = false;
    }

    // 減衰
    this->m_jump_powor *= 0.9f;
    return this->m_jump_powor;
}

//==========================================================================
// ステップ
bool CStep::_Step(float _powor, bool _key, mslib::DX9_3DObject * pOut, const D3DXVECTOR3 * pVec)
{
    // 処理対象が存在
    if (pOut != nullptr)
    {
        // ステップ開始
        if (_key == true && this->m_step_powor == 0.0f)
        {
            // ベクトルが指定されているとき
            if (pVec != nullptr)
            {
                this->m_step_move_powor = mslib::CVector3<float>(pVec->x, pVec->y, pVec->z);
                this->m_step_move_powor *= _powor;
                this->m_step_change = true;
            }

            this->m_step_powor = _powor;
        }

        // ステップの力が働いているとき
        if (0.1f < this->m_step_powor)
        {
            // 減衰
            this->m_step_powor *= 0.9f;
            this->m_step_move_powor *= 0.9f;

            // ベクトル指定がある場合
            if (this->m_step_change)
            {
                pOut->MoveX(this->m_step_move_powor.x);
                pOut->MoveZ(this->m_step_move_powor.z);
            }
            else
            {
                pOut->MoveZ(this->m_step_powor);
            }

            return this->m_key = true;
        }
        else
        {
            this->m_step_move_powor = 0.0f;
            this->m_step_powor = 0.0f;
            this->m_step_change = false;
        }
    }

    return this->m_key = false;
}
