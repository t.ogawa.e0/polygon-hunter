//==========================================================================
// 防御フィールド[EnemyDefensiveField.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "EnemyDefensiveField.h"
#include "EnemyLargeBoss.h"
#include "resource_list.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int __Limit_Life = 30000; // 耐久値
constexpr float __Limit_Scale = 10.0f; // スケール
constexpr float __RotSpeed = 0.005f; // 回転速度

EnemyDefensiveField::EnemyDefensiveField() : DefensiveFieldObject(mslib::DX9_ObjectID::Sphere)
{
    this->SetObjectName("EnemyDefensiveField");
    this->m_EnemyLargeBoss = nullptr;
}

EnemyDefensiveField::~EnemyDefensiveField()
{
}

//=========================================================================
// 初期化
bool EnemyDefensiveField::Init(void)
{
    this->m_EnemyLargeBoss = this->GetObjects(mslib::DX9_ObjectID::Cube, "EnemyLargeBoss");

    return this->_3DSphere()->Init(RESOURCE_BS07s_png, 30);
}

//=========================================================================
// 解放
void EnemyDefensiveField::Uninit(void)
{
}

//=========================================================================
// 更新
void EnemyDefensiveField::Update(void)
{
    // 生成処理
    this->Create();

    // 処理
    this->DefensiveFieldUpdate();

    // 破棄
    this->DefensiveFieldDelete();

    // データの整合性チェック
    this->CheckData();

    // ログから破棄
    this->CheckData();

    // ベクトルの視覚化
    this->SetVector();

    // 更新
    this->_3DSphere()->Update();
}

//=========================================================================
// 描画
void EnemyDefensiveField::Draw(void)
{
    this->_3DSphere()->Draw();
}

//=========================================================================
// デバッグ
void EnemyDefensiveField::Debug(void)
{
    // デバッグ用テキスト
    this->InputParamText();
}

//=========================================================================
// 生成
void EnemyDefensiveField::Create(void)
{
    if (mslib::nullptr_check(this->m_EnemyLargeBoss))
    {
        auto *pEnemyLargeBoss = (EnemyLargeBoss*)this->m_EnemyLargeBoss;
        auto * pEnemy = pEnemyLargeBoss->GetEnemyParam();

        for (auto itr = pEnemy->begin(); itr != pEnemy->end(); ++itr)
        {
            auto itr2 = std::find(this->m_create_log.begin(), this->m_create_log.end(), itr->Get3DObject());

            // ログに存在しない
            if (itr2 == this->m_create_log.end())
            {
                // 生成
                this->m_obj[itr->Get3DObject()];

                // ログに登録
                this->m_create_log.push_back(itr->Get3DObject());

                // 再取得
                auto itr3 = this->m_obj.find(itr->Get3DObject());

                // データセット
                itr3->second.ControlLife(__Limit_Life);
                itr3->second.Get3DObject()->SetIndex(0);
                itr3->second.Get3DObject()->ScaleSet(__Limit_Scale);
                this->_3DSphere()->ObjectInput(itr3->second.Get3DObject());

                this->m_obj_mirror[itr3->second.Get3DObject()];
                auto itr4 = this->m_obj_mirror.find(itr3->second.Get3DObject());
                if (itr4 != this->m_obj_mirror.end())
                {
                    itr4->second.SetIndex(0);
                    itr4->second.ScaleSet(-__Limit_Scale);
                    this->_3DSphere()->ObjectInput(itr4->second);
                }
            }
        }
    }
}

//=========================================================================
// 処理
void EnemyDefensiveField::DefensiveFieldUpdate(void)
{
    if (mslib::nullptr_check(this->m_EnemyLargeBoss))
    {
        auto *pEnemyLargeBoss = (EnemyLargeBoss*)this->m_EnemyLargeBoss;
        auto * pEnemy = pEnemyLargeBoss->GetEnemyParam();

        for (auto itr = pEnemy->begin(); itr != pEnemy->end(); ++itr)
        {
            // このアドレスが存在のチェック
            auto itr2 = this->m_obj.find(itr->Get3DObject());
            // 存在する
            if (itr2 != this->m_obj.end())
            {
                itr2->second.Get3DObject()->SetMatInfoPos(*itr->Get3DObject()->GetMatInfoPos());
                itr2->second.Get3DObject()->RotX(__RotSpeed);
                auto itr3 = this->m_obj_mirror.find(itr2->second.Get3DObject());
                if (itr3 != this->m_obj_mirror.end())
                {
                    itr3->second.SetMatInfoPos(*itr->Get3DObject()->GetMatInfoPos());
                    itr3->second.RotX(__RotSpeed);
                }

                if (itr2->second.GetLife() < 0)
                {
                    itr2->second.DeleteKeyON();
                }
            }
        }
    }
}

//=========================================================================
// 破棄
void EnemyDefensiveField::DefensiveFieldDelete(void)
{
    // オブジェクトの破棄
    for (auto itr = this->m_obj.begin(); itr != this->m_obj.end(); )
    {
        if (itr->second.GetDeleteKey())
        {
            this->_3DSphere()->ObjectDelete(itr->second.Get3DObject());
            auto itr2 = this->m_obj_mirror.find(itr->second.Get3DObject());
            if (itr2 != this->m_obj_mirror.end())
            {
                this->_3DSphere()->ObjectDelete(itr2->second);
                this->m_obj_mirror.erase(itr2);
            }
            itr = this->m_obj.erase(itr);
        }
        else if (itr->second.GetDeleteKey2())
        {
            this->_3DSphere()->ObjectDelete(itr->second.Get3DObject());
            auto itr2 = this->m_obj_mirror.find(itr->second.Get3DObject());
            if (itr2 != this->m_obj_mirror.end())
            {
                this->_3DSphere()->ObjectDelete(itr2->second);
                this->m_obj_mirror.erase(itr2);
            }
            itr = this->m_obj.erase(itr);
        }
        else
        {
            ++itr;
        }
    }
}

//=========================================================================
// データのチェック
void EnemyDefensiveField::CheckData(void)
{
    //if (mslib::nullptr_check(this->m_EnemyLargeBoss))
    //{
    //    if (this->m_obj.size() != 0)
    //    {
    //        auto *pEnemyLargeBoss = (EnemyLargeBoss*)this->m_EnemyLargeBoss;
    //        auto * pEnemy = pEnemyLargeBoss->GetEnemyParam();
    //        auto data_list = this->m_create_log; // データ

    //        // エネミーが存在
    //        for (auto itr = pEnemy->begin(); itr != pEnemy->end(); ++itr)
    //        {
    //            // このアドレスが存在のチェック
    //            auto itr2 = this->m_obj.find(itr->Get3DObject());

    //            // 存在する
    //            if (itr2 != this->m_obj.end())
    //            {
    //                // 存在する場合対象から外す
    //                auto itr3 = std::find(data_list.begin(), data_list.end(), itr->Get3DObject());
    //                data_list.erase(itr3);
    //            }
    //        }

    //        // データが消滅したオブジェクトのアドレスのログを破棄する
    //        for (auto itr = data_list.begin(); itr != data_list.end(); ++itr)
    //        {
    //            // ログから対象を検索
    //            auto itr2 = std::find(this->m_create_log.begin(), this->m_create_log.end(), *itr);
    //            auto itr3 = this->m_obj.find(*itr);
    //            // データを確認
    //            if (itr3 != this->m_obj.end())
    //            {
    //                // 破棄
    //                this->_3DSphere()->ObjectDelete(itr3->second.Get3DObject());
    //                this->m_obj.erase(itr3);
    //            }
    //            // データを確認
    //            if (itr2 != this->m_create_log.end())
    //            {
    //                // 破棄
    //                this->m_create_log.erase(itr2);
    //            }
    //        }
    //    }
    //}
}
