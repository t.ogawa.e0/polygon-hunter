//==========================================================================
// ブーストゲージ[BurstGage.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "BurstGage.h"
#include "resource_list.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr float __tex_scale = 0.5f;
constexpr float __tex_scale2 = 0.3f;
constexpr int __percent_limit = 100;
constexpr int __lv_limit = 99;

BurstGage::BurstGage() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->m_lv = 0;
    this->m_percent = 0;
    this->m_plus_buff = 0;
    this->m_minus_buff = 0;
    this->m_overflow_minus_buff = __percent_limit;
    this->SetObjectName("BurstGage");
}


BurstGage::~BurstGage()
{
}

//==========================================================================
// 初期化
bool BurstGage::Init(void)
{
    this->_2DPolygon()->Init(RESOURCE_BurstGageBack_png, true);
    this->_2DPolygon()->Init(RESOURCE_Lv_IMG_png, true);
    this->_2DPolygon()->Init(RESOURCE_percent_png, true);
    this->_2DNumber()->Init(RESOURCE_Number_png, true);
    this->_2DNumber()->Init(RESOURCE_Number_png, true);

    this->_2DObject()->Reserve((int)ObjectList::End);

    for (int i = (int)ObjectList::Begin; i < (int)ObjectList::End; i++)
    {
        auto* pObj = this->_2DObject()->Create();
        pObj->Init(i);
        this->_2DPolygon()->ObjectInput(pObj);
    }

    this->_2DNumber()->Set((int)ObjectNumberList::Lv, 2, true, false, { 0,0 });
    this->_2DNumber()->SetAnim((int)ObjectNumberList::Lv, 1, 10, 10);
    this->_2DNumber()->SetTexScale((int)ObjectNumberList::Lv, __tex_scale);

    this->_2DNumber()->Set((int)ObjectNumberList::percent, 3, false, false, { 0,0 });
    this->_2DNumber()->SetAnim((int)ObjectNumberList::percent, 1, 10, 10);
    this->_2DNumber()->SetTexScale((int)ObjectNumberList::percent, __tex_scale2);

    this->_2DPolygon()->SetTexScale((int)ObjectList::Percent, __tex_scale);

    auto * ptime = this->_Timer()->Create();
    ptime->Init(1, 60);

    auto & __rand = *this->_rand_int()->Create();
    __rand = mslib::rand_int(5, 50);

    this->ObjectLoad();

    return false;
}

//==========================================================================
// 解放
void BurstGage::Uninit(void)
{
}

//==========================================================================
// 更新
void BurstGage::Update(void)
{
    auto * ptime = this->_Timer()->Get(0);

    if (ptime->Countdown())
    {
        auto & __rand = *this->_rand_int()->Get(0);

        this->Minus(__rand(this->GetMt19937()));
        ptime->Init(1, 60);
    }

    this->ColorChange();
    this->Level();

    // 描画のための更新
    this->_2DNumber()->Update((int)ObjectNumberList::Lv, this->m_lv);
    this->_2DNumber()->Update((int)ObjectNumberList::percent, this->m_percent);
    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void BurstGage::Draw(void)
{
    this->_2DPolygon()->Draw();
    this->_2DNumber()->Draw();
}

//==========================================================================
// デバッグ
void BurstGage::Debug(void)
{
    int nBuffUP = 0;
    int nBuffDOWN = 0;

    if (this->ImGui()->SliderInt("レベルUP", &nBuffUP, 0, 100))
    {
        this->Plus(nBuffUP);
    }
    if (this->ImGui()->SliderInt("レベルDOWN", &nBuffDOWN, 0, 100))
    {
        this->Minus(nBuffDOWN);
    }
}

//==========================================================================
// 加算
void BurstGage::Plus(int value)
{
    this->m_plus_buff += value;
}

//==========================================================================
// 減算
void BurstGage::Minus(int value)
{
    this->m_minus_buff += value;
}

//==========================================================================
// レベルのゲッター
int BurstGage::GetLevel(void)
{
    return this->m_lv;
}

int BurstGage::GetOverflowMinusBuff(void)
{
    return this->m_overflow_minus_buff;
}

//==========================================================================
// 色の更新
void BurstGage::ColorChange(void)
{
    auto *pColorObj = this->_2DObject()->Get((int)ObjectList::BurstGageBackground);
    auto color = *pColorObj->GetColor();
    if (color.r == 255)
    {
        if (0 < color.b)
        {
            color.b--;
        }
        if (color.b == 0)
        {
            color.g++;
        }
    }
    if (color.g == 255)
    {
        if (0 < color.r)
        {
            color.r--;
        }
        if (color.r == 0)
        {
            color.b++;
        }
    }
    if (color.b == 255)
    {
        if (0 < color.g)
        {
            color.g--;
        }
        if (color.g == 0)
        {
            color.r++;
        }
    }
    pColorObj->SetColor(color);
}

//==========================================================================
// レベル処理
void BurstGage::Level(void)
{
    int ncount = 0;

    //==========================================================================
    // レベルUP処理
    //==========================================================================
    for (ncount = 0; ncount < this->m_plus_buff; ncount++)
    {
        this->m_percent++;
        if (__percent_limit < this->m_percent)
        {
            this->m_percent = 0;
            this->m_lv++;
        }
    }
    this->m_plus_buff -= ncount;
    if (__lv_limit < this->m_lv)
    {
        this->m_lv = __lv_limit;
        this->m_percent = __percent_limit;
        this->m_plus_buff = 0;
    }

    //==========================================================================
    // レベルDOWN処理
    //==========================================================================
    for (ncount = 0; ncount < this->m_minus_buff; ncount++)
    {
        this->m_percent--;
        if (this->m_percent < 0)
        {
            this->m_percent = __percent_limit;
            this->m_lv--;
            break;
        }
    }
    this->m_minus_buff -= ncount;
    if (this->m_lv < 0)
    {
        this->m_overflow_minus_buff = this->m_minus_buff;
        this->m_minus_buff = 0;
        this->m_percent = 0;
        this->m_lv = 0;
    }
}
