//==========================================================================
// エフェクトオブジェクト[EffectObject.h]
// author: tatsuya ogawa
//==========================================================================
#include "EffectObject.h"

EffectObject::EffectObject(mslib::DX9_ObjectID ObjectID) : DX9_Object(ObjectID)
{
}

EffectObject::~EffectObject()
{
}

EffectObject::ModelEffectParam::ModelEffectParam()
{
    this->m_obj.Init(0);
    this->m_MoveSpeed = 0.0f;
    this->m_limit_time = 0;
    this->m_time = 0;
    this->m_release_key = false;
}

EffectObject::ModelEffectParam::~ModelEffectParam()
{
}

//=========================================================================
// 解放キー
void EffectObject::ModelEffectParam::ReleaseKey(void)
{
    this->m_release_key = true;
}

//=========================================================================
// 解放キーの取得
bool EffectObject::ModelEffectParam::GetReleaseKey(void)
{
    return this->m_release_key;
}

//=========================================================================
// オブジェクトの取得
mslib::DX9_3DObject * EffectObject::ModelEffectParam::GetData(void)
{
    return &this->m_obj;
}
