//==========================================================================
// ボタンを押してオブジェクト[TitlePressKey.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "TitlePressKey.h"
#include "resource_list.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int __color_buf = 5;

TitlePressKey::TitlePressKey() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->SetObjectName("TitlePressKey");
    this->m_color_buf = __color_buf;
}

TitlePressKey::~TitlePressKey()
{
}

//=========================================================================
// 初期化
bool TitlePressKey::Init(void)
{
    auto *pObj = this->_2DObject()->Create();
    pObj->Init(0);

    this->ObjectLoad();

    this->_2DPolygon()->ObjectInput(pObj);
    return this->_2DPolygon()->Init(RESOURCE_PressKey_png, true);
}

//=========================================================================
// 解放
void TitlePressKey::Uninit(void)
{
}

//=========================================================================
// 更新
void TitlePressKey::Update(void)
{
    auto *pObj = this->_2DObject()->Get(0);
    auto * pColor = const_cast<mslib::CColor<int>*>(pObj->GetColor());

    pColor->a += this->m_color_buf;
    if (pColor->a <= 0)
    {
        this->m_color_buf = __color_buf;
        pColor->a = 0;
    }
    else if (255 <= pColor->a)
    {
        this->m_color_buf = -__color_buf;
        pColor->a = 255;
    }

    this->_2DPolygon()->Update();
}

//=========================================================================
// 描画
void TitlePressKey::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//=========================================================================
// デバッグ
void TitlePressKey::Debug(void)
{
}
