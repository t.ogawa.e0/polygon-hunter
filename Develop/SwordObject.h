//==========================================================================
// 短剣オブジェクト[SwordObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "WeaponObject.h"

//==========================================================================
//
// class  : DaggerBlade
// Content: 刀身オブジェクト
//
//=========================================================================
class DaggerBlade
{
public:
    DaggerBlade();
    ~DaggerBlade();
    // オブジェクト取得
    mslib::DX9_3DObject * Get3DObject();
    // リセット
    void Reset(void);
    // 当たり判定チェック
    bool Hit(void * ptr);
private:
    mslib::DX9_3DObject m_obj; // オブジェクト
    std::list<void*>m_hit_ptr; // 当たり判定ポインタ
};

//==========================================================================
//
// class  : DaggerObject
// Content: 短剣オブジェクト
//
//=========================================================================
class SwordObject : public WeaponObject
{
public:
    enum class MyEnumClass
    {
        body, // 本体
        blade1, // 刀身
        blade2, // 刀身
        blade3, // 刀身
        blade4, // 刀身
    };
public:
    SwordObject();
    ~SwordObject();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    // リセットモーション
    void Reset(void)override;
    // 刀身の取得
    std::list<DaggerBlade> * GetBladeObject(void);
private:
    // vectorセット
    void SetVector(void);
private:
    void * m_PlayerObj; // プレイヤーオブジェクト
    void * m_GameICODrawObject;
    std::list<DaggerBlade> m_blade; // 刀身オブジェクト
    bool m_blade_lock; // ブレードのロック
};

