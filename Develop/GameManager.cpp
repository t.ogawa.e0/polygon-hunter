//==========================================================================
// ゲームマネージャー[GameManager.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameManager.h"
#include "GameCamera.h"
#include "PlayerObject.h"
#include "EnemySmallSize.h"
#include "EnemyLargeBoss.h"
#include "StatusGage.h"
#include "Screen.h" 
#include "ControlWeapon.h"
#include "Game_Timer.h"
#include "PlayObject.h"
#include "HighGround.h"
#include "Game_Over_Clear.h"
#include "UserInformation.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int __num_enemy_small = 10; // 同時出現上限
constexpr int __num_enemy_smal_MAX = 500; // 雑魚エネミー数
constexpr int __num_enemy_boss = 3; // ボスエネミー数

CGameManager::CGameManager()
{
    this->SetObjectName("GameManager");
    this->m_count_enemy_small = 0;
    this->m_pop_count = 0;
    this->m_SetPlayKey = false;
}

CGameManager::~CGameManager()
{
}

//=========================================================================
// 初期化
bool CGameManager::Init(void)
{
    this->m_GameCamera = this->GetObjects(mslib::DX9_ObjectID::Camera, "GameCamera");
    this->m_EnemySmallSize = this->GetObjects(mslib::DX9_ObjectID::Cube, "EnemySmallSize");
    this->m_EnemyLargeBoss = this->GetObjects(mslib::DX9_ObjectID::Cube, "EnemyLargeBoss");
    this->m_PlayObject = this->GetObjects(mslib::DX9_ObjectID::Cube, "PlayerObject");
    this->m_StatusObj = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "StatusGage");
    this->m_ControlWeapon = this->GetObjects(mslib::DX9_ObjectID::Default, "ControlWeapon");
    this->m_Game_Timer = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "Game_Timer");
    this->m_HighGround = this->GetObjects(mslib::DX9_ObjectID::Mesh, "HighGround");
    this->m_Game_Over_Clear = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "Game_Over_Clear");
    this->m_UserInformation = this->GetObjects(mslib::DX9_ObjectID::Default, "UserInformation");

    auto * pEnemySmallSize = (EnemySmallSize*)this->m_EnemySmallSize;
    auto * pEnemyLargeBoss = (EnemyLargeBoss*)this->m_EnemyLargeBoss;

    if (mslib::nullptr_check(pEnemySmallSize))
    {
        for (int i = 0; i < __num_enemy_small; i++)
        {
            pEnemySmallSize->Create();
            this->m_count_enemy_small++;
        }
    }
    if (mslib::nullptr_check(pEnemyLargeBoss))
    {
        pEnemyLargeBoss->Create();
        this->m_pop_count++;
    }

    this->_Timer()->Create()->Init(1, 60);
    this->Light()->Init({ 0.0f,-1.0f,0.0f });
    return false;
}

//=========================================================================
// 解放
void CGameManager::Uninit(void)
{
}

//=========================================================================
// 更新
void CGameManager::Update(void)
{
    this->GemeEnd();
    this->SetUpdateKey();
    this->CreateEnemy();
}

//=========================================================================
// 描画
void CGameManager::Draw(void)
{
}

//=========================================================================
// デバッグ
void CGameManager::Debug(void)
{
}

//=========================================================================
// 全objectの更新有効化
void CGameManager::SetUpdateKey(void)
{
    // 高台が消滅したときと、処理ロック未解除時
    if (mslib::nullptr_check(this->m_HighGround))
    {
        auto * pHighGround = (HighGround*)this->m_HighGround;
        if (pHighGround->GetPlayKey() == false && this->m_SetPlayKey == false)
        {
            this->Play();
            this->m_SetPlayKey = true;
        }
    }
}

//=========================================================================
// エネミーの生成
void CGameManager::CreateEnemy(void)
{
    // 自動生成
    if (mslib::nullptr_check(this->m_EnemySmallSize) && this->m_count_enemy_small < __num_enemy_smal_MAX)
    {
        auto * pEnemySmallSize = (EnemySmallSize*)this->m_EnemySmallSize;
        for (int i = pEnemySmallSize->Size(); i < __num_enemy_small; i++)
        {
            pEnemySmallSize->Create();
            this->m_count_enemy_small++;
        }
    }

    // 自動生成
    if (mslib::nullptr_check(this->m_EnemyLargeBoss))
    {
        auto * pEnemyLargeBoss = (EnemyLargeBoss*)this->m_EnemyLargeBoss;
        if (pEnemyLargeBoss->Size() == 0)
        {
            int i = this->m_pop_count;
            for (; i < __num_enemy_boss; i++)
            {
                pEnemyLargeBoss->Create();
                this->m_pop_count++;
            }
        }
    }
}

//=========================================================================
// ゲーム終了キー
void CGameManager::GemeEnd(void)
{
    // HPが0になったら
    if (mslib::nullptr_check(this->m_StatusObj))
    {
        auto * pStatusGage = (StatusGage*)this->m_StatusObj;

        if (pStatusGage->GetStatusData()->m_HP <= 0.0f)
        {
            this->Stop();
            this->SaveUserInformation(false);
            this->GameOver();
        }
    }

    // 時間切れ
    if (mslib::nullptr_check(this->m_Game_Timer))
    {
        auto * pGame_Timer = (CGame_Timer*)this->m_Game_Timer;

        if (pGame_Timer->GetTime() <= 0)
        {
            this->Stop();
            this->SaveUserInformation(false);
            this->GameOver();
        }
    }

    // ボスエネミーを全て倒した
    if (__num_enemy_boss <= this->m_pop_count)
    {
        if (mslib::nullptr_check(this->m_EnemyLargeBoss))
        {
            auto * pEnemyLargeBoss = (EnemyLargeBoss*)this->m_EnemyLargeBoss;
            if (pEnemyLargeBoss->Size() == 0)
            {
                this->Stop();
                this->SaveUserInformation(true);
                this->GameClear();
            }
        }
    }
}

//=========================================================================
// ゲームオーバー
void CGameManager::GameOver(void)
{
    auto *pGameOver = (Game_Over_Clear*)this->m_Game_Over_Clear;

    if (mslib::nullptr_check(pGameOver))
    {
        pGameOver->SetObjectMenu(Game_Over_Clear::MyEnumClass::GameOver);
        pGameOver->UpdatePlay();
        if (pGameOver->GetPlayKey())
        {
            auto * pTimer = this->_Timer()->Get(0);

            if (pTimer->Countdown())
            {
                CScreen::screenchange(SceneName::Result);
            }
        }
    }
}

//=========================================================================
// ゲームクリア
void CGameManager::GameClear(void)
{
    auto *pGameClear = (Game_Over_Clear*)this->m_Game_Over_Clear;

    if (mslib::nullptr_check(pGameClear))
    {
        pGameClear->SetObjectMenu(Game_Over_Clear::MyEnumClass::GameClear);
        pGameClear->UpdatePlay();
        if (pGameClear->GetPlayKey())
        {
            auto * pTimer = this->_Timer()->Get(0);

            if (pTimer->Countdown())
            {
                CScreen::screenchange(SceneName::Result);
            }
        }
    }
}

//=========================================================================
// 停止
void CGameManager::Stop(void)
{
    auto * pStatusGage = (StatusGage*)this->m_StatusObj;
    auto * pPlayObject = (CPlayerObject*)this->m_PlayObject;
    auto * pControlWeapon = (ControlWeapon*)this->m_ControlWeapon;
    auto * pGame_Timer = (CGame_Timer*)this->m_Game_Timer;
    auto * pEnemySmallSize = (EnemySmallSize*)this->m_EnemySmallSize;
    auto * pEnemyLargeBoss = (EnemyLargeBoss*)this->m_EnemyLargeBoss;

    if (mslib::nullptr_check(pStatusGage))
    {
        pStatusGage->UpdateStop();
    }
    if (mslib::nullptr_check(pEnemySmallSize))
    {
        pEnemySmallSize->UpdateStop();
    }
    if (mslib::nullptr_check(pEnemyLargeBoss))
    {
        pEnemyLargeBoss->UpdateStop();
    }
    if (mslib::nullptr_check(pPlayObject))
    {
        pPlayObject->UpdateStop();
    }
    if (mslib::nullptr_check(pControlWeapon))
    {
        pControlWeapon->UpdateStop();
    }
    if (mslib::nullptr_check(pGame_Timer))
    {
        pGame_Timer->UpdateStop();
    }
}

//=========================================================================
// 実行
void CGameManager::Play(void)
{
    auto * pPlayObject = (CPlayerObject*)this->m_PlayObject;
    auto * pControlWeapon = (ControlWeapon*)this->m_ControlWeapon;
    auto * pGame_Timer = (CGame_Timer*)this->m_Game_Timer;
    auto * pEnemySmallSize = (EnemySmallSize*)this->m_EnemySmallSize;
    auto * pEnemyLargeBoss = (EnemyLargeBoss*)this->m_EnemyLargeBoss;
    auto * pStatusGage = (StatusGage*)this->m_StatusObj;

    if (mslib::nullptr_check(pStatusGage))
    {
        pStatusGage->UpdatePlay();
    }
    if (mslib::nullptr_check(pEnemySmallSize))
    {
        pEnemySmallSize->UpdatePlay();
    }
    if (mslib::nullptr_check(pEnemyLargeBoss))
    {
        pEnemyLargeBoss->UpdatePlay();
    }
    if (mslib::nullptr_check(pPlayObject))
    {
        pPlayObject->UpdatePlay();
    }
    if (mslib::nullptr_check(pControlWeapon))
    {
        pControlWeapon->UpdatePlay();
    }
    if (mslib::nullptr_check(pGame_Timer))
    {
        pGame_Timer->UpdatePlay();
    }
}

//=========================================================================
// 仮データの生成
void CGameManager::SaveUserInformation(bool end_key)
{
    if (mslib::nullptr_check(this->m_UserInformation) && mslib::nullptr_check(this->m_Game_Timer))
    {
        UserInformationData data;
        auto * pGame_Timer = (CGame_Timer*)this->m_Game_Timer;
        auto * pUserInformation = (UserInformation*)this->m_UserInformation;

        // データの受け渡し
        data.m_minute = *pGame_Timer->GetClearMinute();
        data.m_seconds = *pGame_Timer->GetClearSeconds();
        data.m_JudgmentOfSuccess = end_key;

        // 新規データとして保存
        pUserInformation->InputNewData(data);
    }
}
