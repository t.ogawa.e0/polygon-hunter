//==========================================================================
// 重力[Gravity.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGravity_
// Content: 重力処理
//
//=========================================================================
class CGravity_
{
public:
    CGravity_() {
        this->m_power = 0.0f;
        this->m_ground = nullptr;
        this->m_pos = nullptr;
    }
    CGravity_(mslib::DX9_3DObject * _obj, D3DXVECTOR3 * _ground) {
        this->m_power = 0.0f;
        this->m_ground = _ground;
        this->m_pos = _obj;
    }
    ~CGravity_() {}
    bool ObjectAddress(const mslib::DX9_3DObject * _obj);
    // 更新
    void Update(void);
private:
    float m_power; // パワー
    mslib::DX9_3DObject * m_pos; // 座標
    D3DXVECTOR3 *m_ground; // 地面
};

//==========================================================================
//
// class  : CGravity
// Content: 重力
//
//=========================================================================
class CGravity : public mslib::DX9_Object
{
public:
    CGravity() :DX9_Object(mslib::DX9_ObjectID::Default) {
        this->SetObjectName("Gravity");
    }
    ~CGravity() {
        this->Uninit();
    }
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;

    void SetGravity(mslib::DX9_3DObject * _obj, D3DXVECTOR3 * _ground);
    void DeleteGravity(mslib::DX9_3DObject * _obj);
private:
    std::list<CGravity_>m_gravity; // 重力
};
