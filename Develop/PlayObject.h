//==========================================================================
// プレイオブジェクト(継承)[PlayObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
//
// class  : PlayObject(継承)
// Content: プレイオブジェクト(継承)
//
//=========================================================================
class PlayObject
{
private:
    // コピー禁止 (C++11)
    PlayObject(const PlayObject &) = delete;
    PlayObject &operator=(const PlayObject &) = delete;
public:
    PlayObject();
    virtual ~PlayObject();
    // 更新開始
    void UpdatePlay(void);
    // 更新停止
    void UpdateStop(void);
    // 実行フラグ取得
    bool GetPlayKey(void);
private:
    bool m_play; // プレイ
};

