//==========================================================================
// ダメージポリゴン[DamagePolygonObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DamagePolygonObject.h"
#include "LinearPolygon.h"
#include "EnclosurePolygon.h"

DamagePolygonObject::DamagePolygonObject()
{
    this->SetObjectName("DamagePolygonObject");
}

DamagePolygonObject::~DamagePolygonObject()
{
    this->m_object_data.clear();
}

//==========================================================================
// 初期化
bool DamagePolygonObject::Init(void)
{
    auto *pLinearPolygon = (LinearPolygon*)this->GetObjects(mslib::DX9_ObjectID::Cube, "LinearPolygon");
    auto *pEnclosurePolygon = (EnclosurePolygon*)this->GetObjects(mslib::DX9_ObjectID::Cube, "EnclosurePolygon");

    if (mslib::nullptr_check(pLinearPolygon))
    {
        this->m_object_data.push_back(pLinearPolygon->GetDamagePolygonData());
    }
    if (mslib::nullptr_check(pEnclosurePolygon))
    {
        this->m_object_data.push_back(pEnclosurePolygon->GetDamagePolygonData());
    }

    return false;
}

//==========================================================================
// 解放
void DamagePolygonObject::Uninit(void)
{
}

//==========================================================================
// 更新
void DamagePolygonObject::Update(void)
{
}

//==========================================================================
// 描画
void DamagePolygonObject::Draw(void)
{
}

//==========================================================================
// デバッグ
void DamagePolygonObject::Debug(void)
{
}

std::list<std::list<DamagePolygonData>*>* DamagePolygonObject::GetDamagePolygonData(void)
{
    return &this->m_object_data;
}

DamagePolygonInputSystem::DamagePolygonInputSystem()
{
    this->m_ExplosiveEffect = nullptr;
}

DamagePolygonInputSystem::~DamagePolygonInputSystem()
{
    this->m_param.clear();
}

//==========================================================================
// 攻撃データの取得
std::list<DamagePolygonData>* DamagePolygonInputSystem::GetDamagePolygonData(void)
{
    return &this->m_param;
}
