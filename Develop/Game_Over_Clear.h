//==========================================================================
// ゲームオーバーとクリア[Game_Over_Clear.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "PlayObject.h"

//==========================================================================
//
// class  : Game_Over_Clear
// Content: ゲームオーバーとクリア
//
//=========================================================================
class Game_Over_Clear : public mslib::DX9_Object, public PlayObject
{
public:
    enum class MyEnumClass
    {
        GameClear,
        GameOver,
    };
public:
    Game_Over_Clear();
    ~Game_Over_Clear();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
    // 描画情報のセット
    void SetObjectMenu(MyEnumClass list);
private:
    bool m_key; // ロック
    mslib::CColor<int> m_color; // 色
};

