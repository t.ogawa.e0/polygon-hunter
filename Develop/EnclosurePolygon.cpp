//==========================================================================
// 囲いポリゴン[EnclosurePolygon.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "EnclosurePolygon.h"
#include "ExplosiveEffect.h"
#include "resource_list.h"

//=========================================================================
// 定数定義
//=========================================================================
constexpr float __interval = 2.0f;
constexpr float __IntervalRange = 7.0f;
constexpr float __max_scale = 5.0f;

EnclosurePolygon::EnclosurePolygon() : DX9_Object(mslib::DX9_ObjectID::Cube)
{
    this->SetObjectName("EnclosurePolygon");
}

EnclosurePolygon::~EnclosurePolygon()
{
}

//==========================================================================
// 初期化
bool EnclosurePolygon::Init(void)
{
    // スケール乱数
    auto & prand = *this->_rand_float()->Create();
    prand = mslib::rand_float(1.0f, __max_scale);

    // 角度乱数
    prand = *this->_rand_float()->Create();
    prand = mslib::rand_float(mslib::angle_list::angle_0, mslib::angle_list::angle_180 + mslib::angle_list::angle_180);

    this->m_ExplosiveEffect = this->GetObjects(mslib::DX9_ObjectID::Sphere, "ExplosiveEffect");

    this->_3DSphere()->Init(RESOURCE_fire_nucleus_png, 30);
    return this->_3DCube()->Init(RESOURCE_fire_box_png);
}

//==========================================================================
// 解放
void EnclosurePolygon::Uninit(void)
{
}

//==========================================================================
// 更新
void EnclosurePolygon::Update(void)
{
    // 準備
    this->Preparation();

    // 攻撃処理
    this->AttackProcessing();

    // 破棄処理
    this->DeleteProcessing();

    // vectorセット
    this->SetVector();

    this->_3DSphere()->Update();
    this->_3DCube()->Update();
}

//==========================================================================
// 描画
void EnclosurePolygon::Draw(void)
{
    this->_3DCube()->Draw();

    // パンチ抜き
    this->GetDirectX9Device()->SetRenderState(D3DRS_WRAP0, D3DWRAPCOORD_0);
    this->GetDirectX9Device()->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW); // 反時計回りの面を消去
    this->_3DSphere()->Draw();
    this->GetDirectX9Device()->SetRenderState(D3DRS_WRAP0, 0);
}

//==========================================================================
// デバッグ
void EnclosurePolygon::Debug(void)
{
}

//==========================================================================
// 生成
void EnclosurePolygon::Create(mslib::DX9_3DObject & target, mslib::DX9_3DObject & singular_point)
{
    this->m_param.emplace_back(target, singular_point);
    auto itr = --this->m_param.end();

    itr->m_create_timer.Init(1, 1);
    itr->m_processing_timer.Init(2, 60);
}

//==========================================================================
// 攻撃処理
void EnclosurePolygon::AttackProcessing(void)
{
    for (auto itr = this->m_param.begin(); itr != this->m_param.end(); ++itr)
    {
        // 最終データのチェック
        for (auto itr2 = itr->m_obj.begin(); itr2 != itr->m_obj.end(); ++itr2)
        {
            itr2->m_obj_plus.RotX(0.05f);
            itr2->Get3DObject()->RotX(-0.05f);
        }

        // 準備完了
        if (itr->m_ready == true && itr->m_delete == false)
        {
            if (itr->m_processing_timer.Countdown())
            {
                itr->m_delete = true;
            }
        }
    }
}

//==========================================================================
// 消去処理
void EnclosurePolygon::DeleteProcessing(void)
{
    for (auto itr = this->m_param.begin(); itr != this->m_param.end();)
    {
        // 準備消去フラグが立った
        if (itr->m_delete == true)
        {
            auto * pExplosiveEffect = (ExplosiveEffect*)this->m_ExplosiveEffect;

            for (auto itr2 = itr->m_obj.begin(); itr2 != itr->m_obj.end(); ++itr2)
            {
                auto * pObject = itr2->Get3DObject();
                if (mslib::nullptr_check(pExplosiveEffect))
                {
                    pExplosiveEffect->Create(*pObject);
                }
                this->_3DSphere()->ObjectDelete(itr2->m_obj_plus);
                this->_3DCube()->ObjectDelete(pObject);
            }
            itr->m_obj.clear();
            itr = this->m_param.erase(itr);
        }
        else
        {
            for (auto itr2 = itr->m_obj.begin(); itr2 != itr->m_obj.end(); )
            {
                if (itr2->GetDeleteKey())
                {
                    auto * pExplosiveEffect = (ExplosiveEffect*)this->m_ExplosiveEffect;
                    auto * pObject = itr2->Get3DObject();

                    if (mslib::nullptr_check(pExplosiveEffect))
                    {
                        pExplosiveEffect->Create(*pObject);
                    }
                    this->_3DSphere()->ObjectDelete(itr2->m_obj_plus);
                    this->_3DCube()->ObjectDelete(pObject);
                    itr2 = itr->m_obj.erase(itr2);
                }
                else
                {
                    ++itr2;
                }
            }
            ++itr;
        }
    }
}

//==========================================================================
// 準備
void EnclosurePolygon::Preparation(void)
{
    // データ
    for (auto itr = this->m_param.begin(); itr != this->m_param.end(); ++itr)
    {
        // 準備ができてない
        if (itr->m_ready == false)
        {
            // 複製
            auto ptarget = *itr->m_target;
            auto & pRandScale = *this->_rand_float()->Get((int)RandList::Scale);
            auto & pRandAngle = *this->_rand_float()->Get((int)RandList::Angle);

            // ターゲットを基準としてポリゴンを生成、同時に向きを変化させる
            for (auto fcount = 0.0f; fcount < mslib::angle_list::angle_180 + mslib::angle_list::angle_180; fcount += mslib::angle_list::angle_45)
            {
                ptarget.RotX(mslib::angle_list::angle_45);
                itr->m_obj.emplace_back();
                auto itr2 = --itr->m_obj.end();
                *itr2->Get3DObject() = ptarget;
            }

            // 生成オブジェクトを前進させる
            for (auto itr2 = itr->m_obj.begin(); itr2 != itr->m_obj.end(); ++itr2)
            {
                auto * pObject = itr2->Get3DObject();
                pObject->MoveZ(__IntervalRange);
                pObject->ScaleSet(pRandScale(this->GetMt19937()));
                pObject->RotX(pRandAngle(this->GetMt19937()));
                pObject->RotY(pRandAngle(this->GetMt19937()));
                pObject->RotZ(pRandAngle(this->GetMt19937()));
                itr2->m_obj_plus = *pObject;
                itr2->m_obj_plus.SetMatInfoSca({ *itr2->m_obj_plus.GetMatInfoSca() / 2.0f });

                this->_3DSphere()->ObjectInput(itr2->m_obj_plus);
                this->_3DCube()->ObjectInput(pObject);
            }
            itr->m_ready = true;
        }
    }
}

//==========================================================================
// vectorセット
void EnclosurePolygon::SetVector(void)
{
    for (auto itr = this->m_param.begin(); itr != this->m_param.end(); ++itr)
    {
        for (auto itr2 = itr->m_obj.begin(); itr2 != itr->m_obj.end(); ++itr2)
        {
            this->Debug_3DObject(itr2->Get3DObject());
            this->Debug_3DObject(&itr2->m_obj_plus);
        }
    }
}
