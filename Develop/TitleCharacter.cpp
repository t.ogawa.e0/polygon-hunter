//==========================================================================
// タイトルのキャラクター [TitleCharacter.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "TitleCharacter.h"
#include "resource_list.h"

TitleCharacter::TitleCharacter() : DX9_Object(mslib::DX9_ObjectID::Cube)
{
    this->SetObjectName("TitleCharacter");
}

TitleCharacter::~TitleCharacter()
{
}

//=========================================================================
// 初期化
bool TitleCharacter::Init(void)
{
    //=========================================================================
    // プレイヤー
    //=========================================================================
    auto * pObj = this->_3DObject()->Create();
    pObj->Init(0);
    pObj->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);
    pObj->RotX(mslib::angle_list::angle_180);
    pObj->MoveZ(2);
    pObj->MoveX(-2);
    pObj->RotX(mslib::angle_list::angle_45 / 2);
    this->_3DCube()->ObjectInput(pObj);

    //=========================================================================
    // 雑魚エネミー
    //=========================================================================
    pObj = this->_3DObject()->Create();
    pObj->Init(1);
    pObj->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);
    pObj->RotX(mslib::angle_list::angle_180);
    pObj->MoveZ(2);
    pObj->MoveX(2);
    pObj->RotX(-(mslib::angle_list::angle_45 / 2));
    this->_3DCube()->ObjectInput(pObj);

    //=========================================================================
    // ボスエネミー
    //=========================================================================
    pObj = this->_3DObject()->Create();
    pObj->Init(2);
    pObj->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);
    pObj->RotX(mslib::angle_list::angle_180);
    pObj->MoveX(2);
    pObj->MoveY(2);
    pObj->MoveZ(-6);
    pObj->ScalePlus(5);
    pObj->RotX(-(mslib::angle_list::angle_45 / 2));
    this->_3DCube()->ObjectInput(pObj);

    //=========================================================================
    // リソースの登録
    //=========================================================================
    this->_3DCube()->Init(RESOURCE_player_box_png);
    this->_3DCube()->Init(RESOURCE_enemy_1_box_png);
    this->_3DCube()->Init(RESOURCE_enemy_2_box_png);

    return false;
}

//=========================================================================
// 解放
void TitleCharacter::Uninit(void)
{
}

//=========================================================================
// 更新
void TitleCharacter::Update(void)
{
    this->_3DCube()->Update();
}

//=========================================================================
// 描画
void TitleCharacter::Draw(void)
{
    this->_3DCube()->Draw();
}

//=========================================================================
// デバッグ
void TitleCharacter::Debug(void)
{
}
