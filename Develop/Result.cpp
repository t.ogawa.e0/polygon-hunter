//==========================================================================
// リザルトシーン[Result.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Screen.h" // デバッグ用

#include "Result.h"
#include "ResultFomt.h"

#include "InputSystem.h"
#include "KeyboardObject.h"
#include "UserInformation.h"

#include "Title_Camera.h"
#include "TitleBackground.h"
#include "TitlePressKey.h"

CResult::CResult()
{
    TitlePressKey * pTitlePressKey = nullptr;
    Title_Camera * pTitle_Camera = nullptr;
    TitleBackground * pTitleBackground = nullptr;

    ResultFomt * pResultFomt = nullptr;
    KeyboardObject * pKeyboardObject = nullptr;
    CInputSystem * InputSystem = nullptr;
    UserInformation * pUserInformation = nullptr;


    //==========================================================================
    // ファイル指定
    //==========================================================================
    mslib::DX9_Object::SetFilePass("resource/data/level2.bin");

    //==========================================================================
    // システム
    //==========================================================================
    mslib::DX9_Object::NewObject(pUserInformation);
    mslib::DX9_Object::NewObject(InputSystem);
    mslib::DX9_Object::NewObject(pKeyboardObject);

    //==========================================================================
    // 2D
    //==========================================================================
    mslib::DX9_Object::NewObject(pResultFomt);
    mslib::DX9_Object::NewObject(pTitlePressKey);

    //==========================================================================
    // 3D
    //==========================================================================
    mslib::DX9_Object::NewObject(pTitle_Camera);
    mslib::DX9_Object::NewObject(pTitleBackground);
}

CResult::~CResult()
{
}

//==========================================================================
// 初期化
bool CResult::Init(void)
{
    return mslib::DX9_Object::InitAll();
}

//==========================================================================
// 解放
void CResult::Uninit(void)
{
    mslib::DX9_Object::ReleaseAll();
}

//==========================================================================
// 更新
void CResult::Update(void)
{
#if defined(_SETImGui_Dx9_) || defined(_DEBUG) || defined(DEBUG)
    auto *pKeyboard = mslib::DX9_DeviceManager::GetKeyboard();

    if (pKeyboard->Trigger(mslib::DirectInputKeyboardButton::KEY_F1))
    {
        CScreen::screenchange(SceneName::Title);
    }
#endif

    mslib::DX9_Object::UpdateAll();
}

//==========================================================================
// 描画
void CResult::Draw(void)
{
    mslib::DX9_Object::DrawAll();
}
