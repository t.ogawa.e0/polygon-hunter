//==========================================================================
// タイトル名[Title_TitleName.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Title_TitleName.h"
#include "resource_list.h"

Title_TitleName::Title_TitleName() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->SetObjectName("Title_TitleName");
}

Title_TitleName::~Title_TitleName()
{
}

//=========================================================================
// 初期化
bool Title_TitleName::Init(void)
{
    auto*pObj = this->_2DObject()->Create();

    pObj->Init(0);
    this->_2DPolygon()->ObjectInput(pObj);

    this->ObjectLoad();

    return this->_2DPolygon()->Init(RESOURCE_PolygonHunter_png, true);
}

//=========================================================================
// 解放
void Title_TitleName::Uninit(void)
{
}

//=========================================================================
// 更新
void Title_TitleName::Update(void)
{
    this->_2DPolygon()->Update();
}

//=========================================================================
// 描画
void Title_TitleName::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//=========================================================================
// デバッグ
void Title_TitleName::Debug(void)
{
}
