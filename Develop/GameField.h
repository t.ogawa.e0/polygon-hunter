//==========================================================================
// ゲームのフィールド[GameField.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGameField
// Content: ゲームのフィールド
//
//=========================================================================
class CGameField : public mslib::DX9_Object
{
public:
    CGameField() : DX9_Object(mslib::DX9_ObjectID::Field) {
        this->SetObjectName("GameField");
    }
    ~CGameField() {}
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:

};