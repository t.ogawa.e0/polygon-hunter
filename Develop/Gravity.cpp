//==========================================================================
// 重力オブジェクト[Gravity.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Gravity.h"

bool CGravity_::ObjectAddress(const mslib::DX9_3DObject * _obj)
{
    if (this->m_pos == _obj)
    {
        return true;
    }
    return false;
}

//=========================================================================
// 更新
void CGravity_::Update(void)
{
    // 座標を取得
    auto vpos = *this->m_pos->GetMatInfoPos();
    if (this->m_ground->y <= vpos.y)
    {
        vpos.y += this->m_power;
        if (vpos.y <= this->m_ground->y)
        {
            vpos.y = this->m_ground->y;
            this->m_power = 0.0f;
        }
    }
    else
    {
        vpos.y = this->m_ground->y;
        this->m_power = 0.0f;
    }
    this->m_pos->SetMatInfoPos(vpos);

    if (-1.0f < this->m_power)
    {
        this->m_power -= 0.015f;
    }
}

//=========================================================================
// 初期化
bool CGravity::Init(void)
{
    return false;
}

//=========================================================================
// 解放
void CGravity::Uninit(void)
{
    this->m_gravity.clear();
}

//=========================================================================
// 更新
void CGravity::Update(void)
{
    for (auto itr = this->m_gravity.begin(); itr != this->m_gravity.end(); ++itr)
    {
        itr->Update();
    }
}

//=========================================================================
// 描画
void CGravity::Draw(void)
{
}

//=========================================================================
// デバッグ
void CGravity::Debug(void)
{
}

void CGravity::SetGravity(mslib::DX9_3DObject * _obj, D3DXVECTOR3 * _ground)
{
    this->m_gravity.emplace_back(_obj, _ground);
}

void CGravity::DeleteGravity(mslib::DX9_3DObject * _obj)
{
    for (auto itr = this->m_gravity.begin(); itr != this->m_gravity.end(); )
    {
        if (itr->ObjectAddress(_obj))
        {
            itr = this->m_gravity.erase(itr);
        }
        else
        {
            ++itr;
        }
    }
}
