//==========================================================================
// スカイドーム[Game_Skydome.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGame_Skydome
// Content: スカイドーム
//
//=========================================================================
class CGame_Skydome : public mslib::DX9_Object
{
public:
    CGame_Skydome() :DX9_Object(mslib::DX9_ObjectID::Sphere) {
        this->SetObjectName("Game_Skydome");
        this->m_Player = nullptr;
    }
    ~CGame_Skydome() {}
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:
    void * m_Player; // カメラへのアクセスルート
};
