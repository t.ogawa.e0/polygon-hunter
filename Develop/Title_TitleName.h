//==========================================================================
// タイトル名[Title_TitleName.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : Title_TitleName
// Content: タイトル名
//
//=========================================================================
class Title_TitleName : public mslib::DX9_Object
{
public:
    Title_TitleName();
    ~Title_TitleName();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
};

