//==========================================================================
// WindowsAPI[WindowsAPI.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// #include
//==========================================================================
#include <windows.h>
#include <string>

//==========================================================================
//
// class  : CWindowsAPI
// Content: WindowsAPI
//
//=========================================================================
class CWindowsAPI
{
public:
    // Class styles
    enum class Styles
    {
        VREDRAW = CS_VREDRAW,
        HREDRAW = CS_HREDRAW,
        DBLCLKS = CS_DBLCLKS,
        OWNDC = CS_OWNDC,
        CLASSDC = CS_CLASSDC,
        PARENTDC = CS_PARENTDC,
        NOCLOSE = CS_NOCLOSE,
        SAVEBITS = CS_SAVEBITS,
        BYTEALIGNCLIENT = CS_BYTEALIGNCLIENT,
        BYTEALIGNWINDOW = CS_BYTEALIGNWINDOW,
        GLOBALCLAS = CS_GLOBALCLASS,
        VREDRAW_HREDRAW = CS_VREDRAW | CS_HREDRAW,
    };
private:
    // デフォルトコンストラクタ禁止
    CWindowsAPI() = delete;
    // コピー禁止 (C++11)
    CWindowsAPI(const CWindowsAPI &) = delete;
    CWindowsAPI &operator=(const CWindowsAPI &) = delete;
public:
    CWindowsAPI(const std::string class_name, const std::string window_name);
    ~CWindowsAPI();

    // ウィンドウクラスの情報セット
    // style ウィンドウのスタイル
    // hInstance インスタンスハンドル
    // __WndProc WndProc
    // hIcon アイコンのハンドル
    // hIconSm 16×16の小さいサイズのアイコンのハンドル
    // lpszMenuName デフォルトメニュー名
    ATOM MyClass(UINT style, WNDPROC __WndProc, LPCSTR hIcon, LPCSTR hIconSm, LPCSTR lpszMenuName, HINSTANCE hInstance);

    // ウィンドウの生成
    // dwStyle ウィンドウスタイル
    // rect ウィンドウサイズ
    // hWndParent 親ウィンドウまたはオーナーウィンドウのハンドル
    // hMenu メニューハンドルまたは子ウィンドウ ID
    // lpParam ウィンドウ作成データ
    bool Create(DWORD dwStyle, RECT rect, HWND hWndParent, HMENU hMenu, LPVOID lpParam, int nCmdShow);

    // メッセージのゲッター
    MSG & GetMSG(void);

    // ウィンドウRECT
    RECT GetWindowRECT(void);

    // ウィンドウハンドル 
    HWND GetHWND(void);

    // エラーメッセージ
    template <typename ... Args>
    void ErrorMessage(const char * text, Args const & ... args);
private:
    std::string m_class_name; // クラス名
    std::string m_window_name; // ウィンドウ名
    HWND m_hWnd; // ウィンドウハンドル
    MSG m_msg; // メッセージ
    WNDCLASSEX m_wcex; // ウィンドウクラス
    RECT m_rect; // ウィンドウRECT
};

//==========================================================================
// エラーメッセージ
template<typename ...Args>
inline void CWindowsAPI::ErrorMessage(const char * text, Args const & ...args)
{
    char pfon[1024] = { 0 };

    sprintf(pfon, text, args ...);

    MessageBox(this->m_hWnd, pfon, "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
}
