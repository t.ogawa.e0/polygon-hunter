//==========================================================================
// タイマー[Game_Timer.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Timer.h"
#include "resource_list.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int __max_time = 30; // プレイ時間(仮)
constexpr float __tex_scale = 0.5f;

CGame_Timer::CGame_Timer() :DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->SetObjectName("Game_Timer");
    this->m_time = 0;
}

CGame_Timer::~CGame_Timer()
{

}

//==========================================================================
// 初期化
bool CGame_Timer::Init(void)
{
    this->_2DObject()->Reserve(3);
    this->_Timer()->Reserve(2);

    //==========================================================================
    // timeバーobject
    //==========================================================================
    this->_2DPolygon()->Init(RESOURCE_time_ber_png, true);
    auto * p_obj = this->_2DObject()->Create();
    p_obj->Init(0);
    this->_2DPolygon()->ObjectInput(p_obj);

    //==========================================================================
    // タイマーの初期化
    //==========================================================================
    for (int i = (int)ETimer::bigin; i < (int)ETimer::end; i++)
    {
        this->_2DNumber()->Init(RESOURCE_Number_png, true);
        this->_2DNumber()->Set(i, 2, true, true, { 0,0 });
        this->_2DNumber()->SetAnim(i, 1, 10, 10);
        this->_2DNumber()->SetTexScale(i, __tex_scale);
    }

    //==========================================================================
    // タイマーの区切り
    //==========================================================================
    this->_2DPolygon()->Init(RESOURCE_timer_区切り_png, true);
    this->_2DPolygon()->SetTexScale(1, __tex_scale);
    p_obj = this->_2DObject()->Create();
    p_obj->Init(1);
    this->_2DPolygon()->ObjectInput(p_obj);

    //==========================================================================
    // 時計オブジェクト
    //==========================================================================
    this->_2DPolygon()->Init(RESOURCE_time_png, true);
    p_obj = this->_2DObject()->Create();
    p_obj->Init(2);
    this->_2DPolygon()->ObjectInput(p_obj);

    //==========================================================================
    // タイマーのセット
    //==========================================================================
    this->m_time = __max_time;
    this->_Timer()->Create()->Init(this->m_time, 0);
    this->_Timer()->Create()->Init(0, 0);

    this->m_minute.Init(0, 0);
    this->m_seconds.Init(0, 0);

    this->ObjectLoad();

    return false;
}

//==========================================================================
// 解放
void CGame_Timer::Uninit(void)
{
}

//==========================================================================
// 更新
void CGame_Timer::Update(void)
{
    if (this->GetPlayKey())
    {
        auto * p_timer2 = this->_Timer()->Get((int)ETimer::count2);
        p_timer2->Countdown();

        // 秒:折り返しカウンタ
        if (p_timer2->GetComma() == 0 && p_timer2->GetTime() == 0)
        {
            auto * p_timer1 = this->_Timer()->Get((int)ETimer::count1);

            // 分:カウンタ 
            if (p_timer1->GetTime() != 0)
            {
                // 秒カウンタのリセット
                p_timer2->Init(59, 60);

                // 分のカウンタを進める
                this->m_time--;

                // カウンタの更新
                p_timer1->Init(this->m_time, 0);
            }
        }

        this->m_seconds.Count();
        if (this->m_seconds.GetTime() == 60)
        {
            this->m_seconds.Reset();
            this->m_minute.Init(__max_time - this->m_time, 0);
        }
    }

    // 描画のための更新
    for (int i = 0; i < this->_2DNumber()->GetNumTex(); i++)
    {
        auto * p_tim = this->_Timer()->Get(i);
        this->_2DNumber()->Update(i, p_tim->GetTime());
    }
    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CGame_Timer::Draw(void)
{
    this->_2DPolygon()->Draw();
    this->_2DNumber()->Draw();
}

//=========================================================================
// デバッグ
void CGame_Timer::Debug(void)
{
    auto * p_timer1 = this->_Timer()->Get((int)ETimer::count1);
    auto * p_timer2 = this->_Timer()->Get((int)ETimer::count2);
    this->ImGui()->Text("Time[%2d : %2d/%2d]", p_timer1->GetTime(), p_timer2->GetTime(), p_timer2->GetComma());
}

//=========================================================================
// タイマー取得
int CGame_Timer::GetTime(void)
{
    auto * p_timer1 = this->_Timer()->Get((int)ETimer::count1);
    auto * p_timer2 = this->_Timer()->Get((int)ETimer::count2);

    return (60 * p_timer1->GetTime()) + p_timer2->GetTime();
}

//=========================================================================
// タイマー取得
int CGame_Timer::GetMaxTime(void)
{
    return __max_time * 60;
}

//=========================================================================
// クリア時間の取得
mslib::Timer * CGame_Timer::GetClearMinute(void)
{
    return &this->m_minute;
}

//=========================================================================
// クリア秒数の取得
mslib::Timer * CGame_Timer::GetClearSeconds(void)
{
    return &this->m_seconds;
}
