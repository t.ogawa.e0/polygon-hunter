//==========================================================================
// ゲームカメラ[GameCamera.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameCamera.h"
#include "InputSystem.h"
#include "KeyboardObject.h"

//=========================================================================
// 初期化
bool CGameCamera::Init(void)
{
    D3DXVECTOR3 Eye = D3DXVECTOR3(0.0f, 1.0f, -3.0f); // 注視点
    D3DXVECTOR3 At = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // カメラ座標

    // カメラ
    auto * p_camera = this->Camera()->Create();
    p_camera->Init(Eye, At);

    // カメラオブジェクト
    auto * p_obj = this->_3DObject()->Create();
    p_obj->Init(0);
    p_obj->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);

    // カメラオブジェクト
    p_obj = this->_3DObject()->Create();
    p_obj->Init(0);
    p_obj->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);

    // Xインプットへのアクセス
    this->m_input = this->GetObjects(mslib::DX9_ObjectID::Default, "InputSystem");
    this->m_KeyboardObject = this->GetObjects(mslib::DX9_ObjectID::Default, "KeyboardObject");

    return false;
}

//=========================================================================
// 解放
void CGameCamera::Uninit(void)
{
}

//=========================================================================
// 更新
void CGameCamera::Update(void)
{
    auto * p_xinput = (CInputSystem*)this->m_input;
    auto * p_dinput_mouse = this->Dinput_Mouse();
    auto * p_dinput = (KeyboardObject*)this->m_KeyboardObject;
    auto * pObj = this->_3DObject()->Get((int)CameraObjectID::EyeObject);
    auto * pCamera = this->Camera()->Get(0);

    // xinputでの操作
    if (mslib::nullptr_check(p_xinput))
    {
        if (p_xinput->GetXInput()->Check(0) == true)
        {
            D3DXVECTOR3 vecRight;
            if (p_xinput->GetXInput()->AnalogR(0, vecRight))
            {
                pCamera->RotViewX(vecRight.x*0.05f);
                pCamera->RotViewY(-(vecRight.z*0.02f));
                pObj->RotX(vecRight.x*0.05f);
                pObj->RotYLock(-(vecRight.z*0.02f));
            }
        }
    }

    // direct_inputでの操作
    if (mslib::nullptr_check(p_dinput_mouse))
    {
        auto vecRight = D3DXVECTOR3((float)p_dinput_mouse->Speed().m_lX, (float)p_dinput_mouse->Speed().m_lY, (float)p_dinput_mouse->Speed().m_lZ);

        pCamera->RotViewX(vecRight.x*0.005f);
        pCamera->RotViewY(vecRight.y*0.005f);
        pObj->RotX(vecRight.x*0.005f);
        pObj->RotYLock(vecRight.y*0.005f);
    }

    // direct_inputでの操作
    if (mslib::nullptr_check(p_dinput))
    {
        auto vecRight = D3DXVECTOR3(10, 10, 10);

        if (p_dinput->KEY_Q(KeyboardObject::KeyboardType::Press))
        {
            pCamera->RotViewX(-(vecRight.x*0.005f));
            pObj->RotX(-(vecRight.x*0.005f));
        }
        if (p_dinput->KEY_E(KeyboardObject::KeyboardType::Press))
        {
            pCamera->RotViewX((vecRight.x*0.005f));
            pObj->RotX((vecRight.x*0.005f));
        }
    }

    pObj->SetMatInfoPos(pCamera->GetVECTOR(mslib::DX9_CameraVectorList::VAT) + pCamera->GetVECTOR(mslib::DX9_CameraVectorList::VAT2));

    pCamera->Update(this->GetWinSize(), this->GetDirectX9Device());
}

//=========================================================================
// 描画
void CGameCamera::Draw(void)
{
}

//=========================================================================
// デバッグ
void CGameCamera::Debug(void)
{
}

//=========================================================================
// ターゲットをセット
void CGameCamera::SetTarget(int ID, mslib::DX9_3DObject * Input, D3DXVECTOR3 move_pos)
{
    auto * p_obj = this->_3DObject()->Get(0);
    auto * p_camera = this->Camera()->Get(ID);

    if (mslib::nullptr_check(p_camera) && mslib::nullptr_check(p_obj))
    {
        // オブジェクトの複製
        auto obj = *Input;

        obj.MoveX(move_pos.x);
        obj.MoveY(move_pos.y);
        obj.MoveZ(move_pos.z);

        // 距離計算用
        mslib::CVector3<float> vpos1;
        mslib::CVector3<float> vpos2;

        vpos1.D3DXVECTOR3_CAST(*p_obj->GetMatInfoPos());
        vpos2.D3DXVECTOR3_CAST(*obj.GetMatInfoPos());

        vpos1.y = vpos2.y;

        // 距離を速度に
        float f_speed = this->Collision()->Distance(vpos1, vpos2)*0.04f;

        // 対象に追従
        for (int i = 0; i < 10; i++)
        {
            p_obj->LockOn(obj, 1.0f);
        }
        p_obj->MoveZ(f_speed);

        p_obj->SetMatInfoPos({ p_obj->GetMatInfoPos()->x,obj.GetMatInfoPos()->y ,p_obj->GetMatInfoPos()->z });
        p_camera->SetAt(*p_obj->GetMatInfoPos());
        p_camera->SetEye(*p_obj->GetMatInfoPos());
    }
}
