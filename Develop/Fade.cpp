//==========================================================================
// フェード[Fade.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Fade.h"

//==========================================================================
// 初期化
bool CFade::Init(void)
{
    auto vwinsize = mslib::CTexvec<int>(0, 0, this->GetWinSize().x, this->GetWinSize().y);

    // メモリ確保
    // テクスチャの格納
    this->_2DPolygon()->Init();

    // 座標の初期化
    this->_2DObject()->Create()->Init(0);
    this->m_Param.m_Change = false;
    this->m_Param.m_Key = false;
    this->m_Param.m_Draw = true;
    this->m_Param.m_In = true;
    this->m_Param.m_a = 255;

    // テクスチャのサイズ変更
    *this->_2DPolygon()->GetTexSize(this->_2DObject()->Get(0)->GetIndex()) = vwinsize;

    //	α値をセット
    this->_2DObject()->Get(0)->SetColor(0, 0, 0, this->m_Param.m_a);

    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(0));

    return false;
}

//==========================================================================
// 解放
void CFade::Uninit(void)
{
    this->_2DPolygon()->Release();
    this->_2DObject()->Release();
}

//==========================================================================
// 更新
void CFade::Update(void)
{
    // 処理実行判定
    if (this->m_Param.m_Key)
    {
        // フェードin,aut切り替え
        if (this->m_Param.m_Change)
        {
            this->m_Param.m_a += 5;
            if (255 < this->m_Param.m_a)
            {
                this->m_Param.m_Key = false;
                this->m_Param.m_In = true;
                this->m_Param.m_a = 255;
            }
        }
        else
        {
            this->m_Param.m_a -= 5;
            if (this->m_Param.m_a < 0)
            {
                this->m_Param.m_Key = false;
                this->m_Param.m_Draw = false;
                this->m_Param.m_a = 0;
            }
        }
        this->_2DObject()->Get(0)->SetColor(0, 0, 0, this->m_Param.m_a);
        this->_2DPolygon()->Update();
    }
}

//==========================================================================
// 描画
void CFade::Draw(void)
{
    // 描画判定が出ている時のみ
    if (this->m_Param.m_Draw)
    {
        auto pDevice = this->GetDirectX9Device();

        // 画質を高画質にする
        pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
        pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
        pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE); // 元のサイズより小さい時綺麗にする

                                                                      // Zバッファの無効
        pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);

        this->_2DPolygon()->Draw();

        // Zバッファの有効化
        pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);

        // 通常の画質に戻す
        pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
        pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
        pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // 元のサイズより小さい時綺麗にする}
    }
}

//==========================================================================
// デバッグ
void CFade::Debug(void)
{
}

//==========================================================================
// フェードイン
void CFade::In(void)
{
    this->m_Param.m_Key = true;
    this->m_Param.m_Change = true;
    this->m_Param.m_Draw = true;
    this->m_Param.m_In = false;
    this->m_Param.m_a = 0;
}

//==========================================================================
// フェードアウト
void CFade::Out(void)
{
    // out用のパラメータの初期化
    this->m_Param.m_Key = true;
    this->m_Param.m_Change = false;
    this->m_Param.m_In = false;
    this->m_Param.m_a = 255;
}

//==========================================================================
// フェードイン終了判定
bool CFade::FeadInEnd(void)
{
    return this->m_Param.m_In; // 判定
}
