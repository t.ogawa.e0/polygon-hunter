//==========================================================================
// グリッド[Game_Grid.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGame_Grid
// Content: グリッド
//
//=========================================================================
class CGame_Grid : public mslib::DX9_Object
{
public:
    CGame_Grid() : DX9_Object(mslib::DX9_ObjectID::Grid) {
        this->SetObjectName("Game_Grid");
    }
    ~CGame_Grid() {}
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:

};