//==========================================================================
// プレイオブジェクト(継承)[PlayObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "PlayObject.h"

PlayObject::PlayObject()
{
    this->m_play = false;
}

PlayObject::~PlayObject()
{
}

//==========================================================================
// 更新開始
void PlayObject::UpdatePlay(void)
{
    this->m_play = true;
}

//==========================================================================
// 更新停止
void PlayObject::UpdateStop(void)
{
    this->m_play = false;
}

//==========================================================================
// 実行フラグ取得
bool PlayObject::GetPlayKey(void)
{
    return this->m_play;
}
