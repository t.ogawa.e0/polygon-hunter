#include "GameMainScreenSample.h"
#include "resource_list.h"


CGameMainScreenSample::CGameMainScreenSample() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->m_check = false;
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
    this->SetObjectName("GameMainScreenSample");
#endif
}


CGameMainScreenSample::~CGameMainScreenSample()
{
}

//==========================================================================
// 初期化
bool CGameMainScreenSample::Init(void)
{
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
    auto * p_obj = this->_2DObject()->Create();

    p_obj->Init(0);

    this->_2DPolygon()->ObjectInput(p_obj);

    this->_2DPolygon()->Init(RESOURCE_GameMainScreen_png, true);

#endif
    return false;
}

//==========================================================================
// 解放
void CGameMainScreenSample::Uninit(void)
{
}

//==========================================================================
// 更新
void CGameMainScreenSample::Update(void)
{
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
    this->_2DPolygon()->Update();
#endif
}

//==========================================================================
// 描画
void CGameMainScreenSample::Draw(void)
{
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
    if (this->m_check)
    {
        this->_2DPolygon()->Draw();
    }
#endif
}

//==========================================================================
// デバッグ
void CGameMainScreenSample::Debug(void)
{
#if defined(_MSLIB_DebugKey_) || defined(_DEBUG) || defined(DEBUG)
    this->ImGui()->Checkbox("UI設置図の表示", this->m_check);
#endif
}
