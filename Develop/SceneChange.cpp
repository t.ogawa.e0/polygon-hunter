//==========================================================================
// シーン遷移[SceneChange.cpp]
// author: tatuya ogawa
//==========================================================================
#include "SceneChange.h"
#include "make.h"
#include "MapEditor.h"
#include "Game.h"
#include "Title.h"
#include "Result.h"
#include "Tutorial.h"

//==========================================================================
// シーンの切り替え
void CSceneManager::ChangeScene(SceneName Name)
{
	// 解放
	this->Uninit();

	switch (Name)
	{
	case SceneName::begin:
        mslib::DX9_DeviceManager::GetDXDevice()->ErrorMessage("Scene Ellor : -1");
		break;

    case SceneName::Title:
        this->m_pScene = new CTitle;
        break;

    case SceneName::Tutorial:
        this->m_pScene = new Tutorial;
        break;

    case SceneName::Game:
        this->m_pScene = new CGame;
        break;

    case SceneName::Result:
        this->m_pScene = new CResult;
        break;

    case SceneName::MapEditor:
        this->m_pScene = new CMapEditor;
        break;

    case SceneName::Default:
        this->m_pScene = new CMake;
        break;

	default:
        mslib::DX9_DeviceManager::GetDXDevice()->ErrorMessage("Scene Ellor : -1");
		break;
	}
}

//==========================================================================
// 初期化 初期化終了時にtrue
bool CSceneManager::Init(void)
{
    if (this->m_pScene != nullptr)
    {
        return this->m_pScene->Init();
    }
    return true;
}

//==========================================================================
// 解放
void CSceneManager::Uninit(void)
{
	if (this->m_pScene != nullptr)
	{
        this->m_pScene->Uninit();
        delete this->m_pScene;
        this->m_pScene = nullptr;
	}
}

//==========================================================================
// 更新
void CSceneManager::Update(void)
{
    if (this->m_pScene != nullptr)
    {
        this->m_pScene->Update();
    }
}

//==========================================================================
// 描画
void CSceneManager::Draw(void)
{
    if (this->m_pScene != nullptr)
    {
        this->m_pScene->Draw();
    }
}