//==========================================================================
// 短剣オブジェクト[SwordObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "SwordObject.h"
#include "PlayerObject.h"
#include "GameICODrawObject.h"
#include "resource_list.h"

//=========================================================================
// 定数定義
//=========================================================================
constexpr float __attack_motion_speed = 0.03f;

SwordObject::SwordObject() : WeaponObject(mslib::DX9_ObjectID::Xmodel)
{
    this->SetObjectName("SwordObject");
    this->SetWeaponID(EList::Sword);
    this->m_PlayerObj = nullptr;
    this->m_GameICODrawObject = nullptr;
    this->m_blade_lock = false;
}

SwordObject::~SwordObject()
{
    this->m_blade.clear();
}

//=========================================================================
// 初期化
bool SwordObject::Init(void)
{
    auto *pObj = this->_3DObject()->Create();

    pObj->Init(0);

    this->_3DXmodel()->ObjectInput(pObj);

    this->m_PlayerObj = this->GetObjects(mslib::DX9_ObjectID::Cube, "PlayerObject");
    this->m_GameICODrawObject = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "GameICODrawObject");

    for (int i = 0; i < (int)MyEnumClass::blade4 + 1; i++)
    {
        this->m_blade.emplace_back();
    }

    auto *pAnimation = this->_3DObjectMotion()->Create();

    // 待機モーション
    pAnimation->Create(MotionName::Wait, 0, { { 0.0f,0.05f,0.0f },{ 0.2f, 0.2f,  0.0f }, 0.02f });
    pAnimation->Create(MotionName::Wait, 0, { { 0.0f,-0.05f,0.0f },{ -0.2f, -0.2f, 0.0f }, 0.02f });

    // 移動モーション
    pAnimation->Create(MotionName::Moving, 0, { { 0.0f,0.0f,0.0f },{ 1.5f, 0.0f, 0.0f }, 0.05f });
    pAnimation->Create(MotionName::Moving, 0, { { 0.0f,0.0f,0.0f },{ -1.5f, -0.0f, 0.0f }, 0.05f });

    // 攻撃モーション
    pAnimation->Create(MotionName::Attack, 0, { { 0.0f,0.0f,0.0f },{ -2.0f, 0.5f, -1.3f }, 0.03f });
    pAnimation->Create(MotionName::Attack, 0, { { 0.0f,0.0f,0.0f },{ 0.0f, 0.0f, mslib::angle_list::angle_180 }, 1 });
    pAnimation->Create(MotionName::Attack, 0, { { 0.0f,0.0f,0.0f },{ 2.0f, 0.5f, 0.0f }, 0.03f });

    pAnimation->Create(MotionName::Damage, 0, { { 0.0f,0.0f,0.0f },{ 0.0f, -1.5f, 0.0f }, 0.1f });
    pAnimation->Create(MotionName::Damage, 0, { { 0.0f,0.0f,0.0f },{ 0.0f, 1.5f, 0.0f }, 0.1f });

    return this->_3DXmodel()->Init(RESOURCE_blade_刀身_x, mslib::DX9_XmodelMoad::System);
}

//=========================================================================
// 解放
void SwordObject::Uninit(void)
{
}

//=========================================================================
// 更新
void SwordObject::Update(void)
{
    if (this->UpdateKey())
    {
        auto *pPlayer = (CPlayerObject*)this->m_PlayerObj;
        auto *pObj1 = this->_3DObject()->Get((int)MyEnumClass::body);
        auto *pAnimation = this->_3DObjectMotion()->Get(0);
        auto *pGameICODrawObject = (GameICODrawObject*)this->m_GameICODrawObject;

        // 攻撃速度上昇
        if (mslib::nullptr_check(pGameICODrawObject))
        {
            auto *pObj = pGameICODrawObject->GetBuffData(GameICODrawSystemID::AttackSpeed);
            // 対象アニメーションの処理速度変更
            pAnimation->SetPowor(
                MotionName::Attack, 0,
                {
                    __attack_motion_speed + pObj->m_buff*0.1f,
                    1,
                    __attack_motion_speed + pObj->m_buff*0.1f
                });
        }

        if (mslib::nullptr_check(pPlayer))
        {
            auto vpos = D3DXVECTOR3(0, 0, 0);
            auto vrot = D3DXVECTOR3(0, 0, 0);

            *pObj1 = *pPlayer->_3DObject()->Get((int)CPlayerObject::ObjectType::Visualization);
            pObj1->SetIndex(0);

            if (this->GetMotion() == MotionName::Wait)
            {
                pObj1->MoveY(0.5f);
                pObj1->MoveX(0.8f);
                pObj1->RotY(mslib::angle_list::angle_45*0.1f);
                pObj1->RotX(mslib::angle_list::angle_45*0.1f);
                pObj1->RotZ(mslib::angle_list::angle_45*0.5f);

                if (mslib::nullptr_check(pAnimation))
                {
                    pAnimation->Play(this->GetMotion(), 0, pObj1);

                    // 現在の線形補間終了
                    if (pAnimation->GetLinearData()->Check() == true)
                    {
                        // リセット
                        pAnimation->Reset(this->GetMotion(), 0);
                        // 判定データの初期化
                        for (auto itr = this->m_blade.begin(); itr != this->m_blade.end(); ++itr)
                        {
                            itr->Reset();
                        }
                        this->m_blade_lock = false;
                    }
                }
            }
            else if (this->GetMotion() == MotionName::Moving)
            {
                pObj1->MoveY(0.5f);
                pObj1->MoveX(0.8f);
                pObj1->RotY(mslib::angle_list::angle_45*0.5f);
                pObj1->RotX(mslib::angle_list::angle_45*0.5f);
                pObj1->RotZ(mslib::angle_list::angle_45*0.9f);

                if (mslib::nullptr_check(pAnimation))
                {
                    pAnimation->Play(this->GetMotion(), 0, pObj1);

                    // 現在の線形補間終了
                    if (pAnimation->GetLinearData()->Check() == true)
                    {
                        // リセット
                        pAnimation->Reset(this->GetMotion(), 0);
                        // 判定データの初期化
                        for (auto itr = this->m_blade.begin(); itr != this->m_blade.end(); ++itr)
                        {
                            itr->Reset();
                        }
                        this->m_blade_lock = false;
                    }
                }
            }
            else if (this->GetMotion() == MotionName::Attack)
            {
                this->m_blade_lock = true;

                // 現在の線形補間終了
                if (mslib::nullptr_check(pAnimation))
                {
                    if (pAnimation->GetLinearData()->Check() == true)
                    {
                        // 処理可能な線形補間の処理数と処理済み線形補間データを比較
                        if ((pAnimation->GetLinearData()->GetID() + 1) < pAnimation->size(this->GetMotion()))
                        {
                            // 全ての線形データが処理済みなので終了判定
                            this->SetCheck(pAnimation->GetLinearData()->Check(), false);
                        }
                        else
                        {
                            this->SetCheck(pAnimation->GetLinearData()->Check(), true);
                            // 判定データの初期化
                            for (auto itr = this->m_blade.begin(); itr != this->m_blade.end(); ++itr)
                            {
                                itr->Reset();
                            }
                            this->m_blade_lock = false;
                        }
                    }
                }

                pObj1->MoveY(0.5f);
                pObj1->MoveX(0.5f);
                pObj1->MoveZ(1.0f);
                pObj1->RotY(-mslib::angle_list::angle_45*0.5f);
                pObj1->RotX(mslib::angle_list::angle_90);
                pObj1->RotZ(mslib::angle_list::angle_225*0.5f);

                if (mslib::nullptr_check(pAnimation))
                {
                    pAnimation->Play(this->GetMotion(), 0, pObj1);
                }
            }
            else if (this->GetMotion() == MotionName::Damage)
            {
                if (mslib::nullptr_check(pAnimation))
                {
                    // 現在の線形補間終了
                    if (pAnimation->GetLinearData()->Check() == true)
                    {
                        // 処理可能な線形補間の処理数と処理済み線形補間データを比較
                        if ((pAnimation->GetLinearData()->GetID() + 1) < pAnimation->size(this->GetMotion()))
                        {
                            // 全ての線形データが処理済みなので終了判定
                            this->SetCheck(pAnimation->GetLinearData()->Check(), false);
                        }
                        else
                        {
                            this->SetCheck(pAnimation->GetLinearData()->Check(), true);
                            // 判定データの初期化
                            for (auto itr = this->m_blade.begin(); itr != this->m_blade.end(); ++itr)
                            {
                                itr->Reset();
                            }
                            this->m_blade_lock = false;
                        }
                    }
                }

                pObj1->MoveY(0.5f);
                pObj1->MoveX(0.8f);
                pObj1->RotZ(-0.4f);
                pObj1->RotY(-mslib::angle_list::angle_90*0.6f);

                if (mslib::nullptr_check(pAnimation))
                {
                    pAnimation->Play(this->GetMotion(), 0, pObj1);
                }
            }
        }

        mslib::DX9_3DObject * pObjSub = nullptr;
        for (auto itr = this->m_blade.begin(); itr != this->m_blade.end(); ++itr)
        {
            if (mslib::nullptr_check(pObjSub))
            {
                *itr->Get3DObject() = *pObjSub;
            }
            else
            {
                *itr->Get3DObject() = *pObj1;
            }
            itr->Get3DObject()->MoveZ(0.35f);
            pObjSub = itr->Get3DObject();
        }
        this->SetVector();
        this->_3DXmodel()->Update();
    }
}

//=========================================================================
// 描画
void SwordObject::Draw(void)
{
    if (this->UpdateKey())
    {
        this->_3DXmodel()->Draw();
    }
}

//=========================================================================
// デバッグ
void SwordObject::Debug(void)
{
}

//=========================================================================
// リセットモーション
void SwordObject::Reset(void)
{
    auto *pAnimation = this->_3DObjectMotion()->Get(0);

    if (mslib::nullptr_check(pAnimation))
    {
        pAnimation->Reset();
        this->SetCheck(false, false);
    }
}

//=========================================================================
// 刀身の取得
std::list<DaggerBlade>* SwordObject::GetBladeObject(void)
{
    if (this->m_blade_lock == true)
    {
        return &this->m_blade;
    }
    return nullptr;
}

//=========================================================================
// vectorセット
void SwordObject::SetVector(void)
{
    for (auto itr = this->m_blade.begin(); itr != this->m_blade.end(); ++itr)
    {
        this->Debug_3DObject(itr->Get3DObject());
    }
}

DaggerBlade::DaggerBlade()
{
}

DaggerBlade::~DaggerBlade()
{
    this->m_hit_ptr.clear();
}

//=========================================================================
// オブジェクト取得
mslib::DX9_3DObject * DaggerBlade::Get3DObject()
{
    return &this->m_obj;
}

//=========================================================================
// リセット
void DaggerBlade::Reset(void)
{
    this->m_hit_ptr.clear();
}

//=========================================================================
// 当たり判定チェック
bool DaggerBlade::Hit(void * ptr)
{
    // 一度も当たり判定が出ていないとき
    if (std::find(this->m_hit_ptr.begin(), this->m_hit_ptr.end(), ptr) == this->m_hit_ptr.end())
    {
        this->m_hit_ptr.push_back(ptr);
        return true;
    }
    return false;
}
