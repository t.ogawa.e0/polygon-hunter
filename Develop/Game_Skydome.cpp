//==========================================================================
// スカイドーム[Game_Skydome.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Skydome.h"
#include "PlayerObject.h"
#include "resource_list.h"

//=========================================================================
// 初期化
bool CGame_Skydome::Init(void)
{
    auto *pObje = this->_3DObject()->Create();

    pObje->Init(0);
    pObje->ScalePlus(-80);

    this->_3DSphere()->ObjectInput(pObje);
    this->_Fog()->Init({ 255,255,255,255 },1.0f, 90.0f);
    this->_Fog()->ON();

    this->m_Player = this->GetObjects(mslib::DX9_ObjectID::Cube, "PlayerObject");

    return this->_3DSphere()->Init(RESOURCE_sky_jpg, 20);
}

//=========================================================================
// 解放
void CGame_Skydome::Uninit(void)
{
}

//=========================================================================
// 描画
void CGame_Skydome::Update(void)
{
    auto * p_player = (CPlayerObject*)this->m_Player;

    if (mslib::nullptr_check(p_player))
    {
        auto *pObj = this->_3DObject()->Get(0);

        const auto * pv_pos = p_player->_3DObject()->Get(0)->GetMatInfoPos();
        pObj->SetMatInfoPos({ pv_pos->x,pv_pos->y,pv_pos->z });
        pObj->RotX(0.0005f);
    }

    this->_3DSphere()->Update();
}

//=========================================================================
// 描画
void CGame_Skydome::Draw(void)
{
    this->_3DSphere()->Draw();
}

//=========================================================================
// デバッグ
void CGame_Skydome::Debug(void)
{
}
