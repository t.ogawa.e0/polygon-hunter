//==========================================================================
// ゲームウィンドウ[GameWindow.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <string>
#include "dxlib.h"
#include "Screen.h"
#include "WindowsAPI.h"

//==========================================================================
//
// class  : CGameWindow
// Content: ゲームウィンドウ
//
//==========================================================================
class CGameWindow
{
private:
    class CWinSize
    {
    public:
        CWinSize() {
            this->w = 0;
            this->h = 0;
        };
        CWinSize(int _w, int _h) {
            this->w = _w;
            this->h = _h;
        }
        ~CWinSize() {}
    public:
        int w;
        int h;
    };
    class CData {
    public:
        CData() {
            this->m_serect = 0;
            this->m_key = false;
        };
        CData(int serect, bool key) {
            this->m_serect = serect;
            this->m_key = key;
        }
        ~CData() {}
    public:
        int m_serect;
        bool m_key;
    };
public:
    CGameWindow() {
        this->m_WinMood = false;
        this->m_class_name = "Default";
        this->m_window_name = "Default";
        this->m_WindowsAPI = nullptr;
    }
    CGameWindow(const std::string &class_name, const std::string &window_name) {
        this->m_WinMood = false;
        this->m_class_name = class_name;
        this->m_window_name = window_name;
        this->m_WindowsAPI = nullptr;
    }
    ~CGameWindow() {
        this->m_class_name.clear();
        this->m_window_name.clear();
        delete this->m_WindowsAPI;
    }
    // ウィンドウ生成
    int Window(HINSTANCE hInstance, const mslib::CVector2<int> & data, bool Mode, int nCmdShow);
private:
    // ウィンドウプロシージャ
    static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
    // ゲームループ
    int GameLoop(HINSTANCE hInstance, HWND hWnd);
    bool Init(HINSTANCE hInstance, HWND hWnd); //初期化
    void Uninit(void);//終了処理
    bool Update(void);//更新
    void Draw(void);//描画
private:
    CScreen m_screne;
    mslib::AspectRatio::data_t m_WinSize; // ウィンドウサイズ
    bool m_WinMood; // ウィンドウモード
    std::string m_class_name; // クラス名
    std::string m_window_name; // ウィンドウ名
    CWindowsAPI *m_WindowsAPI; // ウィンドウ
};
