//==========================================================================
// ダメージオブジェクト[DamageObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : DamageObjectParam(継承)
// Content: ダメージを与える/受けるオブジェクト
//
//=========================================================================
class DamageObjectParam
{
public:
    DamageObjectParam()
    {
        if (this->m_obj == nullptr) {
            this->m_obj = new mslib::DX9_3DObject;
        }
        this->m_delete = false; this->m_delete2 = false;
    }
    DamageObjectParam(const mslib::DX9_3DObject & obj) {
        if (this->m_obj == nullptr) {
            this->m_obj = new mslib::DX9_3DObject;
        }
        *this->m_obj = obj; this->m_delete = false; this->m_delete2 = false;
    }
    virtual ~DamageObjectParam() {
        if (this->m_obj != nullptr) {
            delete this->m_obj;
            this->m_obj = nullptr;
        }

    }
    // 3Dオブジェクトの取得
    mslib::DX9_3DObject * Get3DObject(void) { return this->m_obj; }
    // 解放キーの取得
    bool GetDeleteKey(void) { return this->m_delete; }
    // 解放キーをON
    void DeleteKeyON(void) { this->m_delete = true; }
    // 解放キーの取得
    bool GetDeleteKey2(void) { return this->m_delete2; }
    // 解放キーをON
    void DeleteKeyON2(void) { this->m_delete2 = true; }
protected:
    mslib::DX9_3DObject *m_obj = nullptr; // オブジェクト
    bool m_delete; // 解放キー
    bool m_delete2; // 解放キー
};

//==========================================================================
//
// class  : DamageObject
// Content: ダメージオブジェクト
//
//=========================================================================
class DamageObject : public mslib::DX9_Object
{
public:
    DamageObject();
    ~DamageObject();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
private:
    // バレットとエネミーのコリジョン
    void Collision_Bullet_Enemy(void);
    // バレットとダメージポリゴンのコリジョン
    void Collision_Bullet_DamagePolygon(void);
    // キャラクターとのコリジョン
    void Collision_Player_Enemy(void);
    // エネミー同士のコリジョン
    void Collision_Enemy_Enemy(void);
    // キャラクターとダメージポリゴンのコリジョン
    void Collision_Player_DamagePolygon(void);
    // 爆風オブジェクトとプレイヤーのコリジョン
    void Collision_Player_ExplosiveEffect(void);
    // ソードと敵とのコリジョン
    void Collision_SwordObject_Enemy(void);
    // ダメージボックとプレイヤーとのコリジョン
    void Collision_Player_DamageBox(void);
    // 防御フィールドと弾のコリジョン
    void Collision_EnemyDefensiveField_Bullet(void);
    // 防御フィールドとプレイヤーのコリジョン
    void Collision_EnemyDefensiveField_Player(void);
    // 高台とプレイヤーとのコリジョン
    void Collision_HighGround_Player(void);
    // コリジョンのポインタ表示
    void Collision_ptr(void * ptr);
private:
    void * m_BulletObject; // バレットオブジェクトへのアクセス
    void * m_EnemyObject; // エネミーオブジェクトへのアクセス
    void * m_PlayerObject; // プレイヤーオブジェクトへのアクセス
    void * m_DamagePolygonObject; // ダメージオブジェクトへのアクセス
    void * m_StatusObj; // ステータスオブジェクトへのアクセス
    void * m_BurstGage; // ブーストゲージオブジェクトへのアクセス
    void * m_ExplosiveEffect; // 爆発オブジェクトへのアクセス
    void * m_SwordObject; // ソードオブジェクトへのアクセス
    void * m_DamageBox; // ダメージボックス
    void * m_EnemyDefensiveField; // エネミーの防御フィールドへのアクセス
    void * m_HighGround; // 高台
    void * m_SparkEffectObject; // 
    std::list<void*>m_collision_ptr; // コリジョンのポインタ
};
