//==========================================================================
// ダメージ箱[DamageBox.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "DamageObject.h"

class DamageBoxParam : public DamageObjectParam
{
public:
    DamageBoxParam();
    ~DamageBoxParam();
public:
    int m_time; // 生存時間
};

//==========================================================================
//
// class  : DamageBox
// Content: ダメージを与える/受けるオブジェクト
//
//=========================================================================
class DamageBox : public mslib::DX9_Object
{
public:
    DamageBox();
    ~DamageBox();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
    // 生成
    void Create(const mslib::DX9_3DObject & obj);
    // ダメージポリゴンの取得
    std::list<DamageBoxParam> * GetDamageBoxParam(void);
private:
    std::list<DamageBoxParam>m_list; // ダメージポリゴン
};

