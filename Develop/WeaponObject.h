//==========================================================================
// ウェポンオブジェクト[WeaponObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
// MotionName
//==========================================================================
namespace MotionName {
    const std::string Wait = "Wait"; // 待機モーション
    const std::string Moving = "Moving"; // 移動モーション
    const std::string Attack = "Attack"; // 攻撃モーション
    const std::string Damage = "Damage"; // ダメージモーション
}

class WBoolKey
{
public:
    WBoolKey() { this->key1 = this->key2 = false; }
    WBoolKey(bool k1, bool k2) { this->key1 = k1; this->key2 = k2; }
    ~WBoolKey() {}

public:
    bool key1;
    bool key2;
};

//==========================================================================
//
// class  : WeaponObject
// Content: オブジェクト管理クラスの拡張版
//
//==========================================================================
class WeaponObject : public mslib::DX9_Object
{
public:
    //enum class MotionID
    //{
    //    Wait, // 待機モーション
    //    Moving, // 移動モーション
    //    Attack, // 攻撃モーション
    //    Damage, // ダメージモーション
    //};
    enum class EList
    {
        Begen = 0,
        Dagger = Begen, // 短剣
        Sword, // 剣
        Gun, // 銃
        Shield, // 縦
        End,
    };
private:
    // コピー禁止 (C++11)
    WeaponObject(const WeaponObject &) = delete;
    WeaponObject &operator=(const WeaponObject &) = delete;
public:
    WeaponObject(mslib::DX9_ObjectID ObjectID = mslib::DX9_ObjectID::Default);
    virtual ~WeaponObject();

    // 継承初期化
    virtual bool Init(void) = 0;

    // 継承解放
    virtual void Uninit(void) = 0;

    // 継承更新
    virtual void Update(void) = 0;

    // 継承描画
    virtual void Draw(void) = 0;

    // 継承デバッグ
    virtual void Debug(void) = 0;
    // リセットモーション
    virtual void Reset(void) = 0;
    // そのウェポンが処理中かどうかのチェック
    const WBoolKey & Check(void);
    // ウェポンIDの登録
    // ロック解除
    void Unlock(void);
    // ロック
    void Lock(void);
    // 更新キー
    bool UpdateKey(void);
    // ウェポンIDの取得
    EList GetWeaponID(void);
    // モーション
    void SetMotion(const std::string & Input);
    // モーション
    const std::string & GetMotion(void);
    // そのウェポンが処理中かどうかのチェック登録(Wキー)
    void SetCheck(bool check1, bool check2);

protected:
    // ウェポンIDの登録
    void SetWeaponID(EList ID);
private:
    EList m_WeaponID; // ウェポンID
    bool m_update_lock; // 処理ロック
    WBoolKey m_processing; // 処理判定
    std::string m_MotionName; // モーションID
};
