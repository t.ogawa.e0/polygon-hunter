//==========================================================================
// ダメージオブジェクト[DamageObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DamageObject.h"
#include "BulletObject.h"
#include "EnemyObject.h"
#include "PlayerObject.h"
#include "DamagePolygonObject.h"
#include "StatusGage.h"
#include "BurstGage.h"
#include "ExplosiveEffect.h"
#include "SwordObject.h"
#include "DamageBox.h"
#include "HighGround.h"
#include "EnemyDefensiveField.h"
#include "SparkEffectObject.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int __Player_DamagePolygon = 30;
constexpr int __Player_DamagePolygon_LV = __Player_DamagePolygon * 8;
constexpr int __Level_up_blade = __Player_DamagePolygon * 2;

DamageObject::DamageObject()
{
    this->SetObjectName("DamageObject");
}

DamageObject::~DamageObject()
{
    this->m_collision_ptr.clear();
}

//=========================================================================
// 初期化
bool DamageObject::Init(void)
{
    this->m_BulletObject = this->GetObjects(mslib::DX9_ObjectID::Default, "BulletObject");
    this->m_EnemyObject = this->GetObjects(mslib::DX9_ObjectID::Default, "EnemyObject");
    this->m_PlayerObject = this->GetObjects(mslib::DX9_ObjectID::Cube, "PlayerObject");
    this->m_DamagePolygonObject = this->GetObjects(mslib::DX9_ObjectID::Default, "DamagePolygonObject");
    this->m_StatusObj = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "StatusGage");
    this->m_BurstGage = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "BurstGage");
    this->m_ExplosiveEffect = this->GetObjects(mslib::DX9_ObjectID::Sphere, "ExplosiveEffect");
    this->m_SwordObject = this->GetObjects(mslib::DX9_ObjectID::Xmodel, "SwordObject");
    this->m_DamageBox = this->GetObjects(mslib::DX9_ObjectID::Cube, "DamageBox");
    this->m_EnemyDefensiveField = this->GetObjects(mslib::DX9_ObjectID::Sphere, "EnemyDefensiveField");
    this->m_HighGround = this->GetObjects(mslib::DX9_ObjectID::Mesh, "HighGround");
    this->m_SparkEffectObject = this->GetObjects(mslib::DX9_ObjectID::Billboard, "SparkEffectObject");

    auto & __rand = *this->_rand_float()->Create();
    __rand = mslib::rand_float(mslib::angle_list::angle_0, mslib::angle_list::angle_180 + mslib::angle_list::angle_180);

    return false;
}

//=========================================================================
// 解放
void DamageObject::Uninit(void)
{
}

//=========================================================================
// 更新
void DamageObject::Update(void)
{
    //=========================================================================
    // 各オブジェクトとのコリジョン
    //=========================================================================
    this->Collision_HighGround_Player();
    this->Collision_EnemyDefensiveField_Player();
    this->Collision_EnemyDefensiveField_Bullet();

    this->Collision_Bullet_Enemy();
    this->Collision_Bullet_DamagePolygon();
    this->Collision_Player_Enemy();
    this->Collision_Enemy_Enemy();
    this->Collision_Player_DamagePolygon();
    this->Collision_Player_ExplosiveEffect();
    this->Collision_SwordObject_Enemy();
}

//=========================================================================
// 描画
void DamageObject::Draw(void)
{
}

//=========================================================================
// デバッグ
void DamageObject::Debug(void)
{
#if defined(_SETImGui_Dx9_) || defined(_DEBUG) || defined(DEBUG)
    if (this->ImGui()->NewMenu("当たり判定が出たポインタオブジェクト"))
    {
        for (auto itr = this->m_collision_ptr.begin(); itr != this->m_collision_ptr.end(); ++itr)
        {
            this->ImGui()->Text("%p", (*itr));
        }
        this->ImGui()->EndMenu();
    }
    this->m_collision_ptr.clear();
#endif
}

//=========================================================================
// バレットとエネミーのコリジョン
void DamageObject::Collision_Bullet_Enemy(void)
{
    auto * pEnemy = (CEnemyObject*)this->m_EnemyObject;
    auto * pBullet = (BulletObject*)this->m_BulletObject;
    auto * pBurstGage = (BurstGage*)this->m_BurstGage;

    // エネミーと弾のオブジェクトが取得できている
    if (mslib::nullptr_check(pEnemy) && mslib::nullptr_check(pBullet))
    {
        // 全種類のエネミー探索
        for (auto EnemyItr1 = pEnemy->GetEnemyObjectPos()->begin(); EnemyItr1 != pEnemy->GetEnemyObjectPos()->end(); ++EnemyItr1)
        {
            // 個々のエネミーの探索
            for (auto EnemyItr2 = EnemyItr1->second->begin(); EnemyItr2 != EnemyItr1->second->end(); ++EnemyItr2)
            {
                // 全種類の弾の探索
                for (auto BulletItr1 = pBullet->GetBulletInputSystem()->begin(); BulletItr1 != pBullet->GetBulletInputSystem()->end(); ++BulletItr1)
                {
                    // 個々の弾の探索
                    for (auto BulletItr2 = (*BulletItr1)->begin(); BulletItr2 != (*BulletItr1)->end(); ++BulletItr2)
                    {
                        if (BulletItr2->GetDeleteKey() == false)
                        {
                            // 対象オブジェクトのサイズの半分を取得
                            float fscale1 = BulletItr2->Get3DObject()->GetMatInfoSca()->x;
                            float fscale2 = EnemyItr2->Get3DObject()->GetMatInfoSca()->x;

                            // 球体のコリジョン
                            if (this->Collision()->Ball(*BulletItr2->Get3DObject(), *EnemyItr2->Get3DObject(), fscale1 + fscale2))
                            {
                                this->Collision_ptr(BulletItr2->Get3DObject());
                                this->Collision_ptr(EnemyItr2->Get3DObject());
                                if (mslib::nullptr_check(pBurstGage))
                                {
                                    pBurstGage->Plus(1);
                                }

                                // 弾の消滅フラグを立てる
                                BulletItr2->DeleteKeyON();

                                // 判定時の処理
                                EnemyItr2->m_Life -= __Player_DamagePolygon;
                                if (EnemyItr2->m_Life <= 0)
                                {
                                    EnemyItr2->DeleteKeyON();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

//=========================================================================
// バレットとダメージポリゴンのコリジョン
void DamageObject::Collision_Bullet_DamagePolygon(void)
{
    auto * pDamagePolygonObject = (DamagePolygonObject*)this->m_DamagePolygonObject;
    auto * pBullet = (BulletObject*)this->m_BulletObject;

    // ダメージポリゴン探索機能
    if (mslib::nullptr_check(pDamagePolygonObject) && mslib::nullptr_check(pBullet))
    {
        // 全種類のダメージポリゴン探索
        for (auto DamageItr1 = pDamagePolygonObject->GetDamagePolygonData()->begin(); DamageItr1 != pDamagePolygonObject->GetDamagePolygonData()->end(); ++DamageItr1)
        {
            // 個々のダメージポリゴングループ探索
            for (auto DamageItr2 = (*DamageItr1)->begin(); DamageItr2 != (*DamageItr1)->end(); ++DamageItr2)
            {
                // 個々のダメージポリゴン探索
                for (auto DamageItr3 = DamageItr2->m_obj.begin(); DamageItr3 != DamageItr2->m_obj.end(); ++DamageItr3)
                {
                    if (DamageItr3->GetDeleteKey() == false)
                    {
                        // 全種類の弾の探索
                        for (auto BulletItr1 = pBullet->GetBulletInputSystem()->begin(); BulletItr1 != pBullet->GetBulletInputSystem()->end(); ++BulletItr1)
                        {
                            // 個々の弾の探索
                            for (auto BulletItr2 = (*BulletItr1)->begin(); BulletItr2 != (*BulletItr1)->end(); ++BulletItr2)
                            {
                                if (BulletItr2->GetDeleteKey() == false)
                                {
                                    // 対象オブジェクトのサイズの半分を取得
                                    float fscale1 = BulletItr2->Get3DObject()->GetMatInfoSca()->x * 2;
                                    float fscale2 = DamageItr3->Get3DObject()->GetMatInfoSca()->x * 2;

                                    // 球体のコリジョン
                                    if (this->Collision()->Ball(*BulletItr2->Get3DObject(), *DamageItr3->Get3DObject(), fscale1 + fscale2))
                                    {
                                        // 弾の消滅フラグを立てる
                                        BulletItr2->DeleteKeyON();
                                        DamageItr3->DeleteKeyON();
                                        this->Collision_ptr(BulletItr2->Get3DObject());
                                        this->Collision_ptr(DamageItr3->Get3DObject());
                                    }
                                }
                            }
                        }
                    }
                }

                // 1つでもデータがある
                if (DamageItr2->m_obj.size() != 0)
                {
                    auto rast_obj = --DamageItr2->m_obj.end();

                    // 最終オブジェクトが破棄判定が出ている場合、準備完了にする
                    if (rast_obj->GetDeleteKey() == true)
                    {
                        DamageItr2->m_ready = true;
                    }
                }
            }
        }
    }
}

//=========================================================================
// キャラクターとのコリジョン
void DamageObject::Collision_Player_Enemy(void)
{
    auto * pEnemy = (CEnemyObject*)this->m_EnemyObject;
    auto * pPlayer = (CPlayerObject*)this->m_PlayerObject;

    // ダメージポリゴン探索機能
    if (mslib::nullptr_check(pEnemy) && mslib::nullptr_check(pPlayer))
    {
        // 全種類のエネミー探索
        for (auto EnemyItr1 = pEnemy->GetEnemyObjectPos()->begin(); EnemyItr1 != pEnemy->GetEnemyObjectPos()->end(); ++EnemyItr1)
        {
            // 個々のエネミーの探索
            for (auto EnemyItr2 = EnemyItr1->second->begin(); EnemyItr2 != EnemyItr1->second->end(); ++EnemyItr2)
            {
                // 各オブジェクトのパラメーターの取得
                auto * pObj1 = pPlayer->_3DObject()->Get((int)CPlayerObject::ObjectType::Visualization);

                // 対象オブジェクトのサイズの半分を取得
                float fscale1 = EnemyItr2->Get3DObject()->GetMatInfoSca()->x / 2;
                float fscale2 = pObj1->GetMatInfoSca()->x / 2;

                // 球体の当たり判定
                if (this->Collision()->Ball(*pObj1, *EnemyItr2->Get3DObject(), fscale1 + fscale2))
                {
                    pPlayer->ProcessAtDecision();
                    EnemyItr2->ProcessAtDecision();
                    this->Collision_ptr(EnemyItr2->Get3DObject());
                    this->Collision_ptr(pObj1);
                }
            }
        }
    }
}

//=========================================================================
// エネミー同士のコリジョン
void DamageObject::Collision_Enemy_Enemy(void)
{
    auto * pEnemy = (CEnemyObject*)this->m_EnemyObject;

    // ダメージポリゴン探索機能
    if (mslib::nullptr_check(pEnemy))
    {
        // 全種類のエネミー探索
        for (auto EnemyItr1 = pEnemy->GetEnemyObjectPos()->begin(); EnemyItr1 != pEnemy->GetEnemyObjectPos()->end(); ++EnemyItr1)
        {
            // 個々のエネミーの探索
            for (auto EnemyItr2 = EnemyItr1->second->begin(); EnemyItr2 != EnemyItr1->second->end(); ++EnemyItr2)
            {
                // 個々のエネミーの探索
                for (auto EnemyItr3 = EnemyItr1->second->begin(); EnemyItr3 != EnemyItr1->second->end(); ++EnemyItr3)
                {
                    // 対象オブジェクトのサイズの半分を取得
                    if (EnemyItr2 != EnemyItr3)
                    {
                        float fscale1 = EnemyItr2->Get3DObject()->GetMatInfoSca()->x;
                        float fscale2 = EnemyItr3->Get3DObject()->GetMatInfoSca()->x;
                        bool bkey = false;
                        const D3DXVECTOR3 vec = D3DXVECTOR3(0, 0, -1);

                        // 球体の当たり判定
                        if (this->Collision()->Ball(*EnemyItr2->Get3DObject(), *EnemyItr3->Get3DObject(), fscale1 + fscale2))
                        {
                            this->Collision_ptr(EnemyItr2->Get3DObject());
                            this->Collision_ptr(EnemyItr3->Get3DObject());
                            auto & __rand = *this->_rand_float()->Get(0);
                            EnemyItr2->Get3DObject()->RotX(__rand(this->GetMt19937()));
                            EnemyItr3->Get3DObject()->RotX(__rand(this->GetMt19937()));
                            bkey = true;
                        }
                        EnemyItr2->m_Step._Step(1.0f, bkey, EnemyItr2->Get3DObject(), &vec);
                        EnemyItr3->m_Step._Step(1.0f, bkey, EnemyItr3->Get3DObject(), &vec);
                    }
                }
            }
        }
    }
}

//=========================================================================
// キャラクターとダメージポリゴンのコリジョン
void DamageObject::Collision_Player_DamagePolygon(void)
{
    auto * pPlayer = (CPlayerObject*)this->m_PlayerObject;
    auto * pStatusGage = (StatusGage*)this->m_StatusObj;
    auto * pDamagePolygonObject = (DamagePolygonObject*)this->m_DamagePolygonObject;
    auto * pBurstGage = (BurstGage*)this->m_BurstGage;

    // ダメージポリゴン探索機能
    if (mslib::nullptr_check(pDamagePolygonObject) && mslib::nullptr_check(pPlayer))
    {
        // 全種類のダメージポリゴン探索
        for (auto DamageItr1 = pDamagePolygonObject->GetDamagePolygonData()->begin(); DamageItr1 != pDamagePolygonObject->GetDamagePolygonData()->end(); ++DamageItr1)
        {
            // 個々のダメージポリゴングループ探索
            for (auto DamageItr2 = (*DamageItr1)->begin(); DamageItr2 != (*DamageItr1)->end(); ++DamageItr2)
            {
                // 個々のダメージポリゴン探索
                for (auto DamageItr3 = DamageItr2->m_obj.begin(); DamageItr3 != DamageItr2->m_obj.end(); ++DamageItr3)
                {
                    // 対象オブジェクトのサイズの半分を取得
                    auto * pObj1 = pPlayer->_3DObject()->Get((int)CPlayerObject::ObjectType::Visualization);
                    float fscale1 = pObj1->GetMatInfoSca()->x / 2;
                    float fscale2 = DamageItr3->Get3DObject()->GetMatInfoSca()->x / 2;

                    // 球体のコリジョン
                    if (this->Collision()->Ball(*pObj1, *DamageItr3->Get3DObject(), fscale1 + fscale2))
                    {
                        if (mslib::nullptr_check(pBurstGage))
                        {
                            pBurstGage->Minus(__Player_DamagePolygon_LV);
                        }
                        if (mslib::nullptr_check(pStatusGage) && mslib::nullptr_check(pBurstGage))
                        {
                            if (pBurstGage->GetOverflowMinusBuff() != 0)
                            {
                                if (pBurstGage->GetLevel() == 0)
                                {
                                    pStatusGage->HP_Operation(-(__Player_DamagePolygon));
                                }
                            }
                        }

                        DamageItr3->DeleteKeyON();
                        this->Collision_ptr(DamageItr3->Get3DObject());
                        this->Collision_ptr(pObj1);
                    }
                }

                // 1つでもデータがある
                if (DamageItr2->m_obj.size() != 0)
                {
                    auto rast_obj = --DamageItr2->m_obj.end();

                    // 最終オブジェクトが破棄判定が出ている場合、準備完了にする
                    if (rast_obj->GetDeleteKey() == true)
                    {
                        DamageItr2->m_ready = true;
                    }
                }
            }
        }
    }
}

//=========================================================================
// 爆風オブジェクトとプレイヤーのコリジョン
void DamageObject::Collision_Player_ExplosiveEffect(void)
{
    auto * pPlayer = (CPlayerObject*)this->m_PlayerObject;
    auto * pExplosiveEffect = (ExplosiveEffect*)this->m_ExplosiveEffect;
    auto * pBurstGage = (BurstGage*)this->m_BurstGage;
    auto * pStatusGage = (StatusGage*)this->m_StatusObj;

    // 探索機能
    if (mslib::nullptr_check(pExplosiveEffect) && mslib::nullptr_check(pPlayer))
    {
        // 爆風オブジェクト
        for (auto itr = pExplosiveEffect->GetExplosiveEffectData()->begin(); itr != pExplosiveEffect->GetExplosiveEffectData()->end(); ++itr)
        {
            // コリジョン未判定時
            if (itr->m_collision == false)
            {
                // 各データから情報の取得
                auto * pObj1 = pPlayer->_3DObject()->Get((int)CPlayerObject::ObjectType::Visualization);
                float fscale1 = pObj1->GetMatInfoSca()->x * 10;
                float fscale2 = itr->m_obj.GetMatInfoSca()->x * 10;

                // 球体のコリジョン
                if (this->Collision()->Ball(*pObj1, itr->m_obj, fscale1 + fscale2))
                {
                    // コリジョン判定を出す
                    itr->m_collision = true;
                    if (mslib::nullptr_check(pBurstGage))
                    {
                        pBurstGage->Minus(__Player_DamagePolygon_LV / 2);
                    }
                    if (mslib::nullptr_check(pStatusGage) && mslib::nullptr_check(pBurstGage))
                    {
                        if (pBurstGage->GetOverflowMinusBuff() != 0)
                        {
                            if (pBurstGage->GetLevel() == 0)
                            {
                                pStatusGage->HP_Operation(-(__Player_DamagePolygon / 2));
                            }
                        }
                    }
                    this->Collision_ptr(&itr->m_obj);
                    this->Collision_ptr(pObj1);
                }
            }
        }
    }
}

//=========================================================================
// ソードと敵とのコリジョン
void DamageObject::Collision_SwordObject_Enemy(void)
{
    auto * pEnemy = (CEnemyObject*)this->m_EnemyObject;
    auto * pSwordObject = (SwordObject*)this->m_SwordObject;
    auto * pBurstGage = (BurstGage*)this->m_BurstGage;

    // 全エネミーオブジェクト管理
    for (auto EnemyItr1 = pEnemy->GetEnemyObjectPos()->begin(); EnemyItr1 != pEnemy->GetEnemyObjectPos()->end(); ++EnemyItr1)
    {
        // 個々のエネミーオブジェクト管理
        for (auto EnemyItr2 = EnemyItr1->second->begin(); EnemyItr2 != EnemyItr1->second->end(); ++EnemyItr2)
        {
            // 刀身の場所オブジェクト管理
            auto *pObj = pSwordObject->GetBladeObject();

            // ポインタチェック
            if (mslib::nullptr_check(pObj))
            {
                // ブレードリストを更新
                for (auto itr = pObj->begin(); itr != pObj->end(); ++itr)
                {
                    // 対象オブジェクト同士の当たり判定
                    if (this->Collision()->Ball(*itr->Get3DObject(), *EnemyItr2->Get3DObject(), 1.0f))
                    {
                        if (itr->Hit(EnemyItr2->Get3DObject()))
                        {
                            if (mslib::nullptr_check(pBurstGage))
                            {
                                pBurstGage->Plus(__Level_up_blade);
                            }
                            this->Collision_ptr(EnemyItr2->Get3DObject());
                            this->Collision_ptr(itr->Get3DObject());

                            // 判定時の処理
                            EnemyItr2->m_Life -= __Player_DamagePolygon;
                            if (EnemyItr2->m_Life <= 0)
                            {
                                EnemyItr2->DeleteKeyON();
                            }
                            auto * pSparkEffect = (SparkEffectObject*)this->m_SparkEffectObject;
                            if (mslib::nullptr_check(pSparkEffect))
                            {
                                pSparkEffect->Create(*itr->Get3DObject());
                            }
                        }
                    }
                }
            }
        }
    }
}

//=========================================================================
// ダメージボックとプレイヤーとのコリジョン
void DamageObject::Collision_Player_DamageBox(void)
{
    auto *pDamageBox = (DamageBox*)this->m_DamageBox;
    auto * pPlayer = (CPlayerObject*)this->m_PlayerObject;
    auto * pBurstGage = (BurstGage*)this->m_BurstGage;
    auto * pStatusGage = (StatusGage*)this->m_StatusObj;

    for (auto itr = pDamageBox->GetDamageBoxParam()->begin(); itr != pDamageBox->GetDamageBoxParam()->end(); ++itr)
    {
        if (itr->GetDeleteKey())
        {
            auto *pObj = pPlayer->_3DObject()->Get((int)CPlayerObject::ObjectType::Visualization);

            float fscale1 = pObj->GetMatInfoSca()->x * 10;
            float fscale2 = itr->Get3DObject()->GetMatInfoSca()->x * 10;
            if (this->Collision()->Ball(*itr->Get3DObject(), *pObj, fscale1 + fscale2))
            {
                this->Collision_ptr(itr->Get3DObject());
                this->Collision_ptr(pObj);

                itr->DeleteKeyON();
                if (mslib::nullptr_check(pStatusGage))
                {
                    pBurstGage->Minus(__Player_DamagePolygon);
                }
                if (mslib::nullptr_check(pStatusGage) && mslib::nullptr_check(pBurstGage))
                {
                    if (pBurstGage->GetOverflowMinusBuff() != 0)
                    {
                        if (pBurstGage->GetLevel() == 0)
                        {
                            pStatusGage->HP_Operation(-(int)(__Player_DamagePolygon*0.1f));
                        }
                    }
                }
            }
        }
    }
}

//=========================================================================
// 防御フィールドと弾のコリジョン
void DamageObject::Collision_EnemyDefensiveField_Bullet(void)
{
    auto *pEnemyDefensiveField = (EnemyDefensiveField*)this->m_EnemyDefensiveField;
    auto *pBulletObject = (BulletObject*)this->m_BulletObject;

    if (mslib::nullptr_check(pBulletObject) && mslib::nullptr_check(pEnemyDefensiveField))
    {
        // 全種類の弾の探索
        for (auto BulletItr1 = pBulletObject->GetBulletInputSystem()->begin(); BulletItr1 != pBulletObject->GetBulletInputSystem()->end(); ++BulletItr1)
        {
            // 個々の弾の探索
            for (auto BulletItr2 = (*BulletItr1)->begin(); BulletItr2 != (*BulletItr1)->end(); ++BulletItr2)
            {
                if (BulletItr2->GetDeleteKey() == false)
                {
                    auto *pDefensiveField = pEnemyDefensiveField->GetDefensiveField();

                    for (auto itr = pDefensiveField->begin(); itr != pDefensiveField->end(); itr++)
                    {
                        // 対象オブジェクトのサイズの半分を取得
                        float fscale1 = BulletItr2->Get3DObject()->GetMatInfoSca()->x * 10;
                        float fscale2 = itr->second.Get3DObject()->GetMatInfoSca()->x * 10;

                        // 球体のコリジョン
                        if (this->Collision()->Ball(*BulletItr2->Get3DObject(), *itr->second.Get3DObject(), fscale1 + fscale2))
                        {
                            this->Collision_ptr(BulletItr2->Get3DObject());
                            this->Collision_ptr(itr->second.Get3DObject());

                            // 弾の消滅フラグを立てる
                            BulletItr2->DeleteKeyON();

                            // 判定時の処理
                            itr->second.ControlLife(-__Player_DamagePolygon);
                            if (itr->second.GetLife() < 0)
                            {
                                itr->second.DeleteKeyON();
                            }
                        }
                    }
                }
            }
        }
    }
}

//=========================================================================
// 防御フィールドとプレイヤーのコリジョン
void DamageObject::Collision_EnemyDefensiveField_Player(void)
{
    auto *pEnemyDefensiveField = (EnemyDefensiveField*)this->m_EnemyDefensiveField;
    auto *pPlayerObject = (CPlayerObject*)this->m_PlayerObject;

    if (mslib::nullptr_check(pPlayerObject) && mslib::nullptr_check(pEnemyDefensiveField))
    {
        auto *pDefensiveField = pEnemyDefensiveField->GetDefensiveField();
        auto * pObj = pPlayerObject->_3DObject()->Get((int)CPlayerObject::ObjectType::Visualization);

        for (auto itr = pDefensiveField->begin(); itr != pDefensiveField->end(); itr++)
        {
            // 対象オブジェクトのサイズの半分を取得
            float fscale1 = pObj->GetMatInfoSca()->x * 10;
            float fscale2 = itr->second.Get3DObject()->GetMatInfoSca()->x * 10;
            bool bkey = false;
            const D3DXVECTOR3 vec = D3DXVECTOR3(0, 0, -1);

            // 球体のコリジョン
            if (this->Collision()->Ball(*pObj, *itr->second.Get3DObject(), fscale1 + fscale2))
            {
                this->Collision_ptr(pObj);
                this->Collision_ptr(itr->second.Get3DObject());
                bkey = true;
            }
            pPlayerObject->_Step(1.0f, bkey, pObj, &vec);
        }
    }
}

//=========================================================================
// 高台とプレイヤーとのコリジョン
void DamageObject::Collision_HighGround_Player(void)
{
    if (mslib::nullptr_check(this->m_PlayerObject) && mslib::nullptr_check(this->m_HighGround))
    {
        auto * pPlayerObject = (CPlayerObject*)this->m_PlayerObject;
        auto * pHighGround = (HighGround*)this->m_HighGround;

        if (pHighGround->GetPlayKey())
        {
            auto * pObj1 = pPlayerObject->_3DObject()->Get((int)CPlayerObject::ObjectType::Invisible);
            auto * pObj2 = pHighGround->_3DObject()->Get(0);

            if (this->Collision()->Ball(*pObj1, *pObj2, pObj2->GetMatInfoSca()->x * 4))
            {
                // 高台の中である
                if (this->Collision()->Ball(*pObj1, *pObj2, pObj2->GetMatInfoSca()->x * 2))
                {
                    pPlayerObject->SetGround(*pObj2->GetMatInfoPos());
                }
            }
            else
            {
                pHighGround->UpdateStop();
            }
        }
    }
}

//=========================================================================
// コリジョンのポインタ表示
void DamageObject::Collision_ptr(void * ptr)
{
#if defined(_SETImGui_Dx9_) || defined(_DEBUG) || defined(DEBUG)
    this->m_collision_ptr.push_back(ptr);
#else
    ptr;
#endif
}
