//==========================================================================
// ユーザー情報[UserInformation.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "UserInformation.h"

//==========================================================================
// 定数定義
//==========================================================================
const std::string __new_data = "resource/data/new_data.bin";
const std::string __user_data = "resource/data/user_data.bin";

UserInformationData::UserInformationData()
{
    this->m_minute.Init(0, 0);
    this->m_user_name.clear();
    this->m_password.clear();
    this->m_rank = 0;
    this->m_time = 0;
    this->m_JudgmentOfSuccess = false;
}

UserInformationData::~UserInformationData()
{
    this->m_user_name.clear();
    this->m_password.clear();
    this->m_rank = 0;
    this->m_JudgmentOfSuccess = false;
}

//=========================================================================
// 算出
void UserInformationData::Calculation(void)
{
    this->m_time = (60 * this->m_minute.GetTime()) + this->m_seconds.GetTime();
}

UserInformation::UserInformation()
{
    this->SetObjectName("UserInformation");
}

UserInformation::~UserInformation()
{
    this->m_list.clear();
}

//=========================================================================
// 初期化
bool UserInformation::Init(void)
{
    this->Load(__user_data);
    return false;
}

//=========================================================================
// 解放
void UserInformation::Uninit(void)
{
    this->m_list.sort();
    int rank = 0;
    for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr)
    {
        rank++;
        itr->m_rank = rank;
        itr->Calculation();
    }
    this->Save(__user_data);
}

//=========================================================================
// 更新
void UserInformation::Update(void)
{
}

//=========================================================================
// 描画
void UserInformation::Draw(void)
{
}

//=========================================================================
// デバッグ
void UserInformation::Debug(void)
{
}

//=========================================================================
// データの登録
void UserInformation::InputData(const UserInformationData & data)
{
    this->m_list.push_back(data);
    int rank = 0;
    for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr)
    {
        itr->Calculation();
    }
    this->m_list.sort();
    for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr)
    {
        rank++;
        itr->m_rank = rank;
    }
}

//=========================================================================
// 新規データの一時保存
void UserInformation::InputNewData(const UserInformationData & data)
{
    std::ofstream fstream;

    // ファイルを開く
    fstream.open(__new_data, std::ios::out | std::ios::binary);

    int size_int = 0;
    const auto & JudgmentOfSuccess = data.m_JudgmentOfSuccess;
    const auto & minute = data.m_minute;
    const auto & rank = data.m_rank;
    const auto & seconds = data.m_seconds;
    const auto & time = data.m_time;

    //=========================================================================
    // 基礎データ
    //=========================================================================
    fstream.write((char *)&JudgmentOfSuccess, sizeof(JudgmentOfSuccess));
    fstream.write((char *)&minute, sizeof(minute));
    fstream.write((char *)&rank, sizeof(rank));
    fstream.write((char *)&seconds, sizeof(seconds));
    fstream.write((char *)&time, sizeof(time));

    //=========================================================================
    // ユーザー名
    //=========================================================================
    size_int = (int)data.m_user_name.size();
    fstream.write((char *)&size_int, sizeof(size_int));
    for (auto itr2 = data.m_user_name.begin(); itr2 != data.m_user_name.end(); ++itr2)
    {
        fstream.write((char *)&(*itr2), sizeof((*itr2)));
    }

    //=========================================================================
    // パスワード
    //=========================================================================
    size_int = (int)data.m_password.size();
    fstream.write((char *)&size_int, sizeof(size_int));
    for (auto itr2 = data.m_password.begin(); itr2 != data.m_password.end(); ++itr2)
    {
        fstream.write((char *)&(*itr2), sizeof((*itr2)));
    }

    // ファイルを閉じる
    fstream.close();
}

//=========================================================================
// 新規データの呼び出し
UserInformationData UserInformation::LoadNewData(void)
{
    std::ifstream fstream;
    UserInformationData data;

    // ファイルを開く
    fstream.open(__new_data, std::ios::in | std::ios::binary);

    int size_int = 0;

    //=========================================================================
    // 基礎データ
    //=========================================================================
    fstream.read((char *)&data.m_JudgmentOfSuccess, sizeof(data.m_JudgmentOfSuccess));
    fstream.read((char *)&data.m_minute, sizeof(data.m_minute));
    fstream.read((char *)&data.m_rank, sizeof(data.m_rank));
    fstream.read((char *)&data.m_seconds, sizeof(data.m_seconds));
    fstream.read((char *)&data.m_time, sizeof(data.m_time));

    //=========================================================================
    // ユーザー名
    //=========================================================================
    fstream.read((char *)&size_int, sizeof(size_int));
    for (int i = 0; i < size_int; i++)
    {
        int read_data = 0;
        fstream.read((char *)&read_data, sizeof(read_data));
        data.m_user_name.push_back(read_data);
    }

    //=========================================================================
    // パスワード
    //=========================================================================
    fstream.read((char *)&size_int, sizeof(size_int));
    for (int i = 0; i < size_int; i++)
    {
        int read_data = 0;
        fstream.read((char *)&read_data, sizeof(read_data));
        data.m_password.push_back(read_data);
    }

    // ファイルを閉じる
    fstream.close();

    return data;
}

//=========================================================================
// 読み取り専用
void UserInformation::Load(const std::string & file)
{
    std::ifstream fstream;
    const std::string str_endtag = "END";
    char data_tag[256] = { 0 };

    // ファイルを開く
    fstream.open(file, std::ios::in | std::ios::binary);
    this->m_list.clear();

    fstream.read((char *)&data_tag, sizeof(data_tag));

    // データ存在判定
    if (std::string("ON_DATA") == data_tag)
    {
        // 読み取り
        for (;;)
        {
            UserInformationData data;

            char endtag[256] = { 0 };
            int size_int = 0;

            //=========================================================================
            // 基礎データ
            //=========================================================================
            fstream.read((char *)&data.m_JudgmentOfSuccess, sizeof(data.m_JudgmentOfSuccess));
            fstream.read((char *)&data.m_minute, sizeof(data.m_minute));
            fstream.read((char *)&data.m_rank, sizeof(data.m_rank));
            fstream.read((char *)&data.m_seconds, sizeof(data.m_seconds));
            fstream.read((char *)&data.m_time, sizeof(data.m_time));

            //=========================================================================
            // ユーザー名
            //=========================================================================
            fstream.read((char *)&size_int, sizeof(size_int));
            for (int i = 0; i < size_int; i++)
            {
                int read_data = 0;
                fstream.read((char *)&read_data, sizeof(read_data));
                data.m_user_name.push_back(read_data);
            }

            //=========================================================================
            // パスワード
            //=========================================================================
            fstream.read((char *)&size_int, sizeof(size_int));
            for (int i = 0; i < size_int; i++)
            {
                int read_data = 0;
                fstream.read((char *)&read_data, sizeof(read_data));
                data.m_password.push_back(read_data);
            }

            // データを登録
            this->m_list.push_back(data);

            fstream.read((char *)&endtag, sizeof(endtag));

            // 終末文字の検出
            if (str_endtag == endtag)
            {
                break;
            }
        }
    }

    // ファイルを閉じる
    fstream.close();
    this->m_list.sort();
}

//=========================================================================
// 書き込み専用
void UserInformation::Save(const std::string & file)
{
    std::ofstream fstream;
    int count = 0;
    this->m_list.sort();

    // ファイルを開く
    fstream.open(file, std::ios::out | std::ios::binary);

    // データ存在判定
    if (this->m_list.size() == 0)
    {
        char data_tag[256] = { "NOT_DATA" };
        fstream.write((char *)&data_tag, sizeof(data_tag));
    }
    else
    {
        char data_tag[256] = { "ON_DATA" };
        fstream.write((char *)&data_tag, sizeof(data_tag));
    }

    for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr)
    {
        count++;
        
        int size_int = 0;
        const auto & JudgmentOfSuccess = itr->m_JudgmentOfSuccess;
        const auto & minute = itr->m_minute;
        const auto & rank = itr->m_rank;
        const auto & seconds = itr->m_seconds;
        const auto & time = itr->m_time;

        //=========================================================================
        // 基礎データ
        //=========================================================================
        fstream.write((char *)&JudgmentOfSuccess, sizeof(JudgmentOfSuccess));
        fstream.write((char *)&minute, sizeof(minute));
        fstream.write((char *)&rank, sizeof(rank));
        fstream.write((char *)&seconds, sizeof(seconds));
        fstream.write((char *)&time, sizeof(time));

        //=========================================================================
        // ユーザー名
        //=========================================================================
        size_int = (int)itr->m_user_name.size();
        fstream.write((char *)&size_int, sizeof(size_int));
        for (auto itr2 = itr->m_user_name.begin(); itr2 != itr->m_user_name.end(); ++itr2)
        {
            fstream.write((char *)&(*itr2), sizeof((*itr2)));
        }

        //=========================================================================
        // パスワード
        //=========================================================================
        size_int = (int)itr->m_password.size();
        fstream.write((char *)&size_int, sizeof(size_int));
        for (auto itr2 = itr->m_password.begin(); itr2 != itr->m_password.end(); ++itr2)
        {
            fstream.write((char *)&(*itr2), sizeof((*itr2)));
        }

        //=========================================================================
        // 終末文字ではない
        //=========================================================================
        if ((int)this->m_list.size() != count)
        {
            char endtag[256] = { "NOTEND" };
            fstream.write((char *)&endtag, sizeof(endtag));
        }

        //=========================================================================
        // 終末文字である
        //=========================================================================
        if ((int)this->m_list.size() == count)
        {
            // 終末文字
            char endtag[256] = { "END" };
            fstream.write((char *)&endtag, sizeof(endtag));
        }
    }

    // ファイルを閉じる
    fstream.close();
}
