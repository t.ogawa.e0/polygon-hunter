//==========================================================================
// フェード[Fade.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CFade
// Content: フェード
//
//==========================================================================
class CFade : public mslib::DX9_Object
{
private:
    // パラメータ
    class CParam
    {
    public:
        CParam()
        {
            this->m_a = 0;
            this->m_Change = false;
            this->m_Key = false;
            this->m_Draw = false;
            this->m_In = false;
        }
        ~CParam() {}
    public:
        int m_a; // α
        bool m_Change; // change
        bool m_Key; // 鍵
        bool m_Draw; // 描画判定
        bool m_In; // フェードイン判定
    };
public:
    CFade() :DX9_Object(mslib::DX9_ObjectID::SpecialBlock) {}
    ~CFade() {}
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    // フェードイン
    void In(void);
    // フェードアウト
    void Out(void);
    // フェードイン終了判定
    bool FeadInEnd(void);
    bool GetDraw(void) { return this->m_Param.m_Draw; }
private:
    CParam m_Param; // パラメータ
};
