//==========================================================================
// テストオブジェクト[TestObject.h]
// author: tatsuya ogawa
//==========================================================================
#include "TestObject.h"
#include "LinearPolygon.h"
#include "EnclosurePolygon.h"
#include "PlayerObject.h"
#include "Game_Over_Clear.h"

TestObject::TestObject()
{
    this->SetObjectName("TestObject");
}

TestObject::~TestObject()
{
}

//=========================================================================
// 初期化
bool TestObject::Init(void)
{
    this->m_LinearPolygon = this->GetObjects(mslib::DX9_ObjectID::Cube, "LinearPolygon");
    this->m_EnclosurePolygon = this->GetObjects(mslib::DX9_ObjectID::Cube, "EnclosurePolygon");
    this->m_PlayerObject = this->GetObjects(mslib::DX9_ObjectID::Cube, "PlayerObject");
    this->m_Game_Over_Clear = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "Game_Over_Clear");

    this->_3DObject()->Create()->Init(0);

    return false;
}

//=========================================================================
// 解放
void TestObject::Uninit(void)
{
}

//=========================================================================
// 更新
void TestObject::Update(void)
{
}

//=========================================================================
// 描画
void TestObject::Draw(void)
{
}

//=========================================================================
// デバッグ
void TestObject::Debug(void)
{
    if (this->ImGui()->Button("LinearPolygon Create!"))
    {
        auto * pPlayerObject = (CPlayerObject*)this->m_PlayerObject;
        auto * pLinearPolygon = (LinearPolygon*)this->m_LinearPolygon;
        if (mslib::nullptr_check(pLinearPolygon) && mslib::nullptr_check(pPlayerObject))
        {
            auto *pObj = pPlayerObject->_3DObject()->Get((int)CPlayerObject::ObjectType::Visualization);
            if (mslib::nullptr_check(pObj))
            {
                pLinearPolygon->Create(*pObj, *this->_3DObject()->Get(0));
            }
        }
    }
    if (this->ImGui()->Button("EnclosurePolygon Create!"))
    {
        auto * pPlayerObject = (CPlayerObject*)this->m_PlayerObject;
        auto * pEnclosurePolygon = (EnclosurePolygon*)this->m_EnclosurePolygon;
        if (mslib::nullptr_check(pEnclosurePolygon) && mslib::nullptr_check(pPlayerObject))
        {
            auto *pObj = pPlayerObject->_3DObject()->Get((int)CPlayerObject::ObjectType::Visualization);
            if (mslib::nullptr_check(pObj))
            {
                pEnclosurePolygon->Create(*pObj, *this->_3DObject()->Get(0));
            }
        }
    }
    if (this->ImGui()->Button("GameOver!"))
    {
        auto *pGameOver = (Game_Over_Clear*)this->m_Game_Over_Clear;

        if (mslib::nullptr_check(pGameOver))
        {
            pGameOver->SetObjectMenu(Game_Over_Clear::MyEnumClass::GameOver);
            pGameOver->UpdatePlay();
        }
    }
    if (this->ImGui()->Button("GameClear!"))
    {
        auto *pGameClear = (Game_Over_Clear*)this->m_Game_Over_Clear;

        if (mslib::nullptr_check(pGameClear))
        {
            pGameClear->SetObjectMenu(Game_Over_Clear::MyEnumClass::GameClear);
            pGameClear->UpdatePlay();
        }
    }
}
