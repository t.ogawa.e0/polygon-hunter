//==========================================================================
// マップエディタ試作品[MapEditor.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "MapEditor.h"
#include "resource_list.h"

constexpr int MAP_VEC_NUM_Y = (60 + 1 + 60); // 一列頂点数
constexpr int MAP_VEC_NUM_YMAX = MAP_VEC_NUM_Y * MAP_VEC_NUM_Y; // 最大頂点数
constexpr int MAP_VEC_NUM_Y_BUFF_NUM = MAP_VEC_NUM_Y / 2; // Y軸移動値変動位置
constexpr float MAP_VEC_LIMIT_Y = -10.0f; // 最大深さ
constexpr float MAP_VEC_MOVE_BUFF = MAP_VEC_LIMIT_Y / MAP_VEC_NUM_Y_BUFF_NUM; // 頂点移動バフ

//==========================================================================
// 演算子の付与
//==========================================================================
template<> class _MSLIB enum_system::calculation<CField::OBje> : std::true_type {};

CMapEditor::CMapEditor()
{
    auto light = mslib::DX9_Light(mslib::DX9_DeviceManager::GetDXDevice()->GetD3DDevice()); // ライト
                                                                                            //CXGrid * gri;
    CXCamera * cam;
    CPlayer * ply;
    CField *Field;
    CSky * Sky;

    light.Init({ 0, -1.0f, -1.0f });

    //CObject::NewObject(gri);
    mslib::DX9_Object::NewObject(Sky);
    mslib::DX9_Object::NewObject(cam);
    mslib::DX9_Object::NewObject(Field);
    mslib::DX9_Object::NewObject(ply);
}

CMapEditor::~CMapEditor()
{
}

//==========================================================================
// 初期化
bool CMapEditor::Init(void)
{
    return mslib::DX9_Object::InitAll();
}

//==========================================================================
// 解放
void CMapEditor::Uninit(void)
{
    mslib::DX9_Object::ReleaseAll();
}

//==========================================================================
// 更新
void CMapEditor::Update(void)
{
    mslib::DX9_Object::UpdateAll();
}

//==========================================================================
// 描画
void CMapEditor::Draw(void)
{
    mslib::DX9_Object::DrawAll();
}

//==========================================================================
//
// class  : CXCamera
// Content: カメラ
//
//==========================================================================
bool CXCamera::Init(void)
{
    D3DXVECTOR3 Eye = D3DXVECTOR3(0.0f, 2.0f, -3.0f); // 注視点
    D3DXVECTOR3 At = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // カメラ座標
    this->Camera()->Create()->Init(Eye, At);
    this->Camera()->Create()->Init(Eye, At);
    this->_3DObject()->Create()->Init(0);
    this->_3DObject()->Create()->Init(0);
    this->_3DObject()->Get(0)->ScalePlus(-0.8f);
    this->_3DObject()->Get(1)->ScalePlus(-0.8f);
    this->DebugON();
    return false;
}

void CXCamera::Uninit(void)
{
}

void CXCamera::Update(void)
{
    if (this->ImGui()->MenuItem("エディタカメラ"))
    {
        this->m_cameraKey = false;
    }
    if (this->ImGui()->MenuItem("追従カメラ"))
    {
        this->m_cameraKey = true;
    }

    if (this->m_cameraKey == false)
    {
        // ホイールを押したときの処理
        if (this->Dinput_Mouse()->Press(mslib::DirectInputMouseButton::Wheel))
        {
            this->Camera()->Get(0)->MoveX(-(float)this->Dinput_Mouse()->Speed().m_lX*0.01f);
            this->Camera()->Get(0)->MoveY((float)this->Dinput_Mouse()->Speed().m_lY*0.01f);
        }

        // 右クリックの時の処理
        if (this->Dinput_Mouse()->Press(mslib::DirectInputMouseButton::Right))
        {
            this->Camera()->Get(0)->RotViewX((float)this->Dinput_Mouse()->Speed().m_lX*0.0025f);
            this->Camera()->Get(0)->RotViewY((float)this->Dinput_Mouse()->Speed().m_lY*0.0025f);
            this->Camera()->Get(0)->RotCameraX((float)this->Dinput_Mouse()->Speed().m_lX*0.0025f);
            this->Camera()->Get(0)->RotCameraY((float)this->Dinput_Mouse()->Speed().m_lY*0.0025f);
            this->_3DObject()->Get(0)->RotX((float)this->Dinput_Mouse()->Speed().m_lX*0.005f);
        }

        // マウスの移動速度を加算
        if (this->Camera()->Get(0)->DistanceFromView((float)this->Dinput_Mouse()->Speed().m_lZ*0.0025f) < 8.0f)
        {
            this->Camera()->Get(0)->DistanceFromView(-(float)this->Dinput_Mouse()->Speed().m_lZ*0.0025f);
        }

        // 右クリック&左クリックの時の処理
        if (this->Dinput_Mouse()->Press(mslib::DirectInputMouseButton::Right) && this->Dinput_Mouse()->Press(mslib::DirectInputMouseButton::Left))
        {
            this->Camera()->Get(0)->MoveZ_2(0.05f);
        }

        const auto & vPos = this->Camera()->Get(0)->GetVECTOR(mslib::DX9_CameraVectorList::VAT);

        // 視点の移動
        this->_3DObject()->Get(0)->SetMatInfoPos(vPos);

        this->Camera()->Get(0)->Update(this->GetWinSize(), this->GetDirectX9Device());

        this->m_pos = mslib::CVector3<float>(vPos.x, vPos.y, vPos.z);
    }

    if (this->m_cameraKey == true)
    {
        auto *pPos = this->GetObjects(mslib::DX9_ObjectID::Xmodel, "CPlayer")->_3DObject()->Get(0);

        // ホイールを押したときの処理
        if (this->Dinput_Mouse()->Press(mslib::DirectInputMouseButton::Wheel))
        {
            pPos->MoveX(-(float)this->Dinput_Mouse()->Speed().m_lX*0.01f);
            pPos->MoveZ(-(float)this->Dinput_Mouse()->Speed().m_lY*0.01f);
        }

        // 右クリックの時の処理
        if (this->Dinput_Mouse()->Press(mslib::DirectInputMouseButton::Right))
        {
            this->Camera()->Get(1)->RotViewX((float)this->Dinput_Mouse()->Speed().m_lX*0.0025f);
            this->Camera()->Get(1)->RotViewY((float)this->Dinput_Mouse()->Speed().m_lY*0.0025f);
            pPos->RotX((float)this->Dinput_Mouse()->Speed().m_lX*0.0025f);
            pPos->RotY((float)this->Dinput_Mouse()->Speed().m_lY*0.0025f);
            this->_3DObject()->Get(1)->RotX((float)this->Dinput_Mouse()->Speed().m_lX*0.0025f);
        }

        // 右クリック&左クリックの時の処理
        if (this->Dinput_Mouse()->Press(mslib::DirectInputMouseButton::Right) && this->Dinput_Mouse()->Press(mslib::DirectInputMouseButton::Left))
        {
            pPos->MoveZ(0.05f);
        }

        this->Camera()->Get(1)->SetAt(*pPos->GetMatInfoPos());
        this->Camera()->Get(1)->SetEye(*pPos->GetMatInfoPos());

        const auto & vPos = this->Camera()->Get(1)->GetVECTOR(mslib::DX9_CameraVectorList::VAT);

        // 視点の移動
        this->_3DObject()->Get(1)->SetMatInfoPos(vPos);

        this->Camera()->Get(1)->Update(this->GetWinSize(), this->GetDirectX9Device());

        this->m_pos = mslib::CVector3<float>(vPos.x, vPos.y, vPos.z);
    }
}

void CXCamera::Draw(void)
{
}

void CXCamera::Debug(void)
{
}

//==========================================================================
//
// class  : CXGrid
// Content: グリッド
//
//==========================================================================
bool CXGrid::Init(void)
{
    this->_3DGrid()->Init(30);
    return false;
}

void CXGrid::Uninit(void)
{
}

void CXGrid::Update(void)
{
}

void CXGrid::Draw(void)
{
    this->_3DGrid()->Draw();
}

//==========================================================================
//
// class  : CPlayer
// Content: プレイヤー
//
//==========================================================================
bool CPlayer::Init(void)
{
    auto * pObje = this->_3DObject()->Create();
    pObje->Init(0);

    this->_3DXmodel()->ObjectInput(pObje);

    this->m_Field = this->GetObjects(mslib::DX9_ObjectID::Field, "CField");

    this->DebugON();

    return this->_3DXmodel()->Init(RESOURCE_Alicia_solid_x);
}

void CPlayer::Uninit(void)
{
}

void CPlayer::Update(void)
{
    if (this->m_Field == nullptr)
    {
        this->m_Field = this->GetObjects(mslib::DX9_ObjectID::Field, "CField");
    }

    auto * p_field = (CField*)this->m_Field;

    float speedZ = 0.0f, speedX = 0.0f;

    // 存在のチェック
    if (this->Dinput_Keyboard() != nullptr)
    {
        if (this->Dinput_Keyboard()->Press(mslib::DirectInputKeyboardButton::KEY_W))
        {
            speedZ = 0.05f;
        }
        if (this->Dinput_Keyboard()->Press(mslib::DirectInputKeyboardButton::KEY_S))
        {
            speedZ = -0.05f;
        }
        if (this->Dinput_Keyboard()->Press(mslib::DirectInputKeyboardButton::KEY_A))
        {
            speedX = -0.05f;
        }
        if (this->Dinput_Keyboard()->Press(mslib::DirectInputKeyboardButton::KEY_D))
        {
            speedX = 0.05f;
        }
    }

    this->_3DObject()->Get(0)->MoveZ(speedZ);
    this->_3DObject()->Get(0)->MoveX(speedX);
    auto vPos = p_field->FieldHeight(0, *this->_3DObject()->Get(0)->GetMatInfoPos(), this->m_HitArea);
    this->_3DObject()->Get(0)->SetMatInfoPos(vPos);

    this->_3DXmodel()->Update();
}

void CPlayer::Draw(void)
{
    this->_3DXmodel()->Draw();
}

void CPlayer::Debug(void)
{
}

bool CField::Init(void)
{
    auto * pObje = this->_3DObject()->Create();

    pObje->Init(0);

    this->_3DField()->ObjectInput(pObje);

    this->DebugON();

    //==========================================================================
    //==========================================================================
    this->m_Map120x120_Y.Reserve(MAP_VEC_NUM_YMAX);

    int YCount = 0;
    for (int YMAX = 0; YMAX < MAP_VEC_NUM_Y; YMAX++)
    {
        //==========================================================================
        MAP_VEC_NUM_Y; // 一列頂点数
        MAP_VEC_NUM_YMAX; // 最大頂点数
        MAP_VEC_NUM_Y_BUFF_NUM; // Y軸移動値変動位置
        MAP_VEC_LIMIT_Y; // 最大深さ
        MAP_VEC_MOVE_BUFF; // 頂点移動バフ
        //==========================================================================

        //==========================================================================
        // 基礎バフ準備
        //==========================================================================
        if (YMAX <= MAP_VEC_NUM_Y_BUFF_NUM)
        {
            YCount++;
        }
        else if (MAP_VEC_NUM_Y_BUFF_NUM < YMAX)
        {
            YCount--;
            if (YCount <= 0)
            {
                YCount = 1;
            }
        }
        //==========================================================================

         float MAIN_BUFF = MAP_VEC_MOVE_BUFF * YCount;
        int YSubCount = 0;
        for (int Y = 0; Y < MAP_VEC_NUM_Y; Y++)
        {
            //==========================================================================
            // 基礎バフ準備
            //==========================================================================
            if (Y <= MAP_VEC_NUM_Y_BUFF_NUM)
            {
                YSubCount++;
            }
            else if (MAP_VEC_NUM_Y_BUFF_NUM < Y)
            {
                YSubCount--;
                if (YSubCount <= 0)
                {
                    YSubCount = 1;
                }
            }

            //==========================================================================
            auto &floatY = *this->m_Map120x120_Y.Create();
            floatY = (MAIN_BUFF / MAP_VEC_NUM_Y_BUFF_NUM) * YSubCount;
        }
    }
    //==========================================================================
    //==========================================================================
    return this->_3DField()->Init(RESOURCE_Asphalt_jpg, 1, 1);
}

void CField::Uninit(void)
{
}

void CField::Update(void)
{
    //==========================================================================
    // エディタ試作型
    //==========================================================================
    this->ObjeRelease();

    this->ImGui()->NewWindow("テストエディタ", true, ImGuiWindowFlags_::ImGuiWindowFlags_MenuBar);

    if (this->ImGui()->NewMenuBar())
    {
        // menuの追加
        if (this->ImGui()->NewMenu("ファイル"))
        {
            if (this->ImGui()->MenuItem("保存"))
            {
                this->_3DField()->Save(RESOURCE_TestField_bin);
            }
            if (this->ImGui()->MenuItem("読み込み"))
            {
                this->_3DField()->Loat(RESOURCE_TestField_bin);

                //==========================================================================
                int ncount = 0;
                auto *mesh = this->_3DField()->GetMeshData()->Get(0);
                mslib::CColor<int> color;
                color.a = 255;
                color.r = 255;
                for (int iZ = 0; iZ < mesh->m_FieldParameter.Size(); iZ++)
                {
                    for (int iX = 0; iX < mesh->m_FieldParameter.Get(iZ)->Size(); iX++)
                    {
                        if (color.r == 255)
                        {
                            if (0 < color.b)
                            {
                                color.b--;
                            }
                            if (color.b == 0)
                            {
                                color.g++;
                            }
                        }
                        if (color.g == 255)
                        {
                            if (0 < color.r)
                            {
                                color.r--;
                            }
                            if (color.r == 0)
                            {
                                color.b++;
                            }
                        }
                        if (color.b == 255)
                        {
                            if (0 < color.g)
                            {
                                color.g--;
                            }
                            if (color.g == 0)
                            {
                                color.r++;
                            }
                        }

                        auto * IDName = mesh->m_FieldParameter.Get(iZ)->Get(iX);
                        auto fvY = this->m_Map120x120_Y.Get(ncount);
                        if (mslib::nullptr_check(fvY))
                        {
                            IDName->vertex.pos.y = *fvY;
                            IDName->m_Color = color;
                            mesh->m_MismatchSubData[IDName->m_NameID].m_Color = IDName->m_Color;
                            mesh->m_MismatchSubData[IDName->m_NameID].m_NameID = IDName->m_NameID;
                            mesh->m_MismatchSubData[IDName->m_NameID].vertex = IDName->vertex;
                        }
                        ncount++;
                    }
                }
                this->_3DField()->RemakeBuffer(mesh);
                //==========================================================================
            }
            this->ImGui()->EndMenu();
        }
        this->ImGui()->EndMenuBar();
    }

    auto *mesh = this->_3DField()->GetMeshData()->Get(0);
    this->ImGui()->Text("マップサイズ : %d × %d", mesh->Info.BoostMeshX, mesh->Info.BoostMeshZ);
    this->ImGui()->Text("頂点数 : %d", (mesh->Info.BoostMeshX + 1)*(mesh->Info.BoostMeshZ + 1));
    if (this->ImGui()->SliderInt("メッシュ数変更 : X", &mesh->Info.NumMeshX, 1, 50))
    {
        this->_3DField()->Resize(mesh, mesh->Info.NumMeshX, mesh->Info.NumMeshZ);
    }
    if (this->ImGui()->SliderInt("メッシュ数変更 : Z", &mesh->Info.NumMeshZ, 1, 50))
    {
        this->_3DField()->Resize(mesh, mesh->Info.NumMeshX, mesh->Info.NumMeshZ);
    }
    if (this->ImGui()->NewTreeNode("各頂点情報の編集", false))
    {
        for (int iZ = 0; iZ < mesh->m_FieldParameter.Size(); iZ++)
        {
            if (this->ImGui()->NewMenu(this->ImGui()->CreateText("Z : %d", iZ).c_str()))
            {
                for (int iX = 0; iX < mesh->m_FieldParameter.Get(iZ)->Size(); iX++)
                {
                    // 参照するX軸データ
                    auto * IDName = mesh->m_FieldParameter.Get(iZ)->Get(iX);
                    auto * ppos = this->_3DObject()->Create();

                    ppos->Init(0);
                    ppos->SetMatInfoPos(IDName->vertex.pos);

                    if (this->ImGui()->NewMenu(IDName->m_NameID.c_str()))
                    {
                        this->ObjeRelease();
                        ppos = this->_3DObject()->Create();
                        ppos->Init(0);
                        ppos->SetMatInfoPos(IDName->vertex.pos);
                        if (this->ImGui()->NewMenu("頂点座標"))
                        {
                            if (this->ImGui()->SliderFloat("X", &IDName->vertex.pos.x, -10, 10))
                            {
                                // 変更地点の記録
                                mesh->m_MismatchSubData[IDName->m_NameID].m_Color = IDName->m_Color;
                                mesh->m_MismatchSubData[IDName->m_NameID].m_NameID = IDName->m_NameID;
                                mesh->m_MismatchSubData[IDName->m_NameID].vertex = IDName->vertex;
                                this->_3DField()->RemakeBuffer(mesh);
                            }
                            if (this->ImGui()->SliderFloat("Z", &IDName->vertex.pos.z, -10, 10))
                            {
                                // 変更地点の記録
                                mesh->m_MismatchSubData[IDName->m_NameID].m_Color = IDName->m_Color;
                                mesh->m_MismatchSubData[IDName->m_NameID].m_NameID = IDName->m_NameID;
                                mesh->m_MismatchSubData[IDName->m_NameID].vertex = IDName->vertex;
                                this->_3DField()->RemakeBuffer(mesh);
                            }
                            if (this->ImGui()->SliderFloat("Y", &IDName->vertex.pos.y, -10, 10))
                            {
                                // 変更地点の記録
                                mesh->m_MismatchSubData[IDName->m_NameID].m_Color = IDName->m_Color;
                                mesh->m_MismatchSubData[IDName->m_NameID].m_NameID = IDName->m_NameID;
                                mesh->m_MismatchSubData[IDName->m_NameID].vertex = IDName->vertex;
                                this->_3DField()->RemakeBuffer(mesh);
                            }
                            this->ImGui()->EndMenu();
                        }
                        if (this->ImGui()->NewMenu("頂点色"))
                        {
                            if (this->ImGui()->SliderInt("α", &IDName->m_Color.a, 0, 255))
                            {
                                // 変更地点の記録
                                mesh->m_MismatchSubData[IDName->m_NameID].m_Color = IDName->m_Color;
                                mesh->m_MismatchSubData[IDName->m_NameID].m_NameID = IDName->m_NameID;
                                mesh->m_MismatchSubData[IDName->m_NameID].vertex = IDName->vertex;
                                this->_3DField()->RemakeBuffer(mesh);
                            }
                            if (this->ImGui()->SliderInt("R", &IDName->m_Color.r, 0, 255))
                            {
                                // 変更地点の記録
                                mesh->m_MismatchSubData[IDName->m_NameID].m_Color = IDName->m_Color;
                                mesh->m_MismatchSubData[IDName->m_NameID].m_NameID = IDName->m_NameID;
                                mesh->m_MismatchSubData[IDName->m_NameID].vertex = IDName->vertex;
                                this->_3DField()->RemakeBuffer(mesh);
                            }
                            if (this->ImGui()->SliderInt("G", &IDName->m_Color.g, 0, 255))
                            {
                                // 変更地点の記録
                                mesh->m_MismatchSubData[IDName->m_NameID].m_Color = IDName->m_Color;
                                mesh->m_MismatchSubData[IDName->m_NameID].m_NameID = IDName->m_NameID;
                                mesh->m_MismatchSubData[IDName->m_NameID].vertex = IDName->vertex;
                                this->_3DField()->RemakeBuffer(mesh);
                            }
                            if (this->ImGui()->SliderInt("B", &IDName->m_Color.b, 0, 255))
                            {
                                // 変更地点の記録
                                mesh->m_MismatchSubData[IDName->m_NameID].m_Color = IDName->m_Color;
                                mesh->m_MismatchSubData[IDName->m_NameID].m_NameID = IDName->m_NameID;
                                mesh->m_MismatchSubData[IDName->m_NameID].vertex = IDName->vertex;
                                this->_3DField()->RemakeBuffer(mesh);
                            }
                            this->ImGui()->EndMenu();
                        }
                        this->ImGui()->EndMenu();
                    }
                }
                this->ImGui()->EndMenu();
            }
        }
        this->ImGui()->EndTreeNode();
    }
    this->ImGui()->EndWindow();

    //==========================================================================
    // エディタ試作型
    //==========================================================================

    this->_3DField()->Update();
}

void CField::Draw(void)
{
    this->_3DField()->Draw();
}

void CField::Debug(void)
{
}

//==========================================================================
//
// class  : CSky
// Content: スカイドーム
//
//==========================================================================
bool CSky::Init(void)
{
    auto *pObje = this->_3DObject()->Create();

    pObje->Init(0);
    pObje->ScalePlus(-50);

    this->_3DSphere()->ObjectInput(pObje);

    this->m_Camera = this->GetObjects(mslib::DX9_ObjectID::Camera, "CXCamera");

    this->DebugON();

    return this->_3DSphere()->Init(RESOURCE_sky_jpg, 20);
}

void CSky::Uninit(void)
{
}

void CSky::Update(void)
{
    if (this->m_Camera == nullptr)
    {
        this->m_Camera = this->GetObjects(mslib::DX9_ObjectID::Camera, "CXCamera");
    }

    auto * p_camera = (CXCamera*)this->m_Camera;

    p_camera->GetPos();
    this->_3DObject()->Get(0)->SetMatInfoPos({ p_camera->GetPos()->x,p_camera->GetPos()->y,p_camera->GetPos()->z });

    this->_3DSphere()->Update();
}

void CSky::Draw(void)
{
    this->_3DSphere()->Draw();
}

void CSky::Debug(void)
{
}
