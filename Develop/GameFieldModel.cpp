//==========================================================================
// フィールドモデル[GameFieldModel.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameFieldModel.h"
#include "resource_list.h"


CGameFieldModel::CGameFieldModel() : DX9_Object(mslib::DX9_ObjectID::Xmodel)
{
    this->SetObjectName("GameFieldModel");
}


CGameFieldModel::~CGameFieldModel()
{
}

//==========================================================================
// 初期化
bool CGameFieldModel::Init(void)
{
    auto * p_obj = this->_3DObject()->Create();

    p_obj->Init(0);

    p_obj->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);

    this->_3DXmodel()->ObjectInput(p_obj);

    return this->_3DXmodel()->Init(RESOURCE_test_stage_x);
}

//==========================================================================
// 解放
void CGameFieldModel::Uninit(void)
{
}

//==========================================================================
// 更新
void CGameFieldModel::Update(void)
{
    this->_3DXmodel()->Update();
}

//==========================================================================
// 描画
void CGameFieldModel::Draw(void)
{
    this->_3DXmodel()->Draw();
}

//==========================================================================
// デバッグ
void CGameFieldModel::Debug(void)
{
    //auto * p_data = this->_3DXmodel()->GetMdelDataParam(*this->_3DObject()->Get(0));

    //auto * p_element = p_data->GetVertexElement();

    //for (int i = 0; i < p_element->Size(); i++)
    //{
    //    const std::string & str_id = this->ImGui()->CreateText("ID : %2d", i);
    //    auto * p_param = p_element->Get(i);

    //    if (this->ImGui()->NewMenu(str_id.c_str()))
    //    {
    //        this->ImGui()->Text("Stream index : %d", (int)p_param->Stream);
    //        this->ImGui()->Text("Offset in the stream in bytes : %d", (int)p_param->Offset);
    //        this->ImGui()->Text("Data type : %d", (int)p_param->Type);
    //        this->ImGui()->Text("Processing method : %d", (int)p_param->Method);
    //        this->ImGui()->Text("Semantics : %d", (int)p_param->Usage);
    //        this->ImGui()->Text("Semantic index : %d", (int)p_param->UsageIndex);
    //        this->ImGui()->EndMenu();
    //    }
    //}
}
