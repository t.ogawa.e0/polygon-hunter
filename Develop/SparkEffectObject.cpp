//==========================================================================
// 火花エフェクト[SparkEffectObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "SparkEffectObject.h"
#include "GameCamera.h"
#include "resource_list.h"

SparkEffectData::SparkEffectData()
{
    this->m_delete = false;
}

SparkEffectData::SparkEffectData(const mslib::DX9_3DObject & obj)
{
    this->m_obj = obj;
    this->m_delete = false;
}

SparkEffectData::~SparkEffectData()
{
}

SparkEffectObject::SparkEffectObject() : DX9_Object(mslib::DX9_ObjectID::Billboard)
{
    this->SetObjectName("SparkEffectObject");
    this->m_CameraObject = nullptr;
}

SparkEffectObject::~SparkEffectObject()
{
    this->m_data.clear();
}

//==========================================================================
// 初期化
bool SparkEffectObject::Init(void)
{
    this->m_CameraObject = this->GetObjects(mslib::DX9_ObjectID::Camera, "GameCamera");

    return this->_3DBillboard()->Init(RESOURCE_spark_effect_png, 1, 25, 5, mslib::DX9_BillboardPlateList::Vertical);
}

//==========================================================================
// 解放
void SparkEffectObject::Uninit(void)
{
}

//==========================================================================
// 更新
void SparkEffectObject::Update(void)
{
    // オブジェクトの更新
    for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
    {
        itr->m_obj.UpdateAnimationCount();
        if (this->_3DBillboard()->GetPattanNum(0, itr->m_obj.GetAnimationCount()))
        {
            itr->m_delete = true;
        }
    }

    // 解放処理
    for (auto itr = this->m_data.begin(); itr != this->m_data.end(); )
    {
        if (itr->m_delete == true)
        {
            this->_3DBillboard()->ObjectDelete(itr->m_obj);
            itr = this->m_data.erase(itr);
        }
        else
        {
            this->Debug_3DObject(&itr->m_obj);
            ++itr;
        }
    }

    this->_3DBillboard()->Update();
}

//==========================================================================
// 描画
void SparkEffectObject::Draw(void)
{
    this->_3DBillboard()->Draw(((CGameCamera*)this->m_CameraObject)->GetCamera(0)->CreateView());
}

//==========================================================================
// デバッグ
void SparkEffectObject::Debug(void)
{
}

//==========================================================================
// 生成
void SparkEffectObject::Create(const mslib::DX9_3DObject & obj)
{
    this->m_data.emplace_back(obj);
    auto itr = --this->m_data.end();
    itr->m_obj.InitAnimationCount();
    this->_3DBillboard()->ObjectInput(itr->m_obj);
}
