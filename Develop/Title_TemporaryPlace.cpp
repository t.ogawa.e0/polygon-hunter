#include "Title_TemporaryPlace.h"
#include "resource_list.h"

CTitle_TemporaryPlace::CTitle_TemporaryPlace() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->SetObjectName("Title_TemporaryPlace");
}


CTitle_TemporaryPlace::~CTitle_TemporaryPlace()
{
}

bool CTitle_TemporaryPlace::Init(void)
{
    this->_2DPolygon()->Init(RESOURCE_タイトル仮置き_png, true);
    auto * p_obj = this->_2DObject()->Create();
    p_obj->Init(0);
    this->_2DPolygon()->ObjectInput(p_obj);

    return false;
}

void CTitle_TemporaryPlace::Uninit(void)
{
}

void CTitle_TemporaryPlace::Update(void)
{
    this->_2DPolygon()->Update();
}

void CTitle_TemporaryPlace::Draw(void)
{
    this->_2DPolygon()->Draw();
}

void CTitle_TemporaryPlace::Debug(void)
{
}
