//==========================================================================
// 防御フィールド(継承用)[DefensiveFieldObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DefensiveFieldObject.h"

DefensiveFieldObject::DefensiveFieldObject(mslib::DX9_ObjectID ObjectID) : DX9_Object(ObjectID)
{
}

DefensiveFieldObject::~DefensiveFieldObject()
{
    this->m_obj.clear();
    this->m_create_log.clear();
    this->m_obj_mirror.clear();
}

//=========================================================================
// 防御フィールドの取得
std::unordered_map<mslib::DX9_3DObject*, DefensiveFieldObject::param>* DefensiveFieldObject::GetDefensiveField(void)
{
    return &this->m_obj;
}

//=========================================================================
// ベクトルのセット
void DefensiveFieldObject::SetVector(void)
{
    for (auto itr = this->m_obj.begin(); itr != this->m_obj.end(); ++itr)
    {
        this->Debug_3DObject(itr->second.Get3DObject());
        auto itr2 = this->m_obj_mirror.find(itr->second.Get3DObject());
        if (itr2 != this->m_obj_mirror.end())
        {
            this->Debug_3DObject(&itr2->second);
        }
    }
}

//=========================================================================
// デバッグInput
void DefensiveFieldObject::InputParamText(void)
{
    for (auto itr = this->m_obj.begin(); itr != this->m_obj.end(); ++itr)
    {
        this->ImGui()->Text("耐久値 : %d", itr->second.GetLife());
    }
}

DefensiveFieldObject::param::param()
{
    this->m_life = 0;
}

DefensiveFieldObject::param::param(const mslib::DX9_3DObject & obj, int life)
{
    *this->m_obj = obj;
    this->m_life = life;
}

DefensiveFieldObject::param::~param()
{
}

//=========================================================================
// 耐久値の取得
int DefensiveFieldObject::param::GetLife(void)
{
    return this->m_life;
}

//=========================================================================
// 耐久値操作
void DefensiveFieldObject::param::ControlLife(int value)
{
    this->m_life += value;
}
