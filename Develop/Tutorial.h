//==========================================================================
// チュートリアル[Tutorial.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"

//==========================================================================
//
// class  : Tutorial
// Content: チュートリアル
//
//==========================================================================
class Tutorial : public CBaseScene
{
public:
    Tutorial();
    ~Tutorial();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
private:
};

