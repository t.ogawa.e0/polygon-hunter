//==========================================================================
// プレイヤーアタックエフェクト_A[PlayerAttackEffect_A.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "PlayerAttackEffect_A.h"
#include "resource_list.h"

PlayerAttackEffect_A::PlayerAttackEffect_A() : EffectObject(mslib::DX9_ObjectID::Xmodel)
{
    this->SetObjectName("PlayerAttackEffect_A");
}

PlayerAttackEffect_A::~PlayerAttackEffect_A()
{
}

//==========================================================================
// 初期化
bool PlayerAttackEffect_A::Init(void)
{
    this->_3DXmodel()->Init(RESOURCE_effect_1_x);
    this->m_rotZ_rand = mslib::rand_float(0.0f, 20.0f);
    return false;
}

//==========================================================================
// 解放
void PlayerAttackEffect_A::Uninit(void)
{
    this->m_param.clear();
}

//==========================================================================
// 更新
void PlayerAttackEffect_A::Update(void)
{
    // イテレーターを最初から最後まで回す
    for (auto itr = this->m_param.begin(); itr != this->m_param.end();)
    {
        // 終了キーが有効
        if (itr->GetReleaseKey())
        {
            // リソースより解放
            this->_3DXmodel()->ObjectDelete(itr->GetData());

            // イテレータの消去
            itr = this->m_param.erase(itr);
        }
        // 無効
        else
        {
            // 更新
            itr->Update();

            // デバッグにセット
            this->Debug_3DObject(itr->GetData());
            ++itr;
        }
    }
    this->_3DXmodel()->Update();
}

//==========================================================================
// 描画
void PlayerAttackEffect_A::Draw(void)
{
    if (this->m_param.size() != 0)
    {
        auto pDevice = this->GetDirectX9Device();
        this->SetRenderALPHAREF_END(pDevice);

        // 全加算合成
        this->SetRenderSUB(pDevice);
        this->SetRenderZWRITEENABLE_START(pDevice);
        this->_3DXmodel()->Draw();
        this->SetRenderADD(pDevice);
        this->SetRenderZWRITEENABLE_END(pDevice);
    }
}

//==========================================================================
// デバッグ
void PlayerAttackEffect_A::Debug(void)
{
    this->ImGui()->Text("Object数 : %d", this->m_param.size());
}

//==========================================================================
// 生成
void PlayerAttackEffect_A::Create(const mslib::DX9_3DObject *pObj, float speed, int limit_time)
{
    auto fRot = this->m_rotZ_rand(this->GetMt19937());

    this->m_param.emplace_back(pObj, speed, limit_time, fRot);
    auto itr = --this->m_param.end();
    this->_3DXmodel()->ObjectInput(itr->GetData());

    this->m_param.emplace_back(pObj, speed, limit_time, fRot);
    itr = --this->m_param.end();
    itr->GetData()->MoveX(0.2f);
    this->_3DXmodel()->ObjectInput(itr->GetData());

    this->m_param.emplace_back(pObj, speed, limit_time, fRot);
    itr = --this->m_param.end();
    itr->GetData()->MoveX(-0.2f);
    this->_3DXmodel()->ObjectInput(itr->GetData());
}

//==========================================================================
// オブジェクトのゲッター
const std::list<PlayerAttackEffect_A::EffectParam>* PlayerAttackEffect_A::GetEffectObject(void)
{
    return &this->m_param;
}

PlayerAttackEffect_A::EffectParam::EffectParam()
{
    this->m_obj.Init(0);
    this->m_MoveSpeed = 0.0f;
    this->m_limit_time = 0;
    this->m_time = -1;
    this->m_release_key = false;
}

PlayerAttackEffect_A::EffectParam::EffectParam(const mslib::DX9_3DObject *pObj, float speed, int limit_time, float rotZ)
{
    this->m_obj = *pObj;
    this->m_obj.SetIndex(0);
    this->m_obj.RotZ(rotZ);
    this->m_MoveSpeed = speed;
    this->m_limit_time = limit_time;
    this->m_time = -1;
    this->m_release_key = false;
}

PlayerAttackEffect_A::EffectParam::~EffectParam()
{
}

//==========================================================================
// 更新
void PlayerAttackEffect_A::EffectParam::Update(void)
{
    // ただの移動
    this->m_obj.MoveZ(this->m_MoveSpeed);

    // 生存check
    if (this->m_limit_time<this->m_time)
    {
        this->ReleaseKey();
    }

    // 生存時間カウンタ
    this->m_time++;
}
