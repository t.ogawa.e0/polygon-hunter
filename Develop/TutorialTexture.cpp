//==========================================================================
// チュートリアルテクスチャ[TutorialTexture.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "TutorialTexture.h"
#include "InputSystem.h"
#include "KeyboardObject.h"
#include "Screen.h" // デバッグ用
#include "resource_list.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int __MAX_TEXID = 6; // 最大のテクスチャ

TutorialTexture::TutorialTexture() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->SetObjectName("TutorialTexture");
    this->m_Index = 0;
}

TutorialTexture::~TutorialTexture()
{
}

//=========================================================================
// 初期化
bool TutorialTexture::Init(void)
{
    auto * pObj = this->_2DObject()->Create();
    pObj->Init(this->m_Index);

    this->_2DPolygon()->ObjectInput(pObj);
    this->_2DPolygon()->Init(RESOURCE_操作説明1_png, true);
    this->_2DPolygon()->Init(RESOURCE_操作説明2_png, true);
    this->_2DPolygon()->Init(RESOURCE_操作説明3_png, true);
    this->_2DPolygon()->Init(RESOURCE_操作説明4_png, true);
    this->_2DPolygon()->Init(RESOURCE_操作説明5_png, true);
    this->_2DPolygon()->Init(RESOURCE_操作説明6_png, true);
    this->_2DPolygon()->Init(RESOURCE_操作説明7_png, true);

    this->m_KeyboardObject = this->GetObjects(mslib::DX9_ObjectID::Default, "KeyboardObject");
    this->m_InputSystem = this->GetObjects(mslib::DX9_ObjectID::Default, "InputSystem");

    pObj->Init(this->m_Index);

    return false;
}

//=========================================================================
// 解放
void TutorialTexture::Uninit(void)
{
}

//=========================================================================
// 更新
void TutorialTexture::Update(void)
{
    auto * pKeyboardObject = (KeyboardObject*)this->m_KeyboardObject;
    auto * pInputSystem = (CInputSystem*)this->m_InputSystem;
    auto * pObj = this->_2DObject()->Get(0);

    if (mslib::nullptr_check(pInputSystem))
    {
        auto * pXinput = pInputSystem->GetXInput();
        if (pXinput->Check(0))
        {
            if (pXinput->Trigger(mslib::XInputButton::DPAD_LEFT, 0))
            {
                this->m_Index--;
                if (this->m_Index < 0)
                {
                    this->m_Index = 0;
                }
            }
            if (pXinput->Trigger(mslib::XInputButton::DPAD_RIGHT, 0))
            {
                this->m_Index++;
                if (__MAX_TEXID < this->m_Index)
                {
                    this->m_Index = __MAX_TEXID;
                }
            }
        }
    }

    if (mslib::nullptr_check(pKeyboardObject))
    {
        if (pKeyboardObject->KEY_A(KeyboardObject::KeyboardType::Trigger))
        {
            this->m_Index--;
            if (this->m_Index < 0)
            {
                this->m_Index = 0;
            }
        }
        if (pKeyboardObject->KEY_D(KeyboardObject::KeyboardType::Trigger))
        {
            this->m_Index++;
            if (__MAX_TEXID < this->m_Index)
            {
                this->m_Index = __MAX_TEXID;
            }
        }
        if (pKeyboardObject->KEY_SPACE(KeyboardObject::KeyboardType::Trigger))
        {
            CScreen::screenchange(SceneName::Game);
        }
    }

    pObj->SetIndex(this->m_Index);

    this->_2DPolygon()->Update();
}

//=========================================================================
// 描画
void TutorialTexture::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//=========================================================================
// デバッグ
void TutorialTexture::Debug(void)
{
}
