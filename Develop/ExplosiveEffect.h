//==========================================================================
// 爆発効果[ExplosiveEffect.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

class ExplosiveEffectData
{
public:
    ExplosiveEffectData();
    ExplosiveEffectData(const mslib::DX9_3DObject & obj);
    ~ExplosiveEffectData();

public:
    mslib::DX9_3DObject m_obj; // オブジェクト
    float m_limit_scale; // サイズ上限
    bool m_delete; // 解放判定
    bool m_collision; // 当たり判定
};

//==========================================================================
//
// class  : ExplosiveEffect
// Content: 爆発効果
//
//=========================================================================
class ExplosiveEffect : public mslib::DX9_Object
{
public:
    ExplosiveEffect();
    ~ExplosiveEffect();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
    // 生成
    void Create(const mslib::DX9_3DObject & obj);
    // 爆発オブジェクトへのアクセス
    std::list<ExplosiveEffectData> * GetExplosiveEffectData(void);
private:
    // サイズの変更
    void ScaleUpdate(void);
    // 消去処理
    void DeleteProcessing(void);
    // vectorセット
    void SetVector(void);
private:
    std::list<ExplosiveEffectData> m_effect; // エフェクト 
};

