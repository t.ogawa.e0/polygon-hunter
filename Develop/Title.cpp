//==========================================================================
// タイトルシーン[Title.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Screen.h" // デバッグ用

#include "InputSystem.h"
#include "KeyboardObject.h"

#include "Title.h"
#include "Title_TitleName.h"
#include "Title_Camera.h"
#include "TitleCharacter.h"
#include "TitleBackground.h"
#include "TitlePressKey.h"
#include "UserInformation.h"
#include "TitleManager.h"

CTitle::CTitle()
{
    Title_TitleName * pTitle_TitleName = nullptr;
    Title_Camera * pTitle_Camera = nullptr;
    TitleCharacter * pTitleCharacter = nullptr;
    TitleBackground * pTitleBackground = nullptr;
    TitlePressKey * pTitlePressKey = nullptr;
    KeyboardObject * pKeyboardObject = nullptr;
    CInputSystem * InputSystem = nullptr;
    UserInformation * pUserInformation = nullptr;
    TitleManager * pTitleManager = nullptr;

    //==========================================================================
    // ファイル指定
    //==========================================================================
    mslib::DX9_Object::SetFilePass("resource/data/level0.bin");

    //==========================================================================
    // システム
    //==========================================================================
    mslib::DX9_Object::NewObject(pTitleManager);
    mslib::DX9_Object::NewObject(pUserInformation);
    mslib::DX9_Object::NewObject(InputSystem);
    mslib::DX9_Object::NewObject(pKeyboardObject);

    //==========================================================================
    // 2D
    //==========================================================================
    mslib::DX9_Object::NewObject(pTitle_TitleName);
    mslib::DX9_Object::NewObject(pTitlePressKey);

    //==========================================================================
    // 3D
    //==========================================================================
    mslib::DX9_Object::NewObject(pTitle_Camera);
    mslib::DX9_Object::NewObject(pTitleCharacter);
    mslib::DX9_Object::NewObject(pTitleBackground);
}

CTitle::~CTitle()
{
}

//==========================================================================
// 初期化
bool CTitle::Init(void)
{
    return mslib::DX9_Object::InitAll();
}

//==========================================================================
// 解放
void CTitle::Uninit(void)
{
    mslib::DX9_Object::ReleaseAll();
}

//==========================================================================
// 更新
void CTitle::Update(void)
{
#if defined(_SETImGui_Dx9_) || defined(_DEBUG) || defined(DEBUG)
    auto *pKeyboard = mslib::DX9_DeviceManager::GetKeyboard();

    if (pKeyboard->Trigger(mslib::DirectInputKeyboardButton::KEY_F1))
    {
        CScreen::screenchange(SceneName::Tutorial);
    }
#endif

    mslib::DX9_Object::UpdateAll();
}

//==========================================================================
// 描画
void CTitle::Draw(void)
{
    mslib::DX9_Object::DrawAll();
}
