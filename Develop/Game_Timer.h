//==========================================================================
// タイマー[Game_Timer.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "PlayObject.h"

//==========================================================================
//
// class  : CGame_Timer
// Content: タイマー
//
//=========================================================================
class CGame_Timer : public mslib::DX9_Object, public PlayObject
{
private:
    enum class ETimer
    {
        bigin = 0,
        count1 = bigin, // 分
        count2, // 秒
        end,
    };
public:
    CGame_Timer();
    ~CGame_Timer();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    // タイマー取得
    int GetTime(void);
    // タイマー取得
    int GetMaxTime(void);
    // クリア時間の取得
    mslib::Timer * GetClearMinute(void);
    // クリア秒数の取得
    mslib::Timer * GetClearSeconds(void);
private:
    int m_time;
    mslib::Timer m_minute; // クリア時分
    mslib::Timer m_seconds; // クリア時秒
};
