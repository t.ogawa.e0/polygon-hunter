//==========================================================================
// 大型ボスLargeBoss.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "EnemyLargeBoss.h"
#include "GameField.h"
#include "Gravity.h"
#include "PlayerObject.h"
#include "LinearPolygon.h"
#include "EnclosurePolygon.h"
#include "resource_list.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr float __SearchRange = 60.0f;
constexpr float __move_speed = 0.05f;
constexpr int __max_life = 10000;

EnemyLargeBoss::EnemyLargeBoss() : CharacterObject(mslib::DX9_ObjectID::Cube)
{
    this->SetObjectName("EnemyLargeBoss");
    this->m_Field = nullptr;
    this->m_gravity = nullptr;
    this->m_PlayerObject = nullptr;
    this->m_EnclosurePolygon = nullptr;
    this->m_LinearPolygon = nullptr;
    this->m_EnclosurePolygon = nullptr;
    auto & __rand = *this->_rand_float()->Create();
    __rand = mslib::rand_float(-40.0f, 40.0f);
}

EnemyLargeBoss::~EnemyLargeBoss()
{
    this->Uninit();
}

//==========================================================================
// 初期化
bool EnemyLargeBoss::Init(void)
{
    this->m_gravity = this->GetObjects(mslib::DX9_ObjectID::Default, "Gravity");
    this->m_Field = this->GetObjects(mslib::DX9_ObjectID::Field, "GameField");
    this->m_PlayerObject = this->GetObjects(mslib::DX9_ObjectID::Cube, "PlayerObject");
    this->m_LinearPolygon = this->GetObjects(mslib::DX9_ObjectID::Cube, "LinearPolygon");
    this->m_EnclosurePolygon = this->GetObjects(mslib::DX9_ObjectID::Cube, "EnclosurePolygon");

    return this->_3DCube()->Init(RESOURCE_enemy_2_box_png);
}

//==========================================================================
// 解放
void EnemyLargeBoss::Uninit(void)
{
}

//==========================================================================
// 更新
void EnemyLargeBoss::Update(void)
{
    // 解放処理
    this->DeleteObject();

    // 古い座標を登録
    this->OldPosSet();

    // 大脳
    this->Cerebrum();

    // アクション
    this->Action();

    // フィールドとのコリジョン
    this->FieldCollision();

    // ベクトルのセット
    this->SetVector();

    this->_3DCube()->Update();
}

//==========================================================================
// 描画
void EnemyLargeBoss::Draw(void)
{
    this->_3DCube()->Draw();
}

//==========================================================================
// デバッグ
void EnemyLargeBoss::Debug(void)
{
}

//==========================================================================
// 生成
void EnemyLargeBoss::Create(void)
{
    auto & __rand = *this->_rand_float()->Get(0);
    this->m_data.emplace_back();
    auto itr = --this->m_data.end();

    itr->Get3DObject()->Init(0);
    itr->Get3DObject()->ScalePlus(10);
    itr->Get3DObject()->MoveZ(__rand(this->GetMt19937()));
    itr->Get3DObject()->MoveX(__rand(this->GetMt19937()));
    itr->Get3DObject()->RotX(this->m_float_rand(this->GetMt19937()));
    itr->m_fil_pos = *itr->Get3DObject()->GetMatInfoPos();
    itr->m_timer.Init(5, 60);
    itr->m_old_obj = *itr->Get3DObject();
    itr->Get3DObject()->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);
    itr->m_SearchRangeCenter.Init(0);
    itr->m_SearchRangeCenter.ScalePlus(__SearchRange);
    itr->m_Life = __max_life;
    itr->m_rand_int = mslib::rand_int(0, 100);
    this->_3DCube()->ObjectInput(itr->Get3DObject());

    if (!mslib::nullptr_check(this->m_gravity))
    {
        this->m_gravity = this->GetObjects(mslib::DX9_ObjectID::Default, "Gravity");
    }
    if (mslib::nullptr_check(this->m_gravity))
    {
        auto * pGra = (CGravity*)this->m_gravity;
        pGra->SetGravity(itr->Get3DObject(), &itr->m_fil_pos);
    }
}

//==========================================================================
// 大脳
void EnemyLargeBoss::Cerebrum(void)
{
    // 索敵
    this->SearchEnemies();

    if (this->GetPlayKey())
    {
        // 認識
        this->Recognition();
    }
}

//==========================================================================
// 索敵
void EnemyLargeBoss::SearchEnemies(void)
{
    for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
    {
        if (itr->m_recognition == false && itr->m_vigilance == false)
        {
            //==========================================================================
            // 索敵範囲の思考
            //==========================================================================

            // 探索範囲外になると向きの再設定を行う
            if (!itr->m_collision.Ball(*itr->Get3DObject(), itr->m_SearchRangeCenter, __SearchRange*__SearchRange))
            {
                itr->Get3DObject()->MoveZ(-__move_speed);
                itr->Get3DObject()->RotX(this->m_float_rand(this->GetMt19937()));
            }
        }
    }
}

//==========================================================================
// 認識
void EnemyLargeBoss::Recognition(void)
{
    auto * pPlayer = (CPlayerObject*)this->m_PlayerObject;

    if (mslib::nullptr_check(pPlayer))
    {
        auto * pObj = pPlayer->_3DObject()->Get((int)CPlayerObject::ObjectType::Visualization);
        if (mslib::nullptr_check(pObj))
        {
            for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
            {
                if (itr->m_collision.Ball(*itr->Get3DObject(), *pObj, __SearchRange*(__SearchRange/2)))
                {
                    itr->Get3DObject()->LockOn(*pObj, 0.05f);
                    itr->m_recognition = true;
                }
                else
                {
                    itr->m_recognition = false;
                }
            }
        }
    }

}

//==========================================================================
// フィールドとのコリジョン
void EnemyLargeBoss::FieldCollision(void)
{
    auto * p_fil = (CGameField*)this->m_Field;
    if (mslib::nullptr_check(p_fil))
    {
        for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
        {
            if (*itr->Get3DObject()->GetMatInfoPos() != *itr->m_old_obj.GetMatInfoPos())
            {
                auto obj_pos = itr->Get3DObject()->GetMatInfoPos();
                itr->m_fil_pos = p_fil->FieldHeight(0, *obj_pos, itr->m_HitArea);
            }
            if ((itr->m_fil_pos.x == 0.0f) && (itr->m_fil_pos.y == 0.0f) && (itr->m_fil_pos.z == 0.0f))
            {
                itr->ProcessAtDecision();
                itr->Get3DObject()->RotX(this->m_float_rand(this->GetMt19937()));
            }
        }
    }
}

//==========================================================================
// ジャンプ
void EnemyLargeBoss::Jump(void)
{
    for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
    {
        bool jump = true;
        auto obj_pos = itr->Get3DObject()->GetMatInfoPos();
        float f_powor = itr->m_Jump._Jump(itr->m_fil_pos.y, obj_pos->y, 1.0f, jump);
        itr->Get3DObject()->MoveY(f_powor);
    }
}

//==========================================================================
// 移動
void EnemyLargeBoss::Move(void)
{
    for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
    {
        itr->Get3DObject()->MoveZ(__move_speed);
    }
}

//==========================================================================
// ステップ
bool EnemyLargeBoss::Step(void)
{
    return false;
}

//==========================================================================
// アクション
void EnemyLargeBoss::Action(void)
{
    if (this->GetPlayKey())
    {
        for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
        {
            if (itr->m_timer.Countdown())
            {
                auto * pLinearPolygon = (LinearPolygon*)this->m_LinearPolygon;
                auto * pEnclosurePolygon = (EnclosurePolygon*)this->m_EnclosurePolygon;
                auto * pPlayerObject = (CPlayerObject*)this->m_PlayerObject;
                if (mslib::nullptr_check(pLinearPolygon) && mslib::nullptr_check(pPlayerObject) && mslib::nullptr_check(pEnclosurePolygon))
                {
                    auto *pObj = pPlayerObject->_3DObject()->Get((int)CPlayerObject::ObjectType::Visualization);

                    if (mslib::nullptr_check(pObj))
                    {
                        auto rand_int = itr->m_rand_int(this->GetMt19937());
                        if (0 <= rand_int && rand_int <= 50)
                        {
                            pLinearPolygon->Create(*pObj, *itr->Get3DObject());
                        }
                        else if (51 <= rand_int && rand_int <= 100)
                        {
                            pEnclosurePolygon->Create(*pObj, *itr->Get3DObject());
                        }
                    }
                }
                itr->m_timer.Reset();
            }
        }
    }
    this->Move();
    this->Step();
}

//==========================================================================
// 消滅処理
void EnemyLargeBoss::DeleteObject(void)
{
    for (auto itr = this->m_data.begin(); itr != this->m_data.end(); )
    {
        if (itr->GetDeleteKey())
        {
            auto * pGra = (CGravity*)this->m_gravity;
            if (mslib::nullptr_check(pGra))
            {
                // 重力処理からオブジェクトを解放する
                pGra->DeleteGravity(itr->Get3DObject());
            }
            // 描画機能からオブジェクトを解放する
            this->_3DCube()->ObjectDelete(itr->Get3DObject());
            // オブジェクトを解放
            itr = this->m_data.erase(itr);
        }
        else
        {
            ++itr;
        }
    }
}

//==========================================================================
// ベクトルセット
void EnemyLargeBoss::SetVector(void)
{
    for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
    {
        this->Debug_3DObject(itr->Get3DObject());
        this->Debug_3DObject(&itr->m_old_obj);
        this->Debug_3DObject(&itr->m_SearchRangeCenter);
    }
}

//==========================================================================
// 古い座標のセット
void EnemyLargeBoss::OldPosSet(void)
{
    for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
    {
        itr->m_old_obj.SetMatInfoPos(*itr->Get3DObject()->GetMatInfoPos());
    }
}
