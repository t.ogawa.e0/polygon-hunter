//==========================================================================
// バレットオブジェクト[BulletObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "BulletObject.h"
#include "BulletNormalEffectObject.h"

BulletObjectStruct::BulletObjectStruct()
{
    this->m_speed = 0.0f;
    this->m_rot = 0.0f;
    this->m_survival_time_limit = 0;
    this->m_survival_time = 0;
}

BulletObjectStruct::BulletObjectStruct(const mslib::DX9_3DObject * pobj, const mslib::CVector3<float> & speed, const mslib::CVector3<float> & rot, int limit_time, int label)
{
    *this->m_obj = *pobj;
    this->m_obj->SetIndex(label);
    this->m_speed = speed;
    this->m_rot = rot;
    this->m_survival_time_limit = limit_time;
    this->m_survival_time = 0;
}

BulletObjectStruct::~BulletObjectStruct()
{
}

//==========================================================================
// 更新
void BulletObjectStruct::Update(void)
{
    if (!this->m_delete2)
    {
        if (this->m_rot != 0.0f)
        {
            this->m_obj->RotZ(this->m_rot.z);
            this->m_obj->RotX(this->m_rot.x);
            this->m_obj->RotY(this->m_rot.y);
        }
        this->m_obj->MoveZ(this->m_speed.z);
        this->m_obj->MoveX(this->m_speed.x);
        this->m_obj->MoveY(this->m_speed.y);
        this->m_obj->UpdateAnimationCount();
        this->m_survival_time++;
        if (this->m_survival_time_limit<this->m_survival_time)
        {
            this->m_delete2 = true;
        }
    }
}

//==========================================================================
// 移動力の取得
const mslib::CVector3<float>* BulletObjectStruct::GetSpeed(void)
{
    return &this->m_speed;
}

//==========================================================================
// 回転力の取得
const mslib::CVector3<float>* BulletObjectStruct::GetRot(void)
{
    return &this->m_rot;
}

//==========================================================================
// 生存時間の取得
int BulletObjectStruct::GetSurvivalTime(void)
{
    return this->m_survival_time;
}

BulletInputSystem::BulletInputSystem()
{
}

BulletInputSystem::~BulletInputSystem()
{
    this->m_bullet_data.clear();
}

std::list<BulletObjectStruct>* BulletInputSystem::GetBulletObjectStruct(void)
{
    return &this->m_bullet_data;
}

BulletObject::BulletObject()
{
    this->SetObjectName("BulletObject");
}

BulletObject::~BulletObject()
{
    this->m_object_data.clear();
}

//==========================================================================
// 初期化
bool BulletObject::Init(void)
{
    auto *pBulletNormalEffectObject = (BulletNormalEffectObject*)this->GetObjects(mslib::DX9_ObjectID::Billboard, "BulletNormalEffectObject");

    if (mslib::nullptr_check(pBulletNormalEffectObject))
    {
        this->m_object_data.push_back(pBulletNormalEffectObject->GetBulletObjectStruct());
    }

    return false;
}

//==========================================================================
// 解放
void BulletObject::Uninit(void)
{
}
//==========================================================================

// 更新
void BulletObject::Update(void)
{
}

//==========================================================================
// 描画
void BulletObject::Draw(void)
{
}

//==========================================================================
// デバッグ
void BulletObject::Debug(void)
{
}

//==========================================================================
// バレットデータ
std::list<std::list<BulletObjectStruct>*>* BulletObject::GetBulletInputSystem(void)
{
    return &this->m_object_data;
}
