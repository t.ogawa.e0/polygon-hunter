//==========================================================================
// タイトルのキャラクター [TitleCharacter.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : TitleCharacter
// Content: タイトルのキャラクター 
//
//=========================================================================
class TitleCharacter : public mslib::DX9_Object
{
public:
    TitleCharacter();
    ~TitleCharacter();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
};

