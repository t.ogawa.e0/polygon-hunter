//==========================================================================
// ダメージ箱[DamageBox.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DamageBox.h"
#include "resource_list.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int __Limit_time = 100; // 生存時間

DamageBox::DamageBox() : DX9_Object(mslib::DX9_ObjectID::Cube)
{
    this->SetObjectName("DamageBox");
}

DamageBox::~DamageBox()
{
    this->m_list.clear();
}

//=========================================================================
// 初期化
bool DamageBox::Init(void)
{
    return this->_3DCube()->Init(nullptr);
}

//=========================================================================
// 解放
void DamageBox::Uninit(void)
{
}

//=========================================================================
// 更新
void DamageBox::Update(void)
{
    // 処理
    for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr)
    {
        itr->Get3DObject()->MoveZ(0.05f);
        itr->m_time--;
        if (itr->m_time < 0)
        {
            itr->DeleteKeyON2();
        }
    }

    // 解放処理
    for (auto itr = this->m_list.begin(); itr != this->m_list.end(); )
    {
        if (itr->GetDeleteKey())
        {
            this->_3DCube()->ObjectDelete(itr->Get3DObject());
            itr = this->m_list.erase(itr);
        }
        else if (itr->GetDeleteKey2())
        {
            this->_3DCube()->ObjectDelete(itr->Get3DObject());
            itr = this->m_list.erase(itr);
        }
        else
        {
            ++itr;
        }
    }

    this->_3DCube()->Update();
}

//=========================================================================
// 描画
void DamageBox::Draw(void)
{
    this->_3DCube()->Draw();
}

//=========================================================================
// デバッグ
void DamageBox::Debug(void)
{
}

//=========================================================================
// 生成
void DamageBox::Create(const mslib::DX9_3DObject & obj)
{
    this->m_list.emplace_back();
    auto itr = --this->m_list.end();

    *itr->Get3DObject() = obj;
    itr->m_time = __Limit_time;
    this->_3DCube()->ObjectInput(itr->Get3DObject());
}

//=========================================================================
// ダメージポリゴンの取得
std::list<DamageBoxParam>* DamageBox::GetDamageBoxParam(void)
{
    return &this->m_list;
}

DamageBoxParam::DamageBoxParam()
{
}

DamageBoxParam::~DamageBoxParam()
{
}
