//==========================================================================
// ゲームカメラ[GameCamera.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGameCamera
// Content: ゲームのカメラ
//
//=========================================================================
class CGameCamera : public mslib::DX9_Object
{
public:
    enum class CameraObjectID
    {
        LockOnObject,
        EyeObject,
    };
public:
    CGameCamera() : DX9_Object(mslib::DX9_ObjectID::Camera) {
        this->SetObjectName("GameCamera");
        this->m_input = nullptr;
        this->m_KeyboardObject = nullptr;
    }
    ~CGameCamera() {}
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    // ターゲットをセット
    void SetTarget(int ID, mslib::DX9_3DObject * Input, D3DXVECTOR3 move_pos);
private:
    void * m_input; // インプットオブジェクトへのアクセス
    void * m_KeyboardObject;
};