//==========================================================================
// リザルト仮置きオブジェクト[Result_TemporaryPlace.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CResult_TemporaryPlace
// Content: リザルト仮置きオブジェクト
//
//=========================================================================
class CResult_TemporaryPlace : public mslib::DX9_Object
{
public:
    CResult_TemporaryPlace();
    ~CResult_TemporaryPlace();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
};

