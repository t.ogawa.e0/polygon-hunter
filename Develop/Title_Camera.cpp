//==========================================================================
// タイトルのカメラ[Title_Camera.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Title_Camera.h"

Title_Camera::Title_Camera() : DX9_Object(mslib::DX9_ObjectID::Camera)
{
    this->SetObjectName("Title_Camera");
}

Title_Camera::~Title_Camera()
{
}

//=========================================================================
// 初期化
bool Title_Camera::Init(void)
{
    D3DXVECTOR3 Eye = D3DXVECTOR3(0.0f, 1.0f, -5.0f); // 注視点
    D3DXVECTOR3 At = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // カメラ座標

    this->Camera()->Create()->Init(Eye, At);
    this->Light()->Init({ 0.0f,-1.0f,1.0f });
    return false;
}

//=========================================================================
// 解放
void Title_Camera::Uninit(void)
{
}

//=========================================================================
// 更新
void Title_Camera::Update(void)
{
    this->Camera()->Get(0)->Update(this->GetWinSize(), this->GetDirectX9Device());
}

//=========================================================================
// 描画
void Title_Camera::Draw(void)
{
}

//=========================================================================
// デバッグ
void Title_Camera::Debug(void)
{
}
