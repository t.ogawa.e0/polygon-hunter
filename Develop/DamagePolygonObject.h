//==========================================================================
// ダメージポリゴン[DamagePolygonObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "DamageObject.h"

//==========================================================================
//
// class  : DamagePolygonData
// Content: ダメージポリゴン構成データ
//
//=========================================================================
class DamagePolygonData 
{
private:
    struct Data : public DamageObjectParam {
        mslib::DX9_3DObject m_obj_plus; // オブジェクト
    };
public:
    DamagePolygonData() {
        this->m_target = nullptr;
        this->m_singular_point = nullptr;
        this->m_ready = false;
        this->m_delete = false;
    }
    DamagePolygonData(mslib::DX9_3DObject & obj, mslib::DX9_3DObject & singular_point) {
        this->m_target = &obj;
        this->m_singular_point = &singular_point;
        this->m_ready = false;
        this->m_delete = false;
    }
    ~DamagePolygonData() {
        this->m_obj.clear();
    }
public:
    std::list<Data> m_obj; // ポリゴングループ
    mslib::DX9_3DObject * m_target; // ターゲット
    mslib::DX9_3DObject * m_singular_point; // 特異点
    mslib::Timer m_create_timer; // クリエイトタイマー
    mslib::Timer m_processing_timer; // 処理タイマー
    bool m_ready; // 準備完了フラグ
    bool m_delete; // deleteフラグ
};

//==========================================================================
//
// class  : DamagePolygonInputSystem
// Content: DamagePolygonInputSystem
//
//=========================================================================
class DamagePolygonInputSystem
{
public:
    DamagePolygonInputSystem();
    virtual ~DamagePolygonInputSystem();
    // 攻撃データの取得
    std::list<DamagePolygonData> * GetDamagePolygonData(void);
    // 生成
    virtual void Create(mslib::DX9_3DObject & target, mslib::DX9_3DObject & singular_point) = 0;
protected:
    std::list<DamagePolygonData>m_param; // 攻撃データ
    void * m_ExplosiveEffect; // 爆発エフェクト
};

//==========================================================================
//
// class  : DamagePolygonObject
// Content: ダメージポリゴン
//
//=========================================================================
class DamagePolygonObject : public mslib::DX9_Object
{
public:
    DamagePolygonObject();
    ~DamagePolygonObject();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;

    std::list<std::list<DamagePolygonData>*> * GetDamagePolygonData(void);
private:
    std::list<std::list<DamagePolygonData>*>m_object_data;
};

