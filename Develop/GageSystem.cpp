//==========================================================================
// ゲージシステム[GageSystem.h]
// author: tatsuya ogawa
//==========================================================================
#include "GageSystem.h"

GageSystem::GageSystem()
{
    this->m_Lock = false;
}


GageSystem::~GageSystem()
{
}

//==========================================================================
// Input 
void GageSystem::Input(mslib::CVector2<float> max_size)
{
    this->m_max_gage_size = max_size;
}

//==========================================================================
// InputMaxParam
void GageSystem::InputMaxParam(int max_param)
{
    if (this->m_Lock == false)
    {
        this->m_buff = this->m_max_gage_size / (float)max_param;
        this->m_Lock = true;
    }
}

//==========================================================================
// Width Update
float GageSystem::UpdateW(int param)
{
    return this->m_buff.x*(float)param;
}

//==========================================================================
// Height Update
float GageSystem::UpdateH(int param)
{
    return this->m_buff.y*(float)param;
}

//==========================================================================
// MAX Gage
const mslib::CVector2<float>& GageSystem::GetMaxSize(void)
{
    return this->m_max_gage_size;
}

//==========================================================================
// ゲージの基本サイズ操作のロック解除
void GageSystem::Unlock(void)
{
    this->m_Lock = false;
}

//==========================================================================
// 割合サイズ
const mslib::CVector2<float>& GageSystem::GetBuffSize(void)
{
    return this->m_buff;
}
