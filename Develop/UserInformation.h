//==========================================================================
// ユーザー情報[UserInformation.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : UserInformationData
// Content: ユーザー情報
//
//=========================================================================
class UserInformationData
{
public:
    UserInformationData();
    ~UserInformationData();
    //// 演算子オーバーロードで比較関数を定義
    //bool operator<(const UserInformationData& right) const {
    //    return this->m_time < right.m_time ? true : false;
    //}
    // 演算子オーバーロードで比較関数を定義
    bool operator<(const UserInformationData& right) const {
        return this->m_time < right.m_time ? true : false;
    }
    // 算出
    void Calculation(void);
public:
    std::list<int> m_user_name; // ユーザーネーム
    std::list<int> m_password; // パスワード
    mslib::Timer m_minute; // クリア時残り分
    mslib::Timer m_seconds; // クリア時残り秒
    int m_time; // 時間
    int m_rank; // 順位
    bool m_JudgmentOfSuccess; // クリア判定
};

//==========================================================================
//
// class  : UserInformation
// Content: ユーザー情報
//
//=========================================================================
class UserInformation : public mslib::DX9_Object
{
public:
    UserInformation();
    ~UserInformation();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
    // データの登録
    void InputData(const UserInformationData & data);
    // 新規データの一時保存
    void InputNewData(const UserInformationData & data);

    // 新規データの呼び出し
    UserInformationData LoadNewData(void);
private:
    // 読み取り専用
    void Load(const std::string & file);
    // 書き込み専用
    void Save(const std::string & file);
private:
    std::list<UserInformationData> m_list; // データ
    UserInformationData m_data; // データ
};
