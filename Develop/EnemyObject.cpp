//==========================================================================
// エネミーオブジェクト[EnemyObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "EnemyObject.h"
#include "EnemySmallSize.h"
#include "EnemyLargeBoss.h"

CEnemyObject::CEnemyObject()
{
    this->SetObjectName("EnemyObject");
}

CEnemyObject::~CEnemyObject()
{
    this->m_object_data.clear();
}

//=========================================================================
// 初期化
bool CEnemyObject::Init(void)
{
    auto *pEnemySmallSize = (EnemySmallSize*)this->GetObjects(mslib::DX9_ObjectID::Cube, "EnemySmallSize");
    auto *pEnemyLargeBoss = (EnemyLargeBoss*)this->GetObjects(mslib::DX9_ObjectID::Cube, "EnemyLargeBoss");

    if (mslib::nullptr_check(pEnemySmallSize))
    {
        this->m_object_data[MyEnumClass::Small] = pEnemySmallSize->GetEnemyParam();
    }
    if (mslib::nullptr_check(pEnemyLargeBoss))
    {
        this->m_object_data[MyEnumClass::Boss] = pEnemyLargeBoss->GetEnemyParam();
    }

    return false;
}

//=========================================================================
// 解放
void CEnemyObject::Uninit(void)
{
}

//=========================================================================
// 更新
void CEnemyObject::Update(void)
{
}

//=========================================================================
// 描画
void CEnemyObject::Draw(void)
{
}

//=========================================================================
// デバッグ
void CEnemyObject::Debug(void)
{
}

std::unordered_map<CEnemyObject::MyEnumClass, std::list<EnemyParam>*> *CEnemyObject::GetEnemyObjectPos(void)
{
    return &this->m_object_data;
}

EnemyParam::EnemyParam()
{
    this->m_old_obj.Init(0);
    this->m_HitArea = 0;
    this->m_fil_pos = D3DXVECTOR3(0, 0, 0);
    this->m_recognition = false;
    this->m_vigilance = false;
    this->m_Life = 0;
    this->m_rand_int = mslib::rand_int(0, 1);
}

EnemyParam::~EnemyParam()
{
}

//=========================================================================
// 判定時の処理
void EnemyParam::ProcessAtDecision(void)
{
    auto *vec1 = this->m_old_obj.GetMatInfoPos();
    auto *vec2 = this->m_obj->GetMatInfoPos();
    this->m_obj->SetMatInfoPos({ vec1->x,vec2->y,vec1->z });
}

EnemyInputSystem::EnemyInputSystem()
{
    this->m_float_rand = mslib::rand_float(mslib::angle_list::angle_0, mslib::angle_list::angle_180 + mslib::angle_list::angle_180);
}

EnemyInputSystem::~EnemyInputSystem()
{
    this->m_data.clear();
}

std::list<EnemyParam>* EnemyInputSystem::GetEnemyParam(void)
{
    return &this->m_data;
}

//=========================================================================
// サイズ
int EnemyInputSystem::Size(void)
{
    return this->m_data.size();
}
