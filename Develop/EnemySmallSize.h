//==========================================================================
// 雑魚エネミー[EnemySmallSize.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "EnemyObject.h"
#include "PlayObject.h"

//==========================================================================
//
// class  : EnemySmallSize
// Content: 雑魚エネミー
//
//=========================================================================
class EnemySmallSize : public CharacterObject, public EnemyInputSystem, public PlayObject
{
public:
    EnemySmallSize();
    ~EnemySmallSize();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    // 生成
    void Create(void)override;
private:
    // 大脳
    void Cerebrum(void);
    // 索敵
    void SearchEnemies(void);
    // 認識
    void Recognition(void);
    // フィールドとのコリジョン
    void FieldCollision(void)override;
    // 古い座標のセット
    void OldPosSet(void);
    // ジャンプ
    void Jump(void)override;
    // 移動
    void Move(void)override;
    // ステップ
    bool Step(void)override;
    // アクション
    void Action(void)override;
    // 消滅処理
    void DeleteObject(void);
    // ベクトルセット
    void SetVector(void);
private:
    void * m_Field; // フィールドへのアクセス権
    void * m_gravity; // 重力へのアクセス
    void * m_player; // プレイヤー
    void * m_DamageBox; // ダメージボックス
};
