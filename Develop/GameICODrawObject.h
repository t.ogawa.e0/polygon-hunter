//==========================================================================
// ゲーム画面に表示するアイコンオブジェクト[GameICODrawObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// enum class : GameICODrawSystemID
// Content    : GameICODrawSystemID
//
//=========================================================================
enum class GameICODrawSystemID
{
    begin = -1,
    AttackSpeed, // 攻撃速度
    MoveSpeed, // 移動速度
    HPRecovery, // HP自動回復
    MPRecovery, // MP自動回復
    SPRecovery, // SP自動回復
    SuperArmor, // スーパーアーマー
    end,
};

//==========================================================================
//
// class  : GameICODrawSystemObject
// Content: GameICODrawSystemObject
//
//=========================================================================
class GameICODrawSystemObject
{
public:
    GameICODrawSystemObject();
    GameICODrawSystemObject(int level, mslib::DX9_2DObject * obj, GameICODrawSystemID id);
    ~GameICODrawSystemObject();

    // ICOIDの取得
    GameICODrawSystemID GetICOID(void);
public:
    int m_sys_level; // 制御レベル
    bool m_sys_draw_key; // 描画キー
    bool m_input_flag; // 入力判定
    GameICODrawSystemID m_ID; // バフID
    mslib::DX9_2DObject * m_obj; // オブジェクト
};

//==========================================================================
//
// class  : GameICOBuff
// Content: GameICOBuff
//
//=========================================================================
class GameICOBuff
{
public:
    GameICOBuff();
    GameICOBuff(bool key, float value);
    ~GameICOBuff();
public:
    bool m_key; // 有効キー
    float m_buff; // バフ
};

//==========================================================================
//
// class  : GameICODrawObject
// Content: GameICODrawObject
//
//=========================================================================
class GameICODrawObject : public mslib::DX9_Object
{
public:
    GameICODrawObject();
    ~GameICODrawObject();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    // バフデータの取得
    const GameICOBuff * GetBuffData(GameICODrawSystemID id);
private:
    std::unordered_map<GameICODrawSystemID, GameICOBuff> m_Buff; // バフの倍率box
    void * m_burst_gage;
    std::list<GameICODrawSystemObject> m_system_level;
};

