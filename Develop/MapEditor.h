//==========================================================================
// マップエディタ試作品[kadai.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"

//==========================================================================
//
// class  : CMapEditor
// Content: マップエディタ試作品
//
//==========================================================================
class CMapEditor : public CBaseScene
{
public:
    CMapEditor();
    ~CMapEditor();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
private:

};

//==========================================================================
//
// class  : CXGrid
// Content: グリッド
//
//==========================================================================
class CXGrid : public mslib::DX9_Object
{
public:
    CXGrid() : DX9_Object(mslib::DX9_ObjectID::Grid) {
        this->SetObjectName("CXGrid");
    }
    ~CXGrid() {}
    bool Init(void)override;
    void Uninit(void)override;
    void Update(void)override;
    void Draw(void)override;
private:

};

//==========================================================================
//
// class  : CXCamera
// Content: カメラ
//
//==========================================================================
class CXCamera : public mslib::DX9_Object
{
public:
    CXCamera() : DX9_Object(mslib::DX9_ObjectID::Camera) {
        this->SetObjectName("CXCamera");
        this->m_cameraKey = false;
    }
    ~CXCamera() {}
    bool Init(void)override;
    void Uninit(void)override;
    void Update(void)override;
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    const mslib::CVector3<float> * GetPos(void) {
        return &this->m_pos;
    }
private:
    bool m_cameraKey;
    mslib::CVector3<float>m_pos; // カメラの座標
};

//==========================================================================
//
// class  : CPlayer
// Content: プレイヤー
//
//==========================================================================
class CPlayer : public mslib::DX9_Object
{
public:
    CPlayer() : DX9_Object(mslib::DX9_ObjectID::Xmodel) {
        this->m_Field = nullptr;
        this->m_HitArea = 0;
        this->SetObjectName("CPlayer");
    }
    ~CPlayer() {}
    bool Init(void)override;
    void Uninit(void)override;
    void Update(void)override;
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:
    void * m_Field; // フィールドへのアクセスルート
    int m_HitArea; // 当たり判定のエリア
};

//==========================================================================
//
// class  : CField
// Content: フィールド
//
//==========================================================================
class CField : public mslib::DX9_Object
{
    enum class OBje
    {
        mesh,
        Begin,
    };
public:
    CField() : DX9_Object(mslib::DX9_ObjectID::Field) {
        this->SetObjectName("CField");
    }
    ~CField() {}
    bool Init(void)override;
    void Uninit(void)override;
    void Update(void)override;
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:
    void ObjeRelease(void) {
        // 描画オブジェクト以外を破棄
        for (OBje i = OBje::Begin; i != (OBje)this->_3DObject()->Size(); )
        {
            this->_3DObject()->PinpointRelease(this->_3DObject()->Get((int)i));
        }
    }
private:
    mslib::vector_wrapper<float> m_Map120x120_Y;
};

//==========================================================================
//
// class  : CSky
// Content: スカイドーム
//
//==========================================================================
class CSky : public mslib::DX9_Object
{
public:
    CSky() : DX9_Object(mslib::DX9_ObjectID::Sphere) {
        this->m_Camera = nullptr;
        this->SetObjectName("CSky");
    }
    ~CSky() {}
    bool Init(void)override;
    void Uninit(void)override;
    void Update(void)override;
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:
    void * m_Camera; // カメラへのアクセスルート
};
