//==========================================================================
// チュートリアル[Tutorial.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Tutorial.h"
#include "Screen.h" // デバッグ用
#include "TutorialTexture.h"
#include "InputSystem.h"
#include "KeyboardObject.h"

Tutorial::Tutorial()
{
    TutorialTexture * pTutorialTexture = nullptr;
    KeyboardObject * pKeyboardObject = nullptr;
    CInputSystem * InputSystem = nullptr;

    //==========================================================================
    // ファイル指定
    //==========================================================================
    mslib::DX9_Object::SetFilePass("resource/data/level3.bin");

    //==========================================================================
    // システム
    //==========================================================================
    mslib::DX9_Object::NewObject(InputSystem);
    mslib::DX9_Object::NewObject(pKeyboardObject);

    //==========================================================================
    // 2D
    //==========================================================================
    mslib::DX9_Object::NewObject(pTutorialTexture);
}

Tutorial::~Tutorial()
{
}

//==========================================================================
// 初期化
bool Tutorial::Init(void)
{
    return mslib::DX9_Object::InitAll();
}

//==========================================================================
// 解放
void Tutorial::Uninit(void)
{
    mslib::DX9_Object::ReleaseAll();
}

//==========================================================================
// 更新
void Tutorial::Update(void)
{
#if defined(_SETImGui_Dx9_) || defined(_DEBUG) || defined(DEBUG)
    auto *pKeyboard = mslib::DX9_DeviceManager::GetKeyboard();

    if (pKeyboard->Trigger(mslib::DirectInputKeyboardButton::KEY_F1))
    {
        CScreen::screenchange(SceneName::Game);
    }
#endif

    mslib::DX9_Object::UpdateAll();
}

//==========================================================================
// 描画
void Tutorial::Draw(void)
{
    mslib::DX9_Object::DrawAll();
}
