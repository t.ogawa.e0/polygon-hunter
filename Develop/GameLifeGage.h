//==========================================================================
// ライフゲージ[GameLifeGage.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GageSystem.h"

//==========================================================================
//
// class  : GameLifeGage
// Content: ライフゲージ
//
//=========================================================================
class GameLifeGage : public mslib::DX9_Object
{
public:
    GameLifeGage();
    ~GameLifeGage();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:
    mslib::vector_wrapper<GageSystem> m_gage; // ゲージ
    int m_debug_gage_max;
    int m_debug_gage;
    int m_debug_buff;
};

