//==========================================================================
// バレットノーマルオブジェクト[BulletNormalEffectObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "BulletNormalEffectObject.h"
#include "GameCamera.h"
#include "SparkEffectObject.h"
#include "resource_list.h"

BulletNormalEffectObject::BulletNormalEffectObject() : DX9_Object(mslib::DX9_ObjectID::Billboard)
{
    this->SetObjectName("BulletNormalEffectObject");
    this->m_CameraObject = nullptr;
    this->m_SparkEffectObject = nullptr;
}

BulletNormalEffectObject::~BulletNormalEffectObject()
{
}

//==========================================================================
// 初期化
bool BulletNormalEffectObject::Init(void)
{
    this->m_CameraObject = this->GetObjects(mslib::DX9_ObjectID::Camera, "GameCamera");
    this->m_SparkEffectObject = this->GetObjects(mslib::DX9_ObjectID::Billboard, "SparkEffectObject");

    return this->_3DBillboard()->Init(RESOURCE_bullet_effect2_png, 1, 15, 5, mslib::DX9_BillboardPlateList::Vertical);
}

//==========================================================================
// 解放
void BulletNormalEffectObject::Uninit(void)
{
}

//==========================================================================
// 更新
void BulletNormalEffectObject::Update(void)
{
    for (auto itr = this->m_bullet_data.begin(); itr != this->m_bullet_data.end(); )
    {
        itr->Update();
        this->_3DBillboard()->GetPattanNum(0, itr->Get3DObject()->GetAnimationCount());

        // 処理終了判定が出ている
        if (itr->GetDeleteKey2())
        {
            // 描画オブジェクトから解放する
            this->_3DBillboard()->ObjectDelete(itr->Get3DObject());
            // イテレータを消去し仮のイテレーターを返す
            itr = this->m_bullet_data.erase(itr);
        }
        else if (itr->GetDeleteKey())
        {
            auto * pSparkEffect = (SparkEffectObject*)this->m_SparkEffectObject;
            // 描画オブジェクトから解放する
            this->_3DBillboard()->ObjectDelete(itr->Get3DObject());
            if (mslib::nullptr_check(pSparkEffect))
            {
                pSparkEffect->Create(*itr->Get3DObject());
            }

            // イテレータを消去し仮のイテレーターを返す
            itr = this->m_bullet_data.erase(itr);
        }
        else
        {
            this->Debug_3DObject(itr->Get3DObject());
            ++itr;
        }
    }

    this->_3DBillboard()->Update();
}

//==========================================================================
// 描画
void BulletNormalEffectObject::Draw(void)
{
    this->_3DBillboard()->Draw(((CGameCamera*)this->m_CameraObject)->GetCamera(0)->CreateView());
}

//==========================================================================
// デバッグ
void BulletNormalEffectObject::Debug(void)
{
}

//==========================================================================
// オブジェクトの生成
void BulletNormalEffectObject::Create(const mslib::DX9_3DObject * pobj, const mslib::CVector3<float> & speed, const mslib::CVector3<float> & rot, int limit_time)
{
    this->m_bullet_data.emplace_back(pobj, speed, rot, limit_time, 0);
    auto itr = --this->m_bullet_data.end();
    itr->Get3DObject()->InitAnimationCount();
    this->_3DBillboard()->ObjectInput(itr->Get3DObject());
}
