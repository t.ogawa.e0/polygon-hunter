//==========================================================================
// 高台[HighGround.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "HighGround.h"
#include "resource_list.h"

HighGround::HighGround() : DX9_Object(mslib::DX9_ObjectID::Mesh)
{
    this->SetObjectName("HighGround");

    auto * pObj = this->_3DObject()->Create();

    pObj->MoveY(20);
    pObj->MoveZ(30);
    pObj->ScalePlus(10);
    pObj->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);

    pObj = this->_3DObject()->Create();
    pObj->MoveY(20);
    pObj->MoveZ(30);
    pObj->ScalePlus(10);
    pObj->RotZ(mslib::angle_list::angle_180);
    pObj->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);
}

HighGround::~HighGround()
{
}

//=========================================================================
// 初期化
bool HighGround::Init(void)
{
    for (int i = 0; i < this->_3DObject()->Size(); i++)
    {
        this->_3DMesh()->ObjectInput(this->_3DObject()->Get(i));
    }

    this->UpdatePlay();

    return this->_3DMesh()->Init(RESOURCE_BS07_KK_png, 1, 1);
}

//=========================================================================
// 解放
void HighGround::Uninit(void)
{
}

//=========================================================================
// 更新
void HighGround::Update(void)
{
    if (this->GetPlayKey())
    {
        this->_3DMesh()->Update();
    }
}

//=========================================================================
// 描画
void HighGround::Draw(void)
{
    if (this->GetPlayKey())
    {
        this->_3DMesh()->Draw();
    }
}

//=========================================================================
// デバッグ
void HighGround::Debug(void)
{
}
