//==========================================================================
// ゲームマネージャー[GameManager.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGameManager
// Content: ゲームマネージャー
//
//=========================================================================
class CGameManager : public mslib::DX9_Object
{
public:
    CGameManager();
    ~CGameManager();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:
    // 全objectの更新有効化
    void SetUpdateKey(void);
    // エネミーの生成
    void CreateEnemy(void);
    // ゲーム終了キー
    void GemeEnd(void);
    // ゲームオーバー
    void GameOver(void);
    // ゲームクリア
    void GameClear(void);
    // 停止
    void Stop(void);
    // 実行
    void Play(void);
    // 仮データの生成
    void SaveUserInformation(bool end_key);
private:
    void * m_GameCamera; // カメラへのアクセス権
    void * m_EnemySmallSize; // エネミー
    void * m_EnemyLargeBoss; // エネミー
    void * m_PlayObject; // プレイヤー
    void * m_StatusObj; // ステータスオブジェクトへのアクセス
    void * m_ControlWeapon; // コントロール
    void * m_Game_Timer; // ゲームタイマー
    void * m_HighGround; // 高台
    void * m_Game_Over_Clear; // ゲーム終了演出
    void * m_UserInformation; // ユーザー情報へアクセス
    int m_count_enemy_small; // 雑魚エネミーカウンター
    int m_pop_count; // ポップカウント
    bool m_SetPlayKey; // 各処理ロックの解除
};