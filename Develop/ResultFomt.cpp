//==========================================================================
// リザルト名[ResultFomt.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ResultFomt.h"
#include "InputSystem.h"
#include "KeyboardObject.h"
#include "Screen.h" // デバッグ用
#include "resource_list.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr float __tex_scale = 1.0f;

ResultFomt::ResultFomt() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->SetObjectName("ResultFomt");
}

ResultFomt::~ResultFomt()
{
}

//=========================================================================
// 初期化
bool ResultFomt::Init(void)
{
    this->m_UserInformation = this->GetObjects(mslib::DX9_ObjectID::Default, "UserInformation");
    this->m_KeyboardObject = this->GetObjects(mslib::DX9_ObjectID::Default, "KeyboardObject");
    this->m_InputSystem = this->GetObjects(mslib::DX9_ObjectID::Default, "InputSystem");

    auto * pUserInformation = (UserInformation*)this->m_UserInformation;

    //==========================================================================
    // 配置データの取得
    //==========================================================================
    this->_2DObject()->Create();
    this->_2DObject()->Create();

    //==========================================================================
    // タイマーの初期化
    //==========================================================================
    for (int i = 0; i < 2; i++)
    {
        this->_2DNumber()->Init(RESOURCE_Number_png, true);
        this->_2DNumber()->Set(i, 2, true, true, { 0,0 });
        this->_2DNumber()->SetAnim(i, 1, 10, 10);
        this->_2DNumber()->SetTexScale(i, __tex_scale);
        this->_2DNumber()->SetMasterColor(i, { 255,0,0,255 });
        if (this->m_data.m_JudgmentOfSuccess)
        {
            this->_2DNumber()->SetMasterColor(i, { 0,0,0,255 });
        }
    }

     this->_2DObject()->Get(1)->Init(2);

    this->ObjectLoad();

    //==========================================================================
    // 2Dオブジェクトの生成
    //==========================================================================
    auto * pObj = this->_2DObject()->Get(0);

    //==========================================================================
    // クリアデータの取得
    //==========================================================================
    if (mslib::nullptr_check(pUserInformation))
    {
        this->m_data = pUserInformation->LoadNewData();
        pObj->Init((int)this->m_data.m_JudgmentOfSuccess);
        pUserInformation->InputData(this->m_data);
    }
    this->_2DPolygon()->ObjectInput(pObj);

    //==========================================================================
    // リソース登録
    //==========================================================================
    this->_2DPolygon()->Init(RESOURCE_ResultGameOver_png, true);
    this->_2DPolygon()->Init(RESOURCE_ResultGameClear_png, true);

    //==========================================================================
    // 2Dオブジェクトの生成
    //==========================================================================
    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(1));
    this->_2DPolygon()->Init(RESOURCE_timer_区切り_png, true);

    //==========================================================================
    // 色の再設定
    //==========================================================================
    pObj = this->_2DObject()->Get(1);
    pObj->SetColor({ 255,0,0,255 });
    if (this->m_data.m_JudgmentOfSuccess)
    {
        pObj->SetColor({ 0,0,0,255 });
    }
    for (int i = 0; i < 2; i++)
    {
        this->_2DNumber()->SetMasterColor(i, { 255,0,0,255 });
        if (this->m_data.m_JudgmentOfSuccess)
        {
            this->_2DNumber()->SetMasterColor(i, { 0,0,0,255 });
        }
    }

    return false;
}

//=========================================================================
// 解放
void ResultFomt::Uninit(void)
{
}

//=========================================================================
// 更新
void ResultFomt::Update(void)
{
    this->EndKey();

    //=========================================================================
    auto * p_tim = this->_Timer()->Get(0);
    this->_2DNumber()->Update(0, this->m_data.m_minute.GetTime());
    p_tim = this->_Timer()->Get(1);
    this->_2DNumber()->Update(1, this->m_data.m_seconds.GetTime());

    this->_2DPolygon()->Update();
}

//=========================================================================
// 描画
void ResultFomt::Draw(void)
{
    this->_2DPolygon()->Draw();
    this->_2DNumber()->Draw();
}

//=========================================================================
// デバッグ
void ResultFomt::Debug(void)
{
}

//=========================================================================
// 終了キー
void ResultFomt::EndKey(void)
{
    auto * pKeyboardObject = (KeyboardObject*)this->m_KeyboardObject;
    auto * pInputSystem = (CInputSystem*)this->m_InputSystem;
    bool bkey = false;

    if (mslib::nullptr_check(pKeyboardObject))
    {
        if (pKeyboardObject->KEY_Q(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_W(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_E(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_A(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_S(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_D(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_U(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_I(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_O(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_J(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_K(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_L(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_SPACE(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
    }

    if (mslib::nullptr_check(pInputSystem))
    {
        int value = 0;
        auto * pXinput = pInputSystem->GetXInput();
        if (pXinput->Check(value))
        {
            if (pXinput->AnalogL(value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogLTrigger(mslib::XInputAnalog::UP, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogLTrigger(mslib::XInputAnalog::DOWN, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogLTrigger(mslib::XInputAnalog::LEFT, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogLTrigger(mslib::XInputAnalog::RIGHT, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogR(value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogRTrigger(mslib::XInputAnalog::UP, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogRTrigger(mslib::XInputAnalog::DOWN, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogRTrigger(mslib::XInputAnalog::LEFT, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogRTrigger(mslib::XInputAnalog::RIGHT, value))
            {
                bkey = true;
            }
            else if (pXinput->LT(value))
            {
                bkey = true;
            }
            else if (pXinput->RT(value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::A, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::B, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::BACK, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::DPAD_DOWN, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::DPAD_LEFT, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::DPAD_RIGHT, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::DPAD_UP, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::LEFT_LB, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::LEFT_THUMB, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::RIGHT_RB, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::RIGHT_THUMB, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::START, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::X, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::Y, value))
            {
                bkey = true;
            }
        }
    }

    if (bkey == true)
    {
        CScreen::screenchange(SceneName::Title);
    }
}
