//==========================================================================
// 高台[HighGround.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "PlayObject.h"

//==========================================================================
//
// class  : HighGround
// Content: 高台
//
//=========================================================================
class HighGround : public mslib::DX9_Object, public PlayObject
{
public:
    HighGround();
    ~HighGround();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
};

