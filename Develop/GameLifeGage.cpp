//==========================================================================
// ライフゲージ[GameLifeGage.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameLifeGage.h"
#include "resource_list.h"

GameLifeGage::GameLifeGage() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->SetObjectName("GameLifeGage");
    this->m_debug_gage_max = 100;
    this->m_debug_buff = 1;
    this->m_debug_gage = 100;
}

GameLifeGage::~GameLifeGage()
{
}

//==========================================================================
// 初期化
bool GameLifeGage::Init(void)
{
    this->_2DObject()->Reserve(5);

    //==========================================================================
    // timeバーobject
    //==========================================================================
    this->_2DPolygon()->Init(RESOURCE_time_ber_png, true);
    auto * p_obj = this->_2DObject()->Create();
    p_obj->Init(0);

    //==========================================================================
    // ハート表示
    //==========================================================================
    this->_2DPolygon()->Init(RESOURCE_life_gage_heart_png, true);
    p_obj = this->_2DObject()->Create();
    p_obj->Init(1);

    //==========================================================================
    // 枠表示
    //==========================================================================
    this->_2DPolygon()->Init(RESOURCE_Timer_Gage_Begin_png, true);
    p_obj = this->_2DObject()->Create();
    p_obj->Init(2);

    //==========================================================================
    // 枠表示
    //==========================================================================
    this->_2DPolygon()->Init(RESOURCE_Timer_Gage_End_png, true);
    p_obj = this->_2DObject()->Create();
    p_obj->Init(3);

    //==========================================================================
    // ゲージ後ろ
    //==========================================================================
    this->_2DPolygon()->Init(nullptr, true);
    p_obj = this->_2DObject()->Create();
    p_obj->Init(4);

    //==========================================================================
    // ゲージ
    //==========================================================================
    this->_2DPolygon()->Init(nullptr, true);
    p_obj = this->_2DObject()->Create();
    p_obj->Init(5);

    this->ObjectLoad();

    auto * ptex1 = this->_2DPolygon()->GetTexSize(4);
    auto * ptex2 = this->_2DPolygon()->GetTexSize(5);
    auto *p_obj1 = this->_2DObject()->Get(2);
    auto *p_obj2 = this->_2DObject()->Get(3);
    auto * pGage = this->m_gage.Create();

    pGage->Input({
        p_obj2->GetPos()->x - p_obj1->GetPos()->x,
        1.0f
        });
    ptex1->w = (int)pGage->GetMaxSize().x;
    ptex1->h = this->_2DPolygon()->GetTexSize(2)->h / 2;
    ptex2->w = ptex1->w;
    ptex2->h = ptex1->h;

    for (int i = 0; i < this->_2DObject()->Size(); i++)
    {
        this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(i));
    }

    return false;
}

//==========================================================================
// 解放
void GameLifeGage::Uninit(void)
{
}

//==========================================================================
// 更新
void GameLifeGage::Update(void)
{
    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void GameLifeGage::Draw(void)
{
    //this->_2DPolygon()->Draw();
}

//=========================================================================
// デバッグ
void GameLifeGage::Debug(void)
{
}
