//==========================================================================
// タイマーゲージオブジェクト[Game_Timer_Gage.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GageSystem.h"

//==========================================================================
//
// class  : Game_Timer_Gage
// Content: タイマーゲージオブジェクト
//
//=========================================================================
class Game_Timer_Gage : public mslib::DX9_Object
{
public:
    Game_Timer_Gage();
    ~Game_Timer_Gage();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:
    void * m_timer_obj;
    mslib::vector_wrapper<GageSystem> m_gage; // ゲージ
};

