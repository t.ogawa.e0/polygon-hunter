//==========================================================================
// エフェクトオブジェクト[EffectObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : EffectObject
// Content: オブジェクト管理クラスの拡張版
//
//==========================================================================
class EffectObject : public mslib::DX9_Object
{
public:
    //==========================================================================
    //
    // class  : ModelEffect基礎パラメータ
    // Content: Effect基礎パラメータ
    //
    //=========================================================================
    class ModelEffectParam
    {
    public:
        ModelEffectParam();
        virtual ~ModelEffectParam();

        // 解放キー
        void ReleaseKey(void);
        // 解放キーの取得
        bool GetReleaseKey(void);
        // オブジェクトの取得
        mslib::DX9_3DObject * GetData(void);
        // 継承更新
        virtual void Update(void) = 0;
    protected:
        mslib::DX9_3DObject m_obj; // オブジェクト
        float m_MoveSpeed; // 移動速
        int m_limit_time; // 制限時間
        int m_time; // 生存時間
        bool m_release_key; // 解放キー
    };
private:
    // コピー禁止 (C++11)
    EffectObject(const EffectObject &) = delete;
    EffectObject &operator=(const EffectObject &) = delete;
protected:
    EffectObject(mslib::DX9_ObjectID ObjectID = mslib::DX9_ObjectID::Default);
    virtual ~EffectObject();

    // 継承初期化
    virtual bool Init(void) = 0;

    // 継承解放
    virtual void Uninit(void) = 0;

    // 継承更新
    virtual void Update(void) = 0;

    // 継承描画
    virtual void Draw(void) = 0;

    // 継承デバッグ
    virtual void Debug(void) = 0;
};

