//==========================================================================
// タイトルの背景[TitleBackground.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : TitleBackground
// Content: タイトルの背景
//
//=========================================================================
class TitleBackground : public mslib::DX9_Object
{
public:
    TitleBackground();
    ~TitleBackground();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
};

