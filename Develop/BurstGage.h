//==========================================================================
// ブーストゲージ[BurstGage.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : BurstGage
// Content: ブーストゲージ
//
//=========================================================================
class BurstGage : public mslib::DX9_Object
{
private:
    enum class ObjectList
    {
        Begin,
        BurstGageBackground = Begin, // 背景
        BurstLv, // レベル
        Percent,
        End,
    };
    enum class ObjectNumberList
    {
        Begin,
        Lv = Begin,
        percent,
        End,
    };
public:
    BurstGage();
    ~BurstGage();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    // 加算
    void Plus(int value);
    // 減算
    void Minus(int value);
    // レベルのゲッター
    int GetLevel(void);
    // オーバーフローした反映しきれなかった値(減算値)
    int GetOverflowMinusBuff(void);
private:
    // 色の更新
    void ColorChange(void);
    // レベル処理
    void Level(void);
private:
    int m_lv; // レベル
    int m_percent; // 反映する値s
    int m_plus_buff; // 加算する予定値
    int m_minus_buff; // 減算する予定値
    int m_overflow_minus_buff; // オーバーフローした反映しきれなかった値(減算値)
};

