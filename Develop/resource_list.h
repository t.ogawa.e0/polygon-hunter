//=============================================================================
// resource file path
// 2018/09/06
// 13:49:59
//=============================================================================
#pragma once

//=============================================================================
// filename extension [.png]
//=============================================================================

#ifndef RESOURCE_操作説明1_png
#define RESOURCE_操作説明1_png "resource/texture/Tutorial/操作説明1.png"
#endif // !RESOURCE_操作説明1_png
#ifndef RESOURCE_操作説明2_png
#define RESOURCE_操作説明2_png "resource/texture/Tutorial/操作説明2.png"
#endif // !RESOURCE_操作説明2_png
#ifndef RESOURCE_操作説明3_png
#define RESOURCE_操作説明3_png "resource/texture/Tutorial/操作説明3.png"
#endif // !RESOURCE_操作説明3_png
#ifndef RESOURCE_操作説明4_png
#define RESOURCE_操作説明4_png "resource/texture/Tutorial/操作説明4.png"
#endif // !RESOURCE_操作説明4_png
#ifndef RESOURCE_操作説明5_png
#define RESOURCE_操作説明5_png "resource/texture/Tutorial/操作説明5.png"
#endif // !RESOURCE_操作説明5_png
#ifndef RESOURCE_操作説明6_png
#define RESOURCE_操作説明6_png "resource/texture/Tutorial/操作説明6.png"
#endif // !RESOURCE_操作説明6_png
#ifndef RESOURCE_操作説明7_png
#define RESOURCE_操作説明7_png "resource/texture/Tutorial/操作説明7.png"
#endif // !RESOURCE_操作説明7_png
#ifndef RESOURCE_PolygonHunter_png
#define RESOURCE_PolygonHunter_png "resource/texture/Title/PolygonHunter.png"
#endif // !RESOURCE_PolygonHunter_png
#ifndef RESOURCE_PressKey_png
#define RESOURCE_PressKey_png "resource/texture/Title/PressKey.png"
#endif // !RESOURCE_PressKey_png
#ifndef RESOURCE_タイトル仮置き_png
#define RESOURCE_タイトル仮置き_png "resource/texture/Title/タイトル仮置き.png"
#endif // !RESOURCE_タイトル仮置き_png
#ifndef RESOURCE_ResultGameClear_png
#define RESOURCE_ResultGameClear_png "resource/texture/Result/ResultGameClear.png"
#endif // !RESOURCE_ResultGameClear_png
#ifndef RESOURCE_ResultGameOver_png
#define RESOURCE_ResultGameOver_png "resource/texture/Result/ResultGameOver.png"
#endif // !RESOURCE_ResultGameOver_png
#ifndef RESOURCE_リザルト仮置き_png
#define RESOURCE_リザルト仮置き_png "resource/texture/Result/リザルト仮置き.png"
#endif // !RESOURCE_リザルト仮置き_png
#ifndef RESOURCE_Brick_png
#define RESOURCE_Brick_png "resource/texture/Open/Brick.png"
#endif // !RESOURCE_Brick_png
#ifndef RESOURCE_Number_png
#define RESOURCE_Number_png "resource/texture/Open/Number.png"
#endif // !RESOURCE_Number_png
#ifndef RESOURCE_timer_区切り_png
#define RESOURCE_timer_区切り_png "resource/texture/Open/timer_区切り.png"
#endif // !RESOURCE_timer_区切り_png
#ifndef RESOURCE_status_list_background_sub_png
#define RESOURCE_status_list_background_sub_png "resource/texture/Game/sub2/status_list_background_sub.png"
#endif // !RESOURCE_status_list_background_sub_png
#ifndef RESOURCE_status_list_name_HP_sub_png
#define RESOURCE_status_list_name_HP_sub_png "resource/texture/Game/sub2/status_list_name_HP_sub.png"
#endif // !RESOURCE_status_list_name_HP_sub_png
#ifndef RESOURCE_status_list_name_MP_sub_png
#define RESOURCE_status_list_name_MP_sub_png "resource/texture/Game/sub2/status_list_name_MP_sub.png"
#endif // !RESOURCE_status_list_name_MP_sub_png
#ifndef RESOURCE_status_list_name_SP_sub_png
#define RESOURCE_status_list_name_SP_sub_png "resource/texture/Game/sub2/status_list_name_SP_sub.png"
#endif // !RESOURCE_status_list_name_SP_sub_png
#ifndef RESOURCE_BS07s_png
#define RESOURCE_BS07s_png "resource/texture/Game/BS07s.png"
#endif // !RESOURCE_BS07s_png
#ifndef RESOURCE_BS07_KK_png
#define RESOURCE_BS07_KK_png "resource/texture/Game/BS07_KK.png"
#endif // !RESOURCE_BS07_KK_png
#ifndef RESOURCE_bullet_effect2_png
#define RESOURCE_bullet_effect2_png "resource/texture/Game/bullet_effect2.png"
#endif // !RESOURCE_bullet_effect2_png
#ifndef RESOURCE_BurstGageBack_png
#define RESOURCE_BurstGageBack_png "resource/texture/Game/BurstGageBack.png"
#endif // !RESOURCE_BurstGageBack_png
#ifndef RESOURCE_Crosshair_png
#define RESOURCE_Crosshair_png "resource/texture/Game/Crosshair.png"
#endif // !RESOURCE_Crosshair_png
#ifndef RESOURCE_enemy_1_box_png
#define RESOURCE_enemy_1_box_png "resource/texture/Game/enemy_1_box.png"
#endif // !RESOURCE_enemy_1_box_png
#ifndef RESOURCE_enemy_2_box_png
#define RESOURCE_enemy_2_box_png "resource/texture/Game/enemy_2_box.png"
#endif // !RESOURCE_enemy_2_box_png
#ifndef RESOURCE_fire_box_png
#define RESOURCE_fire_box_png "resource/texture/Game/fire_box.png"
#endif // !RESOURCE_fire_box_png
#ifndef RESOURCE_fire_nucleus_png
#define RESOURCE_fire_nucleus_png "resource/texture/Game/fire_nucleus.png"
#endif // !RESOURCE_fire_nucleus_png
#ifndef RESOURCE_GameClear_png
#define RESOURCE_GameClear_png "resource/texture/Game/GameClear.png"
#endif // !RESOURCE_GameClear_png
#ifndef RESOURCE_GameMainScreen_png
#define RESOURCE_GameMainScreen_png "resource/texture/Game/GameMainScreen.png"
#endif // !RESOURCE_GameMainScreen_png
#ifndef RESOURCE_GameOver_png
#define RESOURCE_GameOver_png "resource/texture/Game/GameOver.png"
#endif // !RESOURCE_GameOver_png
#ifndef RESOURCE_HP自動回復_ICO_png
#define RESOURCE_HP自動回復_ICO_png "resource/texture/Game/HP自動回復_ICO.png"
#endif // !RESOURCE_HP自動回復_ICO_png
#ifndef RESOURCE_life_gage_heart_png
#define RESOURCE_life_gage_heart_png "resource/texture/Game/life_gage_heart.png"
#endif // !RESOURCE_life_gage_heart_png
#ifndef RESOURCE_Lv_IMG_png
#define RESOURCE_Lv_IMG_png "resource/texture/Game/Lv_IMG.png"
#endif // !RESOURCE_Lv_IMG_png
#ifndef RESOURCE_MP自動回復_ICO_png
#define RESOURCE_MP自動回復_ICO_png "resource/texture/Game/MP自動回復_ICO.png"
#endif // !RESOURCE_MP自動回復_ICO_png
#ifndef RESOURCE_percent_png
#define RESOURCE_percent_png "resource/texture/Game/percent.png"
#endif // !RESOURCE_percent_png
#ifndef RESOURCE_player_box_png
#define RESOURCE_player_box_png "resource/texture/Game/player_box.png"
#endif // !RESOURCE_player_box_png
#ifndef RESOURCE_spark_effect_png
#define RESOURCE_spark_effect_png "resource/texture/Game/spark_effect.png"
#endif // !RESOURCE_spark_effect_png
#ifndef RESOURCE_SP自動回復_ICO_png
#define RESOURCE_SP自動回復_ICO_png "resource/texture/Game/SP自動回復_ICO.png"
#endif // !RESOURCE_SP自動回復_ICO_png
#ifndef RESOURCE_status_list_name_gage_Begin_png
#define RESOURCE_status_list_name_gage_Begin_png "resource/texture/Game/status_list_name_gage_Begin.png"
#endif // !RESOURCE_status_list_name_gage_Begin_png
#ifndef RESOURCE_status_list_name_gage_End_png
#define RESOURCE_status_list_name_gage_End_png "resource/texture/Game/status_list_name_gage_End.png"
#endif // !RESOURCE_status_list_name_gage_End_png
#ifndef RESOURCE_status_list_name_HP_png
#define RESOURCE_status_list_name_HP_png "resource/texture/Game/status_list_name_HP.png"
#endif // !RESOURCE_status_list_name_HP_png
#ifndef RESOURCE_status_list_name_MP_png
#define RESOURCE_status_list_name_MP_png "resource/texture/Game/status_list_name_MP.png"
#endif // !RESOURCE_status_list_name_MP_png
#ifndef RESOURCE_status_list_name_SP_png
#define RESOURCE_status_list_name_SP_png "resource/texture/Game/status_list_name_SP.png"
#endif // !RESOURCE_status_list_name_SP_png
#ifndef RESOURCE_time_png
#define RESOURCE_time_png "resource/texture/Game/time.png"
#endif // !RESOURCE_time_png
#ifndef RESOURCE_Timer_Gage_Begin_png
#define RESOURCE_Timer_Gage_Begin_png "resource/texture/Game/Timer_Gage_Begin.png"
#endif // !RESOURCE_Timer_Gage_Begin_png
#ifndef RESOURCE_Timer_Gage_End_png
#define RESOURCE_Timer_Gage_End_png "resource/texture/Game/Timer_Gage_End.png"
#endif // !RESOURCE_Timer_Gage_End_png
#ifndef RESOURCE_time_ber_png
#define RESOURCE_time_ber_png "resource/texture/Game/time_ber.png"
#endif // !RESOURCE_time_ber_png
#ifndef RESOURCE_UI_ICO_png
#define RESOURCE_UI_ICO_png "resource/texture/Game/UI_ICO.png"
#endif // !RESOURCE_UI_ICO_png
#ifndef RESOURCE_スーパーアーマー_ICO_png
#define RESOURCE_スーパーアーマー_ICO_png "resource/texture/Game/スーパーアーマー_ICO.png"
#endif // !RESOURCE_スーパーアーマー_ICO_png
#ifndef RESOURCE_攻撃速度UP_ICO_png
#define RESOURCE_攻撃速度UP_ICO_png "resource/texture/Game/攻撃速度UP_ICO.png"
#endif // !RESOURCE_攻撃速度UP_ICO_png
#ifndef RESOURCE_移動速度UP_ICO_png
#define RESOURCE_移動速度UP_ICO_png "resource/texture/Game/移動速度UP_ICO.png"
#endif // !RESOURCE_移動速度UP_ICO_png
#ifndef RESOURCE_Color_Picker_png
#define RESOURCE_Color_Picker_png "resource/3DModel/gun blade v1.02/Color_Picker.png"
#endif // !RESOURCE_Color_Picker_png
#ifndef RESOURCE_gun_blade_png
#define RESOURCE_gun_blade_png "resource/3DModel/gun blade v1.02/gun_blade.png"
#endif // !RESOURCE_gun_blade_png
#ifndef RESOURCE_LineColor_png
#define RESOURCE_LineColor_png "resource/3DModel/gun blade v1.02/LineColor.png"
#endif // !RESOURCE_LineColor_png
#ifndef RESOURCE_LineTex_png
#define RESOURCE_LineTex_png "resource/3DModel/gun blade v1.02/LineTex.png"
#endif // !RESOURCE_LineTex_png
#ifndef RESOURCE_steel_png
#define RESOURCE_steel_png "resource/3DModel/gun blade v1.02/steel.png"
#endif // !RESOURCE_steel_png
#ifndef RESOURCE_test_stage_png
#define RESOURCE_test_stage_png "resource/3DModel/debug/test_stage.png"
#endif // !RESOURCE_test_stage_png
#ifndef RESOURCE_AttackEffect_1_png
#define RESOURCE_AttackEffect_1_png "resource/3DModel/Effect/AttackEffect_1.png"
#endif // !RESOURCE_AttackEffect_1_png

//=============================================================================
// filename extension [.jpg]
//=============================================================================

#ifndef RESOURCE_Asphalt_jpg
#define RESOURCE_Asphalt_jpg "resource/texture/Open/Asphalt.jpg"
#endif // !RESOURCE_Asphalt_jpg
#ifndef RESOURCE_sky_jpg
#define RESOURCE_sky_jpg "resource/texture/Game/sky.jpg"
#endif // !RESOURCE_sky_jpg

//=============================================================================
// filename extension [.DDS]
//=============================================================================

#ifndef RESOURCE_LoadTex_DDS
#define RESOURCE_LoadTex_DDS "resource/texture/Load/LoadTex.DDS"
#endif // !RESOURCE_LoadTex_DDS
#ifndef RESOURCE_NowLoading_DDS
#define RESOURCE_NowLoading_DDS "resource/texture/Load/NowLoading.DDS"
#endif // !RESOURCE_NowLoading_DDS
#ifndef RESOURCE_test_stage_DDS
#define RESOURCE_test_stage_DDS "resource/3DModel/debug/test_stage.DDS"
#endif // !RESOURCE_test_stage_DDS

//=============================================================================
// filename extension [.x]
//=============================================================================

#ifndef RESOURCE_blade_刀身_x
#define RESOURCE_blade_刀身_x "resource/3DModel/gun blade v1.02/blade_刀身.x"
#endif // !RESOURCE_blade_刀身_x
#ifndef RESOURCE_blade_小刀_x
#define RESOURCE_blade_小刀_x "resource/3DModel/gun blade v1.02/blade_小刀.x"
#endif // !RESOURCE_blade_小刀_x
#ifndef RESOURCE_blade_銃_x
#define RESOURCE_blade_銃_x "resource/3DModel/gun blade v1.02/blade_銃.x"
#endif // !RESOURCE_blade_銃_x
#ifndef RESOURCE_blade_鞘_x
#define RESOURCE_blade_鞘_x "resource/3DModel/gun blade v1.02/blade_鞘.x"
#endif // !RESOURCE_blade_鞘_x
#ifndef RESOURCE_test_stage_x
#define RESOURCE_test_stage_x "resource/3DModel/debug/test_stage.x"
#endif // !RESOURCE_test_stage_x
#ifndef RESOURCE_effect_1_x
#define RESOURCE_effect_1_x "resource/3DModel/Effect/effect_1.x"
#endif // !RESOURCE_effect_1_x
#ifndef RESOURCE_Alicia_solid_x
#define RESOURCE_Alicia_solid_x "resource/3DModel/Alicia/Alicia_solid.x"
#endif // !RESOURCE_Alicia_solid_x

//=============================================================================
// filename extension [.bin]
//=============================================================================

#ifndef RESOURCE_level0_bin
#define RESOURCE_level0_bin "resource/data/level0.bin"
#endif // !RESOURCE_level0_bin
#ifndef RESOURCE_level1_bin
#define RESOURCE_level1_bin "resource/data/level1.bin"
#endif // !RESOURCE_level1_bin
#ifndef RESOURCE_level1_sub_bin
#define RESOURCE_level1_sub_bin "resource/data/level1_sub.bin"
#endif // !RESOURCE_level1_sub_bin
#ifndef RESOURCE_level2_bin
#define RESOURCE_level2_bin "resource/data/level2.bin"
#endif // !RESOURCE_level2_bin
#ifndef RESOURCE_level3_bin
#define RESOURCE_level3_bin "resource/data/level3.bin"
#endif // !RESOURCE_level3_bin
#ifndef RESOURCE_new_data_bin
#define RESOURCE_new_data_bin "resource/data/new_data.bin"
#endif // !RESOURCE_new_data_bin
#ifndef RESOURCE_TestField_bin
#define RESOURCE_TestField_bin "resource/data/TestField.bin"
#endif // !RESOURCE_TestField_bin
#ifndef RESOURCE_user_data_bin
#define RESOURCE_user_data_bin "resource/data/user_data.bin"
#endif // !RESOURCE_user_data_bin

//=============================================================================
// filename extension [.tga]
//=============================================================================

#ifndef RESOURCE_Alicia_body_tga
#define RESOURCE_Alicia_body_tga "resource/3DModel/Alicia/Alicia_body.tga"
#endif // !RESOURCE_Alicia_body_tga
#ifndef RESOURCE_Alicia_eye_tga
#define RESOURCE_Alicia_eye_tga "resource/3DModel/Alicia/Alicia_eye.tga"
#endif // !RESOURCE_Alicia_eye_tga
#ifndef RESOURCE_Alicia_face_tga
#define RESOURCE_Alicia_face_tga "resource/3DModel/Alicia/Alicia_face.tga"
#endif // !RESOURCE_Alicia_face_tga
#ifndef RESOURCE_Alicia_hair_tga
#define RESOURCE_Alicia_hair_tga "resource/3DModel/Alicia/Alicia_hair.tga"
#endif // !RESOURCE_Alicia_hair_tga
#ifndef RESOURCE_Alicia_other_tga
#define RESOURCE_Alicia_other_tga "resource/3DModel/Alicia/Alicia_other.tga"
#endif // !RESOURCE_Alicia_other_tga
#ifndef RESOURCE_Alicia_wear_tga
#define RESOURCE_Alicia_wear_tga "resource/3DModel/Alicia/Alicia_wear.tga"
#endif // !RESOURCE_Alicia_wear_tga

//=============================================================================
// filename extension [.bmp]
//=============================================================================

#ifndef RESOURCE_画像表示枠サイズ_bmp
#define RESOURCE_画像表示枠サイズ_bmp "画像表示枠サイズ.bmp"
#endif // !RESOURCE_画像表示枠サイズ_bmp
#ifndef RESOURCE_blade_s_bmp
#define RESOURCE_blade_s_bmp "resource/3DModel/Alicia/blade_s.bmp"
#endif // !RESOURCE_blade_s_bmp
#ifndef RESOURCE_eye_s_bmp
#define RESOURCE_eye_s_bmp "resource/3DModel/Alicia/eye_s.bmp"
#endif // !RESOURCE_eye_s_bmp
#ifndef RESOURCE_face_s_bmp
#define RESOURCE_face_s_bmp "resource/3DModel/Alicia/face_s.bmp"
#endif // !RESOURCE_face_s_bmp
#ifndef RESOURCE_hair_s_bmp
#define RESOURCE_hair_s_bmp "resource/3DModel/Alicia/hair_s.bmp"
#endif // !RESOURCE_hair_s_bmp
#ifndef RESOURCE_ramp_s_bmp
#define RESOURCE_ramp_s_bmp "resource/3DModel/Alicia/ramp_s.bmp"
#endif // !RESOURCE_ramp_s_bmp
#ifndef RESOURCE_rod_s_bmp
#define RESOURCE_rod_s_bmp "resource/3DModel/Alicia/rod_s.bmp"
#endif // !RESOURCE_rod_s_bmp
#ifndef RESOURCE_shoes_s_bmp
#define RESOURCE_shoes_s_bmp "resource/3DModel/Alicia/shoes_s.bmp"
#endif // !RESOURCE_shoes_s_bmp
#ifndef RESOURCE_skin_s_bmp
#define RESOURCE_skin_s_bmp "resource/3DModel/Alicia/skin_s.bmp"
#endif // !RESOURCE_skin_s_bmp
#ifndef RESOURCE_tongue_s_bmp
#define RESOURCE_tongue_s_bmp "resource/3DModel/Alicia/tongue_s.bmp"
#endif // !RESOURCE_tongue_s_bmp
