//==========================================================================
// キーボードのオブジェクト[KeyboardObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : KeyboardObject
// Content: キーボードのオブジェクト
//
//=========================================================================
class KeyboardObject : public mslib::DX9_Object
{
public:
    enum class KeyboardType
    {
        Press,
        Trigger,
        Repeat,
    };
public:
    KeyboardObject();
    ~KeyboardObject();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;

    bool KEY_Q(KeyboardType type);
    bool KEY_W(KeyboardType type);
    bool KEY_E(KeyboardType type);
    bool KEY_A(KeyboardType type);
    bool KEY_S(KeyboardType type);
    bool KEY_D(KeyboardType type);
    bool KEY_U(KeyboardType type);
    bool KEY_I(KeyboardType type);
    bool KEY_O(KeyboardType type);
    bool KEY_J(KeyboardType type);
    bool KEY_K(KeyboardType type);
    bool KEY_L(KeyboardType type);
    bool KEY_SPACE(KeyboardType type);
private:
    // マスターキー
    bool KEY_MASTER(KeyboardType type, mslib::DirectInputKeyboardButton key);
};

