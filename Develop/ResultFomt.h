//==========================================================================
// リザルト名[ResultFomt.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "UserInformation.h"

//==========================================================================
//
// class  : ResultFomt
// Content: リザルト名
//
//=========================================================================
class ResultFomt : public mslib::DX9_Object
{
public:
    ResultFomt();
    ~ResultFomt();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:
    // 終了キー
    void EndKey(void);
private:
    void * m_UserInformation; // ユーザー情報へアクセス
    void * m_KeyboardObject; // キーオブジェクト
    void * m_InputSystem; // XInput
    UserInformationData m_data; // データ
};

