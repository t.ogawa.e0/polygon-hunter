//==========================================================================
// ゲームオーバーとクリア[Game_Over_Clear.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Over_Clear.h"
#include "resource_list.h"

Game_Over_Clear::Game_Over_Clear() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->SetObjectName("Game_Over_Clear");
    this->m_key = false;
    this->m_color = 255;
    this->m_color.a = 0;
}

Game_Over_Clear::~Game_Over_Clear()
{
}

//=========================================================================
// 初期化
bool Game_Over_Clear::Init(void)
{
    this->_2DPolygon()->Init(RESOURCE_GameClear_png, true);
    this->_2DPolygon()->Init(RESOURCE_GameOver_png, true);
    return false;
}

//=========================================================================
// 解放
void Game_Over_Clear::Uninit(void)
{

}

//=========================================================================
// 更新
void Game_Over_Clear::Update(void)
{
    for (int i = 0; i < this->_2DObject()->Size(); i++)
    {
        if (this->m_color.a < 255)
        {
            this->m_color.a += 5;
        }
        auto *pObj = this->_2DObject()->Get(i);
        pObj->SetColor(this->m_color);
    }
    this->_2DPolygon()->Update();
}

//=========================================================================
// 描画
void Game_Over_Clear::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//=========================================================================
// デバッグ
void Game_Over_Clear::Debug(void)
{
}

//=========================================================================
// 描画情報のセット
void Game_Over_Clear::SetObjectMenu(MyEnumClass list)
{
    if (this->m_key == false)
    {
        auto *pObj = this->_2DObject()->Create();

        pObj->Init((int)list);
        pObj->SetColor(this->m_color);

        this->_2DPolygon()->ObjectInput(pObj);
        this->m_key = true;
    }
}
