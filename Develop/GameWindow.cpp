//==========================================================================
// ゲームウィンドウ[GameWindow.cpp]
// author: tatuya ogawa
//==========================================================================
#include "GameWindow.h"
#include "resource.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr DWORD WINDOW_STYLE = (WS_OVERLAPPEDWINDOW& ~(WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX));

//==========================================================================
// ウィンドウ生成
int CGameWindow::Window(HINSTANCE hInstance, const mslib::CVector2<int> & data, bool Mode, int nCmdShow)
{
    RECT wr, dr; // 画面サイズ
    HWND hWnd = nullptr;

    this->m_WindowsAPI = new CWindowsAPI(this->m_class_name, this->m_window_name);
    this->m_WindowsAPI->MyClass(CS_VREDRAW | CS_HREDRAW, this->WndProc, (LPCSTR)IDI_ICON2, (LPCSTR)IDI_ICON2, nullptr, hInstance);
    this->m_WinSize = data;
    this->m_WinMood = Mode;
    wr = this->m_WindowsAPI->GetWindowRECT();

    // マウスカーソル表示設定
    ShowCursor(true);

    //デスクトップサイズ習得
    GetWindowRect(GetDesktopWindow(), &dr);

    wr.right = this->m_WinSize.x - (wr.top * 2);
    wr.bottom = this->m_WinSize.y - (wr.top * 2);
    wr.left = 0;
    wr.top = 0;

    //Direct3Dオブジェクトの作成 に失敗時終了 
    if (mslib::DX9_DeviceManager::Init(mslib::DX9_DeviceInitList::DXDevice, hInstance, nullptr))
    {
        return -1;
    }

    // 色々失敗した時終了
    if (mslib::DX9_DeviceManager::GetDXDevice()->CreateWindowMode(this->m_WinSize, this->m_WinMood))
    {
        return -1;
    }

    this->m_WindowsAPI->Create(WINDOW_STYLE, wr, nullptr, nullptr, nullptr, nCmdShow);

    hWnd = this->m_WindowsAPI->GetHWND();
    mslib::DX9_DeviceManager::GetDXDevice()->SetHwnd(hWnd);

    return this->GameLoop(hInstance, hWnd);
}

//==========================================================================
// ウィンドウプロシージャ
LRESULT CALLBACK CGameWindow::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    auto pDevice = mslib::DX9_DeviceManager::GetDXDevice()->GetD3DDevice();	//デバイス渡し	
    auto pd3dpp = mslib::DX9_DeviceManager::GetDXDevice()->Getd3dpp();
    mslib::DX9_ImGui imgui;

    if (mslib::DX9_DeviceManager::GetDXDevice()->GetHwnd() != nullptr)
    {
        hWnd = mslib::DX9_DeviceManager::GetDXDevice()->GetHwnd();
    }

    if (imgui.ImGui_WndProcHandler(hWnd, uMsg, wParam, lParam))
    {
        return true;
    }

    // メッセージの種類に応じて処理を分岐します。
    switch (uMsg)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    case WM_KEYDOWN:	// キー入力
        switch (wParam)
        {
        case VK_ESCAPE:
            // [ESC]キーが押されたら
            if (MessageBox(hWnd, "終了しますか？", "終了メッセージ", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
            {
                DestroyWindow(hWnd);	// ウィンドウを破棄
            }
            else
            {
                return 0;	// いいえの時
            }
        }
        break;
    case WM_LBUTTONDOWN:
        SetFocus(hWnd);
        break;
    case WM_CLOSE:	// ×ボタン押した時
        if (MessageBox(hWnd, "終了しますか？", "終了メッセージ", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
        {
            DestroyWindow(hWnd);	// ウィンドウを破棄
        }
        else
        {
            return 0;	// いいえの時
        }
        break;
    default:
        break;
    }

    //デフォルトの処理
    return imgui.SetMenu(hWnd, uMsg, wParam, lParam, pDevice, &pd3dpp);
}

//==========================================================================
// ゲームループ
int CGameWindow::GameLoop(HINSTANCE hInstance, HWND hWnd)
{
    auto & msg = this->m_WindowsAPI->GetMSG(); // メッセージ構造体
    DWORD DNewTime = 0, DOldTime = 0; //時間格納

    if (this->Init(hInstance, hWnd))
    {
        this->m_WindowsAPI->ErrorMessage("初期化に失敗しました");
        return-1;
    }

    timeBeginPeriod(1);//時間を正確に

                       // メッセージループ
    for (;;)
    {
        // 何があってもスルー
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                break;
            }
            else
            {
                //メッセージ処理
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            DNewTime = timeGetTime();
            if ((DNewTime - DOldTime) >= (1000 / 60))
            {
                if (this->Update())
                {
                    this->m_WindowsAPI->ErrorMessage("初期化に失敗しました");
                    return-1;
                }

                //描画
                this->Draw();

                //時間渡し
                DOldTime = DNewTime;
            }
        }
    }
    timeEndPeriod(1);
    this->Uninit();

    return (int)msg.wParam;
}

//==========================================================================
//	初期化
bool CGameWindow::Init(HINSTANCE hInstance, HWND hWnd)
{
    return this->m_screne.Init(hInstance, hWnd);
}

//==========================================================================
//	終了処理
void CGameWindow::Uninit(void)
{
    this->m_screne.Uninit();
}

//==========================================================================
//	更新処理
bool CGameWindow::Update(void)
{
    return this->m_screne.Update();
}

//==========================================================================
//	描画処理
void CGameWindow::Draw(void)
{
    this->m_screne.Draw();
}
