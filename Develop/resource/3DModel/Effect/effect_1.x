xof 0303txt 0032

Frame Root {
  FrameTransformMatrix {
     1.000000, 0.000000, 0.000000, 0.000000,
     0.000000,-0.000000, 1.000000, 0.000000,
     0.000000, 1.000000, 0.000000, 0.000000,
     0.000000, 0.000000, 0.000000, 1.000000;;
  }
  Frame Plane {
    FrameTransformMatrix {
       0.000000, 0.000000,-0.500000, 0.000000,
       0.000000,-0.500000,-0.000000, 0.000000,
       0.000000, 0.000000,-0.000000, 0.000000,
       0.000000, 0.000000, 0.000000, 1.000000;;
    }
    Mesh { // Plane mesh
      6;
       1.000000;-1.000000; 0.778703;,
      -1.000000; 1.000000; 0.778703;,
      -1.000000;-1.000000; 0.778703;,
       1.000000;-1.000000; 0.778703;,
       1.000000; 1.000000; 0.778703;,
      -1.000000; 1.000000; 0.778703;;
      2;
      3;2,1,0;,
      3;5,4,3;;
      MeshNormals { // Plane normals
        2;
        -0.000000; 0.000000; 1.000000;,
        -0.000000; 0.000000; 1.000000;;
        2;
        3;0,0,0;,
        3;1,1,1;;
      } // End of Plane normals
      MeshTextureCoords { // Plane UV coordinates
        6;
         0.000000; 1.000000;,
         1.000000; 0.000000;,
         0.000000; 0.000000;,
         0.000000; 1.000000;,
         1.000000; 1.000000;,
         1.000000; 0.000000;;
      } // End of Plane UV coordinates
      MeshMaterialList { // Plane material list
        1;
        2;
        0,
        0;
        Material Material {
           0.640000; 0.640000; 0.640000; 1.000000;;
           96.078431;
           0.500000; 0.500000; 0.500000;;
           0.000000; 0.000000; 0.000000;;
          TextureFilename {"AttackEffect_1.png";}
        }
      } // End of Plane material list
    } // End of Plane mesh
  } // End of Plane
  Frame Plane_001 {
    FrameTransformMatrix {
      -0.000000,-0.000000, 0.500000, 0.000000,
       0.000000,-0.500000, 0.000000, 0.000000,
       0.000000, 0.000000, 0.000000, 0.000000,
       0.000000, 0.000000, 0.000000, 1.000000;;
    }
    Mesh { // Plane_001 mesh
      6;
       1.000000;-1.000000; 0.778703;,
      -1.000000; 1.000000; 0.778703;,
      -1.000000;-1.000000; 0.778703;,
       1.000000;-1.000000; 0.778703;,
       1.000000; 1.000000; 0.778703;,
      -1.000000; 1.000000; 0.778703;;
      2;
      3;2,1,0;,
      3;5,4,3;;
      MeshNormals { // Plane_001 normals
        2;
        -0.000000; 0.000000; 1.000000;,
        -0.000000; 0.000000; 1.000000;;
        2;
        3;0,0,0;,
        3;1,1,1;;
      } // End of Plane_001 normals
      MeshTextureCoords { // Plane_001 UV coordinates
        6;
         0.000000; 1.000000;,
         1.000000; 0.000000;,
         0.000000; 0.000000;,
         0.000000; 1.000000;,
         1.000000; 1.000000;,
         1.000000; 0.000000;;
      } // End of Plane_001 UV coordinates
      MeshMaterialList { // Plane_001 material list
        1;
        2;
        0,
        0;
        Material Material {
           0.640000; 0.640000; 0.640000; 1.000000;;
           96.078431;
           0.500000; 0.500000; 0.500000;;
           0.000000; 0.000000; 0.000000;;
          TextureFilename {"AttackEffect_1.png";}
        }
      } // End of Plane_001 material list
    } // End of Plane_001 mesh
  } // End of Plane_001
} // End of Root
