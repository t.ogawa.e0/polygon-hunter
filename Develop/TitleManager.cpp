//==========================================================================
// タイトルマネージャー [TitleManager.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "TitleManager.h"
#include "Screen.h" // デバッグ用
#include "InputSystem.h"
#include "KeyboardObject.h"

TitleManager::TitleManager()
{
    this->SetObjectName("TitleManager");
}

TitleManager::~TitleManager()
{
}

//=========================================================================
// 初期化
bool TitleManager::Init(void)
{
    this->m_KeyboardObject = this->GetObjects(mslib::DX9_ObjectID::Default, "KeyboardObject");
    this->m_InputSystem = this->GetObjects(mslib::DX9_ObjectID::Default, "InputSystem");

    return false;
}

//=========================================================================
// 解放
void TitleManager::Uninit(void)
{
}

//=========================================================================
// 更新
void TitleManager::Update(void)
{
    auto * pKeyboardObject = (KeyboardObject*)this->m_KeyboardObject;
    auto * pInputSystem = (CInputSystem*)this->m_InputSystem;
    bool bkey = false;

    if (mslib::nullptr_check(pKeyboardObject))
    {
        if (pKeyboardObject->KEY_Q(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_W(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_E(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_A(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_S(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_D(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_U(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_I(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_O(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_J(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_K(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_L(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
        else if (pKeyboardObject->KEY_SPACE(KeyboardObject::KeyboardType::Trigger))
        {
            bkey = true;
        }
    }

    if (mslib::nullptr_check(pInputSystem))
    {
        int value = 0;
        auto * pXinput = pInputSystem->GetXInput();
        if (pXinput->Check(value))
        {
            if (pXinput->AnalogL(value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogLTrigger(mslib::XInputAnalog::UP, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogLTrigger(mslib::XInputAnalog::DOWN, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogLTrigger(mslib::XInputAnalog::LEFT, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogLTrigger(mslib::XInputAnalog::RIGHT, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogR(value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogRTrigger(mslib::XInputAnalog::UP, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogRTrigger(mslib::XInputAnalog::DOWN, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogRTrigger(mslib::XInputAnalog::LEFT, value))
            {
                bkey = true;
            }
            else if (pXinput->AnalogRTrigger(mslib::XInputAnalog::RIGHT, value))
            {
                bkey = true;
            }
            else if (pXinput->LT(value))
            {
                bkey = true;
            }
            else if (pXinput->RT(value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::A, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::B, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::BACK, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::DPAD_DOWN, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::DPAD_LEFT, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::DPAD_RIGHT, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::DPAD_UP, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::LEFT_LB, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::LEFT_THUMB, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::RIGHT_RB, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::RIGHT_THUMB, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::START, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::X, value))
            {
                bkey = true;
            }
            else if (pXinput->Trigger(mslib::XInputButton::Y, value))
            {
                bkey = true;
            }
        }
    }

    if (bkey == true)
    {
        CScreen::screenchange(SceneName::Tutorial);
    }
}

//=========================================================================
// 描画
void TitleManager::Draw(void)
{
}

//=========================================================================
// デバッグ
void TitleManager::Debug(void)
{
}
