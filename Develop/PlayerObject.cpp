//==========================================================================
// プレイヤーのオブジェクト[PlayerObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "PlayerObject.h"
#include "GameField.h"
#include "Gravity.h"
#include "GameCamera.h"
#include "InputSystem.h"
#include "PlayerAttackEffect_A.h"
#include "StatusGage.h"
#include "KeyboardObject.h"
#include "GameICODrawObject.h"
#include "HighGround.h"
#include "resource_list.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int Consumption_SP = 30;
constexpr int Consumption_Move_SP = 2;

CPlayerObject::CPlayerObject() : CharacterObject(mslib::DX9_ObjectID::Cube)
{
    this->SetObjectName("PlayerObject");
    this->m_Field = nullptr;
    this->m_gravity = nullptr;
    this->m_GameCamera = nullptr;
    this->m_input = nullptr;
    this->m_GameICODrawObject = nullptr;
    this->m_KeyboardObject = nullptr;
    this->m_HighGround = nullptr;
    this->m_HitArea = 0;
    this->m_special_count = 0;
}

CPlayerObject::~CPlayerObject()
{
    this->Uninit();
}

//==========================================================================
// 初期化
bool CPlayerObject::Init(void)
{
    //==========================================================================
    // オブジェクト取得
    //==========================================================================
    this->m_gravity = this->GetObjects(mslib::DX9_ObjectID::Default, "Gravity");
    this->m_Field = this->GetObjects(mslib::DX9_ObjectID::Field, "GameField");
    this->m_GameCamera = this->GetObjects(mslib::DX9_ObjectID::Camera, "GameCamera");
    this->m_input = this->GetObjects(mslib::DX9_ObjectID::Default, "InputSystem");
    this->m_KeyboardObject = this->GetObjects(mslib::DX9_ObjectID::Default, "KeyboardObject");
    this->m_StatusObj = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "StatusGage");
    this->m_GameICODrawObject = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "GameICODrawObject");
    this->m_HighGround = this->GetObjects(mslib::DX9_ObjectID::Mesh, "HighGround");

    auto * pHighGround = (HighGround*)this->m_HighGround;

    this->_3DObject()->Reserve((int)ObjectType::End);

    // 以下、二つのオブジェクトは同一である
    for (int i = 0; i < (int)ObjectType::End; i++)
    {
        auto *pObj = this->_3DObject()->Create();
        pObj->Init(0);

        if (mslib::nullptr_check(pHighGround))
        {
            pObj->SetMatInfoPos(*pHighGround->_3DObject()->Get(0)->GetMatInfoPos());
        }

        pObj->SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);
    }

    // 可視化オブジェクトのみ登録
    this->_3DCube()->ObjectInput(this->_3DObject()->Get((int)ObjectType::Visualization));

    auto * p_gra = (CGravity*)this->m_gravity;

    // 不可視化オブジェクトを渡す
    this->m_fil_pos = *this->_3DObject()->Get((int)ObjectType::Invisible)->GetMatInfoPos();
    if (mslib::nullptr_check(p_gra))
    {
        p_gra->SetGravity(this->_3DObject()->Get((int)ObjectType::Invisible), &this->m_fil_pos);
    }

    return this->_3DCube()->Init(RESOURCE_player_box_png);
}

//==========================================================================
// 解放
void CPlayerObject::Uninit(void)
{
}

//==========================================================================
// 更新
void CPlayerObject::Update(void)
{
    auto * pObj1 = this->_3DObject()->Get((int)ObjectType::Invisible);
    auto * pObj2 = this->_3DObject()->Get((int)ObjectType::Visualization);
    auto * pObj3 = this->_3DObject()->Get((int)ObjectType::OldObject);

    // 古い座標を登録
    pObj3->SetMatInfoPos(*pObj1->GetMatInfoPos());

    // アクション
    this->Action();

    // フィールドとのコリジョン
    this->FieldCollision();

    // カメラに追従情報を教える
    auto * p_camera = (CGameCamera*)this->m_GameCamera;
    if (mslib::nullptr_check(p_camera))
    {
        // サードパーソン 不可視化オブジェクトを渡す
        p_camera->SetTarget(0, this->_3DObject()->Get((int)ObjectType::Invisible), { 0.5f,0.5f,0.0f });
    }

    // 可視化オブジェクトと不可視化オブジェクトとの座標統合
    pObj2->SetMatInfoPos(*pObj1->GetMatInfoPos());

    this->_3DCube()->Update();
}

//==========================================================================
// 描画
void CPlayerObject::Draw(void)
{
    this->_3DCube()->Draw();
}

//==========================================================================
// デバッグ
void CPlayerObject::Debug(void)
{

}

//==========================================================================
// 判定時の処理
void CPlayerObject::ProcessAtDecision(void)
{
    auto * pObj1 = this->_3DObject()->Get((int)ObjectType::Invisible);
    auto * pObj2 = this->_3DObject()->Get((int)ObjectType::Visualization);
    auto * pObj3 = this->_3DObject()->Get((int)ObjectType::OldObject);

    auto *vec1 = pObj1->GetMatInfoPos();
    auto *vec2 = pObj2->GetMatInfoPos();
    auto *vec3 = pObj3->GetMatInfoPos();

    pObj1->SetMatInfoPos({ vec3->x,vec1->y,vec3->z });
    pObj2->SetMatInfoPos({ vec3->x,vec2->y,vec3->z });
}

//==========================================================================
// 地面のセット
void CPlayerObject::SetGround(const D3DXVECTOR3 & vec)
{
    this->m_fil_pos = vec;
}

//==========================================================================
// フィールドとのコリジョン
void CPlayerObject::FieldCollision(void)
{
    auto * p_fil = (CGameField*)this->m_Field;
    auto * pObj1 = this->_3DObject()->Get((int)ObjectType::Invisible);
    auto * pObj3 = this->_3DObject()->Get((int)ObjectType::OldObject);
    auto obj_pos1 = pObj1->GetMatInfoPos();
    auto obj_pos3 = pObj3->GetMatInfoPos();

    // 古い座標と一致しない場合
    if (*obj_pos1 != *obj_pos3)
    {
        if (mslib::nullptr_check(p_fil))
        {
            this->m_fil_pos = p_fil->FieldHeight(0, *obj_pos1, this->m_HitArea);
        }
    }

    if ((this->m_fil_pos.x == 0.0f) && (this->m_fil_pos.y == 0.0f) && (this->m_fil_pos.z == 0.0f))
    {
        this->ProcessAtDecision();
    }
}

//==========================================================================
// ジャンプ
void CPlayerObject::Jump(void)
{
    auto * p_obj = this->_3DObject()->Get((int)ObjectType::Invisible);
    auto obj_pos = p_obj->GetMatInfoPos();
    auto * p_xinput = (CInputSystem*)this->m_input;
    auto * p_dinput = (KeyboardObject*)this->m_KeyboardObject;
    auto *pStatusGage = (StatusGage*)this->m_StatusObj;
    bool bkey = false;
    auto *pGameICODrawObject = (GameICODrawObject*)this->m_GameICODrawObject;
    float fBuf1 = 1.0f; // ジャンプバフ
    float fBuf2 = 0.0f; // 追加ジャンプバフ

    // 速度バフを取り出す
    if (mslib::nullptr_check(pGameICODrawObject))
    {
        fBuf2 = pGameICODrawObject->GetBuffData(GameICODrawSystemID::MoveSpeed)->m_buff;
    }

    // xinputでの操作
    if (mslib::nullptr_check(p_xinput) && bkey == false)
    {
        if (p_xinput->GetXInput()->Check(0))
        {
            bkey = p_xinput->GetXInput()->Trigger(mslib::XInputButton::A, 0);
        }
    }

    // direct_inputでの操作
    if (mslib::nullptr_check(p_dinput) && bkey == false)
    {
        bkey = p_dinput->KEY_SPACE(KeyboardObject::KeyboardType::Trigger);
    }

    if (0 < pStatusGage->GetStatusData()->m_SP)
    {
        p_obj->MoveY(this->_Jump(this->m_fil_pos.y, obj_pos->y, fBuf1 + fBuf2, bkey));
    }

    if (bkey == true)
    {
        auto oldsp = pStatusGage->GetStatusData()->m_SP;
        if (!pStatusGage->SP_Operation(-Consumption_SP))
        {
            pStatusGage->SP_Operation(oldsp);
        }
    }
}

//==========================================================================
// 移動
void CPlayerObject::Move(void)
{
    D3DXVECTOR3 vposL;
    auto * p_obj = this->_3DObject()->Get((int)ObjectType::Invisible);
    auto * p_xinput = (CInputSystem*)this->m_input;
    auto * p_dinput = (KeyboardObject*)this->m_KeyboardObject;
    auto *pStatusGage = (StatusGage*)this->m_StatusObj;
    auto *pGameICODrawObject = (GameICODrawObject*)this->m_GameICODrawObject;
    float fBuf1 = 0.1f; // 速度バフ
    float fBuf2 = 0.0f; // 追加速度バフ
    bool bbkey = false;

    // 速度バフを取り出す
    if (mslib::nullptr_check(pGameICODrawObject))
    {
        fBuf2 = pGameICODrawObject->GetBuffData(GameICODrawSystemID::MoveSpeed)->m_buff;
    }

    // xinputでの操作
    if (mslib::nullptr_check(p_xinput))
    {
        if (p_xinput->GetXInput()->Check(0))
        {
            if (this->GetPlayKey())
            {
                bbkey = p_xinput->GetXInput()->Press(mslib::XInputButton::RIGHT_RB, 0);

                // ダッシュ
                if (bbkey)
                {
                    fBuf1 *= 2;
                }
            }

            // 移動
            if (p_xinput->GetXInput()->AnalogL(0, vposL))
            {
                if (bbkey)
                {
                    auto oldsp = pStatusGage->GetStatusData()->m_SP;
                    if (!pStatusGage->SP_Operation(-Consumption_Move_SP))
                    {
                        pStatusGage->SP_Operation(oldsp);
                        fBuf1 = 0.1f;
                    }
                }
                p_obj->MoveZ(vposL.z*(fBuf1 + fBuf2));
                p_obj->MoveX(vposL.x*(fBuf1 + fBuf2));
                this->Correction();
            }
        }
    }

    // direct_inputでの操作
    if (mslib::nullptr_check(p_dinput))
    {
        bbkey = this->Dinput_Mouse()->Press(mslib::DirectInputMouseButton::Wheel);
        if (this->GetPlayKey())
        {
            // ダッシュ
            if (bbkey)
            {
                fBuf1 *= 2;
                this->m_special_count++;
            }
            else
            {
                this->m_special_count = 0;
            }
        }

        auto oldsp = pStatusGage->GetStatusData()->m_SP;
        vposL = D3DXVECTOR3(1, 1, 1)*(fBuf1 + fBuf2);
        if (p_dinput->KEY_W(KeyboardObject::KeyboardType::Press))
        {
            if (bbkey)
            {
                if (!pStatusGage->SP_Operation(-Consumption_Move_SP))
                {
                    pStatusGage->SP_Operation(oldsp);
                    fBuf1 = 0.1f;
                }
            }
            p_obj->MoveZ(vposL.z);
            this->Correction();
        }
        if (p_dinput->KEY_S(KeyboardObject::KeyboardType::Press))
        {
            if (bbkey)
            {
                if (!pStatusGage->SP_Operation(-Consumption_Move_SP))
                {
                    pStatusGage->SP_Operation(oldsp);
                    fBuf1 = 0.1f;
                }
            } 
            p_obj->MoveZ(-vposL.z);
            this->Correction();
        }
        if (p_dinput->KEY_A(KeyboardObject::KeyboardType::Press))
        {
            if (bbkey)
            {
                if (!pStatusGage->SP_Operation(-Consumption_Move_SP))
                {
                    pStatusGage->SP_Operation(oldsp);
                    fBuf1 = 0.1f;
                }
            }
            p_obj->MoveX(-vposL.x);
            this->Correction();
        }
        if (p_dinput->KEY_D(KeyboardObject::KeyboardType::Press))
        {
            if (bbkey)
            {
                if (!pStatusGage->SP_Operation(-Consumption_Move_SP))
                {
                    pStatusGage->SP_Operation(oldsp);
                    fBuf1 = 0.1f;
                }
            }
            p_obj->MoveX(vposL.x);
            this->Correction();
        }
    }
}

//==========================================================================
// ステップ
bool CPlayerObject::Step(void)
{
    auto * p_obj = this->_3DObject()->Get((int)ObjectType::Invisible);
    bool bkey = false;
    auto * p_xinput = (CInputSystem*)this->m_input;
    auto *pStatusGage = (StatusGage*)this->m_StatusObj;
    bool bstep = false;
    D3DXVECTOR3 vset;
    auto vposL = D3DXVECTOR3(0, 0, 0);
    auto *pGameICODrawObject = (GameICODrawObject*)this->m_GameICODrawObject;
    float fBuf1 = 2.0f; // ステップバフ
    float fBuf2 = 0.0f; // 追加ステップバフ

    if (this->GetPlayKey())
    {
        // 速度バフを取り出す
        if (mslib::nullptr_check(pGameICODrawObject))
        {
            fBuf2 = pGameICODrawObject->GetBuffData(GameICODrawSystemID::MoveSpeed)->m_buff;
        }

        // xinputでの操作
        if (mslib::nullptr_check(p_xinput))
        {
            if (p_xinput->GetXInput()->Check(0))
            {
                p_xinput->GetXInput()->AnalogL(0, vposL);
                bkey = p_xinput->GetXInput()->Trigger(mslib::XInputButton::B, 0);
            }
        }

        // direct_inputでの操作
        if (this->m_special_count < 10)
        {
            if (this->Dinput_Mouse()->Release(mslib::DirectInputMouseButton::Wheel))
            {
                vposL =  *this->_3DObject()->Get((int)ObjectType::Visualization)->GetMatInfoPos();
                bkey = true;
            }
        }

        if (bkey == true)
        {
            if (this->_GetKey() == false)
            {
                auto oldsp = pStatusGage->GetStatusData()->m_SP;
                if (!pStatusGage->SP_Operation(-Consumption_SP))
                {
                    pStatusGage->SP_Operation(oldsp);
                    bkey = false;
                }
            }
        }

        if (0 < pStatusGage->GetStatusData()->m_SP)
        {
            bstep = this->_Step(fBuf1 + fBuf2, bkey, p_obj, &vposL);
        }
    }

    return bstep;
}

//==========================================================================
// アクション
void CPlayerObject::Action(void)
{
    if (this->GetPlayKey())
    {
        this->Jump();
    }

    if (this->Step() != true)
    {
        this->Move();
    }

    auto * p_camera = (CGameCamera*)this->m_GameCamera;
    auto * p_obj = this->_3DObject()->Get((int)ObjectType::Invisible);

    // 向きをカメラベクトルに合わせます
    if (mslib::nullptr_check(p_camera))
    {
        auto v_vec_rot = p_camera->GetCamera(0)->GetVECTOR(mslib::DX9_CameraVectorList::VECFRONT);
        v_vec_rot.y = 0.0f;
        p_obj->RotX(v_vec_rot, 0.5f);
    }
}

//==========================================================================
// 補正
void CPlayerObject::Correction(void)
{
    auto * pObj1 = this->_3DObject()->Get((int)ObjectType::Invisible);
    auto * pObj2 = this->_3DObject()->Get((int)ObjectType::Visualization);
    for (int i = 0; i < 5; i++)
    {
        pObj2->LockOn(*pObj1, 1.0f);
    }
}
