//==========================================================================
// エネミーオブジェクト[EnemyObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "DamageObject.h"
#include "CharacterObject.h"

//==========================================================================
//
// class  : EnemyParam
// Content: エネミーのステータス
//
//=========================================================================
class EnemyParam : public DamageObjectParam
{
public:
    EnemyParam();
    ~EnemyParam();
    // 判定時の処理
    void ProcessAtDecision(void);
public:
    mslib::DX9_3DObject *m_target; // ターゲット
    mslib::DX9_3DObject m_old_obj; // 古いオブジェクト座標
    mslib::Timer m_timer; // タイマー
    mslib::DX9_Collision m_collision; // コリジョン
    mslib::DX9_3DObject m_SearchRangeCenter; // 索敵範囲の中心
    mslib::rand_int m_rand_int; // 乱数
    D3DXVECTOR3 m_fil_pos; // フィールド上の座標格納
    CJump m_Jump; // ジャンプ
    CStep m_Step; // ステップ
    int m_HitArea; // 当たったエリア
    bool m_recognition; // 認識
    bool m_vigilance; // 警戒
    int m_Life; // ライフ
};

//==========================================================================
//
// class  : EnemyInputSystem
// Content: EnemyInputSystem
//
//=========================================================================
class EnemyInputSystem
{
public:
    EnemyInputSystem();
    virtual ~EnemyInputSystem();
    // エネミーのパラメータ
    std::list<EnemyParam> * GetEnemyParam(void);
    // 生成
    virtual void Create(void) = 0;
    // サイズ
    int Size(void);
protected:
    std::list<EnemyParam> m_data; // エネミーのパラメータ
    mslib::rand_float m_float_rand; // 乱数
};

class CEnemyObject : public mslib::DX9_Object
{
public:
    enum class MyEnumClass
    {
        Small,
        Boss,
    };
public:
    CEnemyObject();
    ~CEnemyObject();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    std::unordered_map<MyEnumClass, std::list<EnemyParam>*> *GetEnemyObjectPos(void);
private:
    std::unordered_map<MyEnumClass, std::list<EnemyParam>*> m_object_data; // エネミーのパラメータ
};
