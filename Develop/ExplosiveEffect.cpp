//==========================================================================
// 爆発効果[ExplosiveEffect.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ExplosiveEffect.h"
#include "GameCamera.h"
#include "resource_list.h"

//=========================================================================
// 定数定義
//=========================================================================
constexpr float __scale = 5.0f;

ExplosiveEffect::ExplosiveEffect() : DX9_Object(mslib::DX9_ObjectID::Sphere)
{
    this->SetObjectName("ExplosiveEffect");
}

ExplosiveEffect::~ExplosiveEffect()
{
    this->m_effect.clear();
}

//=========================================================================
// 初期化
bool ExplosiveEffect::Init(void)
{
    return this->_3DSphere()->Init(RESOURCE_fire_nucleus_png, 30);
}

//=========================================================================
// 解放
void ExplosiveEffect::Uninit(void)
{
}

//=========================================================================
// 更新
void ExplosiveEffect::Update(void)
{
    // サイズ変更処理
    this->ScaleUpdate();

    // 破棄処理
    this->DeleteProcessing();

    // vector視覚化
    this->SetVector();

    this->_3DSphere()->Update();
}

//=========================================================================
// 描画
void ExplosiveEffect::Draw(void)
{
    this->_3DSphere()->Draw();
}

//=========================================================================
// デバッグ
void ExplosiveEffect::Debug(void)
{
}

//=========================================================================
// 生成
void ExplosiveEffect::Create(const mslib::DX9_3DObject & obj)
{
    this->m_effect.emplace_back(obj);
    auto itr = --this->m_effect.end();
    this->_3DSphere()->ObjectInput(itr->m_obj);
}

//=========================================================================
// 爆発オブジェクトへのアクセス
std::list<ExplosiveEffectData>* ExplosiveEffect::GetExplosiveEffectData(void)
{
    return &this->m_effect;
}

//=========================================================================
    // サイズの変更
void ExplosiveEffect::ScaleUpdate(void)
{
    for (auto itr = this->m_effect.begin(); itr != this->m_effect.end(); ++itr)
    {
        itr->m_obj.RotX(0.5f);
        itr->m_obj.ScalePlus(itr->m_limit_scale*0.05f);
        if (itr->m_limit_scale < itr->m_obj.GetMatInfoSca()->x)
        {
            itr->m_delete = true;
        }
    }
}

//=========================================================================
// 消去処理
void ExplosiveEffect::DeleteProcessing(void)
{
    for (auto itr = this->m_effect.begin(); itr != this->m_effect.end(); )
    {
        if (itr->m_delete == true)
        {
            this->_3DSphere()->ObjectDelete(itr->m_obj);
            itr = this->m_effect.erase(itr);
        }
        else
        {
            ++itr;
        }
    }
}

//=========================================================================
// vectorセット
void ExplosiveEffect::SetVector(void)
{
    for (auto itr = this->m_effect.begin(); itr != this->m_effect.end(); ++itr)
    {
        this->Debug_3DObject(&itr->m_obj);
    }
}

ExplosiveEffectData::ExplosiveEffectData()
{
    this->m_collision = false;
    this->m_delete = false;
}

ExplosiveEffectData::ExplosiveEffectData(const mslib::DX9_3DObject & obj)
{
    this->m_obj.SetMatInfoPos(*obj.GetMatInfoPos());
    this->m_obj.SetMatrixType(mslib::DX9_3DObjectMatrixType::Vector1);
    this->m_limit_scale = obj.GetMatInfoSca()->x * 2;
    this->m_delete = false;
    this->m_collision = false;
}

ExplosiveEffectData::~ExplosiveEffectData()
{
}
