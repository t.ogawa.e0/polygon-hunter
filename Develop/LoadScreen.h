//==========================================================================
// ロードスクリーン[LoadScreen.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CLoadScreen
// Content: ロードスクリーン
//
//==========================================================================
class CLoadScreen : public mslib::DX9_Object
{
private:
    // パラメータ
    class CParam
    {
    public:
        CParam()
        {
            this->m_a = 0;
            this->m_Change = false;
        }
        ~CParam() {}
    public:
        int m_a; // α
        bool m_Change; // change
    };
public:
    CLoadScreen() :DX9_Object(mslib::DX9_ObjectID::SpecialBlock) {}
    ~CLoadScreen() {}

    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:
    // αチェンジ
    void Change(int Speed);
private:
    CParam m_paramload; // パラメータ
    DWORD m_NewTime = 0, m_OldTime = 0; //時間格納
};
