//==========================================================================
// キャラクターオブジェクト[CharacterObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Game_Action_List.h"

//==========================================================================
//
// class  : CharacterObject
// Content: キャラクターオブジェクト
//
//=========================================================================
class CharacterObject : public mslib::DX9_Object
{
private:
    // コピー禁止 (C++11)
    CharacterObject(const CharacterObject &) = delete;
    CharacterObject &operator=(const CharacterObject &) = delete;
public:
    CharacterObject(mslib::DX9_ObjectID ObjectID = mslib::DX9_ObjectID::Default);
    virtual ~CharacterObject();
    // 初期化
    virtual bool Init(void) = 0;
    // 解放
    virtual void Uninit(void) = 0;
    // 更新
    virtual void Update(void) = 0;
    // 描画
    virtual void Draw(void) = 0;
    // デバッグ
    virtual void Debug(void) = 0;
private:
    // フィールドとのコリジョン
    virtual void FieldCollision(void) = 0;
    // ジャンプ
    virtual void Jump(void) = 0;
    // 移動
    virtual void Move(void) = 0;
    // ステップ
    virtual bool Step(void) = 0;
    // アクション
    virtual void Action(void) = 0;
};

