//==========================================================================
// キューブ[DX9_Cube.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Cube.h"

_MSLIB_BEGIN

//==========================================================================
// 定数定義
//==========================================================================
constexpr int nBite = 256; // バイト数
constexpr int nMaxIndex = 36; // 
constexpr int nNumVertex = 24; // 頂点数
constexpr int nNumTriangle = 12; // 三角形の数
constexpr float VCRCT = 0.5f;
constexpr int NumDfaltIndex = 6;

DX9_CubeTexture::DX9_CubeTexture()
{
    this->Direction1 = 0;
    this->Direction2 = 0;
    this->Pattern = 0;
    this->texsize = 0;
    this->texcutsize = 0;
}

DX9_CubeTexture::~DX9_CubeTexture()
{
}

DX9_Cube::DX9_Cube(LPDIRECT3DDEVICE9 pDevice, HWND hWnd)
{
    this->m_texture = new DX9_TextureLoader(pDevice, hWnd);
    this->m_pVertexBuffer = nullptr;
    this->m_pIndexBuffer = nullptr;
    this->m_Device = pDevice;
    this->m_hWnd = hWnd;
    this->m_Lock = false;
}

DX9_Cube::~DX9_Cube()
{
    this->Release();
}

//==========================================================================
/**
@brief 初期化
@param Input [in] 使用するテクスチャのパス ダブルポインタに対応
@param Input [in] 要素数
@return 失敗時 true
*/
bool DX9_Cube::Init(const char** Input, int size)
{
	for (int i = 0; i < size; i++)
	{
		if (this->Init(Input[i]))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
/**
@brief 初期化
@param Input [in] 使用するテクスチャのパス
@param Input [in] 要素数
@return 失敗時 true
*/
bool DX9_Cube::Init(const char * Input)
{
	if (this->m_texture->init(Input))
	{
		return true;
	}

	if (!this->m_Lock)
	{
		CUv<float> aUv[6]; // 各面のUV
		VERTEX_4* pPseudo = nullptr;// 頂点バッファのロック
		WORD* pIndex = nullptr;
        DX9_CubeTexture texparam;

		texparam.Direction1 = 4;
		texparam.Direction2 = 4;
		texparam.Pattern = 6;

		// 画像データの格納
		texparam.texsize = CTexvec<float>(0, 0, 1.0f, 1.0f);
		texparam.texcutsize = CTexvec<float>(0, 0, 0, 0);
		texparam.texcutsize.w = texparam.texsize.w / texparam.Direction1;
		texparam.texcutsize.h = texparam.texsize.h / texparam.Direction2;

		for (int s = 0; s < texparam.Pattern; s++)
		{
			this->SetUV(&texparam, &aUv[s], s);
		}

		if (this->CreateVertexBuffer(this->m_Device, this->m_hWnd, sizeof(VERTEX_4) * nNumVertex, D3DUSAGE_WRITEONLY, this->FVF_VERTEX_4, D3DPOOL_MANAGED, &this->m_pVertexBuffer, nullptr))
		{
			return true;
		}

		if (this->CreateIndexBuffer(this->m_Device, this->m_hWnd, sizeof(WORD) * nMaxIndex, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &this->m_pIndexBuffer, nullptr))
		{
			return true;
		}

		this->m_pVertexBuffer->Lock(0, 0, (void**)&pPseudo, D3DLOCK_DISCARD);
		this->SetCube(pPseudo, aUv);
		this->m_pVertexBuffer->Unlock();	// ロック解除

		this->m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
		this->RectangleIndex(pIndex, nMaxIndex / NumDfaltIndex);
		this->m_pIndexBuffer->Unlock();	// ロック解除

		this->m_Lock = true;
	}

	return false;
}

//==========================================================================
/**
@brief 更新
*/
void DX9_Cube::Update(void)
{
    // 登録済みオブジェクトのデータの更新
    for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
    {
        // 行列がデフォルトの指定の時
        if (itr->m_obj->GetMatrixType() == DX9_3DObjectMatrixType::Default)
        {
            itr->m_obj->SetMatrixType(DX9_3DObjectMatrixType::NotVector);
        }

        // 行列を生成
        itr->m_obj->CreateMtxWorld(itr->MtxWorld);
    }
}

//==========================================================================
/**
@brief 解放
*/
void DX9_Cube::Release(void)
{
    // バッファ解放
    if (this->m_pVertexBuffer != nullptr)
    {
        this->m_pVertexBuffer->Release();
        this->m_pVertexBuffer = nullptr;
    }

    // インデックスバッファ解放
    if (this->m_pIndexBuffer != nullptr)
    {
        this->m_pIndexBuffer->Release();
        this->m_pIndexBuffer = nullptr;
    }

    // テクスチャの解放
    if (this->m_texture != nullptr)
    {
        this->m_texture->Release();
        delete this->m_texture;
        this->m_texture = nullptr;
    }

    this->ObjectRelease();
}

//==========================================================================
/**
@brief 描画
*/
void DX9_Cube::Draw(void)
{
    if (this->m_texture->size() != 0 && this->m_ObjectData.size() != 0)
    {
        // サイズ
        this->m_Device->SetStreamSource(0, this->m_pVertexBuffer, 0, sizeof(VERTEX_4)); // パイプライン

        this->m_Device->SetIndices(this->m_pIndexBuffer);

        // FVFの設定
        this->m_Device->SetFVF(this->FVF_VERTEX_4);

        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            // テクスチャの取得
            auto * pTexList = this->m_texture->get(itr->m_obj->GetIndex());
            if (pTexList != nullptr)
            {
                // マテリアルの設定
                this->m_Device->SetMaterial(&itr->m_obj->GetD3DMaterial9());

                // 行列をセット
                this->m_Device->SetTransform(D3DTS_WORLD, &itr->MtxWorld);

                pTexList->DrawBegin();
                this->m_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, nNumVertex, 0, nNumTriangle);
                pTexList->DrawEnd();
            }
        }
    }
}

//==========================================================================
/**
@brief キューブ生成
@param Output [out] 頂点情報
@param _this [in] UV座標
*/
void DX9_Cube::SetCube(VERTEX_4 * Output, const CUv<float> * _this)
{
	VERTEX_4 vVertex[] =
	{
		// 手前
		{ D3DXVECTOR3(-VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u0,_this[0].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u1,_this[0].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u1,_this[0].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u0,_this[0].v1) }, // 左下

		// 奥
		{ D3DXVECTOR3(-VCRCT,-VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u0,_this[1].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,-VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u1,_this[1].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT, VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u1,_this[1].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT, VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u0,_this[1].v1) }, // 左下

		// 右
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u0,_this[2].v0) }, // 左上
		{ D3DXVECTOR3(-VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u1,_this[2].v0) }, // 右上
		{ D3DXVECTOR3(-VCRCT, VCRCT, VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u1,_this[2].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u0,_this[2].v1) }, // 左下

		// 左
		{ D3DXVECTOR3(VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u0,_this[3].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT, VCRCT, VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u1,_this[3].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u1,_this[3].v1) }, // 右下
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u0,_this[3].v1) }, // 左下

		// 上
		{ D3DXVECTOR3(-VCRCT,VCRCT, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u0,_this[4].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,VCRCT, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u1,_this[4].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,VCRCT,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u1,_this[4].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,VCRCT,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u0,_this[4].v1) }, // 左下

		// 下
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u0,_this[5].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u1,_this[5].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u1,_this[5].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u0,_this[5].v1) }, // 左下
	};

	for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++) { Output[i] = vVertex[i]; }
}

//==========================================================================
/**
@brief UV生成
@param Input [in] 面情報
@param Output [in] UV情報
@param nNum [in] 現在の面
*/
void DX9_Cube::SetUV(const DX9_CubeTexture * Input, CUv<float> * Output, const int nNum)
{
	int patternNum = (nNum / 1) % Input->Pattern; //2フレームに１回	%10=パターン数
	int patternV = patternNum % Input->Direction1; //横方向のパターン
	int patternH = patternNum / Input->Direction2; //縦方向のパターン
	float fTcx = patternV * Input->texcutsize.w; //切り取りサイズ
	float fTcy = patternH * Input->texcutsize.h; //切り取りサイズ

	Output->u0 = fTcx;// 左
	Output->v0 = fTcy;// 上
	Output->u1 = fTcx + Input->texcutsize.w;// 右
	Output->v1 = fTcy + Input->texcutsize.h;//下
}

_MSLIB_END