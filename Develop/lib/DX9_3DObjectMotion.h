//==========================================================================
// 3Dオブジェクトモーション[DX9_3DObjectMotion.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <string>
#include <unordered_map>
#include <list>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"
#include "DX9_3DObject.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_3DObjectMotionData
// Content: モーションデータ
//
//==========================================================================
class DX9_3DObjectMotionData
{
public:
    DX9_3DObjectMotionData();
    DX9_3DObjectMotionData(const D3DXVECTOR3 max_pos, const D3DXVECTOR3 max_rot, float fpowor);
    ~DX9_3DObjectMotionData();

    /**
    @brief 更新
    */
    void Update(void);

    /**
    @brief データの反映
    @param Out [in/out] アニメーション対象オブジェクト
    */
    void ReflectData(DX9_3DObject * Out);

    /**
    @brief リセット
    */
    void Reset(void);

    /**
    @brief 処理終了判定チェック
    @return 処理終了時に true が返ります
    */
    bool Check(void);

    /**
    @brief 移動量の取得
    @return 移動量が返ります
    */
    const D3DXVECTOR3 & GetPos(void);

    /**
    @brief 移動量の登録
    @param Set [in] 移動量
    */
    void SetPos(const D3DXVECTOR3 & Set);

    /**
    @brief 回転量の取得
    @return 回転量の返ります
    */
    const D3DXVECTOR3 & GetRot(void);

    /**
    @brief 回転量の登録
    @param Set [in] 回転量
    */
    void SetRot(const D3DXVECTOR3 & Set);

    /**
    @brief 処理量に登録
    @param Set [in] 処理量
    */
    void SetPowor(float fpowor);

    /**
    @brief 処理量の取得
    @return 処理量
    */
    float GetPowor(void);
private:
    /**
    @brief データの更新
    @param key [in/out] 処理判定キー
    @param none [in] 中間の値
    @param buff [in] 移動バフ
    @param comparison_result [in] min/max
    @param data [in] 加算量
    */
    void SetData(bool &key, float none, float buff, float comparison_result, float data);
private:
    D3DXVECTOR3 m_max_rot; // 回転量
    D3DXVECTOR3 m_max_pos; // 移動量
    D3DXVECTOR3 m_buf_rot; // 現在の移動値 継承用
    D3DXVECTOR3 m_buf_pos; // 現在の回転値 継承用
    D3DXVECTOR3 m_rot; // 現在の移動値
    D3DXVECTOR3 m_pos; // 現在の回転値
    D3DXVECTOR3 m_comparison_result_pos; // min/max
    D3DXVECTOR3 m_comparison_result_rot; // min/max
    CVector3<bool> m_key1; // 処理判定キー
    CVector3<bool> m_key2; // 処理判定キー
    float m_powor; // 処理量
    bool m_setkey1; // 値登録キー1
    bool m_setkey2; // 値登録キー2
};

//==========================================================================
//
// class  : DX9_3DObjectMotionLinearData
// Content: 線形データ
//
//==========================================================================
class DX9_3DObjectMotionLinearData
{
public:
    DX9_3DObjectMotionLinearData();
    DX9_3DObjectMotionLinearData(const std::string & MotionName, int ID, const D3DXVECTOR3 & vpos, const D3DXVECTOR3 & vrot, bool check);
    ~DX9_3DObjectMotionLinearData();

    /**
    @brief 初期化
    */
    void Reset(void);

    /**
    @brief 補間データ 移動量の取得
    @return 移動量
    */
    const D3DXVECTOR3 & GetPos(void);

    /**
    @brief 補間データ 回転量の取得
    @return 回転量
    */
    const D3DXVECTOR3 & GetRot(void);

    /**
    @brief 補間データの保有データ名の取得
    @return データ名
    */
    const std::string & GetMotionName(void);

    /**
    @brief 補間データの保有グループID
    @return グループID
    */
    int GetID(void);

    /**
    @brief 処理判定
    @return 処理終了時に true が返ります
    */
    bool Check(void);
private:
    D3DXVECTOR3 m_vpos; // 補間値
    D3DXVECTOR3 m_vrot; // 補間値
    std::string m_MotionName; // 保有データ名
    int m_ID; // 保有ID
    bool m_check; // 処理判定
};

//==========================================================================
//
// class  : DX9_3DObjectMotion
// Content: 3Dオブジェクトモーション
//
//==========================================================================
class DX9_3DObjectMotion
{
private:
    using AccessStruct = std::unordered_map<int, std::list<DX9_3DObjectMotionData>>;
    using MotionData = std::unordered_map<std::string, AccessStruct>;
    using LinearData = DX9_3DObjectMotionLinearData;
public:
    DX9_3DObjectMotion();
    ~DX9_3DObjectMotion();

    /**
    @brief モーションの生成
    @param MotionName [in] モーション名
    @param ID [in] グループID
    @param param [in] モーションデータ
    */
    void Create(const std::string & MotionName, int ID, DX9_3DObjectMotionData param);

    /**
    @brief モーションの生成
    @param MotionName [in] モーション名
    @param ID [in] グループID
    @param Object [in/out] 対象オブジェクト
    */
    void Play(const std::string & MotionName, int ID, DX9_3DObject * Object);

    /**
    @brief 補間データのセット
    @param linear [in] 補間データ
    */
    void SetLinearData(const LinearData & linear);

    /**
    @brief 補間データの取得
    @return 補間データ
    */
    LinearData * GetLinearData(void);

    /**
    @brief リセット
    */
    void Reset(void);

    /**
    @brief リセット
    @param MotionName [in] モーション名
    */
    void Reset(const std::string & MotionName);

    /**
    @brief リセット
    @param MotionName [in] モーション名
    @param ID [in] グループID
    */
    void Reset(const std::string & MotionName, int ID);

    /**
    @brief モーション名の取得
    @return モーション名
    */
    std::list<std::string> GetMotionName(void);

    /**
    @brief サイズの取得
    @return サイズ
    */
    int size(void);

    /**
    @brief グループサイズ
    @param MotionName [in] モーション名
    @return サイズ
    */
    int size(const std::string & MotionName);

    /**
    @brief グループサイズ
    @param MotionName [in] モーション名
    @param ID [in] グループID
    @return サイズ
    */
    int size(const std::string & MotionName, int ID);

    /**
    @brief 指定アニメーションの処理速度変更
    @param MotionName [in] モーション名
    @param ID [in] グループID
    @param value [in] 処理速度
    */
    void SetPowor(const std::string & MotionName, int ID, float value);

    /**
    @brief 指定アニメーションの処理速度変更
    @param MotionName [in] モーション名
    @param ID [in] グループID
    @param value [in] 処理速度グループ
    */
    void SetPowor(const std::string & MotionName, int ID, const std::vector<float> & value);

    /**
    @brief 指定アニメーションの処理速度取得
    @param MotionName [in] モーション名
    @param ID [in] グループID
    @return 処理速度
    */
    float GetPowor(const std::string & MotionName, int ID);
private:
    MotionData m_motion_data; // モーションデータ
    LinearData m_linear; // 線形
};

_MSLIB_END