//==========================================================================
// タイマー[Timer.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Timer.h"

_MSLIB_BEGIN

Timer::Timer()
{
}

Timer::~Timer()
{
}

//==========================================================================
/**
@brief 初期化
@param Time [in] 秒数を入れてください
@param Comma [in] コンマの値を入れてください 0〜60
*/
void Timer::Init(int Time, int Comma)
{
	this->m_Time.Set(Time, 0);
	this->m_Comma.Set(Comma, 0);
    this->m_MasterComma = this->m_Comma;
    this->m_MasterTime = this->m_Time;
}

//==========================================================================
/**
@brief カウントダウン処理
@return カウンタの終了時に true が返ります
*/
bool Timer::Countdown(void)
{
	if ((this->m_Time.m_Count != 0) || (this->m_Comma.m_Count != 0))
	{
		if (this->m_Time.m_Limit <= this->m_Time.m_Count)
		{
			if (this->m_Comma.m_Count <= this->m_Comma.m_Limit)
			{
				this->m_Time.m_Count--;
				this->m_Comma.m_Count = this->m_Comma.m_Defalt;
			}
			this->m_Comma.m_Count--;
		}
	}

    if ((this->m_Time.m_Count <= 0) && (this->m_Comma.m_Count <= 0))
    {
        return true;
    }

	return false;
}

//==========================================================================
/**
@brief カウント処理
*/
void Timer::Count(void)
{
	this->m_Comma.m_Count++;
	if (this->m_Comma.m_Limit <= this->m_Comma.m_Count)
	{
		this->m_Time.m_Count++;
		this->m_Comma.m_Count = 0;
	}
}

//==========================================================================
/**
@brief 時間の取得
@return 現在の時間
*/
int Timer::GetTime(void)
{
    return this->m_Time.m_Count;
}

//==========================================================================
/**
@brief コンマの取得
@return 現在のコンマ
*/
int Timer::GetComma(void)
{
    return this->m_Comma.m_Count;
}

//==========================================================================
/**
@brief リセット
*/
void Timer::Reset(void)
{
    this->m_Comma = this->m_MasterComma;
    this->m_Time = this->m_MasterTime;
}

TimerData::TimerData()
{
    this->m_Count = 0;
    this->m_Limit = 0;
    this->m_Defalt = 0;
}

TimerData::~TimerData()
{
}

//==========================================================================
/**
@brief セット
@param Count [in] 初期カウンタ
@param Limit [in] カウンタ上限
*/
void TimerData::Set(int Count, int Limit)
{
	if (Count == 0)
	{
		this->m_Count = Count;
		this->m_Limit = 60;
	}
	else
	{
		this->m_Count = Count;
		this->m_Limit = Limit;
	}
	this->m_Defalt = this->m_Count;
}

_MSLIB_END