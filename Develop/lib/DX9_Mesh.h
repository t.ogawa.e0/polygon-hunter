//==========================================================================
// メッシュ[DX9_Mesh.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_Renderer.h"
#include "DX9_SetRender.h"
#include "DX9_3DObject.h"
#include "vector_wrapper.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_MeshData
// Content: 縮退ポリゴンのパラメーター
//
//==========================================================================
class DX9_MeshData
{
public:
    DX9_MeshData();
    ~DX9_MeshData();
public:
    int NumMeshX; // 面の数
    int NumMeshZ; // 面の数
    int VertexOverlap; // 重複する頂点数
    int	NumXVertexWey; // 視覚化されている1列の頂点数
    int	NumZVertex; // 基礎頂点数
    int	NumXVertex; // 基礎頂点数
    int	NumMeshVertex; // 視覚化されている全体の頂点数
    int	MaxPrimitive; // プリミティブ数
    int	MaxIndex; // 最大Index数
    int	MaxVertex; // 最大頂点数
};

//==========================================================================
//
// class  : DX9_MeshParam
// Content: 縮退ポリゴンの情報
//
//==========================================================================
class DX9_MeshParam
{
public:
    DX9_MeshParam();
    ~DX9_MeshParam();
    void Release(void);
public:
    LPDIRECT3DVERTEXBUFFER9 pVertexBuffer; // 頂点バッファ
    LPDIRECT3DINDEXBUFFER9 pIndexBuffer; // インデックスバッファ
    DX9_MeshData Info; // 縮退ポリゴンの情報
};

//==========================================================================
//
// class  : DX9_Mesh
// Content: 縮退ポリゴン
//
//==========================================================================
class DX9_Mesh : public DX9_Renderer<DX9_3DObject>
{
private:
    // コピー禁止 (C++11)
    DX9_Mesh(const DX9_Mesh &) = delete;
    DX9_Mesh &operator=(const DX9_Mesh &) = delete;
public:
    DX9_Mesh(LPDIRECT3DDEVICE9 pDevice, HWND hWnd);
    ~DX9_Mesh();

    /**
    @brief 初期化
    @param Input [in] テクスチャのパス
    @param x [in] 横幅
    @param z [in] 奥行
    @return 失敗時に true が返ります
    */
	bool Init(const char * Input, int x, int z);

    /**
    @brief 初期化
    @param Input [in] テクスチャのパス ダブルポインタに対応
    @param x [in] 横幅
    @param z [in] 奥行
    @return 失敗時に true が返ります
    */
	bool Init(const char ** Input, int Size, int x, int z);

    /**
    @brief 更新
    */
    void Update(void);

    /**
    @brief 解放
    */
	void Release(void);

    /**
    @brief 描画
    */
    void Draw(void);

    /**
    @brief データ数取得
    @return データ数
    */
    int GetNumData(void);

    /**
    @brief メッシュデータの取得
    @param num [in] 管理番号
    @return メッシュデータ
    */
    DX9_MeshData GetMeshData(int num);
private:
    /**
    @brief メッシュ情報の生成
    @param Output [out] データの出力
    @param num [numX] X軸の面の数
    @param num [numZ] Z軸の面の数
    */
	void MeshFieldInfo(DX9_MeshData & Output, const int numX, const int numZ);

    /**
    @brief インデックス情報の生成
    @param Output [out] データの出力
    @param Input [in] メッシュデータ
    */
	void CreateIndex(LPWORD * Output, const DX9_MeshData & Input);

    /**
    @brief バーテックス情報の生成
    @param Output [out] データの出力
    @param Input [in] バーテックス
    */
	void CreateVertex(VERTEX_4 * Output, const DX9_MeshData & Input);
private:
	vector_wrapper<DX9_MeshParam> m_MeshData; // メッシュデータ
};

_MSLIB_END