//==========================================================================
// mslib[mslib.hpp]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <chrono> // c++ 時間演算
#include <codecvt> // c++ 文字コード変換
#include <cstdlib> // c++ 数値変換
#include <locale> // c++ 文字の判定
#include <system_error> // c++ OSエラー
#include <vector> // c++ 動的構造体
#include <list> // c++ 双方向list
#include <map> // c++ 平衡二分木 ソート機能付き
#include <unordered_map> // c++ 平衡二分木 ソート機能無し
#include <iomanip> // c++ 時間
#include <string> // c++ char
#include <random> // c++ rand
#include <fstream> // c++ file
#include <type_traits> // c++ メタ関数
#include <algorithm> // c++ アルゴリズム

//==========================================================================
// namespace
//==========================================================================

#ifndef _MSLIB_BEGIN
#define _MSLIB_BEGIN namespace mslib {
#endif // !_MSLIB_BEGIN

#ifndef _MSLIB
#define _MSLIB mslib::
#endif // !_MSLIB

#ifndef _MSLIB_
#define _MSLIB_ ::mslib::
#endif // !_MSLIB_

#ifndef _MSLIB_END
#define _MSLIB_END }
#endif // !_MSLIB_END

//==========================================================================
// mslib デフォルトの機能
//==========================================================================

_MSLIB_BEGIN
/**
@brief max
@param a [in] 比較対象
@param b [in] 比較対象
@return 大きい方が返ります
*/
template<typename _Ty>
inline _Ty _max(_Ty & a, _Ty & b) {
    return (((a) > (b)) ? (a) : (b));
}
/**
@brief min
@param a [in] 比較対象
@param b [in] 比較対象
@return 小さい方が返ります
*/
template<typename _Ty>
inline _Ty _min(_Ty & a, _Ty & b) {
    return (((a) < (b)) ? (a) : (b));
}
/**
@brief angle_list
*/
namespace angle_list {
    constexpr float angle_0 = 0.00000000f; // 0度
    constexpr float angle_45 = 0.785398185f; // 45度
    constexpr float angle_90 = 1.57079637f; // 90度
    constexpr float angle_135 = 2.35619450f; // 135度
    constexpr float angle_180 = 3.14159274f; // 180度
    constexpr float angle_225 = -2.35619450f; // 225度
    constexpr float angle_270 = -1.57079637f; // 270度
    constexpr float angle_315 = -0.785398185f; // 315度
}
/**
@brief データ要素数
@param p [in] サイズ計測
@return 要素数
*/
template<typename _Ty, size_t p>
inline int _sizeof(const _Ty(&)[p]) {
    return (int)size_t(p);
}
/**
@brief メモリ確保
@param p [in] アドレスを渡す対象
@param size [in] 確保数
@return 先頭アドレス
*/
template<typename _Ty, typename _S>
inline _Ty * _new(_Ty *& p, _S size) {
    p = nullptr;
    p = new _Ty[(int)(size)];
    return p;
}
/**
@brief メモリ確保
@param p [in] アドレスを渡す対象
@return 先頭アドレス
*/
template<typename _Ty>
inline _Ty * _new(_Ty *& p) {
    p = nullptr;
    p = new _Ty[1];
    return p;
}
/**
@brief メモリ解放
@param p [in] delete[]
*/
template<typename _Ty>
inline void _delete(_Ty *& p) {
    if (p != nullptr) {
        delete[]p;
        p = nullptr;
    }
}
/**
@brief デコイ
@param p [in] デコイ化対象
*/
template<typename _Ty>
inline void _decoy(const _Ty & p) {
    (p);
}
/**
@brief 奇数偶数判定
@param p [in] 判定対象
@return 偶数[false]/奇数[true]
*/
template<typename _Ty>
inline bool _even_or_odd(const _Ty & p) {
    return ((((int)(p)) % 2)) ? true : false;
}
/**
@brief メモリ内のデータ破棄
@param p [in] p->Release();
*/
template<typename _Ty>
inline void _release(_Ty *& p) {
    if (p != nullptr) {
        p->Release();
        p = nullptr;
    }
}
/**
@brief null check
@param p [in] チェック対象
@return null ではない場合true
*/
template<typename _Ty>
inline bool nullptr_check(const _Ty * p) {
    return (p != nullptr) ? true : false;
}
/**
@brief bool change
@param p [in] 変更対象
@return 結果
*/
inline bool bool_change(bool & p) {
    return p = p ^ true;
}
template <typename _Ty> constexpr _Ty pi = static_cast<_Ty>(3.141592);
template <typename _Ty> constexpr _Ty gravity = static_cast<_Ty>(9.80);
using rand_int = std::uniform_int_distribution<int>;
using rand_float = std::uniform_real_distribution<float>;

_MSLIB_END