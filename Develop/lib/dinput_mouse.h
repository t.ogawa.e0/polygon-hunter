//==========================================================================
// ダイレクトインプットのマウス[dinput_mouse.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "dinput_manager.h"

_MSLIB_BEGIN

class DirectInputMouseSpeed
{
public:
    DirectInputMouseSpeed();
    ~DirectInputMouseSpeed();
public:
    LONG m_lX; // X軸への速度
    LONG m_lY; // Y軸への速度
    LONG m_lZ; // Z軸への速度
};

// 有効ボタン
enum class DirectInputMouseButton
{
    Left = 0,	// 左クリック
    Right,	// 右クリック
    Wheel,	// ホイール
    MAX,
};

//==========================================================================
//
// class  : DirectInputMouse
// Content: マウス
//
//==========================================================================
class DirectInputMouse : public DirectInput, public DirectInputManager
{
private:
    // コピー禁止 (C++11)
    DirectInputMouse(const DirectInputMouse &) = delete;
    DirectInputMouse &operator=(const DirectInputMouse &) = delete;
public:
    DirectInputMouse();
    ~DirectInputMouse();

    /**
    @brief 初期化
    @param hInstance [in] インスタンスハンドル
    @param hWnd [in] ウィンドウハンドル
    @return 失敗時 true が返ります
    */
    bool Init(HINSTANCE hInstance, HWND hWnd);

    /**
    @brief 更新
    */
    void Update(void)override;

    /**
    @brief Button Cast(int)
    @param cast [in] キャスト対象
    @return キャストされた値が返ります
    */
    DirectInputMouseButton KeyCast(int cast);

    /**
    @brief プレス
    @param key [in] 入力キー
    @return 入力されている場合 true が返ります
    */
    bool Press(DirectInputMouseButton key);

    /**
    @brief トリガー
    @param key [in] 入力キー
    @return 入力されている場合 true が返ります
    */
    bool Trigger(DirectInputMouseButton key);

    /**
    @brief リピート
    @param key [in] 入力キー
    @return 入力されている場合 true が返ります
    */
    bool Repeat(DirectInputMouseButton key);

    /**
    @brief リリース
    @param key [in] 入力キー
    @return 入力されている場合 true が返ります
    */
    bool Release(DirectInputMouseButton key);

    /**
    @brief マウスの速度
    @return マウスの移動速度が返ります
    */
    DirectInputMouseSpeed Speed(void);

    /**
    @brief カーソルの座標
    @return 画面内座標が返ります
    */
    POINT WIN32Cursor(void);

    /**
    @brief 左クリック
    @return 入力されている場合値が返ります
    */
    SHORT WIN32LeftClick(void);

    /**
    @brief 右クリック
    @return 入力されている場合値が返ります
    */
    SHORT WIN32RightClick(void);

    /**
    @brief マウスホイールのホールド判定
    @return 入力されている場合値が返ります
    */
    SHORT WIN32WheelHold(void);
private:
    static BOOL CALLBACK EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, void *pContext);
private: // DirectInput
    DIMOUSESTATE2 m_State; // 入力情報ワーク
    BYTE m_StateTrigger[(int)sizeof(DIMOUSESTATE2::rgbButtons)];	// トリガー情報ワーク
    BYTE m_StateRelease[(int)sizeof(DIMOUSESTATE2::rgbButtons)]; // リリース情報ワーク
    BYTE m_StateRepeat[(int)sizeof(DIMOUSESTATE2::rgbButtons)]; // リピート情報ワーク
    int m_StateRepeatCnt[(int)sizeof(DIMOUSESTATE2::rgbButtons)]; // リピートカウンタ
private: // WindowsAPI
    POINT m_mousePos;
    HWND m_hWnd;
};

_MSLIB_END