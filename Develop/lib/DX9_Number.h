//==========================================================================
// 数字処理[DX9_Number.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <string>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_2DPolygon.h"
#include "DX9_2DObject.h"
#include "mslib_struct.h"
#include "vector_wrapper.h"

_MSLIB_BEGIN

//==========================================================================
// データリスト
//==========================================================================
class DX9_NumberList
{
public:
    DX9_NumberList();
    DX9_NumberList(int digit, int _max, bool zero, bool leftAlignment);
    ~DX9_NumberList();
public:
    int m_Digit; // 桁
    int m_Max; // 上限
    bool m_Zero; // ゼロ描画判定
    bool m_LeftAlignment; // 左寄せ判定
};

class DX9_NumberParam {
public:
    DX9_NumberParam();
    ~DX9_NumberParam();
public:
    DX9_2DObject m_2DPos;
    int count;
};

class DX9_NumberData {
public:
    DX9_NumberData();
    ~DX9_NumberData();
public:
    int index, Frame, Pattern, Direction;
    DX9_NumberList m_list;
    vector_wrapper<DX9_NumberParam>m_param;
    CVector2<float> m_masterv2pos;
    CVector2<float> m_PercentPos; // 位置割合座標
    CColor<int> m_masterv_color;
    float m_rot;
};

//==========================================================================
//
// class  : DX9_Number
// Content: 数字
//
//==========================================================================
class DX9_Number
{
public:
    DX9_Number(LPDIRECT3DDEVICE9 pDevice, HWND hWnd, float scale);
    ~DX9_Number();

    /**
    @brief 初期化
    @param strTexName [in] テクスチャパス
    @param AutoTextureResize [in] テクスチャのサイズ自動調節機能 trueで有効化
    @return 失敗時 trueが返ります
    */
    bool Init(const std::string strTexName, bool AutoTextureResize = false);

    /**
    @brief 数値データのセット
    @param index [in] アクセスデータ
    @param Digit [in] 桁
    @param LeftAlignment [in] 左寄せ有効[trur]/無効[false]
    @param Zero [in] 0埋め有効[trur]/無効[false]
    @param v2pos [in] 始点座標
    */
    void Set(int index, int Digit, bool LeftAlignment, bool Zero, const CVector2<float> v2pos);

    /**
    @brief アニメーション用初期化
    @param index [in] アクセスデータ
    @param Frame [in] 更新フレーム
    @param Pattern [in] アニメーション数
    @param Direction [in] 横一列のアニメーション数
    */
    void SetAnim(int index, int Frame, int Pattern, int Direction);

    /**
    @brief 解放
    */
    void Release(void);

    /**
    @brief 座標の登録
    @param index [in] アクセスデータ
    @param v2pos [in] 始点座標
    */
    void SetPos(int index, const CVector2<float> v2pos);

    /**
    @brief 座標の登録
    @param index [in] アクセスデータ
    @param speed [in] 始点X移動速度
    */
    void MoveXPos(int index, float speed);

    /**
    @brief 座標の登録
    @param index [in] アクセスデータ
    @param speed [in] 始点Y移動速度
    */
    void MoveYPos(int index, float speed);

    /**
    @brief テクスチャサイズ
    @param index [in] アクセスデータ
    @return テクスチャのサイズ
    */
    CTexvec<int> * GetTexSize(int Index);

    /**
    @brief テクスチャの枚数
    @return 枚数
    */
    int GetNumTex(void);

    /**
    @brief テクスチャのサイズ変更
    @param index [in] アクセスデータ
    @param Widht [in] 幅
    @param Height [in] 高さ
    */
    void SetTexSize(int index, int Widht, int Height);

    /**
    @brief テクスチャのスケール変更
    @param index [in] アクセスデータ
    @param scale [in] 入力した値が加算されます
    */
    void SetTexScale(int index, float scale);

    /**
    @brief テクスチャのサイズリセット
    @param index [in] アクセスデータ
    */
    void ResetTexSize(int index);

    /**
    @brief 回転
    @param index [in] アクセスデータ
    @param rot [in] 回転
    */
    void Rotation(int index, float rot);

    /**
    @brief 更新
    @param index [in] アクセスデータ
    @param SetNumber [in] 表示数値
    */
    CVector2<float> Update(int index, int SetNumber = 0);

    /**
    @brief 描画
    @param ADD [in] 加算合成有効[true]/無効[false]
    */
    void Draw(bool ADD = false);

    /**
    @brief 各数字Objectの親オブジェクト座標の取得
    @param index [in] アクセスデータ
    @return 失敗時 nullptrが返ります
    */
    const CVector2<float> * GetMasterPos(int index);

    /**
    @brief データサイズ
    @return サイズが返ります
    */
    int GetSize(void);

    /**
    @brief 割合座標
    @param index [in] アクセスデータ
    @param Proportion [in] 割合
    @param WinSize [in] ウィンドウサイズ
    */
    void SetPercentPos(int index, CVector2<float> & Proportion, CVector2<float> & WinSize);

    /**
    @brief 割合の取得
    @param index [in] アクセスデータ
    @return 割合
    */
    const CVector2<float> * GetPercentPos(int index);

    /**
    @brief マスターカラーのセット
    @param index [in] アクセスデータ
    @param color [in] 親の色
    */
    void SetMasterColor(int index, const CColor<int> & color);

    /**
    @brief マスターカラーの取得
    @param index [in] アクセスデータ
    @return 親の色
    */
    const CColor<int> * GetMasterColor(int index);

    /**
    @brief 2DPolygonの取得
    @return 2DPolygonのインスタンス
    */
    const DX9_2DPolygon * Get2DPolygon(void);
private:
    /**
    @brief max
    @param a [in] 比較対象
    @param b [in] 比較対象
    @return 大きい方が返ります
    */
    template <typename T> inline T Max(T a, T b);

    /**
    @brief min
    @param a [in] 比較対象
    @param b [in] 比較対象
    @return 小さい方が返ります
    */
    template <typename T> inline T Min(T a, T b);
private:
    vector_wrapper<DX9_NumberData>m_Data;
    DX9_2DPolygon * m_poly;
};

//==========================================================================
/**
@brief max
@param a [in] 比較対象
@param b [in] 比較対象
@return 大きい方が返ります
*/
template<typename T>
inline T DX9_Number::Max(T a, T b)
{
    return (((a) > (b)) ? (a) : (b));
}

//==========================================================================
/**
@brief min
@param a [in] 比較対象
@param b [in] 比較対象
@return 小さい方が返ります
*/
template<typename T>
inline T DX9_Number::Min(T a, T b)
{
    return (((a) < (b)) ? (a) : (b));
}

_MSLIB_END