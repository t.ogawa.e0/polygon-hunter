//==========================================================================
// vector_wrapper[vector_wrapper.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <vector>
#include <algorithm>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

_MSLIB_BEGIN

//==========================================================================
//
// class  : vector_wrapper
// Content: std::vector >> vector_wrapper
//
//==========================================================================
template <typename _Ty>
class vector_wrapper
{
private:
    // コピー禁止 (C++11)
    vector_wrapper(const vector_wrapper &) = delete;
    vector_wrapper &operator=(const vector_wrapper &) = delete;
public:
    vector_wrapper() {}
    ~vector_wrapper() {
        this->Release();
    }

    /**
    @brief 解放
    */
    void Release(void) {
        for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr) {
            this->Delete((*itr));
        }
        this->m_list.clear();
        for (auto itr = this->m_list_sub.begin(); itr != this->m_list_sub.end(); ++itr) {
            this->Delete((*itr));
        }
        this->m_list_sub.clear();
    }

    /**
    @brief インスタンスの生成
    @return インスタンス
    */
    _Ty * Create(void) {
        _Ty * Object = nullptr;
        this->m_list.emplace_back(this->New(Object));
        return Object;
    }

    /**
    @brief サブメモリに管理権限を移します
    @param Object [in] 移動インスタンス
    @return インスタンス
    */
    _Ty * GetDelete(_Ty * Object) {
        auto itr = std::find(this->m_list.begin(), this->m_list.end(), Object);
        if (itr != this->m_list.end()) {
            // メインメモリから破棄
            this->m_list.erase(itr);
            // サブメモリに管理権限を持たせる
            this->m_list_sub.emplace_back(Object);
        }
        else if (itr == this->m_list.end()) {
            return nullptr;
        }
        return Object; // 戻り値として返す
    }

    /**
    @brief サブメモリに管理権限を移します
    @param label [in] 管理番号
    @return インスタンス
    */
    _Ty * GetDelete(int label) {
        return this->GetDelete(this->Get(label));
    }


    /**
    @brief メインメモリに管理権限を戻す
    @param Object [in] インスタンス
    */
    void PushBack(_Ty * Object) {
        auto itr = std::find(this->m_list_sub.begin(), this->m_list_sub.end(), Object);
        if (itr != this->m_list_sub.end()) {
            // メインメモリから破棄
            this->m_list_sub.erase(itr);
            // サブメモリに管理権限を持たせる
            this->m_list.emplace_back(Object);
        }
    }

    /**
    @brief 特定のObjectの破棄
    @param Object [in] 破棄するオブジェクトのアドレスを入れてください
    */
    void PinpointRelease(_Ty * Object) {
        auto itr1 = std::find(this->m_list.begin(), this->m_list.end(), Object);
        auto itr2 = std::find(this->m_list_sub.begin(), this->m_list_sub.end(), Object);
        if (itr1 != this->m_list.end()) {
            this->Delete((*itr1));
            this->m_list.erase(itr1);
        }
        if (itr2 != this->m_list_sub.end()) {
            this->Delete((*itr2));
            this->m_list_sub.erase(itr2);
        }
    }

    /**
    @brief 特定のObjectの破棄
    @param Object [in] 破棄する管理IDを入れてください
    */
    void PinpointRelease(int label) {
        this->PinpointRelease(this->Get(label));
    }

    /**
    @brief 管理しているObject数
    @return データ数
    */
    int Size(void) {
        return (int)this->m_list.size();
    }

    /**
    @brief Objectの取得
    @param label [in] 管理IDを入れてください
    @return 取得したい情報
    */
    _Ty * Get(int label) {
        // メモリ領域内の時
        if (0 <= label&&label<this->Size()) {
            return this->m_list[label];
        }
        return nullptr;
    }

    /**
    @brief 現在のキャパシティーの取得
    @return キャパシティー
    */
    int GetCapacity(void) {
        return this->m_list.capacity();
    }

    /**
    @brief 領域予約
    @param _Newcapacity [in] 予約数
    */
    void Reserve(int _Newcapacity) {
        this->m_list.reserve(_Newcapacity);
    }
private:
    /**
    @brief インスタンス生成
    @param Object [in] インスタンス生成対象
    @return インスタンス
    */
    _Ty * New(_Ty *& Object) {
        Object = new _Ty;
        return Object;
    }

    /**
    @brief インスタンスの破棄
    @param Object [in] インスタンス破棄対象
    @return nullptr が返ります
    */
    _Ty * Delete(_Ty *& Object) {
        // 破棄メモリがある時
        if (Object != nullptr) {
            delete Object;
            Object = nullptr;
        }
        return Object;
    }
private:
    std::vector<_Ty*>m_list; // メインメモリ(アドレスで登録)
    std::vector<_Ty*>m_list_sub; // サブメモリ(アドレスで登録)
};

_MSLIB_END