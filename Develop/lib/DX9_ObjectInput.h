//==========================================================================
// DX9_ObjectInput[DX9_ObjectInput.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <list>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_TextureLoader.h"
#include "mslib_struct.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_ObjectInput
// Content: オブジェクトの一括管理
// param _Ty DX9_2DObject or DX9_3DObject
//
//==========================================================================
template<typename _Ty>
class DX9_ObjectInput
{
private:
    // コピー禁止 (C++11)
    DX9_ObjectInput(const DX9_ObjectInput &) = delete;
    DX9_ObjectInput &operator=(const DX9_ObjectInput &) = delete;
private:
    class CObjectAddress : public DX9_VERTEX_3D
    {
    public:
        CObjectAddress() {
            this->m_obj = nullptr;
            D3DXMatrixIdentity(&this->MtxWorld);
        }
        CObjectAddress(_Ty*_this) {
            this->m_obj = _this;
            D3DXMatrixIdentity(&this->MtxWorld);
        }
        ~CObjectAddress() {}
    public:
        _Ty* m_obj; // 3Dオブジェクト
        D3DXMATRIX MtxWorld; // ワールド行列
        CColor<int> color; // 色
        DX9_VERTEX_3D::VERTEX_3 vertex_3[4]; // バーテックス3
    };
protected:
    DX9_ObjectInput() {}
    virtual ~DX9_ObjectInput() {
        this->m_ObjectData.clear();
    }
public:
    /**
    @brief オブジェクト登録
    @param Input [in] オブジェクト
    */
    void ObjectInput(_Ty&Input) {
        this->ObjectInput(&Input);
    }

    /**
    @brief オブジェクト登録
    @param Input [in] オブジェクト
    */
    void ObjectInput(_Ty*Input) {
        this->m_ObjectData.emplace_back(Input);
    }

    /**
    @brief オブジェクトを描画機能から破棄
    @param Input [in] オブジェクト
    */
    void ObjectDelete(_Ty&Input) {
        this->ObjectDelete(&Input);
    }

    /**
    @brief オブジェクトを描画機能から破棄
    @param Input [in] オブジェクト
    */
    void ObjectDelete(_Ty*Input) {
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end();) {
            if (Input == itr->m_obj) {
                itr = this->m_ObjectData.erase(itr);
            }
            else {
                ++itr;
            }
        }
    }

    /**
    @brief 全てのオブジェクトを描画機能から破棄
    */
    void ObjectRelease(void) {
        this->m_ObjectData.clear();
    }
protected:
    std::list<CObjectAddress>m_ObjectData; // オブジェクト
};
_MSLIB_END