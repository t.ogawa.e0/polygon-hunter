//==========================================================================
// ダイレクインプットマネージャー[dinput_manager.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#define DIRECTINPUT_VERSION (0x0800) // DirectInputのバージョン指定
#include<dinput.h>
#pragma comment(lib,"dinput8.lib")
#pragma comment(lib,"dxguid.lib")
#include <list>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DirectInputManager
// Content: DirectInputManager
//
//==========================================================================
class DirectInputManager
{
private:
    // コピー禁止 (C++11)
    DirectInputManager(const DirectInputManager &) = delete;
    DirectInputManager &operator=(const DirectInputManager &) = delete;
protected:
    DirectInputManager();
    virtual ~DirectInputManager();
    /**
    @brief 更新
    */
    virtual void Update(void) = 0;
public:
    /**
    @brief 登録済みデバイスの更新
    */
    static void UpdateAll(void);

    /**
    @brief 登録済みデバイスの破棄
    */
    static void ReleaseALL(void);

    /**
    @brief デバイスの生成
    @param インスタンス生成対象
    @return インスタンス
    */
    template <typename T> static T* NewDirectInput(T*& p);
private:
    static std::list<DirectInputManager*> m_DirectInput; // デバイスの格納
};

//==========================================================================
/**
@brief デバイスの生成
@param インスタンス生成対象
@return インスタンス
*/
template<typename T>
inline T * DirectInputManager::NewDirectInput(T *& p)
{
    p = nullptr;
    p = new T;

    return p;
}

//==========================================================================
//
// class  : DirectInput
// Content: DirectInput
//
//==========================================================================
class DirectInput
{
private:
    // コピー禁止 (C++11)
    DirectInput(const DirectInput &) = delete;
    DirectInput &operator=(const DirectInput &) = delete;
public:
    DirectInput();
    virtual ~DirectInput();
private:
    /**
    @brief 解放
    */
    void ReleaseDevice(void);
protected:
    LPDIRECTINPUT8 m_DInput; // DirectInputオブジェクトへのポインタ
    LPDIRECTINPUTDEVICE8 m_DIDevice; // 入力デバイス(キーボード)へのポインタ
};

_MSLIB_END