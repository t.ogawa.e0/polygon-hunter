//==========================================================================
// ダイレクインプットマネージャー[dinput_manager.h]
// author: tatsuya ogawa
//==========================================================================
#include "dinput_manager.h"

_MSLIB_BEGIN

//==========================================================================
// 実体
//==========================================================================
std::list<DirectInputManager*> DirectInputManager::m_DirectInput; // オブジェクト格納

DirectInputManager::DirectInputManager()
{
    this->m_DirectInput.emplace_back(this);
}

DirectInputManager::~DirectInputManager()
{
}

//==========================================================================
/**
@brief 登録済みデバイスの更新
*/
void DirectInputManager::UpdateAll(void)
{
    for (auto itr = m_DirectInput.begin(); itr != m_DirectInput.end(); ++itr)
    {
        (*itr)->Update();
    }
}

//==========================================================================
/**
@brief 登録済みデバイスの破棄
*/
void DirectInputManager::ReleaseALL(void)
{
    for (auto itr = m_DirectInput.begin(); itr != m_DirectInput.end(); ++itr)
    {
        delete (*itr);
    }
    m_DirectInput.clear();
}

DirectInput::DirectInput()
{
    this->m_DInput = nullptr;
    this->m_DIDevice = nullptr;
}

DirectInput::~DirectInput()
{
    this->ReleaseDevice();
}

//==========================================================================
/**
@brief 解放
*/
void DirectInput::ReleaseDevice(void)
{
    if (this->m_DIDevice != nullptr)
    {// 入力デバイス(キーボード)の開放
     // キーボードへのアクセス権を開放(入力制御終了)
        this->m_DIDevice->Unacquire();

        this->m_DIDevice->Release();
        this->m_DIDevice = nullptr;
    }

    if (this->m_DInput != nullptr)
    {// DirectInputオブジェクトの開放
        this->m_DInput->Release();
        this->m_DInput = nullptr;
    }
}

_MSLIB_END