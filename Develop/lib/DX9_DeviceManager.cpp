//==========================================================================
// デバイスマネージャー[DX9_DeviceManager.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_DeviceManager.h"

_MSLIB_BEGIN

//==========================================================================
// 実体
//==========================================================================
vector_wrapper<DX9_DeviceInitList> DX9_DeviceManager::m_InitList; // 初期化終了情報の格納
DirectInputMouse * DX9_DeviceManager::m_Mouse = nullptr; // DirectInputマウス
DirectInputKeyboard * DX9_DeviceManager::m_Keyboard = nullptr; // DirectInputキーボード
DirectInputController * DX9_DeviceManager::m_Controller = nullptr; // DirectInputコントローラー
DX9_DirectXDevice * DX9_DeviceManager::m_DXDevice = nullptr; // DirectX9デバイス
XAudio2Device * DX9_DeviceManager::m_XAudio2 = nullptr; // XAudio2Device

DX9_DeviceManager::DX9_DeviceManager()
{
    m_InitList.Reserve((int)DX9_DeviceInitList::MAXDevice);
}

DX9_DeviceManager::~DX9_DeviceManager()
{
}

//==========================================================================
/**
@brief 初期化
@param Serect [in] 初期化デバイス
@param hInstance [in] インスタンスハンドル
@param hWnd [in] ウィンドウハンドル
@return 失敗時に true が返ります
*/
bool DX9_DeviceManager::Init(DX9_DeviceInitList Serect, HINSTANCE hInstance, HWND hWnd)
{
    // 重複生成回避用
    for (int i = 0; i < m_InitList.Size(); i++)
    {
        if (*m_InitList.Get(i) == Serect)
        {
            return false;
        }
    }

    // 未初期化の場合初期化作業を行う
    switch (Serect)
    {
    case DX9_DeviceInitList::Mouse:
        *m_InitList.Create() = Serect;
        DirectInputManager::NewDirectInput(m_Mouse);
        m_Mouse->Init(hInstance, hWnd);
        break;
    case DX9_DeviceInitList::keyboard:
        *m_InitList.Create() = Serect;
        DirectInputManager::NewDirectInput(m_Keyboard);
        m_Keyboard->Init(hInstance, hWnd);
        break;
    case DX9_DeviceInitList::Controller:
        *m_InitList.Create() = Serect;
        DirectInputManager::NewDirectInput(m_Controller);
        m_Controller->Init(hInstance, hWnd);
        break;
    case DX9_DeviceInitList::DXDevice:
        *m_InitList.Create() = Serect;
        m_DXDevice = new DX9_DirectXDevice;
        return m_DXDevice->Init();
        break;
    case DX9_DeviceInitList::XAudio2:
        *m_InitList.Create() = Serect;
        m_XAudio2 = new XAudio2Device;
        m_XAudio2->Init(hWnd);
        break;
    default:
        break;
    }
    return false;
}

//==========================================================================
/**
@brief 解放
*/
void DX9_DeviceManager::Release(void)
{
    DirectInputManager::ReleaseALL();
    if (m_DXDevice != nullptr)
    {
        delete m_DXDevice;
        m_DXDevice = nullptr;
    }
    if (m_XAudio2 != nullptr)
    {
        delete m_XAudio2;
        m_XAudio2 = nullptr;
    }
    m_InitList.Release();
}

//==========================================================================
/**
@brief 更新
*/
void DX9_DeviceManager::Update(void)
{
    DirectInputManager::UpdateAll();
}

//==========================================================================
/**
@brief DirectInputマウスの取得
@return DirectInputマウス
*/
DirectInputMouse * DX9_DeviceManager::GetMouse(void)
{
    return m_Mouse;
}

//==========================================================================
/**
@brief DirectInputキーボードの取得
@return DirectInputキーボード
*/
DirectInputKeyboard * DX9_DeviceManager::GetKeyboard(void)
{
    return m_Keyboard;
}

//==========================================================================
/**
@brief DirectInputコントローラーの取得
@return DirectInputコントローラー
*/
DirectInputController * DX9_DeviceManager::GetController(void)
{
    return m_Controller;
}

//==========================================================================
/**
@brief DirectX9デバイスの取得
@return DirectX9デバイス
*/
DX9_DirectXDevice * DX9_DeviceManager::GetDXDevice(void)
{
    return m_DXDevice;
}

_MSLIB_END