//==========================================================================
// list_wrapper[list_wrapper.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <list>
#include <vector>
#include <algorithm>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

_MSLIB_BEGIN

//==========================================================================
//
// class  : list_wrapper
// Content: std::list >> list_wrapper
//
//==========================================================================
template<typename _Ty>
class list_wrapper
{
private:
    // コピー禁止 (C++11)
    list_wrapper(const list_wrapper &) = delete;
    list_wrapper &operator=(const list_wrapper &) = delete;
public:
    list_wrapper() {}
    ~list_wrapper() {
        this->Release();
    }

    /**
    @brief 解放
    */
    void Release(void) {
        for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr) {
            this->Delete((*itr));
        }
        this->m_list.clear();
        for (auto itr = this->m_list_sub.begin(); itr != this->m_list_sub.end(); ++itr) {
            this->Delete((*itr));
        }
        this->m_list_sub.clear();
        this->m_route.clear();
    }

    /**
    @brief インスタンスの生成
    @return インスタンス
    */
    _Ty * Create(void) {
        _Ty * Object = nullptr;
        this->m_list.emplace_back(this->New(Object));
        return Object;
    }

    /**
    @brief 生成したデータの読み込み
    */
    void LoadData(void) {
        this->m_route.clear();
        if (this->m_route.capacity()<this->m_list.size()) {
            this->m_route.reserve(this->m_list.size());
        }
        for (_Ty* pObj : this->m_list) {
            this->m_route.push_back(pObj);
        }
    }

    /**
    @brief サブメモリに管理権限を移します
    @param Object [in] 移動インスタンス
    @return インスタンス
    */
    _Ty * GetDelete(_Ty * Object) {
        auto itr = std::find(this->m_list.begin(), this->m_list.end(), Object);
        if (itr != this->m_list.end()) {
            // メインメモリから破棄
            this->m_list.erase(itr);
            // サブメモリに管理権限を持たせる
            this->m_list_sub.emplace_back(Object);
            this->LoadData();
        }
        else if (itr == this->m_list.end()) {
            return nullptr;
        }
        return Object; // 戻り値として返す
    }

    /**
    @brief サブメモリに管理権限を移します
    @param label [in] 管理番号
    @return インスタンス
    */
    _Ty * GetDelete(int label) {
        return this->GetDelete(this->Get(label));
    }

    /**
    @brief メインメモリに管理権限を戻す
    @param Object [in] インスタンス
    */
    void PushBack(_Ty * Object) {
        auto itr = std::find(this->m_list_sub.begin(), this->m_list_sub.end(), Object);
        if (itr != this->m_list_sub.end()) {
            // メインメモリから破棄
            this->m_list_sub.erase(itr);
            // サブメモリに管理権限を持たせる
            this->m_list.emplace_back(Object);
            this->LoadData();
        }
    }

    /**
    @brief 特定のObjectの破棄
    @param Object [in] 破棄するオブジェクトのアドレスを入れてください
    */
    void PinpointRelease(_Ty * Object) {
        auto itr1 = std::find(this->m_list.begin(), this->m_list.end(), Object);
        auto itr2 = std::find(this->m_list_sub.begin(), this->m_list_sub.end(), Object);
        if (itr1 != this->m_list.end()) {
            this->Delete((*itr1));
            this->m_list.erase(itr1);
            this->LoadData();
        }
        if (itr2 != this->m_list_sub.end()) {
            this->Delete((*itr2));
            this->m_list_sub.erase(itr2);
            this->LoadData();
        }
    }

    /**
    @brief 特定のObjectの破棄
    @param Object [in] 破棄する管理IDを入れてください
    */
    void PinpointRelease(int label) {
        this->PinpointRelease(this->Get(label));
    }

    /**
    @brief 管理しているObject数
    @return データ数
    */
    int Size(void) {
        return (int)this->m_route.size();
    }

    /**
    @brief Objectの取得
    @param label [in] 管理IDを入れてください
    @return 取得したい情報
    */
    _Ty * Get(int label) {
        // メモリ領域内の時
        if (0 <= label&&label<this->Size()) {
            return this->m_route[label];
        }
        return nullptr;
    }
private:
    /**
    @brief インスタンス生成
    @param Object [in] インスタンス生成対象
    @return インスタンス
    */
    _Ty * New(_Ty *& Object) {
        Object = new _Ty;
        return Object;
    }

    /**
    @brief インスタンスの破棄
    @param Object [in] インスタンス破棄対象
    @return nullptr が返ります
    */
    _Ty * Delete(_Ty *& Object) {
        // 破棄メモリがある時
        if (Object != nullptr) {
            delete Object;
            Object = nullptr;
        }
        return Object;
    }
private:
    std::list<_Ty*>m_list; // メインメモリ
    std::list<_Ty*>m_list_sub; // サブメモリ
    std::vector<_Ty*>m_route; // アクセスルート
};

_MSLIB_END