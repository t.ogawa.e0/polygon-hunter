//==========================================================================
// 数字処理[DX9_Number.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Number.h"

_MSLIB_BEGIN

//==========================================================================
// 定数定義
//==========================================================================
constexpr int Pusyu_Score = 10; // 繰り上げ定数

DX9_NumberList::DX9_NumberList()
{
    this->m_Digit = 0;
    this->m_Max = 0;
    this->m_Zero = 0;
    this->m_LeftAlignment = 0;
}

DX9_NumberList::DX9_NumberList(int digit, int _max, bool zero, bool leftAlignment)
{
    this->m_Digit = digit;
    this->m_Max = _max;
    this->m_Zero = zero;
    this->m_LeftAlignment = leftAlignment;
}

DX9_NumberList::~DX9_NumberList()
{
}

DX9_NumberParam::DX9_NumberParam()
{
    this->m_2DPos.Init(0);
    this->count = 0;
}

DX9_NumberParam::~DX9_NumberParam()
{
}

DX9_NumberData::DX9_NumberData()
{
    this->index = this->Frame = this->Pattern = this->Direction = 0;
    this->m_rot = 0.0f;
    this->m_masterv2pos = 0;
    this->m_masterv_color = 255;
    this->m_PercentPos = 0;
}

DX9_NumberData::~DX9_NumberData()
{
}

DX9_Number::DX9_Number(LPDIRECT3DDEVICE9 pDevice, HWND hWnd, float scale)
{
    this->m_poly = new DX9_2DPolygon(pDevice, hWnd, scale);
}

DX9_Number::~DX9_Number()
{
    this->Release();
}

//==========================================================================
/**
@brief 初期化
@param strTexName [in] テクスチャパス
@param AutoTextureResize [in] テクスチャのサイズ自動調節機能 trueで有効化
@return 失敗時 trueが返ります
*/
bool DX9_Number::Init(const std::string strTexName, bool AutoTextureResize)
{
    this->m_Data.Create();
    return this->m_poly->Init(strTexName.c_str(), AutoTextureResize);
}

//==========================================================================
/**
@brief 数値データのセット
@param index [in] アクセスデータ
@param Digit [in] 桁
@param LeftAlignment [in] 左寄せ有効[trur]/無効[false]
@param Zero [in] 0埋め有効[trur]/無効[false]
@param v2pos [in] 始点座標
*/
void DX9_Number::Set(int index, int Digit, bool LeftAlignment, bool Zero, const CVector2<float> v2pos)
{
    auto * pda = this->m_Data.Get(index);
    pda->m_masterv2pos = v2pos;

    int MaxScore = Pusyu_Score;

    for (int i = 1; i < Digit; i++)
    {
        MaxScore *= Pusyu_Score;
    }
    for (int i = 0; i < Digit; i++)
    {
        pda->m_param.Create();
    }
    MaxScore--;

    pda->m_list = DX9_NumberList(Digit, MaxScore, Zero, LeftAlignment);
}

//==========================================================================
/**
@brief アニメーション用初期化
@param index [in] アクセスデータ
@param Frame [in] 更新フレーム
@param Pattern [in] アニメーション数
@param Direction [in] 横一列のアニメーション数
*/
void DX9_Number::SetAnim(int index, int Frame, int Pattern, int Direction)
{
    auto * pdata = this->m_Data.Get(index);
    pdata->index = index;
    pdata->Frame = Frame;
    pdata->Pattern = Pattern;
    pdata->Direction = Direction;

    for (int i = 0; i < pdata->m_param.Size(); i++)
    {
        pdata->m_param.Get(i)->m_2DPos.Init(index, Frame, Pattern, Direction);
    }
}

//==========================================================================
/**
@brief 解放
*/
void DX9_Number::Release(void)
{
    this->m_poly->Release();
    this->m_Data.Release();
    delete this->m_poly;
}

//==========================================================================
/**
@brief 座標の登録
@param index [in] アクセスデータ
@param v2pos [in] 始点座標
*/
void DX9_Number::SetPos(int index, const CVector2<float> v2pos)
{
    this->m_Data.Get(index)->m_masterv2pos = v2pos;
}

//==========================================================================
/**
@brief 座標の登録
@param index [in] アクセスデータ
@param speed [in] 始点X移動速度
*/
void DX9_Number::MoveXPos(int index, float speed)
{
    this->m_Data.Get(index)->m_masterv2pos->x += speed;
}

//==========================================================================
/**
@brief 座標の登録
@param index [in] アクセスデータ
@param speed [in] 始点Y移動速度
*/
void DX9_Number::MoveYPos(int index, float speed)
{
    this->m_Data.Get(index)->m_masterv2pos->y += speed;
}

//==========================================================================
/**
@brief テクスチャサイズ
@param index [in] アクセスデータ
@return テクスチャのサイズ
*/
CTexvec<int>* DX9_Number::GetTexSize(int Index)
{
    return this->m_poly->GetTexSize(Index);
}

//==========================================================================
/**
@brief テクスチャの枚数
@return 枚数
*/
int DX9_Number::GetNumTex(void)
{
    return this->m_poly->GetNumTex();
}

//==========================================================================
/**
@brief テクスチャのサイズ変更
@param index [in] アクセスデータ
@param Widht [in] 幅
@param Height [in] 高さ
*/
void DX9_Number::SetTexSize(int index, int Widht, int Height)
{
    this->m_poly->SetTexSize(index, Widht, Height);
}

//==========================================================================
/**
@brief テクスチャのスケール変更
@param index [in] アクセスデータ
@param scale [in] 入力した値が加算されます
*/
void DX9_Number::SetTexScale(int index, float scale)
{
    this->m_poly->SetTexScale(index, scale);
}

//==========================================================================
/**
@brief テクスチャのサイズリセット
@param index [in] アクセスデータ
*/
void DX9_Number::ResetTexSize(int index)
{
    this->m_poly->ResetTexSize(index);
}

//==========================================================================
/**
@brief 回転
@param index [in] アクセスデータ
@param rot [in] 回転
*/
void DX9_Number::Rotation(int index, float rot)
{
    this->m_Data.Get(index)->m_rot = rot;
}

//==========================================================================
/**
@brief 更新
@param index [in] アクセスデータ
@param SetNumber [in] 表示数値
*/
CVector2<float> DX9_Number::Update(int index, int SetNumber)
{
    auto * pda = this->m_Data.Get(index);

    // nullではないとき
    if (pda != nullptr)
    {
        float fWidht = (float)this->m_poly->GetTexSize(pda->index)->w / pda->Direction;
        int nDigit = pda->m_list.m_Digit;
        CVector2<float> pluspos = pda->m_masterv2pos;

        //左詰め対応
        if (pda->m_list.m_LeftAlignment == true)
        {
            int Set = SetNumber;

            nDigit = 1;
            for (;;)
            {
                Set /= Pusyu_Score;
                if (Set == 0)
                {
                    break;
                }
                nDigit++;
                if (nDigit == pda->m_list.m_Digit)
                {
                    break;
                }
            }
        }

        SetNumber = this->Min(pda->m_list.m_Max, SetNumber);

        if (pda->m_list.m_Zero == true)
        {
            nDigit = pda->m_param.Size();
        }

        for (int i = nDigit; i != 0; i--)
        {
            auto * ppar = pda->m_param.Get(i - 1);

            this->m_poly->ObjectInput(ppar->m_2DPos);

            pluspos.x = pda->m_masterv2pos->x + (i - 1)*fWidht;

            ppar->m_2DPos.AnimationCount(SetNumber%Pusyu_Score);

            // 123456
            ppar->m_2DPos.SetPos(pluspos);

            ppar->m_2DPos.SetAnglePlus(pda->m_rot);

            // 123456
            SetNumber /= Pusyu_Score;

            // 12345.6
            if (pda->m_list.m_Zero == false && SetNumber == 0)
            {
                break;
            }
        }

        pda->m_rot = 0.0f;
        this->m_poly->Update();
        return pluspos;
    }
    return CVector2<float>();
}

//==========================================================================
/**
@brief 描画
@param ADD [in] 加算合成有効[true]/無効[false]
*/
void DX9_Number::Draw(bool ADD)
{
    this->m_poly->Draw(ADD);
    this->m_poly->ObjectRelease();
}

//==========================================================================
/**
@brief 各数字Objectの親オブジェクト座標の取得
@param index [in] アクセスデータ
@return 失敗時 nullptrが返ります
*/
const CVector2<float>* DX9_Number::GetMasterPos(int index)
{
    auto * pda = this->m_Data.Get(index);

    if (pda != nullptr)
    {
        return &pda->m_masterv2pos;
    }

    return nullptr;
}

//==========================================================================
/**
@brief データサイズ
@return サイズが返ります
*/
int DX9_Number::GetSize(void)
{
    return this->m_Data.Size();
}

//==========================================================================
/**
@brief 割合座標
@param index [in] アクセスデータ
@param Proportion [in] 割合
@param WinSize [in] ウィンドウサイズ
*/
void DX9_Number::SetPercentPos(int index, CVector2<float>& Proportion, CVector2<float>& WinSize)
{
    auto * pda = this->m_Data.Get(index);

    if (pda != nullptr)
    {
        pda->m_PercentPos = Proportion;
        pda->m_masterv2pos = WinSize * pda->m_PercentPos;
    }
}

//==========================================================================
/**
@brief 割合の取得
@param index [in] アクセスデータ
@return 割合
*/
const CVector2<float>* DX9_Number::GetPercentPos(int index)
{
    auto * pda = this->m_Data.Get(index);

    if (pda != nullptr)
    {
        return &pda->m_PercentPos;
    }
    return nullptr;
}

//==========================================================================
/**
@brief マスターカラーのセット
@param index [in] アクセスデータ
@param color [in] 親の色
*/
void DX9_Number::SetMasterColor(int index, const CColor<int> & color)
{
    auto * pda = this->m_Data.Get(index);

    if (pda != nullptr)
    {
        pda->m_masterv_color = color;
        for (int i = 0; i < pda->m_param.Size(); i++)
        {
            pda->m_param.Get(i)->m_2DPos.SetColor(pda->m_masterv_color);
        }
    }
}

//==========================================================================
/**
@brief マスターカラーの取得
@param index [in] アクセスデータ
@return 親の色
*/
const CColor<int>* DX9_Number::GetMasterColor(int index)
{
    auto * pda = this->m_Data.Get(index);

    if (pda != nullptr)
    {
        return &pda->m_masterv_color;
    }
    return nullptr;
}

//==========================================================================
/**
@brief 2DPolygonの取得
@return 2DPolygonのインスタンス
*/
const DX9_2DPolygon * DX9_Number::Get2DPolygon(void)
{
    return this->m_poly;
}

_MSLIB_END