//==========================================================================
// メッシュ[DX9_Mesh.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Mesh.h"

_MSLIB_BEGIN

//==========================================================================
// 定数定義
//==========================================================================
constexpr int xInfo = 1;
constexpr int x_2 = 2;
constexpr float CorrectionValue = 0.5f;

DX9_MeshData::DX9_MeshData()
{
    this->NumMeshX = 0;
    this->NumMeshZ = 0;
    this->VertexOverlap = 0;
    this->NumXVertexWey = 0;
    this->NumZVertex = 0;
    this->NumXVertex = 0;
    this->NumMeshVertex = 0;
    this->MaxPrimitive = 0;
    this->MaxIndex = 0;
    this->MaxVertex = 0;
}

DX9_MeshData::~DX9_MeshData()
{
}

DX9_MeshParam::DX9_MeshParam()
{
    this->pVertexBuffer = nullptr;
    this->pIndexBuffer = nullptr;
}

DX9_MeshParam::~DX9_MeshParam()
{
    this->Release();
}

void DX9_MeshParam::Release(void)
{
    // バッファ解放
    if (this->pVertexBuffer != nullptr)
    {
        this->pVertexBuffer->Release();
        this->pVertexBuffer = nullptr;
    }

    // インデックスバッファ解放
    if (this->pIndexBuffer != nullptr)
    {
        this->pIndexBuffer->Release();
        this->pIndexBuffer = nullptr;
    }
}

DX9_Mesh::DX9_Mesh(LPDIRECT3DDEVICE9 pDevice, HWND hWnd)
{
    this->m_texture = new DX9_TextureLoader(pDevice, hWnd);
    this->m_Device = pDevice;
    this->m_hWnd = hWnd;
}

DX9_Mesh::~DX9_Mesh()
{
    this->Release();
}

//==========================================================================
/**
@brief 初期化
@param Input [in] テクスチャのパス
@param x [in] 横幅
@param z [in] 奥行
@return 失敗時に true が返ります
*/
bool DX9_Mesh::Init(const char * Input, int x, int z)
{
	VERTEX_4* pPseudo = nullptr;
	LPWORD* pIndex = nullptr;

	if (this->m_texture->init(Input))
	{
		return true;
	}

    auto * pdepoly = this->m_MeshData.Create();

	// メッシュフィールドの情報算出
	this->MeshFieldInfo(pdepoly->Info, x, z);

	if (this->CreateVertexBuffer(this->m_Device, this->m_hWnd, sizeof(VERTEX_4) * pdepoly->Info.MaxVertex, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &pdepoly->pVertexBuffer, nullptr))
	{
		return true;
	}

	if (this->CreateIndexBuffer(this->m_Device, this->m_hWnd, sizeof(LPWORD) * pdepoly->Info.MaxIndex, D3DUSAGE_WRITEONLY, D3DFMT_INDEX32, D3DPOOL_MANAGED, &pdepoly->pIndexBuffer, nullptr))
	{
		return true;
	}

	pdepoly->pVertexBuffer->Lock(0, 0, (void**)&pPseudo, D3DLOCK_DISCARD);
	this->CreateVertex(pPseudo, pdepoly->Info);
	pdepoly->pVertexBuffer->Unlock();	// ロック解除

	pdepoly->pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
	this->CreateIndex(pIndex, pdepoly->Info);
	pdepoly->pIndexBuffer->Unlock();	// ロック解除

	return false;
}

//==========================================================================
/**
@brief 初期化
@param Input [in] テクスチャのパス ダブルポインタに対応
@param x [in] 横幅
@param z [in] 奥行
@return 失敗時に true が返ります
*/
bool DX9_Mesh::Init(const char ** Input, int Size, int x, int z)
{
	for (int i = 0; i < Size; i++)
	{
		if (this->Init(Input[i], x, z))
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
/**
@brief 更新
*/
void DX9_Mesh::Update(void)
{
    // 登録済みオブジェクトのデータの更新
    for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
    {
        // 行列がデフォルトの指定の時
        if (itr->m_obj->GetMatrixType() == DX9_3DObjectMatrixType::Default)
        {
            itr->m_obj->SetMatrixType(DX9_3DObjectMatrixType::NotVector);
        }

        // 行列の生成
        itr->m_obj->CreateMtxWorld(itr->MtxWorld);
    }
}

//==========================================================================
/**
@brief 解放
*/
void DX9_Mesh::Release(void)
{
    // テクスチャの解放
    if (this->m_texture != nullptr)
    {
        this->m_texture->Release();
        delete this->m_texture;
        this->m_texture = nullptr;
    }
    this->m_MeshData.Release();
    this->ObjectRelease();
}

//==========================================================================
/**
@brief 描画
*/
void DX9_Mesh::Draw(void)
{
    if (this->m_texture->size() != 0 && this->m_ObjectData.size() != 0)
    {
        // FVFの設定
        this->m_Device->SetFVF(this->FVF_VERTEX_4);

        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            auto * pdepoly = this->m_MeshData.Get(itr->m_obj->GetIndex());
            // テクスチャの取得
            auto * pTexList = this->m_texture->get(itr->m_obj->GetIndex());
            if (pTexList != nullptr)
            {
                // サイズ
                this->m_Device->SetStreamSource(0, pdepoly->pVertexBuffer, 0, sizeof(VERTEX_4));	// パイプライン

                this->m_Device->SetIndices(pdepoly->pIndexBuffer);

                // マテリアル情報をセット
                this->m_Device->SetMaterial(&itr->m_obj->GetD3DMaterial9());

                // 各種行列の設定
                this->m_Device->SetTransform(D3DTS_WORLD, &itr->MtxWorld);

                pTexList->DrawBegin();
                this->m_Device->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, pdepoly->Info.MaxVertex, 0, pdepoly->Info.MaxPrimitive);
                pTexList->DrawEnd();
            }
        }
    }
}

//==========================================================================
/**
@brief データ数取得
@return データ数
*/
int DX9_Mesh::GetNumData(void)
{
    return this->m_texture->size();
}

//==========================================================================
/**
@brief メッシュデータの取得
@param num [in] 管理番号
@return メッシュデータ
*/
DX9_MeshData DX9_Mesh::GetMeshData(int num)
{
    return this->m_MeshData.Get(num)->Info;
}

//==========================================================================
/**
@brief メッシュ情報の生成
@param Output [out] データの出力
@param num [numX] X軸の面の数
@param num [numZ] Z軸の面の数
*/
void DX9_Mesh::MeshFieldInfo(DX9_MeshData & Output, const int numX, const int numZ)
{
	Output.NumMeshX = numX;
	Output.NumMeshZ = numZ;
	Output.NumXVertex = numX + 1; // 基礎頂点数
	Output.NumZVertex = numZ + 1; // 基礎頂点数
	Output.MaxVertex = Output.NumXVertex * Output.NumZVertex; // 最大頂点数
	Output.NumXVertexWey = x_2 * Output.NumXVertex; // 視覚化されている1列の頂点数
	Output.VertexOverlap = x_2 * (numZ - 1); // 重複する頂点数
	Output.NumMeshVertex = Output.NumXVertexWey * numZ; // 視覚化されている全体の頂点数
	Output.MaxIndex = Output.NumMeshVertex + Output.VertexOverlap; // 最大Index数
	Output.MaxPrimitive = ((numX * numZ) * x_2) + (Output.VertexOverlap * x_2); // プリミティブ数
}

//==========================================================================
/**
@brief インデックス情報の生成
@param Output [out] データの出力
@param Input [in] メッシュデータ
*/
void DX9_Mesh::CreateIndex(LPWORD * Output, const DX9_MeshData & Input)
{
	for (int i = 0, Index1 = 0, Index2 = Input.NumXVertex, Overlap = 0; i < Input.MaxIndex; Index1++, Index2++)
	{
		// 通常頂点
		Output[i] = (LPWORD)Index2;
		i++;

		// 重複点
		if (Overlap == Input.NumXVertexWey&&i < Input.MaxIndex)
		{
			Output[i] = (LPWORD)Index2;
			i++;
			Overlap = 0;
		}

		// 通常頂点
		Output[i] = (LPWORD)Index1;
		i++;

		Overlap += x_2;

		// 重複点
		if (Overlap == Input.NumXVertexWey&&i < Input.MaxIndex)
		{
			Output[i] = (LPWORD)Index1;
			i++;
		}
	}
}

//==========================================================================
/**
@brief バーテックス情報の生成
@param Output [out] データの出力
@param Input [in] バーテックス
*/
void DX9_Mesh::CreateVertex(VERTEX_4 * Output, const DX9_MeshData & Input)
{
	bool bZ = false;
	bool bX = false;
	float fPosZ = Input.NumMeshZ*CorrectionValue;
	float fPosX = 0.0f;

	for (int iZ = 0; iZ < Input.NumZVertex; iZ++, fPosZ--)
	{
		bX = true;
		fPosX = -(Input.NumMeshX*CorrectionValue);
		for (int iX = 0; iX < Input.NumXVertex; iX++, fPosX++, Output++)
		{
			Output->pos = D3DXVECTOR3(fPosX, 0.0f, fPosZ);
			Output->color = D3DCOLOR_RGBA(255, 255, 255, 255);
			Output->Tex = D3DXVECTOR2((FLOAT)bX, (FLOAT)bZ);
			Output->Normal = D3DXVECTOR3(0, 1, 0);
			bX = bX ^ 1;
		}
		bZ = bZ ^ 1;
	}
}

_MSLIB_END