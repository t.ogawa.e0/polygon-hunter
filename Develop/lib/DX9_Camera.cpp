//==========================================================================
// カメラ[DX9_Camera.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Camera.h"

_MSLIB_BEGIN

DX9_Camera::DX9_Camera()
{
    this->m_Eye = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    this->m_At = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    this->m_Eye2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    this->m_At2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    this->m_Up = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    this->m_VecUp = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    this->m_VecFront = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    this->m_VecRight = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    this->m_ViewPort = nullptr;
}

DX9_Camera::~DX9_Camera()
{
    if (this->m_ViewPort != nullptr)
    {
        delete[]this->m_ViewPort;
        this->m_ViewPort = nullptr;
    }
}

//==========================================================================
/**
@brief 初期化
*/
void DX9_Camera::Init(void)
{
	this->m_Eye = D3DXVECTOR3(0.0f, 3.0f, -10.0f); // 注視点
	this->m_At = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // カメラ座標
	this->m_Eye2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f); // 注視点
	this->m_At2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f); // カメラ座標
	this->m_Up = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // ベクター
	this->m_VecUp = D3DXVECTOR3(0, 1, 0); // 上ベクトル
	this->m_VecFront = D3DXVECTOR3(0, 0, 1); // 前ベクトル
	this->m_VecRight = D3DXVECTOR3(1, 0, 0); //  右ベクトル

	// 前ベクトルの正規化
	D3DXVec3Normalize(&this->m_VecFront, &this->m_VecFront);
	D3DXVec3Normalize(&this->m_VecUp, &this->m_VecUp);
	D3DXVec3Normalize(&this->m_VecRight, &this->m_VecRight);
}

//==========================================================================
/**
@brief 初期化
@param pEye [in] 注視点
@param pAt [in] 座標
*/
void DX9_Camera::Init(const D3DXVECTOR3 & pEye, const D3DXVECTOR3 & pAt)
{
	this->m_Eye = pEye; // 注視点
	this->m_At = pAt; // カメラ座標
	this->m_Eye2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f); // 注視点
	this->m_At2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f); // カメラ座標
	this->m_Up = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // ベクター
	this->m_VecUp = D3DXVECTOR3(0, 1, 0); // 上ベクトル
	this->m_VecFront = D3DXVECTOR3(0, 0, 1); // 前ベクトル
	this->m_VecRight = D3DXVECTOR3(1, 0, 0); //  右ベクトル

	// 前ベクトルの正規化
	D3DXVec3Normalize(&this->m_VecFront, &this->m_VecFront);
	D3DXVec3Normalize(&this->m_VecUp, &this->m_VecUp);
	D3DXVec3Normalize(&this->m_VecRight, &this->m_VecRight);
}

//==========================================================================
/**
@brief 解放
*/
void DX9_Camera::Release(void)
{
	if (this->m_ViewPort != nullptr)
	{
		delete[]this->m_ViewPort;
		this->m_ViewPort = nullptr;
	}
}

//==========================================================================
/**
@brief 更新
@param win_size [in] {x,y}ウィンドウサイズ
@param pDevice [in] デバイス
*/
void DX9_Camera::Update(const CVector2<int> & win_size, LPDIRECT3DDEVICE9 pDevice)
{
    // プロジェクション行列
    D3DXMATRIX aMtxProjection;

    // プロジェクション行列の作成
    // ズームイン、ズームアウトのような物
    D3DXMatrixPerspectiveFovLH(&aMtxProjection, D3DXToRadian(60)/*D3DX_PI/3*/, (float)win_size.x / (float)win_size.y, 0.1f/*ニヤ*/, 1000.0f/*ファー*/);
    pDevice->SetTransform(D3DTS_VIEW, this->CreateView());
    pDevice->SetTransform(D3DTS_PROJECTION, &aMtxProjection);
}

//==========================================================================
/**
@brief 更新 ビューポート mode
@param MtxView [in] ワールド行列
@param ViewPort [in] ビューポート
@param fWidth [in] 幅
@param fHeight [in] 高さ
@param pDevice [in] デバイス
*/
void DX9_Camera::UpdateViewPort(const D3DXMATRIX * MtxView, const D3DVIEWPORT9 * ViewPort, int fWidth, int fHeight, LPDIRECT3DDEVICE9 pDevice)
{
    D3DXMATRIX aMtxProjection; // プロジェクション行列

    // プロジェクション行列の作成
    // ズームイン、ズームアウトのような物
    D3DXMatrixPerspectiveFovLH(&aMtxProjection, D3DXToRadian(60)/*D3DX_PI/3*/, (float)fWidth / (float)fHeight, 0.1f/*ニヤ*/, 1000.0f/*ファー*/);
    pDevice->SetTransform(D3DTS_VIEW, MtxView);
    pDevice->SetTransform(D3DTS_PROJECTION, &aMtxProjection);
    pDevice->SetViewport(ViewPort);
}

//==========================================================================
/**
@brief ビュー行列生成
@return ビュー行列
*/
D3DXMATRIX * DX9_Camera::CreateView(void)
{
	D3DXVECTOR3 Eye; // 注視点
	D3DXVECTOR3 At; // カメラ座標
	D3DXVECTOR3 Up; // ベクター

	Eye = this->m_Eye + this->m_Eye2;
	At = this->m_At + this->m_At2;
	Up = this->m_Up;

	// ビュー変換行列 ,(LH = 左手座標 ,LR = 右手座標)
	D3DXMatrixLookAtLH(&this->m_aMtxView, &Eye, &At, &Up);

	return &this->m_aMtxView;
}

//==========================================================================
/**
@brief 平行処理
@param pVec [in] ベクトル
@param pOut1 [out] 出力1
@param pOut2 [out] 出力2
@param pSpeed [in] 移動速度
*/
void DX9_Camera::CameraMoveXYZ(D3DXVECTOR3 * pVec, D3DXVECTOR3 * pOut1, D3DXVECTOR3 * pOut2, const float * pSpeed)
{
	D3DXVec3Cross(&this->m_VecRight, &this->m_VecUp, &this->m_VecFront); // 外積
	D3DXVec3Normalize(pVec, pVec);
	*pOut1 += (*pVec)*(*pSpeed);
	*pOut2 += (*pVec)*(*pSpeed);
}

//==========================================================================
/**
@brief X軸回転処理
@param pDirection [in] ベクトル
@param pRot [in] 回転情報
@param pOut1 [out] 出力1
@param pOut2 [out] 出力2
@param pRang [in] 回転角度
*/
void DX9_Camera::CameraRangX(D3DXVECTOR3 * pDirection, D3DXMATRIX * pRot, D3DXVECTOR3 * pOut1, D3DXVECTOR3 * pOut2, const float * pRang)
{
	D3DXVec3Cross(&this->m_VecRight, &this->m_VecUp, &this->m_VecFront); // 外積
	D3DXMatrixRotationY(pRot, *pRang); // 回転
	*pDirection = (*pOut1) - (*pOut2); // 向きベクトル
	D3DXVec3TransformNormal(pDirection, pDirection, pRot);
	D3DXVec3TransformNormal(&this->m_VecFront, &this->m_VecFront, pRot);
	D3DXVec3TransformNormal(&this->m_VecRight, &this->m_VecRight, pRot);
	*pOut1 = (*pOut2) + (*pDirection);
}

//==========================================================================
/**
@brief Y軸回転処理
@param pDirection [in] ベクトル
@param pRot [in] 回転情報
@param pOut1 [out] 出力1
@param pOut2 [out] 出力2
@param pRang [in] 回転角度
*/
void DX9_Camera::CameraRangY(D3DXVECTOR3 * pDirection, D3DXMATRIX * pRot, D3DXVECTOR3 * pOut1, D3DXVECTOR3 * pOut2, const float * pRang)
{
	D3DXVec3Cross(&this->m_VecRight, &this->m_VecUp, &this->m_VecFront); // 外積
	D3DXMatrixRotationAxis(pRot, &this->m_VecRight, *pRang); // 回転
	*pDirection = (*pOut1) - (*pOut2); // 向きベクトル
	D3DXVec3TransformNormal(pDirection, pDirection, pRot);
	D3DXVec3TransformNormal(&this->m_VecFront, &this->m_VecFront, pRot);
	D3DXVec3TransformNormal(&this->m_VecRight, &this->m_VecRight, pRot);
	*pOut1 = (*pOut2) + (*pDirection);
}

//==========================================================================
/**
@brief 視点変更
@param pVec [in] ベクトル
@param pOut1 [out] 出力1
@param pOut2 [out] 出力2
@param pSpeed [in] 移動速度
@return 戻り値は 距離
*/
float DX9_Camera::ViewPos(D3DXVECTOR3 * pVec, D3DXVECTOR3 * pOut1, D3DXVECTOR3 * pOut2, const float * pSpeed)
{
	D3DXVec3Cross(&this->m_VecRight, &this->m_VecUp, &this->m_VecFront); // 外積
	D3DXVec3Normalize(pVec, pVec);
	*pOut1 += (*pVec)*(*pSpeed);

	float fDistance = powf
	(
		((pOut1->x) - (pOut2->x))*
		((pOut1->x) - (pOut2->x)) +
		((pOut1->y) - (pOut2->y))*
		((pOut1->y) - (pOut2->y)) +
		((pOut1->z) - (pOut2->z))*
		((pOut1->z) - (pOut2->z)),
		0.5f
	);

	return fDistance;
}

//==========================================================================
/**
@brief 内積
@param pRot [in] 回転情報
@param pRang [in] 回転速度
@return 移動可能範囲なら true
*/
bool DX9_Camera::Restriction(D3DXMATRIX pRot, const float* pRang)
{
	D3DXVECTOR3 dir = D3DXVECTOR3(0, 1.0f, 0); // 単位ベクトル
	float Limit = 0.75f;

	D3DXVECTOR3 VecRight = this->m_VecRight;
	D3DXVECTOR3 VecUp = this->m_VecUp;
	D3DXVECTOR3 VecFront = this->m_VecFront;

	// ベクトルの座標変換
	D3DXVec3Cross(&VecRight, &VecUp, &VecFront); // 外積
	D3DXMatrixRotationAxis(&pRot, &VecRight, *pRang); // 回転
	D3DXVec3TransformNormal(&VecFront, &VecFront, &pRot);
	D3DXVec3TransformNormal(&VecRight, &VecRight, &pRot);
	float fVec3Dot = atanf(D3DXVec3Dot(&VecFront, &dir));

	// 内積
	if (-Limit<fVec3Dot && Limit>fVec3Dot) { return true; }

	return false;
}

//==========================================================================
/**
@brief 視点中心にX軸回転
@param Rang [in] 入れた値が加算されます
*/
void DX9_Camera::RotViewX(float Rang)
{
	D3DXVECTOR3 Direction; // 向き
	D3DXMATRIX RotX; //X回転行列

	// ベクトルの座標変換
	this->CameraRangX(&Direction, &RotX, &this->m_Eye, &this->m_At, &Rang);
}

//==========================================================================
/**
@brief 視点中心にY軸回転
@param Rang [in] 入れた値が加算されます
*/
void DX9_Camera::RotViewY(float Rang)
{
	D3DXVECTOR3 Direction; // 向き
	D3DXMATRIX RotY; //Y回転行列

	// ベクトルの座標変換
	if (this->Restriction(RotY, &Rang))
	{
		this->CameraRangY(&Direction, &RotY, &this->m_Eye, &this->m_At, &Rang);
	}
}

//==========================================================================
/**
@brief カメラ中心にX軸回転
@param Rang [in] 入れた値が加算されます
*/
void DX9_Camera::RotCameraX(float Rang)
{
	D3DXVECTOR3 Direction; // 向き
	D3DXMATRIX RotX; //X回転行列

	// ベクトルの座標変換
	this->CameraRangX(&Direction, &RotX, &this->m_At, &this->m_Eye, &Rang);
}

//==========================================================================
/**
@brief カメラ中心にY軸回転
@param Rang [in] 入れた値が加算されます
*/
void DX9_Camera::RotCameraY(float Rang)
{
	D3DXVECTOR3 Direction; // 向き
	D3DXMATRIX RotY; //Y回転行列

	// ベクトルの座標変換
	if (this->Restriction(RotY, &Rang))
	{
		this->CameraRangY(&Direction, &RotY, &this->m_At, &this->m_Eye, &Rang);
	}
}

//==========================================================================
/**
@brief X軸軸移動
@param Speed [in] 入れた値が加算されます
*/
void DX9_Camera::MoveX(float Speed)
{
	D3DXVECTOR3 vec = this->m_VecRight;
	vec.y = 0;

	this->CameraMoveXYZ(&vec, &this->m_Eye, &this->m_At, &Speed);
}

//==========================================================================
/**
@brief Y軸軸移動
@param Speed [in] 入れた値が加算されます
*/
void DX9_Camera::MoveY(float Speed)
{
	D3DXVECTOR3 vec = this->m_VecUp;
	vec.z = 0;
	vec.x = 0;

	this->CameraMoveXYZ(&vec, &this->m_Eye, &this->m_At, &Speed);
}

//==========================================================================
/**
@brief Z軸軸移動
@param Speed [in] 入れた値が加算されます
*/
void DX9_Camera::MoveZ(float Speed)
{
	D3DXVECTOR3 vec = this->m_VecFront;
	vec.y = 0;

	this->CameraMoveXYZ(&vec, &this->m_Eye, &this->m_At, &Speed);
}

//==========================================================================
/**
@brief [非推奨]Z軸平行移動(Y軸有効)
@param Speed [in] 入れた値が加算されます
*/
void DX9_Camera::MoveZ_2(float Speed)
{
    D3DXVECTOR3 vec = this->m_VecFront;

    this->CameraMoveXYZ(&vec, &this->m_Eye, &this->m_At, &Speed);
}

//==========================================================================
/**
@brief 視点変更
@param Distance [in] 入れた値が加算されます
@return 視点とカメラの距離
*/
float DX9_Camera::DistanceFromView(float Distance)
{
	D3DXVECTOR3 vec = this->m_VecFront;

	return this->ViewPos(&vec, &this->m_Eye, &this->m_At, &Distance);
}

//==========================================================================
/**
@brief 各ベクトルの取得
@param List [in] 取得するベクトルの種類
@return 取得したいベクトル
*/
D3DXVECTOR3 DX9_Camera::GetVECTOR(DX9_CameraVectorList List)
{
	switch (List)
	{
    case DX9_CameraVectorList::VEYE:
        return this->m_Eye;
        break;
    case DX9_CameraVectorList::VAT:
        return this->m_At;
        break;
    case DX9_CameraVectorList::VEYE2:
        return this->m_Eye2;
        break;
    case DX9_CameraVectorList::VAT2:
        return this->m_At2;
        break;
    case DX9_CameraVectorList::VUP:
		return this->m_Up;
		break;
	case DX9_CameraVectorList::VECUP:
		return this->m_VecUp;
		break;
	case DX9_CameraVectorList::VECFRONT:
		return this->m_VecFront;
		break;
	case DX9_CameraVectorList::VECRIGHT:
		return this->m_VecRight;
		break;
	default:
		break;
	}

	return D3DXVECTOR3(0, 0, 0);
}

//==========================================================================
/**
@brief カメラY軸回転情報
@return 内積
*/
float DX9_Camera::GetRestriction_Y(void)
{
    D3DXVECTOR3 dir1 = D3DXVECTOR3(0, 1.0f, 0); // 単位ベクトル
    D3DXVECTOR3 dir2 = D3DXVECTOR3(0, this->m_VecFront.y, 0); // 単位ベクトル

    return atanf(D3DXVec3Dot(&dir2, &dir1)); // 内積
}

//==========================================================================
/**
@brief カメラX軸回転情報
@return 内積
*/
float DX9_Camera::GetRestriction_X(void)
{
    D3DXVECTOR3 dir1 = D3DXVECTOR3(1.0f, 0, 0); // 単位ベクトル
    D3DXVECTOR3 dir2 = D3DXVECTOR3(this->m_VecFront.x, 0, 0); // 単位ベクトル

    return atanf(D3DXVec3Dot(&dir2, &dir1)); // 内積
}

//==========================================================================
/**
@brief カメラZ軸回転情報
@return 内積
*/
float DX9_Camera::GetRestriction_Z(void)
{
    D3DXVECTOR3 dir1 = D3DXVECTOR3(0, 0, 1.0f); // 単位ベクトル
    D3DXVECTOR3 dir2 = D3DXVECTOR3(0, 0, this->m_VecFront.z); // 単位ベクトル

    return atanf(D3DXVec3Dot(&dir2, &dir1)); // 内積
}

//==========================================================================
/**
@brief カメラ座標をセット
@param Eye [in] 注視点
@param At [in] カメラ座標
@param Up [in] ベクター
*/
void DX9_Camera::SetCameraPos(const D3DXVECTOR3 & Eye, const D3DXVECTOR3 & At, const D3DXVECTOR3 & Up)
{
    this->m_Eye = Eye; this->m_At = At; this->m_Up = Up;
}

//==========================================================================
/**
@brief カメラ座標
@param At [in] カメラ座標
*/
void DX9_Camera::SetAt(const D3DXVECTOR3 & At)
{
    this->m_At2 = At;
}

//==========================================================================
/**
@brief 注視点
@param Eye [in] 注視点
*/
void DX9_Camera::SetEye(const D3DXVECTOR3 & Eye)
{
    this->m_Eye2 = Eye;
}

_MSLIB_END