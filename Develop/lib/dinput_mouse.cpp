//==========================================================================
// ダイレクトインプットのマウス[dinput_mouse.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "dinput_mouse.h"

_MSLIB_BEGIN

DirectInputMouseSpeed::DirectInputMouseSpeed()
{
    this->m_lX = (LONG)0;
    this->m_lY = (LONG)0;
    this->m_lZ = (LONG)0;
}

DirectInputMouseSpeed::~DirectInputMouseSpeed()
{
}

DirectInputMouse::DirectInputMouse()
{
}

DirectInputMouse::~DirectInputMouse()
{
}

//==========================================================================
/**
@brief 初期化
@param hInstance [in] インスタンスハンドル
@param hWnd [in] ウィンドウハンドル
@return 失敗時 true が返ります
*/
bool DirectInputMouse::Init(HINSTANCE hInstance, HWND hWnd)
{
    this->m_hWnd = hWnd;
    this->m_mousePos.x = this->m_mousePos.y = (LONG)0;

	if (FAILED(DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&this->m_DInput, nullptr)))
	{
		MessageBox(hWnd, "DirectInputオブジェクトが作れませんでした", "警告", MB_OK);
		return true;
	}

	if (FAILED(this->m_DInput->CreateDevice(GUID_SysMouse, &this->m_DIDevice, nullptr)))
	{
		MessageBox(hWnd, "マウスがありませんでした。", "警告", MB_OK);
		return true;
	}

	if (this->m_DIDevice != nullptr)
	{
		// マウス用のデータ・フォーマットを設定
		if (FAILED(this->m_DIDevice->SetDataFormat(&c_dfDIMouse2)))
		{
			MessageBox(hWnd, "マウスのデータフォーマットを設定できませんでした。", "警告", MB_ICONWARNING);
			return true;
		}
		
		// 協調モードを設定（フォアグラウンド＆非排他モード）
		if (FAILED(this->m_DIDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND)))
		{
			MessageBox(hWnd, "マウスの協調モードを設定できませんでした。", "警告", MB_ICONWARNING);
			return true;
		}

		if (FAILED(this->m_DIDevice->EnumObjects(EnumAxesCallback, hWnd, DIDFT_AXIS)))
		{
			MessageBox(hWnd, "プロパティを設定できません", "警告", MB_OK);
			return true;
		}

		if (FAILED(this->m_DIDevice->Poll()))
		{
			while (this->m_DIDevice->Acquire() == DIERR_INPUTLOST)
			{
                this->m_DIDevice->Acquire();
			}
		}
	}

	return false;
}

//==========================================================================
// 軸のコールバック
BOOL CALLBACK DirectInputMouse::EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, void *pContext)
{
    DIPROPDWORD diprop;

    pContext;
    diprop.diph.dwSize = sizeof(DIPROPDWORD);
    diprop.diph.dwHeaderSize = sizeof(DIPROPHEADER);
    diprop.diph.dwHow = DIPH_DEVICE;
    diprop.diph.dwObj = pdidoi->dwType;
    diprop.dwData = DIPROPAXISMODE_REL; // 相対値モードで設定（絶対値はDIPROPAXISMODE_ABS）    

    return DIENUM_CONTINUE;
}

//==========================================================================
/**
@brief 更新
*/
void DirectInputMouse::Update(void)
{
	DIMOUSESTATE2 aState;

	GetCursorPos(&this->m_mousePos);
	ScreenToClient(this->m_hWnd, &this->m_mousePos);

    if (this->m_DIDevice == nullptr)
    {
        return;
    }

    // デバイスからデータを取得
    if (SUCCEEDED(this->m_DIDevice->GetDeviceState(sizeof(aState), &aState)))
    {
        for (int i = 0; i < (int)sizeof(DIMOUSESTATE2::rgbButtons); i++)
        {
            // トリガー・リリース情報を生成
            this->m_StateTrigger[i] = (this->m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & aState.rgbButtons[i];
            this->m_StateRelease[i] = (this->m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & this->m_State.rgbButtons[i];

            // リピート情報を生成
            if (aState.rgbButtons[i])
            {
                if (this->m_StateRepeatCnt[i] < 20)
                {
                    this->m_StateRepeatCnt[i]++;
                    if (this->m_StateRepeatCnt[i] == 1 || this->m_StateRepeatCnt[i] >= 20)
                    {// 押し始めた最初のフレーム、または一定時間経過したらリピート情報ON
                        this->m_StateRepeat[i] = aState.rgbButtons[i];
                    }
                    else
                    {
                        this->m_StateRepeat[i] = 0;
                    }
                }
            }
            else
            {
                this->m_StateRepeatCnt[i] = 0;
                this->m_StateRepeat[i] = 0;
            }
            // プレス情報を保存
            this->m_State.rgbButtons[i] = aState.rgbButtons[i];
        }
        this->m_State.lX = aState.lX;
        this->m_State.lY = aState.lY;
        this->m_State.lZ = aState.lZ;
    }
    else
    {
        // アクセス権を取得
        this->m_DIDevice->Acquire();
    }
}

//==========================================================================
/**
@brief Button Cast(int)
@param cast [in] キャスト対象
@return キャストされた値が返ります
*/
DirectInputMouseButton DirectInputMouse::KeyCast(int cast)
{
    return (DirectInputMouseButton)cast;
}

//==========================================================================
/**
@brief プレス
@param key [in] 入力キー
@return 入力されている場合 true が返ります
*/
bool DirectInputMouse::Press(DirectInputMouseButton key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_State.rgbButtons[(int)key] & 0x80) ? true : false;
}

//==========================================================================
/**
@brief トリガー
@param key [in] 入力キー
@return 入力されている場合 true が返ります
*/
bool DirectInputMouse::Trigger(DirectInputMouseButton key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_StateTrigger[(int)key] & 0x80) ? true : false;
}

//==========================================================================
/**
@brief リピート
@param key [in] 入力キー
@return 入力されている場合 true が返ります
*/
bool DirectInputMouse::Repeat(DirectInputMouseButton key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_StateRelease[(int)key] & 0x80) ? true : false;
}

//==========================================================================
/**
@brief リリース
@param key [in] 入力キー
@return 入力されている場合 true が返ります
*/
bool DirectInputMouse::Release(DirectInputMouseButton key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_StateRelease[(int)key] & 0x80) ? true : false;
}

//==========================================================================
/**
@brief マウスの速度
@return マウスの移動速度が返ります
*/
DirectInputMouseSpeed DirectInputMouse::Speed(void)
{
    DirectInputMouseSpeed speed;

	speed.m_lX = this->m_State.lX;
	speed.m_lY = this->m_State.lY;
	speed.m_lZ = this->m_State.lZ;

	return speed;
}

//==========================================================================
/**
@brief カーソルの座標
@return 画面内座標が返ります
*/
POINT DirectInputMouse::WIN32Cursor(void)
{
	return this->m_mousePos;
}

//==========================================================================
/**
@brief 左クリック
@return 入力されている場合値が返ります
*/
SHORT DirectInputMouse::WIN32LeftClick(void)
{
	return GetAsyncKeyState(VK_LBUTTON);
}

//==========================================================================
/**
@brief 右クリック
@return 入力されている場合値が返ります
*/
SHORT DirectInputMouse::WIN32RightClick(void)
{
	return GetAsyncKeyState(VK_RBUTTON);
}

//==========================================================================
/**
@brief マウスホイールのホールド判定
@return 入力されている場合値が返ります
*/
SHORT DirectInputMouse::WIN32WheelHold(void)
{
	return GetAsyncKeyState(VK_MBUTTON);
}

_MSLIB_END