//==========================================================================
// 球体[DX9_Sphere.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// Include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_3DObject.h"
#include "DX9_Renderer.h"
#include "vector_wrapper.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_SphereData
// Content: 球体のデータ管理
//
//==========================================================================
class DX9_SphereData : private DX9_VERTEX_3D
{
public:
    DX9_SphereData();
    ~DX9_SphereData();

    /**
    @brief 解放
    */
    void Release(void);

    /**
    @brief クリエイト
    @param SubdivisionRatio [in] 線の本数
    @param pDevice [in] デバイス
    */
    bool CreateSphere(int SubdivisionRatio, LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief コンバート
    */
    void convert(void);

    /**
    @brief コピー
    @param pinp [in] コピー対象
    */
    void copy(const DX9_SphereData* pinp);
public:
    int m_SubdivisionRatio; // 球体の品質
    LPD3DXMESH m_mesh1; // メッシュ
    LPD3DXMESH m_mesh2; // メッシュ
    LPDIRECT3DVERTEXBUFFER9 m_vertexbuffer; // バッファ
    VERTEX_4* m_pseudo;// 頂点バッファのロック
    bool m_origin; // 親データかの判定
};

//==========================================================================
//
// class  : DX9_Sphere
// Content: 球体
//
//==========================================================================
class DX9_Sphere : public DX9_Renderer<DX9_3DObject>
{
private:
    // コピー禁止 (C++11)
    DX9_Sphere(const DX9_Sphere &) = delete;
    DX9_Sphere &operator=(const DX9_Sphere &) = delete;
public:
    DX9_Sphere(LPDIRECT3DDEVICE9 pDevice, HWND hWnd);
    ~DX9_Sphere();

    /**
    @brief 初期化
    @param Input [in] 使用するテクスチャのパス
    @param SubdivisionRatio [in] 線の数
    @return 失敗時に true が返ります
    */
	bool Init(const char * ptex, int SubdivisionRatio);

    /**
    @brief 初期化
    @param Input [in] 使用するテクスチャのパス ダブルポインタに対応
    @param SubdivisionRatio [in] 線の数
    @param num [in] 要素数
    @return 失敗時に true が返ります
    */
    bool Init(const char ** ptex, int num, int SubdivisionRatio);

    /**
    @brief 初期化
    @param Input [in] 使用するテクスチャのパス ダブルポインタに対応
    @param SubdivisionRatio [in] 線の数
    @param num [in] 要素数
    @return 失敗時に true が返ります
    */
    bool Init(const char ** ptex, int num, int *SubdivisionRatio);

    /**
    @brief 更新
    */
    void Update(void);

    /**
    @brief 解放
    */
	void Release(void);

    /**
    @brief 描画
    */
    void Draw(void);
private:
    /**
    @brief 重複検索
    @param pOut [in] データ
    @param SubdivisionRatio [in] 線の数
    */
	bool OverlapSearch(DX9_SphereData *& pOut, int SubdivisionRatio);
private:
    vector_wrapper<DX9_SphereData> m_matdata; // オリジナルデータ
    vector_wrapper<DX9_SphereData> m_matdata_path; // コピーデータ
};

_MSLIB_END