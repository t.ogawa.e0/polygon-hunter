//==========================================================================
// オブジェクト[DX9_3DObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"
#include "DX9_MaterialLighting.h"
#include "DX9_QuaternionObject.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_3DObject
// Content: オブジェクト
//
//==========================================================================

// 使うベクトルの種類の選択
enum class DX9_3DObjectMatrixType
{
    Default = 0, // デフォルト
    Vector1, // ベクトルの使用
    NotVector, // ベクトルを使用しません
    MirrorVector, // 逆ベクトルを使用
    MirrorNotVector, // 逆ベクトルを使用し、ベクトルを使用しません
    Quaternion, // クオータニオンの利用
};

class DX9_3DObject : public DX9_MaterialLighting, public DX9_QuaternionObject
{
private:
	//==========================================================================
	//
	// class  : CMatLook
	// Content: 視覚情報
	//
	//==========================================================================
	class CMatLook
	{
	public:
		CMatLook()
		{
			this->Up = this->Eye = this->At = D3DXVECTOR3(0, 0, 0);
		}
		~CMatLook() {}
	public:
		D3DXVECTOR3 Eye; // 視点
		D3DXVECTOR3 At; // 座標
		D3DXVECTOR3 Up; // ベクター
	};

	//==========================================================================
	//
	// class  : CVec3
	// Content: ベクトル
	//
	//==========================================================================
	class CVec3
	{
	public:
		CVec3()
		{
			this->Up = this->Front = this->Right = D3DXVECTOR3(0, 0, 0);
		}
		~CVec3() {}
	public:
		D3DXVECTOR3 Up; // 上ベクトル
		D3DXVECTOR3 Front; // 前ベクトル
		D3DXVECTOR3 Right; //  右ベクトル
	};

	//==========================================================================
	//
	// class  : CObjInfo
	// Content: オブジェクトの情報
	//
	//==========================================================================
	class CObjInfo
	{
	public:
		CObjInfo()
		{
			this->pos = this->rot = this->sca = D3DXVECTOR3(0, 0, 0);
		}
		~CObjInfo() {}
	public:
		D3DXVECTOR3 pos; // 平行
		D3DXVECTOR3 rot; // 回転
		D3DXVECTOR3 sca; // スケール
	};
public:
    DX9_3DObject();
    ~DX9_3DObject();

    /**
    @brief 初期化
    @param index [in] 使用するリソース番号
    */
    void Init(int index);

    /**
    @brief X軸回転
    @param value [in] 入力した値が加算されます
    */
	void RotX(float value);

    /**
    @brief X軸回転
    @param rot [in] 回したい方向
    @param power [in] 回転にかけるパワー
    @return 回転した角度
    */
    float RotX(D3DXVECTOR3 & rot, float power);

    /**
    @brief Y軸回転
    @param value [in] 入力した値が加算されます
    */
    void RotY(float value);

    /**
    @brief Y軸回転(制限あり)
    @param value [in] 入力した値が加算されます
    */
    void RotYLock(float value);

    /**
    @brief Y軸回転
    @param rot [in] 回したい方向
    @param power [in] 回転にかけるパワー
    @return 回転した角度
    */
    float RotY(D3DXVECTOR3 & rot, float power);

    /**
    @brief Z軸回転
    @param value [in] 入力した値が加算されます
    */
    void RotZ(float value);

    /**
    @brief Z軸回転
    @param rot [in] 回したい方向
    @param power [in] 回転にかけるパワー
    @return 回転した角度
    */
    float RotZ(D3DXVECTOR3 & rot, float power);

    /**
    @brief Z軸移動
    @param value [in] 入力した値が加算されます
    */
	void MoveZ(float value);

    /**
    @brief X軸移動
    @param value [in] 入力した値が加算されます
    */
    void MoveX(float value);

    /**
    @brief Y軸移動
    @param value [in] 入力した値が加算されます
    */
	void MoveY(float value);

    /**
    @brief スケール
    @param value [in] 入力した値が加算されます
    */
    void ScalePlus(float value);

    /**
    @brief スケール
    @param value [in] 入力した値が加算されます
    */
    void ScalePlus(const D3DXVECTOR3 & value);

    /**
    @brief スケール
    @param value [in] 入力した値入ります
    */
    void ScaleSet(float value);

    /**
    @brief スケール
    @param value [in] 入力した値入ります
    */
    void ScaleSet(const D3DXVECTOR3 & value);

    /**
    @brief ワールド行列の生成
    @param InOut [in/out] ワールド行列を受け取る場所です
    @return 成功時にワールド行列が返ります/失敗時に nullptr が返ります
    */
	const D3DXMATRIX *CreateMtxWorld(D3DXMATRIX & InOut);

    /**
    @brief ラジコン回転
    @param vecRight [in] 向かせたい方向ベクトル
    @param speed [in] 向かせる速度
    */
	void RadioControl(const D3DXVECTOR3 & vecRight, float speed);

    /**
    @brief 前ベクトルのセット
    @param Input [in] 前ベクトル
    */
    void SetVecFront(const D3DXVECTOR3 & Input);

    /**
    @brief 前ベクトルの取得
    @return 前ベクトル
    */
    const D3DXVECTOR3 * GetVecFront(void) const;

    /**
    @brief 上ベクトルのセット
    @param Input [in] 上ベクトル
    */
    void SetVecUp(const D3DXVECTOR3 & Input);

    /**
    @brief 上ベクトルの取得
    @return 上ベクトル
    */
    const D3DXVECTOR3 * GetVecUp(void) const;

    /**
    @brief 右ベクトルのセット
    @param Input [in] 右ベクトル
    */
    void SetVecRight(const D3DXVECTOR3 & Input);

    /**
    @brief 右ベクトルの取得
    @return 右ベクトル
    */
    const D3DXVECTOR3 * GetVecRight(void) const;

    /**
    @brief 上のどの方向を見ているかのセット
    @param Input [in] ベクトル
    */
    void SetLockUp(const D3DXVECTOR3 & Input);

    /**
    @brief 上のどの方向を見ているかの取得
    @return ベクトル
    */
    const D3DXVECTOR3 * GetLockUp(void) const;

    /**
    @brief 視点ベクトルセット
    @param Input [in] 視点ベクトル
    */
    void SetLockAt(const D3DXVECTOR3 & Input);

    /**
    @brief 視点ベクトルの取得
    @return 視点ベクトル
    */
    const D3DXVECTOR3 * GetLockAt(void) const;

    /**
    @brief 視線ベクトルセット
    @param Input [in] 視線ベクトル
    */
    void SetLockEye(const D3DXVECTOR3 & Input);

    /**
    @brief 視線ベクトルの取得
    @return 視線ベクトル
    */
    const D3DXVECTOR3 * GetLockEye(void) const;

    /**
    @brief 座標情報のセット
    @param Input [in] 座標情報
    */
    void SetMatInfoPos(const D3DXVECTOR3 & Input);

    /**
    @brief 座標情報の取得
    @return 座標情報
    */
    const D3DXVECTOR3 * GetMatInfoPos(void) const;

    /**
    @brief 回転情報のセット
    @param Input [in] 回転情報
    */
    void SetMatInfoRot(const D3DXVECTOR3 & Input);

    /**
    @brief 回転情報の取得
    @return 回転情報
    */
    const D3DXVECTOR3 * GetMatInfoRot(void) const;

    /**
    @brief スケールのセット
    @param Input [in] スケール
    */
    void SetMatInfoSca(const D3DXVECTOR3 & Input);

    /**
    @brief スケールの取得
    @return スケール
    */
    const D3DXVECTOR3 * GetMatInfoSca(void) const;

    /**
    @brief 対象の方向に向かせる(ベクトル計算を行い、処理を行います)
    @param Input [in] ターゲット
    @param power 向かせる力
    */
    void LockOn(const DX9_3DObject & Input, float power);

    /**
    @brief 対象の方向に向かせる(直接ベクトルを操作します)
    @param Input [in] ターゲット
    */
    void LockOn(const DX9_3DObject & Input);

    /**
    @brief 使用リソース番号の取得
    @return リソース番号
    */
    int GetIndex(void) const;

    /**
    @brief 使用リソース番号のセット
    @param label [in] リソース番号のセット
    */
    void SetIndex(int label);

    /**
    @brief 使うベクトルのタイプ指定
    @param type [in] ベクトルのタイプ
    */
    void SetMatrixType(const DX9_3DObjectMatrixType & type);

    /**
    @brief 使用中のベクトルの種類の取得
    @return ベクトルの種類
    */
    DX9_3DObjectMatrixType GetMatrixType(void) const;

    /**
    @brief ビュー行列の登録
    @param Input [in] ビュー行列
    */
    void SetMtxView(D3DXMATRIX * Input);

    /**
    @brief サードパーソン用の座標取得
    @param move_pos [in] 対象オブジェクトを始点とした座標を入力してください
    @return サードパーソン用の座標
    */
    D3DXVECTOR3 GetThirdPerson(D3DXVECTOR3 move_pos);

    /**
    @brief 内部ベクトルの正規化
    */
    void VectorNormalize(void);

    /**
    @brief アニメーションの更新
    */
    void UpdateAnimationCount(void);

    /**
    @brief アニメーションの取得
    @return カウンタ
    */
    int & GetAnimationCount(void);

    /**
    @brief アニメーションの登録
    @param count [in] カウンタ
    */
    void SetAnimationCount(int count);

    /**
    @brief アニメーションの初期化
    */
    void InitAnimationCount(void);
private:
    /**
    @brief ベクトルを使用した行列の生成
    @param InOut [in/out] ワールド行列を受け取る場所です
    @return ワールド行列が返ります
    */
	const D3DXMATRIX *CreateMtxWorld1(D3DXMATRIX & InOut);

    /**
    @brief ベクトルを使用しない行列の生成
    @param InOut [in/out] ワールド行列を受け取る場所です
    @return ワールド行列が返ります
    */
	const D3DXMATRIX *CreateMtxWorld2(D3DXMATRIX & InOut);

    /**
    @brief ベクトルを使用したカメラの方向を向かせる行列
    @param InOut [in/out] ワールド行列を受け取る場所です
    @return ワールド行列が返ります
    */
	const D3DXMATRIX *CreateMtxWorld4(D3DXMATRIX & InOut, D3DXMATRIX & MtxView);

    /**
    @brief 直接値を入れるカメラの方向を向かせる行列
    @param InOut [in/out] ワールド行列を受け取る場所です
    @return ワールド行列が返ります
    */
    const D3DXMATRIX *CreateMtxWorld5(D3DXMATRIX & InOut, D3DXMATRIX & MtxView);

    /**
    @brief クオータニオンを利用した行列
    @param InOut [in/out] ワールド行列を受け取る場所です
    @return ワールド行列が返ります
    */
    const D3DXMATRIX *CreateMtxWorld6(D3DXMATRIX & InOut);

    /**
    @brief 回転の制限
    @param Rot [in] 回転行列
    @param pRang [in] 回転角度
    @return 回転可能な際に true が返ります
    */
	bool Restriction(D3DXMATRIX Rot, const float * pRang);

    /**
    @brief 回転行列生成
    @param InOut [in/out] ワールド行列を受け取る場所です
    @return ワールド行列が返ります
    */
	const D3DXMATRIX* CalcLookAtMatrix(D3DXMATRIX & InOut);

    /**
    @brief 回転行列生成
    @param InOut [in/out] ワールド行列を受け取る場所です
    @return ワールド行列が返ります
    */
    const D3DXMATRIX* CalcLookAtMatrixAxisFix(D3DXMATRIX & InOut);
private:
	CMatLook Look; // 視覚情報
	CVec3 Vec; // ベクトル
	CObjInfo Info; // オブジェクトの情報
    DX9_3DObjectMatrixType m_MatType;
	D3DXMATRIX *m_MtxView; // ビュー行列の登録
	int m_index; // データ
    int m_AnimationCount; // アニメーションカウンタ
};

_MSLIB_END