//==========================================================================
// マテリアルのライティング処理[DX9_MaterialLighting.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "mslib_struct.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_MaterialLighting
// Content: マテリアルのライティング
//
//==========================================================================
class DX9_MaterialLighting
{
public:
    DX9_MaterialLighting() {
        ZeroMemory(&this->MeshLighting, sizeof(D3DMATERIAL9));
        this->SetAmbient(CColor<float>(0.5f, 0.5f, 0.5f, 1.0f));
        this->SetDiffuse(CColor<float>(1.0f, 1.0f, 1.0f, 1.0f));
        this->SetSpecular(CColor<float>(1.0f, 1.0f, 1.0f, 1.0f));
        this->SetEmissive(CColor<float>(0.5f, 0.5f, 0.5f, 1.0f));
    }
    virtual ~DX9_MaterialLighting() {}

    /**
    @brief アンビエントライト
    @param color [in] ライト設定(0.0f〜1.0f)
    */
    void SetAmbient(const CColor<float> color) {
        this->MeshLighting.Ambient.r = color.r; // アンビエントライト
        this->MeshLighting.Ambient.g = color.g; // アンビエントライト
        this->MeshLighting.Ambient.b = color.b; // アンビエントライト
        this->MeshLighting.Ambient.a = color.a; // アンビエントライト
    }

    /**
    @brief ディレクショナルライト
    @param color [in] ライト設定(0.0f〜1.0f)
    */
    void SetDiffuse(const CColor<float> color) {
        this->MeshLighting.Diffuse.r = color.r; // 光源
        this->MeshLighting.Diffuse.g = color.g; // 光源
        this->MeshLighting.Diffuse.b = color.b; // 光源
        this->MeshLighting.Diffuse.a = color.a; // 光源
    }

    /**
    @brief スペキュラー
    @param color [in] ライト設定(0.0f〜1.0f)
    */
    void SetSpecular(const CColor<float> color) {
        this->MeshLighting.Specular.r = color.r; // スペキュラー
        this->MeshLighting.Specular.g = color.g; // スペキュラー
        this->MeshLighting.Specular.b = color.b; // スペキュラー
        this->MeshLighting.Specular.a = color.a; // スペキュラー
    }

    /**
    @brief 放射性
    @param color [in] ライト設定(0.0f〜1.0f)
    */
    void SetEmissive(const CColor<float> color) {
        this->MeshLighting.Emissive.r = color.r; // 放射性
        this->MeshLighting.Emissive.g = color.g; // 放射性
        this->MeshLighting.Emissive.b = color.b; // 放射性
        this->MeshLighting.Emissive.a = color.a; // 放射性
    }

    /**
    @brief マテリアルのライティングの取得
    @return マテリアルのライティング情報
    */
    const D3DMATERIAL9 & GetD3DMaterial9(void) {
        return this->MeshLighting;
    }
private:
    D3DMATERIAL9 MeshLighting;
};

_MSLIB_END