//==========================================================================
// 矩形[DX9_Rectangle.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Rectangle.h"

_MSLIB_BEGIN

//==========================================================================
// 定数定義
//==========================================================================
constexpr int NumDfaltIndex = 6;

DX9_Rectangle::DX9_Rectangle()
{
}

DX9_Rectangle::~DX9_Rectangle()
{
}

//==========================================================================
/**
@brief Indexの生成
@param Output [in/out] 受け取り
@param NumRectangle [in] 数
*/
void DX9_Rectangle::RectangleIndex(WORD * Output, int NumRectangle)
{
	for (int i = 0, s = 0, ncount = 0; i < NumDfaltIndex*NumRectangle; i++, s++, ncount++)
	{
		switch (ncount)
		{
		case 3:
			s -= 3;
			break;
		case 4:
			s += 1;
			break;
		case 6:
			ncount = 0;
			break;
		default:
			break;
		}
		Output[i] = (WORD)s;
	}
}

_MSLIB_END