//==========================================================================
// テクスチャローダー[DX9_TextureLoader.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_TextureLoader.h"

_MSLIB_BEGIN

DX9_TextureLoader::DX9_TextureLoader(LPDIRECT3DDEVICE9 pDevice, HWND hWnd)
{
    this->m_Device = pDevice;
    this->m_hWnd = hWnd;
}

DX9_TextureLoader::~DX9_TextureLoader()
{
    this->Release();
}

//==========================================================================
/**
@brief 読み込み
@param Input [in] テクスチャ名
@param Num [in] 格納数
@return 失敗時に true が返ります
*/
bool DX9_TextureLoader::init(const char ** Input, int Num)
{
	for (int i = 0; i < Num; i++)
	{
		if (this->init(Input[i]))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
/**
@brief 読み込み
@param Input [in] テクスチャ名
@return 失敗時に true が返ります
*/
bool DX9_TextureLoader::init(const char * Input)
{
    auto *path = this->m_path.Create();

	// ファイルパスがある場合
	if (Input != nullptr)
	{
        std::string strName = Input;
        std::string str_file_pass;

        // マルチテクスチャ
        if (strName.find("*") != std::string::npos)
        {
            // ファイルパスを生成
            for (auto itr = strName.begin(); itr != strName.end(); ++itr)
            {
                std::string str_;
                str_ = (*itr);

                // マルチテクスチャ判定以外の時に生成
                if (str_ != "*")
                {
                    str_file_pass += (*itr);
                }
                else if (str_ == "*")
                {
                    this->load(path, str_file_pass);
                    str_file_pass.clear();
                }
            }
            // 生成終了の最後のパス
            this->load(path, str_file_pass);
            str_file_pass.clear();
        }
        else
        {
            this->load(path, strName);
        }
        path->SetTextureName(strName);
	}
	else if (Input == nullptr)
	{
        DX9_TextureData *tex = nullptr;
		this->create(tex, Input);
        path->SetTextureName(tex->gettag());
        path->SetTextureData(*tex);
	}

    path->SetTextureFVF();
    path->SetDevice(this->m_Device);

	return false;
}

//==========================================================================
/**
@brief 解放
*/
void DX9_TextureLoader::Release(void)
{
	this->m_texture.Release();
    this->m_path.Release();
}

//==========================================================================
/**
@brief データの取得
@param nID [in] アクセスID
*/
DX9_TextureList * DX9_TextureLoader::get(int nID)
{
    if (0 <= nID && nID < this->m_path.Size())
    {
        return this->m_path.Get(nID);
    }
    return nullptr;
}

//==========================================================================
/**
@brief データ数
@return 数
*/
int DX9_TextureLoader::size(void)
{
    return this->m_path.Size();
}

//==========================================================================
/**
@brief マスターデータの取得
@param nID [in] アクセスID
*/
const vector_wrapper<DX9_TextureData> * DX9_TextureLoader::get_master(void)
{
    return &this->m_texture;
}

//==========================================================================
/**
@brief 生成
@param pinp [in/out] データ受け取り変数
@param Input [in] タグ
@return 生成データ
*/
DX9_TextureData * DX9_TextureLoader::create(DX9_TextureData *& pinp, const char * Input)
{
    pinp = this->m_texture.Create();

	pinp->settag(Input);

	return pinp;
}

//==========================================================================
/**
@brief リソース読み取り
@param InOut [in/out] データ受け取り変数
@param strName [in] リソース名
*/
void DX9_TextureLoader::load(DX9_TextureList * InOut, const std::string & strName)
{
    DX9_TextureData *tex = nullptr;
    bool bkey = false; // 鍵

    // 重複読み込みのチェック
    for (int i = 0; i < this->m_texture.Size(); i++)
    {
        tex = this->m_texture.Get(i);
        // 重複しているとき
        if (tex->check(strName.c_str()))
        {
            bkey = true;
            break;
        }
    }

    // 重複していないとき
    if (bkey == false)
    {
        this->create(tex, strName.c_str());

        // テクスチャの読み込み
        if (FAILED(tex->load(this->m_Device)))
        {
            char pfon[1024] = { 0 };
            sprintf(pfon, "テクスチャが存在しません \n %s", tex->gettag());
            MessageBox(this->m_hWnd, pfon, "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
        }
    }

    InOut->SetTextureData(*tex);
}

DX9_TextureData::DX9_TextureData()
{
    this->m_texture = nullptr;
    this->m_texparam = 0;
    this->m_mastersize = 0;
    this->m_original = false;
}

DX9_TextureData::~DX9_TextureData()
{
    this->Release();
}

//==========================================================================
/**
@brief サイズのリセット
*/
void DX9_TextureData::resetsize(void)
{
	this->m_texparam = this->m_mastersize;
}

//==========================================================================
/**
@brief サイズのリセット
@param Input [in] サイズ指定
*/
void DX9_TextureData::setsize(CTexvec<int> Input)
{
	this->m_texparam = Input;
}

//==========================================================================
/**
@brief サイズの取得
@return テクスチャのサイズ
*/
CTexvec<int>* DX9_TextureData::getsize(void)
{
    return &this->m_texparam;
}

//==========================================================================
/**
@brief 読み込み
@return Component Object Model defines, and macros
*/
HRESULT DX9_TextureData::load(LPDIRECT3DDEVICE9 pDevice)
{
	HRESULT hr = D3DXCreateTextureFromFile(pDevice, this->m_strName.c_str(), &this->m_texture);

	if (!FAILED(hr))
	{
		// 画像データの格納
		D3DXGetImageInfoFromFile(this->m_strName.c_str(), &this->m_ImageInfo);
		this->m_texparam = CTexvec<int>(0, 0, this->m_ImageInfo.Width, this->m_ImageInfo.Height);
		this->m_mastersize = CTexvec<int>(0, 0, this->m_ImageInfo.Width, this->m_ImageInfo.Height);
	}

	return hr;
}

//==========================================================================
/**
@brief 解放
*/
void DX9_TextureData::Release(void)
{
    if (this->m_original == true)
    {
        if (this->m_texture != nullptr)
        {
            this->m_texture->Release();
        }
    }
    this->m_texture = nullptr;
    this->m_strName.clear();
}

//==========================================================================
/**
@brief tagのセット
@param ptag [in] タグのセット
*/
void DX9_TextureData::settag(const char * ptag)
{
	if (ptag != nullptr)
	{
		this->m_strName = ptag;
	}
	else
	{
		this->m_strName = "";
	}

    this->m_original = true;
}

//==========================================================================
/**
@brief tagの取得
@return タグ名
*/
const char * DX9_TextureData::gettag(void)
{
    return this->m_strName.c_str();
}

//==========================================================================
/**
@brief 同一データチェック
@return 同一データが存在する場合 true
*/
bool DX9_TextureData::check(const char * Input)
{
	if (Input != nullptr)
	{
		if (this->m_strName == Input)
		{
			return true;
		}
	}
	else if (Input == nullptr)
	{
		if (this->m_strName == "")
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
/**
@brief リソースの取得
@return テクスチャのリソース
*/
const LPDIRECT3DTEXTURE9 DX9_TextureData::gettex(void)
{
    return this->m_texture;
}

//==========================================================================
/**
@brief データの複製
@param pinp [in] 複製対象
*/
void DX9_TextureData::path(const DX9_TextureData * pinp)
{
    this->m_texture = pinp->m_texture;
    this->m_texparam = pinp->m_texparam;
    this->m_mastersize = pinp->m_mastersize;
    this->m_strName = pinp->m_strName;
    this->m_original = false;
}

//==========================================================================
/**
テクスチャの情報取得
@return テクスチャの情報
*/
const D3DXIMAGE_INFO DX9_TextureData::GetImageInfo(void)
{
    return this->m_ImageInfo;
}

DX9_TextureList::DX9_TextureList()
{

}

DX9_TextureList::~DX9_TextureList()
{
    this->m_data.clear();
    this->m_strName.clear();
}

//==========================================================================
/**
@brief デバイスのセット
@param pDevice [in] デバイス
*/
void DX9_TextureList::SetDevice(LPDIRECT3DDEVICE9 pDevice)
{
    this->m_Device = pDevice;
}

//==========================================================================
/**
@brief リソースの登録
@param data [in] データ
*/
void DX9_TextureList::SetTextureData(const DX9_TextureData & data)
{
    DX9_TextureData data_;
    data_.path(&data);
    this->m_data.push_back(data_);
}

//==========================================================================
/**
@brief リソースの取得
@return アクセス用データ
*/
std::list<DX9_TextureData>* DX9_TextureList::GetTextureList(void)
{
    return &this->m_data;
}

//==========================================================================
/**
@brief リソース数の取得
@return リソース数
*/
int DX9_TextureList::Size(void)
{
    return this->m_data.size();
}

//==========================================================================
/**
@brief リソース名の登録
@param name [in] リソース名
*/
void DX9_TextureList::SetTextureName(const std::string & name)
{
    this->m_strName = name;
}

//==========================================================================
/**
@brief リソース名の取得
@param name [in] リソース名
*/
const std::string * DX9_TextureList::GetTextureName(void)
{
    return &this->m_strName;
}

//==========================================================================
/**
@brief テクスチャFVFの生成
*/
void DX9_TextureList::SetTextureFVF(void)
{
    int fvf__ = (int)this->m_data.size();
    if (D3DFVF_TEXCOUNT_SHIFT<fvf__)
    {
        fvf__ = D3DFVF_TEXCOUNT_SHIFT;
    }
    this->m_texture_fvf = D3DFVF_TEX1 * fvf__;
}

//==========================================================================
/**
@brief テクスチャFVFの取得
@return テクスチャFVF
*/
const DWORD & DX9_TextureList::GetTextureFVF(void)
{
    return this->m_texture_fvf;
}

//==========================================================================
/**
@brief 描画の開始
*/
void DX9_TextureList::DrawBegin(void)
{
    //int Stage = 0;
    //for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr, Stage++)
    //{
    //    this->m_Device->SetTexture((DWORD)Stage, itr->gettex());
    //}
    this->m_Device->SetTexture(0, this->m_data.begin()->gettex());
}

//==========================================================================
/**
@brief 描画の終了
*/
void DX9_TextureList::DrawEnd(void)
{
    //for (int i = 0; i < this->m_data.size(); i++)
    //{
    //    this->m_Device->SetTexture((DWORD)i, nullptr);
    //}
    this->m_Device->SetTexture(0, nullptr);
}

_MSLIB_END