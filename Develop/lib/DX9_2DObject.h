//==========================================================================
// 2Dオブジェクト[DX9_2DObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_Vertex3D.h"
#include "mslib_struct.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_2DObjectAnim
// Content: アニメーション情報
//
//==========================================================================
class DX9_2DObjectAnim
{
public:
    DX9_2DObjectAnim();
    /**
    @brief アニメーション設定用コンストラクタ
    @param count [in] アニメーションカウンタ(基本-1)
    @param frame [in] 更新フレーム
    @param pattern [in] アニメーションのパターン数
    @param direction [in] 一行のアニメーション数
    @param key [in] アニメーション有効(true)無効(false)
    */
    DX9_2DObjectAnim(int count, int frame, int pattern, int direction, bool key);
    ~DX9_2DObjectAnim();
public:
    int Count; // アニメーションカウンタ
    int Frame; // 更新タイミング
    int Pattern; // アニメーションのパターン数
    int Direction; // 一行のアニメーション数
    bool Key; // アニメーションの有無
};

//==========================================================================
//
// class  : DX9_2DObjectAffine
// Content: 回転情報
//
//==========================================================================
class DX9_2DObjectAffine
{
public:
    DX9_2DObjectAffine();
    /**
    @brief 座標のセット
    @param Angle [in] 回転情報
    @param key [in] 回転の有効(true)無効(false)
    */
    DX9_2DObjectAffine(float Angle, bool key);
    ~DX9_2DObjectAffine();
public:
    float Angle; // 回転情報
    bool Key; // 回転の有無
};

//==========================================================================
//
// class  : DX9_2DObject
// Content: 2Dオブジェクト オブジェクトを動かすためのもの
//
//==========================================================================
class DX9_2DObject : private DX9_VERTEX_3D
{
public:
    DX9_2DObject();
	~DX9_2DObject();

    /**
    @brief 初期化
    @param index [in] 使用するテクスチャ番号
    */
	void Init(int index);

    /**
    @brief アニメーション用初期化
    @param index [in] テクスチャ番号
    @param Frame [in] 更新フレーム
    @param Pattern [in] アニメーション数
    @param Direction [in] 横一列のアニメーション数
    */
	void Init(int index, int Frame, int Pattern, int Direction);

    /**
	@brief アニメーションだけの情報セット
	@param index [in] テクスチャ番号
	@param Frame [in] 更新フレーム 
	@param Pattern [in] アニメーション数
	@param Direction [in] 横一列のアニメーション数
    */
	void SetAnim(int index, int Frame, int Pattern, int Direction);

    /**
    @brief 座標のセット
    @param x [in] X座標
    @param y [in] Y座標
    */
	void SetPos(float x, float y);

    /**
    @brief 座標のセット
    @param x [in] X座標
    */
	void SetX(float x);

    /**
    @brief 座標のセット
    @param y [in] Y座標
    */
    void SetY(float y);

    /**
    @brief 座標の加算
    @param x [in] X座標
    */
	void SetXPlus(float x);

    /**
    @brief 座標の加算
    @param y [in] Y座標
    */
	void SetYPlus(float y);

    /**
    @brief 座標のセット
    @param vpos [in] {x,y}座標
    */
    void SetPos(const CVector2<float> & vpos);

    /**
    @brief 座標の加算
    @param vpos [in] {x,y}座標
    */
	void SetPosPlus(const CVector2<float> & vpos);

    /**
    @brief 中心座標モード
    @param mood [in] trueで有効化
    */
    void SetCentralCoordinatesMood(bool mood);

    /**
    @brief 座標の取得
    @return x,y 座標
    */
    const CVector2<float> *GetPos(void) const;

    /**
    @brief サイズ加算
    @param Size [in] 入力した値が加算されます
    */
    void SetScalePlus(float Size);

    /**
    @brief サイズ入力
    @param Size [in] サイズ設定です
    */
    void SetScale(float Size);

    /**
    @brief サイズ取得
    @return サイズ
    */
    float GetScale(void) const;

    /**
    @brief 回転角度入力
    @param Angle [in] 角度が加算されます
    */
	void SetAnglePlus(float Angle);

    /**
    @brief 回転角度リセット
    */
	void AngleReset(void);

    /**
    @brief 回転角度の取得
    @return 回転角度
    */
    float GetAngle(void);

    /**
    @brief 回転角度入力
    @param Angle [in] 角度が入ります
    */
    void SetAngle(float Angle);

    /**
    @brief 色セット
    @param r [in] R値が入ります
    @param g [in] G値が入ります
    @param b [in] B値が入ります
    @param a [in] A値が入ります
    */
	void SetColor(int r, int g, int b, int a);

    /**
    @brief 色セット
    @param color [in] {r,g,b,a} RGBA値が入ります
    */
    void SetColor(const CColor<int> & color);

    /**
    @brief 色の取得
    @return r,g,b,a値が返ります
    */
    const CColor<int> * GetColor(void);

    /**
    @brief 無回転頂点 非推奨です
    @param pPseudo [in] 頂点バッファ
    @param ptexsize [in] テクスチャのサイズ
    @return VERTEX_3 型が返ります
    */
	const VERTEX_3 * CreateVertex(VERTEX_3 * pPseudo, const CTexvec<int> & ptexsize);

    /**
    @brief 回転頂点 非推奨です
    @param pPseudo [in] 頂点バッファ
    @param ptexsize [in] テクスチャのサイズ
    @return VERTEX_3 型が返ります
    */
	const VERTEX_3 * CreateVertexAngle(VERTEX_3 * pPseudo, const CTexvec<int> & ptexsize);

    /**
    @brief アニメーション切り替わり時の判定
    @return アニメーション終了時に true が返ります
    */
	bool GetPattanNum(void);

    /**
    @brief アニメーション切り替わり時の判定
    @param AnimationCount [in] アニメーションカウンタ
    @return アニメーション終了時に true が返ります
    */
    bool GetPattanNum(int & AnimationCount);

    /**
    @brief アニメーションカウンタのセット
    @param nCount [in] アニメーションカウント
    */
	void AnimationCount(int nCount);

    /**
    @brief アニメーションカウンタ
    */
    void AnimationCount(void);

    /**
    @brief アニメーション情報のゲッター
    @return アニメーション情報
    */
    DX9_2DObjectAnim * GetAnimParam(void);

    /**
    @brief アニメーションカウンタの初期化
    @param AnimationCount [in] アニメーションカウンタ
    */
    void InitAnimationCount(int & AnimationCount);

    /**
    @brief アニメーションカウンタの初期化
    */
    void InitAnimationCount(void);
	
    /**
    @brief 使用テクスチャ番号の取得
    @return テクスチャ番号
    */
    int GetIndex(void) const;

    /**
    @brief 使用テクスチャ番号のセット
    @param Input [in] テクスチャ番号
    */
    void SetIndex(int Input);

    /**
    @brief 現在使用しているテクスチャのサイズを取得
    @return テクスチャのサイズ
    */
    const CTexvec<float> * GetTexSize(void) const;

    /**
    @brief 割合座標 ウィンドウサイズの割合座標の設定ができます
    @param Proportion [in] {x,y}軸割合
    @param WinSize [in] {x,y}ウィンドウサイズ
    */
    void SetPercentPos(const CVector2<float> & Proportion, CVector2<float> & WinSize);

    /**
    @brief 座標調節に使用している割合の取得
    @return 座標調節に使用している割合
    */
    const CVector2<float> * GetPercentPos(void);

    /**
    @brief 頂点情報を登録
    @param label [in] アクセスラベル
    @param vertex [in] 頂点情報
    */
    void SetVertexPath(int label, VERTEX_3 * vertex);

    /**
    @brief 頂点情報を取得
    @param label [in] アクセスラベル
    @return 頂点情報
    */
    const VERTEX_3 * GetVertexPath(int label);
private:
    /**
    @brief UVの生成
    @param UV [in/out] {u0,v0,u1,v1}UV座標
    @param ptexsize [in] テクスチャサイズ
    */
	void UV(CUv<float> * UV, const CTexvec<int> & ptexsize);

    /**
    @brief アニメーションの更新
    */
	void AnimationPalam(void);

    /**
    @brief テクスチャの切り取り ウィンドウサイズの割合座標の設定ができます
    @param ptexsize [in] デフォルトのテクスチャサイズ
    */
	void SetTexturCut(const CTexvec<int> & ptexsize);
private:
    VERTEX_3 * m_vertex_3[4] = { nullptr }; // バーテックス3
	CTexvec<float> m_Cut; // テクスチャカット位置
    CVector2<float> m_Pos; // ポリゴンの座標
    CVector2<float> m_PercentPos; // 位置割合座標
	CColor<int> m_Color; // ポリゴンの色
    DX9_2DObjectAnim m_Anim; // アニメーション
    DX9_2DObjectAffine m_Affine; // アフィン変換
	float m_Scale; // スケール
	bool m_Key; // キー
	bool m_CentralCoordinates; // 中心座標モード
	int m_index; // テクスチャ番号
};

_MSLIB_END