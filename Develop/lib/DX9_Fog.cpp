//==========================================================================
// フォグ[DX9_Fog.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Fog.h"

_MSLIB_BEGIN

DX9_Fog::DX9_Fog(LPDIRECT3DDEVICE9 pDevice)
{
    this->m_Device = pDevice;
    this->m_color = 255;
    this->m_VertexMode = D3DFOGMODE::D3DFOG_NONE;
    this->m_TableMode = D3DFOGMODE::D3DFOG_NONE;
    this->m_EffectRange = EffectRangePos(0, 0);
    ZeroMemory(&this->m_caps, sizeof(D3DCAPS9));    //初期化
}

DX9_Fog::~DX9_Fog()
{
    this->OFF(); //フォグ：off
}

//==========================================================================
/**
@brief 初期化
@param color [in] 色
@param start_pos [in] フォグ影響開始地点
@param end_pos [in] フォグ完全影響地点
@param VertexMode [in] 頂点のモード指定
@param TableMode [in] テーブルモード設定
*/
void DX9_Fog::Init(const CColor<int>& color, float start_pos, float end_pos, D3DFOGMODE VertexMode, D3DFOGMODE TableMode)
{
    this->m_color = color;
    this->m_EffectRange = EffectRangePos(start_pos, end_pos);
    this->m_VertexMode = VertexMode;
    this->m_TableMode = TableMode;

    this->ON();
    this->m_Device->GetDeviceCaps(&this->m_caps);
    this->m_Device->SetRenderState(D3DRS_FOGCOLOR, this->m_color.get()); //白色で不透明
    this->m_Device->SetRenderState(D3DRS_FOGVERTEXMODE, this->m_VertexMode); //頂点モード
    this->m_Device->SetRenderState(D3DRS_FOGTABLEMODE, this->m_TableMode); //テーブルモード
    this->m_Device->SetRenderState(D3DRS_FOGSTART, *(DWORD*)(&this->m_EffectRange.start)); //開始位置
    this->m_Device->SetRenderState(D3DRS_FOGEND, *(DWORD*)(&this->m_EffectRange.end)); //終了位置
}

//==========================================================================
/**
@brief フォクの有効化
*/
void DX9_Fog::ON(void)
{
    this->m_Device->SetRenderState(D3DRS_FOGENABLE, D3DZB_TRUE);
}

//==========================================================================
/**
@brief フォクの無効化
*/
void DX9_Fog::OFF(void)
{
    this->m_Device->SetRenderState(D3DRS_FOGENABLE, D3DZB_FALSE);
}

//==========================================================================
/**
@brief テーブルモード設定
@param TableMode [in] テーブルモード
*/
void DX9_Fog::SetTableMode(D3DFOGMODE TableMode)
{
    this->m_TableMode = TableMode;
}

//==========================================================================
/**
@brief テーブルモードの取得
@return モード情報
*/
D3DFOGMODE DX9_Fog::GetTableMode(void)
{
    return this->m_TableMode;
}

//==========================================================================
/**
@brief 頂点モード設定
@param VertexMode [in] 頂点モード
*/
void DX9_Fog::SetVertexMode(D3DFOGMODE VertexMode)
{
    this->m_VertexMode = VertexMode;
}

//==========================================================================
/**
@brief 頂点モードの取得
@return モード情報
*/
D3DFOGMODE DX9_Fog::GetVertexMode(void)
{
    return this->m_VertexMode;
}

//==========================================================================
/**
@brief 効果範囲の設定
@param start_pos [in] フォグ影響開始地点
@param end_pos [in]　フォグ完全影響地点
*/
void DX9_Fog::SetEffectRange(float start_pos, float end_pos)
{
    this->m_EffectRange = EffectRangePos(start_pos, end_pos);
}

//==========================================================================
/**
@brief 効果範囲の取得
@return 効果範囲
*/
const DX9_Fog::EffectRangePos * DX9_Fog::GetEffectRange(void)
{
    return &this->m_EffectRange;
}

//==========================================================================
/**
@brief 色の設定
@param color [in] 色
*/
void DX9_Fog::SetColor(const CColor<int>& color)
{
    this->m_color = color;
}

//==========================================================================
/**
@brief 色の取得
@return 色
*/
const CColor<int>* DX9_Fog::GetColor(void)
{
    return &this->m_color;
}

_MSLIB_END