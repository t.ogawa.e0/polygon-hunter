//==========================================================================
// グリッド[DX9_Grid.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DX9_Grid.h"

_MSLIB_BEGIN

DX9_Grid::DX9_Grid(LPDIRECT3DDEVICE9 pDevice)
{
    this->m_Device = pDevice;
    this->m_pos = nullptr;
    this->m_num = 0;
    this->m_scale = 0;
}

DX9_Grid::~DX9_Grid()
{
    this->Release();
}

//==========================================================================
/**
@brief 初期化
@param scale [in] グリッドの表示範囲指定
*/
void DX9_Grid::Init(int scale)
{
	this->m_scale = scale; // サイズの記録
	this->m_num = 4; // 十字を作るための線
	float X = (float)this->m_scale; // サイズの記録
	float Z = (float)this->m_scale; // サイズの記録

	// サイズ分の外枠の線を追加
	for (int i = 0; i < this->m_scale; i++)
	{
		this->m_num += 8; 
	}

    if (this->m_pos != nullptr)
    {
        delete[]this->m_pos;
        this->m_pos = nullptr;
    }

    this->m_pos = new VERTEX_2[this->m_num];

	int nNumLine = (int)(this->m_num / 2);

	for (int i = 0; i < nNumLine; i += 2, X--, Z--)
	{
		this->m_pos[i].pos = D3DXVECTOR3(1.0f * X, 0.0f, (float)this->m_scale); // x座標に線
		this->m_pos[i + 1].pos = D3DXVECTOR3(1.0f * X, 0.0f, -(float)this->m_scale); // x座標に線
		this->m_pos[nNumLine + i].pos = D3DXVECTOR3((float)this->m_scale, 0.0f, 1.0f * Z); // z座標に線
		this->m_pos[nNumLine + i + 1].pos = D3DXVECTOR3(-(float)this->m_scale, 0.0f, 1.0f * Z); // z座標に線

		this->m_pos[i].color = D3DCOLOR_RGBA(255, 255, 255, 255);
		this->m_pos[i + 1].color = D3DCOLOR_RGBA(255, 255, 255, 255);
		this->m_pos[nNumLine + i].color = D3DCOLOR_RGBA(255, 255, 255, 255);
		this->m_pos[nNumLine + i + 1].color = D3DCOLOR_RGBA(255, 255, 255, 255);

		if (i == (int)((float)this->m_scale * 2))
		{
			this->m_pos[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);
			this->m_pos[i + 1].color = D3DCOLOR_RGBA(255, 0, 0, 255);
			this->m_pos[nNumLine + i].color = D3DCOLOR_RGBA(255, 0, 0, 255);
			this->m_pos[nNumLine + i + 1].color = D3DCOLOR_RGBA(255, 0, 0, 255);
		}
	}
}

//==========================================================================
/**
@brief 解放
*/
void DX9_Grid::Release(void)
{
    if (this->m_pos != nullptr)
    {
        delete[]this->m_pos;
        this->m_pos = nullptr;
    }
}

//==========================================================================
/**
@brief 描画
*/
void DX9_Grid::Draw(void)
{
	D3DXMATRIX m_MtxWorld; // ワールド行列

	D3DXMatrixIdentity(&m_MtxWorld);

	// 各種行列の設定
    this->m_Device->SetTransform(D3DTS_WORLD, &m_MtxWorld);

	// FVFの設定
    this->m_Device->SetFVF(this->FVF_VERTEX_2);

    this->m_Device->SetTexture(0, nullptr);
    this->m_Device->DrawPrimitiveUP(D3DPT_LINELIST, (int)(this->m_num / 2), this->m_pos, sizeof(VERTEX_2));
}

_MSLIB_END