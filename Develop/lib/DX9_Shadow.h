//==========================================================================
// 影[DX9_Shadow.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_3DObject.h"
#include "DX9_Renderer.h"
#include "vector_wrapper.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_Shadow
// Content: 影
//
//==========================================================================
class DX9_Shadow : public DX9_Renderer<DX9_3DObject>
{
private:
    // コピー禁止 (C++11)
    DX9_Shadow(const DX9_Shadow &) = delete;
    DX9_Shadow &operator=(const DX9_Shadow &) = delete;
public:
    DX9_Shadow(LPDIRECT3DDEVICE9 pDevice, HWND hWnd);
    ~DX9_Shadow();

    /**
    @brief 初期化
    @param Input [in] 使用するテクスチャのパス
    @return 失敗時に true が返ります
    */
	bool Init(const char * Input);

    /**
    @brief 更新
    */
    void Update(void);

    /**
    @brief 解放
    */
	void Release(void);

    /**
    @brief 描画
    */
    void Draw(void);
private:
    /**
    @brief 頂点バッファの生成
    @param Output [in] バッファ
    @param uv [in] UV
    */
    void VertexBuffer(VERTEX_4 * Output, CUv<float> * uv);
private:
    VERTEX_4 * m_pPseudo;// 頂点バッファのロック
	bool m_Lock;
};

_MSLIB_END