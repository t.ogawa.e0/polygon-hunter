//==========================================================================
// 球体[DX9_Sphere.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DX9_Sphere.h"

_MSLIB_BEGIN

DX9_SphereData::DX9_SphereData()
{
    this->m_origin = false;
    this->m_SubdivisionRatio = 0;
    this->m_mesh1 = nullptr;
    this->m_mesh2 = nullptr;
    this->m_vertexbuffer = nullptr;
    this->m_pseudo = nullptr;
}

DX9_SphereData::~DX9_SphereData()
{
    this->Release();
}

DX9_Sphere::DX9_Sphere(LPDIRECT3DDEVICE9 pDevice, HWND hWnd)
{
    this->m_texture = new DX9_TextureLoader(pDevice, hWnd);
    this->m_Device = pDevice;
    this->m_hWnd = hWnd;
}

DX9_Sphere::~DX9_Sphere()
{
    this->Release();
}

//==========================================================================
/**
@brief 初期化
@param Input [in] 使用するテクスチャのパス
@param SubdivisionRatio [in] 線の数
@return 失敗時に true が返ります
*/
bool DX9_Sphere::Init(const char * ptex, int SubdivisionRatio)
{
    DX9_SphereData* matdata = nullptr;

	if (this->m_texture->init(ptex)) { return true; }

	if (SubdivisionRatio <= 2)
	{
		SubdivisionRatio = 2;
	}

    if (this->OverlapSearch(matdata, SubdivisionRatio) == false)
    {
        matdata = this->m_matdata.Create();

        if (matdata->CreateSphere(SubdivisionRatio, this->m_Device))
        {
            return true;
        }

		matdata->convert();

        matdata->m_origin = true;
	}

    auto* psps = this->m_matdata_path.Create();
    psps->copy(matdata);

	return false;
}

//==========================================================================
/**
@brief 初期化
@param Input [in] 使用するテクスチャのパス ダブルポインタに対応
@param SubdivisionRatio [in] 線の数
@param num [in] 要素数
@return 失敗時に true が返ります
*/
bool DX9_Sphere::Init(const char ** ptex, int num, int SubdivisionRatio)
{
	for (int i = 0; i < num; i++)
	{
		if (this->Init(ptex[i], SubdivisionRatio))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
/**
@brief 初期化
@param Input [in] 使用するテクスチャのパス ダブルポインタに対応
@param SubdivisionRatio [in] 線の数
@param num [in] 要素数
@return 失敗時に true が返ります
*/
bool DX9_Sphere::Init(const char ** ptex, int num, int *SubdivisionRatio)
{
	for (int i = 0; i < num; i++)
	{
		if (this->Init(ptex[i], SubdivisionRatio[i]))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
/**
@brief 更新
*/
void DX9_Sphere::Update(void)
{
    // 登録済みオブジェクトのデータの更新
    for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
    {
        // 行列がデフォルトの指定の時
        if (itr->m_obj->GetMatrixType() == DX9_3DObjectMatrixType::Default)
        {
            itr->m_obj->SetMatrixType(DX9_3DObjectMatrixType::Vector1);
        }

        itr->m_obj->CreateMtxWorld(itr->MtxWorld);
    }
}

//==========================================================================s
/**
@brief 解放
*/
void DX9_Sphere::Release(void)
{
    this->m_matdata.Release();
    this->m_matdata_path.Release();

    // テクスチャの解放
    if (this->m_texture != nullptr)
    {
        this->m_texture->Release();
        delete this->m_texture;
        this->m_texture = nullptr;
    }
    this->ObjectRelease();
}

//==========================================================================
/**
@brief 描画
*/
void DX9_Sphere::Draw(void)
{
    if (this->m_texture->size() != 0 && this->m_ObjectData.size() != 0)
    {
        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            auto* matdata = this->m_matdata_path.Get(itr->m_obj->GetIndex());
            // テクスチャの取得
            auto * pTexList = this->m_texture->get(itr->m_obj->GetIndex());
            if (pTexList != nullptr)
            {
                this->m_Device->SetFVF(matdata->m_mesh2->GetFVF());

                // マテリアルの設定
                this->m_Device->SetMaterial(&itr->m_obj->GetD3DMaterial9());

                this->m_Device->SetTransform(D3DTS_WORLD, &itr->MtxWorld);

                pTexList->DrawBegin();
                matdata->m_mesh2->DrawSubset(0);
                pTexList->DrawEnd();
            }
        }
    }
}

//==========================================================================
/**
@brief 重複検索
@param pOut [in] データ
@param SubdivisionRatio [in] 線の数
*/
bool DX9_Sphere::OverlapSearch(DX9_SphereData *& pOut, int SubdivisionRatio)
{
    for (int i = 0; i < this->m_matdata.Size(); i++)
    {
        auto * pinp = this->m_matdata.Get(i);

        if (SubdivisionRatio == pinp->m_SubdivisionRatio)
        {
            pOut = pinp;
            return true;
        }
    }

	return false;
}

//==========================================================================
/**
@brief 解放
*/
void DX9_SphereData::Release(void)
{
	if (this->m_origin)
	{
        // バッファ解放
        if (this->m_vertexbuffer != nullptr)
        {
            this->m_vertexbuffer->Release();
            this->m_vertexbuffer = nullptr;
        }

        // メッシュ解放
        if (this->m_mesh2 != nullptr)
        {
            this->m_mesh2->Release();
            this->m_mesh2 = nullptr;
        }

        // メッシュ解放
        if (this->m_mesh1 != nullptr)
        {
            this->m_mesh1->Release();
            this->m_mesh1 = nullptr;
        }
		this->m_pseudo = nullptr;
        this->m_SubdivisionRatio = 0;
	}
}

//==========================================================================
/**
@brief クリエイト
@param SubdivisionRatio [in] 線の本数
@param pDevice [in] デバイス
*/
bool DX9_SphereData::CreateSphere(int SubdivisionRatio, LPDIRECT3DDEVICE9 pDevice)
{
	this->m_SubdivisionRatio = SubdivisionRatio;
	if (FAILED(D3DXCreateSphere(pDevice, 1.0f, this->m_SubdivisionRatio * 2, this->m_SubdivisionRatio, &this->m_mesh1, nullptr)))
	{
		return true;
	}

	this->m_mesh1->CloneMeshFVF(0, this->FVF_VERTEX_4, pDevice, &this->m_mesh2);
	this->m_mesh2->GetVertexBuffer(&this->m_vertexbuffer);

	return false;
}

//==========================================================================
/**
@brief コンバート
*/
void DX9_SphereData::convert(void)
{
	float r = 1.0f;
	this->m_vertexbuffer->Lock(0, 0, (void**)&this->m_pseudo, 0);
	for (DWORD i = 0; i < this->m_mesh2->GetNumVertices(); i++)
	{
		float q = 0.0f;
		float q2 = 0.0f;
		auto* pPseudo = &this->m_pseudo[i];

		q = atan2(pPseudo->pos.z, pPseudo->pos.x);

		pPseudo->Tex.x = q / (2.0f * 3.1415f);
		q2 = asin(pPseudo->pos.y / r);
		pPseudo->Tex.y = (1.0f - q2 / (3.1415f / 2.0f)) / 2.0f;
		if (pPseudo->Tex.x > 1.0f)
		{
			pPseudo->Tex.x = 1.0f;
		}

        pPseudo->Tex = -pPseudo->Tex;
		pPseudo->color = D3DCOLOR_RGBA(255, 255, 255, 255);
		pPseudo->Normal = D3DXVECTOR3(1, 1, 1);
	}
	this->m_vertexbuffer->Unlock();
}

//==========================================================================
/**
@brief コピー
@param pinp [in] コピー対象
*/
void DX9_SphereData::copy(const DX9_SphereData * pinp)
{
	this->m_SubdivisionRatio = pinp->m_SubdivisionRatio;
	this->m_mesh1 = pinp->m_mesh1;
	this->m_mesh2 = pinp->m_mesh2;
	this->m_vertexbuffer = pinp->m_vertexbuffer;
	this->m_pseudo = pinp->m_pseudo;
	this->m_origin = false;
}

_MSLIB_END