//==========================================================================
// 銃モード時の処理[GunModeObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GunModeObject.h"
#include "WeaponObject.h"
#include "resource_list.h"

GunModeObject::GunModeObject() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->m_WeaponCase = nullptr;
    this->SetObjectName("GunModeObject");
}

GunModeObject::~GunModeObject()
{
    this->m_WeaponCase = nullptr;
}

//=========================================================================
// 初期化
bool GunModeObject::Init(void)
{
    this->m_WeaponCase = this->GetObjects(mslib::DX9_ObjectID::Xmodel, "GunObject");

    auto *pObj = this->_2DObject()->Create();

    pObj->Init(0);
    pObj->SetCentralCoordinatesMood(true);
    pObj->SetPos({ this->GetWinSize().x / 2.0f,this->GetWinSize().y / 2.0f });
    this->_2DPolygon()->ObjectInput(pObj);
    this->_2DPolygon()->Init(RESOURCE_Crosshair_png, true);

    return false;
}

//=========================================================================
// 解放
void GunModeObject::Uninit(void)
{
}

//=========================================================================
// 更新
void GunModeObject::Update(void)
{
    if (mslib::nullptr_check(this->m_WeaponCase))
    {
        if (((WeaponObject*)this->m_WeaponCase)->UpdateKey())
        {
            this->_2DPolygon()->Update();
        }
    }
}

//=========================================================================
// 描画
void GunModeObject::Draw(void)
{
    if (mslib::nullptr_check(this->m_WeaponCase))
    {
        if (((WeaponObject*)this->m_WeaponCase)->UpdateKey())
        {
            this->_2DPolygon()->Draw();
        }
    }
}

//=========================================================================
// デバッグ
void GunModeObject::Debug(void)
{
}
