//==========================================================================
// タイマーゲージオブジェクト[Game_Timer_Gage.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Timer_Gage.h"
#include "Game_Timer.h"
#include "resource_list.h"

Game_Timer_Gage::Game_Timer_Gage() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->SetObjectName("Game_Timer_Gage");
    this->m_timer_obj = nullptr;
}


Game_Timer_Gage::~Game_Timer_Gage()
{
    this->m_gage.Release();
}

//==========================================================================
// 初期化
bool Game_Timer_Gage::Init(void)
{
    this->_2DPolygon()->Init(RESOURCE_Timer_Gage_Begin_png, true);
    this->_2DPolygon()->Init(RESOURCE_Timer_Gage_End_png, true);
    this->_2DPolygon()->Init(nullptr, true);
    this->_2DPolygon()->Init(nullptr, true);

    this->_2DObject()->Reserve(4);

    this->m_timer_obj = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "Game_Timer");

    auto * p_obj = this->_2DObject()->Create();

    p_obj->Init(0);

    p_obj = this->_2DObject()->Create();
    p_obj->Init(1);

    p_obj = this->_2DObject()->Create();
    p_obj->Init(2);

    p_obj = this->_2DObject()->Create();
    p_obj->Init(3);

    this->ObjectLoad();

    auto * ptex1 = this->_2DPolygon()->GetTexSize(2);
    auto * ptex2 = this->_2DPolygon()->GetTexSize(3);
    auto *p_obj1 = this->_2DObject()->Get(0);
    auto *p_obj2 = this->_2DObject()->Get(1);
    auto * pGage = this->m_gage.Create();

    pGage->Input({
        p_obj2->GetPos()->x - p_obj1->GetPos()->x,
        1.0f
        });
    ptex1->w = (int)pGage->GetMaxSize().x;
    ptex1->h = this->_2DPolygon()->GetTexSize(0)->h / 2;
    ptex2->w = ptex1->w;
    ptex2->h = ptex1->h;

    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(3));
    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(2));
    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(0));
    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(1));

    return false;
}

//==========================================================================
// 解放
void Game_Timer_Gage::Uninit(void)
{
}

//==========================================================================
// 更新
void Game_Timer_Gage::Update(void)
{
    auto *ptimer_obj = (CGame_Timer*)this->m_timer_obj;
    auto * pGage = this->m_gage.Get(0);
    auto * ptex = this->_2DPolygon()->GetTexSize(this->_2DObject()->Get(2)->GetIndex());

    if (mslib::nullptr_check(ptimer_obj))
    {
        pGage->InputMaxParam(ptimer_obj->GetMaxTime());
        ptex->w = (int)pGage->UpdateW(ptimer_obj->GetTime());
    }

    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void Game_Timer_Gage::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//==========================================================================
// デバッグ
void Game_Timer_Gage::Debug(void)
{   
    auto *ptimer_obj = (CGame_Timer*)this->m_timer_obj;

    if (mslib::nullptr_check(ptimer_obj))
    {
        auto * pGage = this->m_gage.Get(0);

        this->ImGui()->Text("time : %d", ptimer_obj->GetTime());
        this->ImGui()->Text("ゲージ加算幅 : %.2f", pGage->GetBuffSize().x);
        this->ImGui()->Text("現在の幅 : %d", (int)pGage->UpdateW(ptimer_obj->GetTime()));
    }
}
