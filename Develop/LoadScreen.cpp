//==========================================================================
// ロードスクリーン[LoadScreen.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "LoadScreen.h"
#include "resource_list.h"

//==========================================================================
// 初期化
bool CLoadScreen::Init(void)
{
    auto vwinsize = mslib::CTexvec<int>(0, 0, this->GetWinSize().x, this->GetWinSize().y);
    auto vpos = mslib::CVector2<float>(0, 0);
    mslib::CTexvec<int> *ptexsize = nullptr;

    // テクスチャの確保
    this->_2DPolygon()->Init();
    this->_2DPolygon()->Init(RESOURCE_LoadTex_DDS);
    this->_2DPolygon()->Init(RESOURCE_NowLoading_DDS);

    //==========================================================================
    // 背景
    this->_2DPolygon()->SetTexSize(0, vwinsize.w, vwinsize.h);
    this->_2DObject()->Create()->Init(0);
    this->_2DObject()->Get(0)->SetColor(0, 0, 0, 255);
    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(0));

    //==========================================================================
    // リング
    ptexsize = this->_2DPolygon()->GetTexSize(1);
    auto texsize = mslib::CTexvec<float>(0, 0, (float)ptexsize->w, (float)ptexsize->h);
    texsize *= 0.1f;
    *ptexsize = mslib::CTexvec<int>(0, 0, (int)texsize.w, (int)texsize.h);

    vpos = mslib::CVector2<float>((float)vwinsize.w - (ptexsize->w*0.5f), (float)vwinsize.h - (ptexsize->h*0.5f));
    this->_2DObject()->Create()->Init(1);
    this->_2DObject()->Get(1)->SetCentralCoordinatesMood(true);
    this->_2DObject()->Get(1)->SetColor(255, 255, 0, 255);
    this->_2DObject()->Get(1)->SetPos(vpos);
    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(1));

    //==========================================================================
    // ロードフォント
    ptexsize = this->_2DPolygon()->GetTexSize(2);
    texsize = mslib::CTexvec<float>(0, 0, (float)ptexsize->w, (float)ptexsize->h);
    texsize *= 0.1f;
    *ptexsize = mslib::CTexvec<int>(0, 0, (int)texsize.w, (int)texsize.h);

    this->m_paramload.m_a = 255;
    this->m_paramload.m_Change = false;
    this->m_paramload.m_a = 100;

    this->_2DObject()->Create()->Init(2);
    this->_2DObject()->Get(2)->SetCentralCoordinatesMood(true);
    this->_2DObject()->Get(2)->SetColor(255, 255, 0, this->m_paramload.m_a);
    this->_2DObject()->Get(2)->SetPos(vpos);
    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(2));

    return false;
}

//==========================================================================
// 解放
void CLoadScreen::Uninit(void)
{
    this->_2DPolygon()->Release();
    this->_2DObject()->Release();
}

//==========================================================================
// 更新
void CLoadScreen::Update(void)
{
    // 現在時刻の取得
    this->m_NewTime = timeGetTime();

    // 前回との差を取得
    auto ___time = this->m_NewTime - this->m_OldTime;

    //時間渡し
    this->m_OldTime = this->m_NewTime;

    // 差を回転力に変換
    this->_2DObject()->Get(1)->SetAnglePlus(___time*0.0025f);

    this->Change((int)(___time*0.5f));
    this->_2DObject()->Get(2)->SetColor(255, 255, 0, this->m_paramload.m_a);

    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CLoadScreen::Draw(void)
{
    auto pDevice = this->GetDirectX9Device();

    // 画質を高画質にする
    pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
    pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
    pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE); // 元のサイズより小さい時綺麗にする

                                                                  // Zバッファの無効
    pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);

    this->_2DPolygon()->Draw();

    // Zバッファの有効化
    pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);

    // 通常の画質に戻す
    pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
    pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
    pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // 元のサイズより小さい時綺麗にする}
}

//==========================================================================
// デバッグ
void CLoadScreen::Debug(void)
{
}

//==========================================================================
// αチェンジ
void CLoadScreen::Change(int Speed)
{
    // チェンジフラグ
    if (this->m_paramload.m_Change)
    {
        this->m_paramload.m_a -= Speed;
        if (this->m_paramload.m_a <= 0)
        {
            this->m_paramload.m_a = 0;
            this->m_paramload.m_Change = false;
        }
    }
    else
    {
        this->m_paramload.m_a += Speed;
        if (255 <= this->m_paramload.m_a)
        {
            this->m_paramload.m_a = 255;
            this->m_paramload.m_Change = true;
        }
    }
}
