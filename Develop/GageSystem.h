//==========================================================================
// ゲージシステム[GageSystem.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : GageSystem
// Content: ゲージシステム
//
//==========================================================================
class GageSystem
{
public:
    GageSystem();
    ~GageSystem();
    // Input 
    void Input(mslib::CVector2<float> max_size);
    // InputMaxParam
    void InputMaxParam(int max_param);
    // Width Update
    float UpdateW(int param);
    // Height Update
    float UpdateH(int param);
    // MAX Gage
    const mslib::CVector2<float> & GetMaxSize(void);
    // ゲージの基本サイズ操作のロック解除
    void Unlock(void);
    // 割合サイズ
    const mslib::CVector2<float> & GetBuffSize(void);
private:
    mslib::CVector2<float> m_max_gage_size; // 最大のサイズ
    mslib::CVector2<float> m_buff; // 割合サイズ
    bool m_Lock; // 鍵
};

