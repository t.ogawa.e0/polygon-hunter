//==========================================================================
// 消滅エフェクト[ExtinctionEffect.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : ExtinctionEffect
// Content: 消滅エフェクト
//
//=========================================================================
class ExtinctionEffect : mslib::DX9_Object
{
public:
    ExtinctionEffect();
    ~ExtinctionEffect();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
};

