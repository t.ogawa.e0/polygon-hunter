//==========================================================================
// 銃オブジェクト[CGunObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "WeaponObject.h"

//==========================================================================
//
// class  : CGunObject
// Content: 銃オブジェクト
//
//=========================================================================
class CGunObject : public WeaponObject
{
private:
    enum class EObject
    {
        Gun, // 銃
        Muzzle, // 銃口
        Material, // ポイント
    };
public:
    CGunObject();
    ~CGunObject();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    // リセットモーション
    void Reset(void)override;
private:
    void * m_PlayerObj; // プレイヤーオブジェクト
    void * m_CameraObj; // カメラオブジェクト
    void * m_BulletNormal; // ノーマルバレット
    void * m_input; // インプットオブジェクトへのアクセス
    void * m_keyboard_object; // キーボードオブジェクト
    void * m_StatusObj; // ステータスオブジェクト
};

