//==========================================================================
// タイトルマネージャー [TitleManager.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : TitleManager
// Content: タイトルマネージャー 
//
//=========================================================================
class TitleManager : public mslib::DX9_Object
{
public:
    TitleManager();
    ~TitleManager();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
private:
    void * m_KeyboardObject; // キーオブジェクト
    void * m_InputSystem; // XInput
};

