//==========================================================================
// ゲーム画面に表示するアイコンオブジェクト[GameICODrawObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameICODrawObject.h"
#include "BurstGage.h"
#include "resource_list.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int __num_buff = 16; // バフの数
constexpr float __buff_buff = 0.1f; // バフのバフ
constexpr float __recovery_buff = 1.0f; // リカバリーのバフ

GameICODrawSystemObject::GameICODrawSystemObject()
{
    this->m_sys_level = 0;
    this->m_input_flag = false;
    this->m_sys_draw_key = false;
    this->m_ID = GameICODrawSystemID::begin;
    this->m_obj = nullptr;

}

GameICODrawSystemObject::GameICODrawSystemObject(int level, mslib::DX9_2DObject * obj, GameICODrawSystemID id)
{
    this->m_sys_level = level;
    this->m_input_flag = false;
    this->m_sys_draw_key = false;
    this->m_ID = id;
    this->m_obj = obj;
}

GameICODrawSystemObject::~GameICODrawSystemObject()
{
}

//==========================================================================
// ICOIDの取得
GameICODrawSystemID GameICODrawSystemObject::GetICOID(void)
{
    return this->m_ID;
}

GameICODrawObject::GameICODrawObject() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->SetObjectName("GameICODrawObject");
    this->m_burst_gage = nullptr;
}

GameICODrawObject::~GameICODrawObject()
{
    this->m_system_level.clear();
    this->m_Buff.clear();
}

//==========================================================================
// 初期化
bool GameICODrawObject::Init(void)
{
    this->_2DPolygon()->Init(RESOURCE_攻撃速度UP_ICO_png, true);
    this->_2DPolygon()->Init(RESOURCE_移動速度UP_ICO_png, true);
    this->_2DPolygon()->Init(RESOURCE_HP自動回復_ICO_png, true);
    this->_2DPolygon()->Init(RESOURCE_MP自動回復_ICO_png, true);
    this->_2DPolygon()->Init(RESOURCE_SP自動回復_ICO_png, true);

    this->_2DPolygon()->Init(RESOURCE_攻撃速度UP_ICO_png, true);
    this->_2DPolygon()->Init(RESOURCE_移動速度UP_ICO_png, true);
    this->_2DPolygon()->Init(RESOURCE_HP自動回復_ICO_png, true);
    this->_2DPolygon()->Init(RESOURCE_MP自動回復_ICO_png, true);
    this->_2DPolygon()->Init(RESOURCE_SP自動回復_ICO_png, true);

    this->_2DPolygon()->Init(RESOURCE_攻撃速度UP_ICO_png, true);
    this->_2DPolygon()->Init(RESOURCE_移動速度UP_ICO_png, true);
    this->_2DPolygon()->Init(RESOURCE_HP自動回復_ICO_png, true);
    this->_2DPolygon()->Init(RESOURCE_MP自動回復_ICO_png, true);
    this->_2DPolygon()->Init(RESOURCE_SP自動回復_ICO_png, true);

    this->_2DPolygon()->Init(RESOURCE_スーパーアーマー_ICO_png, true);

    for (int i = 0; i < __num_buff; i++)
    {
        this->_2DObject()->Create()->Init(i);
    }

    this->ObjectLoad();

    this->m_burst_gage = this->GetObjects(mslib::DX9_ObjectID::Polygon2D, "BurstGage");

    this->m_system_level.emplace_back(5, this->_2DObject()->Get(0), GameICODrawSystemID::AttackSpeed);
    this->m_system_level.emplace_back(10, this->_2DObject()->Get(1), GameICODrawSystemID::MoveSpeed);
    this->m_system_level.emplace_back(15, this->_2DObject()->Get(2), GameICODrawSystemID::HPRecovery);
    this->m_system_level.emplace_back(20, this->_2DObject()->Get(3), GameICODrawSystemID::MPRecovery);
    this->m_system_level.emplace_back(25, this->_2DObject()->Get(4), GameICODrawSystemID::SPRecovery);

    this->m_system_level.emplace_back(35, this->_2DObject()->Get(5), GameICODrawSystemID::AttackSpeed);
    this->m_system_level.emplace_back(40, this->_2DObject()->Get(6), GameICODrawSystemID::MoveSpeed);
    this->m_system_level.emplace_back(45, this->_2DObject()->Get(7), GameICODrawSystemID::HPRecovery);
    this->m_system_level.emplace_back(50, this->_2DObject()->Get(8), GameICODrawSystemID::MPRecovery);
    this->m_system_level.emplace_back(55, this->_2DObject()->Get(9), GameICODrawSystemID::SPRecovery);

    this->m_system_level.emplace_back(65, this->_2DObject()->Get(10), GameICODrawSystemID::AttackSpeed);
    this->m_system_level.emplace_back(70, this->_2DObject()->Get(11), GameICODrawSystemID::MoveSpeed);
    this->m_system_level.emplace_back(75, this->_2DObject()->Get(12), GameICODrawSystemID::HPRecovery);
    this->m_system_level.emplace_back(80, this->_2DObject()->Get(13), GameICODrawSystemID::MPRecovery);
    this->m_system_level.emplace_back(85, this->_2DObject()->Get(14), GameICODrawSystemID::SPRecovery);

    this->m_system_level.emplace_back(99, this->_2DObject()->Get(15), GameICODrawSystemID::SuperArmor);

    this->m_Buff[GameICODrawSystemID::AttackSpeed] = GameICOBuff();
    this->m_Buff[GameICODrawSystemID::MoveSpeed] = GameICOBuff();
    this->m_Buff[GameICODrawSystemID::HPRecovery] = GameICOBuff();
    this->m_Buff[GameICODrawSystemID::MPRecovery] = GameICOBuff();
    this->m_Buff[GameICODrawSystemID::SPRecovery] = GameICOBuff();
    this->m_Buff[GameICODrawSystemID::SuperArmor] = GameICOBuff();

    return false;
}

//==========================================================================
// 解放
void GameICODrawObject::Uninit(void)
{
}

//==========================================================================
// 更新
void GameICODrawObject::Update(void)
{
    auto *pLevel = (BurstGage*)this->m_burst_gage;

    if (mslib::nullptr_check(pLevel))
    {
        // レベルを元に処理の制御を行う
        for (auto itr = this->m_system_level.begin(); itr != this->m_system_level.end(); ++itr)
        {
            // 現在参照中のレベルが現在のレベルより低い場合描画キーを有効化
            if (itr->m_sys_level <= pLevel->GetLevel())
            {
                itr->m_sys_draw_key = true;
            }
            // 現在のレベルを超えたので描画キーを無効化
            else
            {
                if (itr->m_input_flag == true)
                {
                    itr->m_sys_draw_key = false;
                    itr->m_input_flag = false;
                    this->_2DPolygon()->ObjectDelete(itr->m_obj);

                    auto itr2 = this->m_Buff.find(itr->m_ID);
                    if (itr2 != this->m_Buff.end())
                    {
                        switch (itr2->first)
                        {
                        case GameICODrawSystemID::begin:
                            itr2->second.m_buff = 0.0f;
                            break;
                        case GameICODrawSystemID::AttackSpeed:
                            itr2->second.m_buff -= __buff_buff;
                            break;
                        case GameICODrawSystemID::MoveSpeed:
                            itr2->second.m_buff -= __buff_buff;
                            break;
                        case GameICODrawSystemID::HPRecovery:
                            itr2->second.m_buff -= __recovery_buff;
                            break;
                        case GameICODrawSystemID::MPRecovery:
                            itr2->second.m_buff -= __recovery_buff;
                            break;
                        case GameICODrawSystemID::SPRecovery:
                            itr2->second.m_buff -= __recovery_buff;
                            break;
                        case GameICODrawSystemID::SuperArmor:
                            itr2->second.m_key = false;
                            break;
                        case GameICODrawSystemID::end:
                            itr2->second.m_buff = 0.0f;
                            break;
                        default:
                            itr2->second.m_buff = 0.0f;
                            break;
                        }
                    }
                }
            }

            // 描画キーが有効
            if (itr->m_sys_draw_key == true)
            {
                // 入力済みではない
                if (itr->m_input_flag == false)
                {
                    // 入力されていないので入力処理
                    this->_2DPolygon()->ObjectInput(itr->m_obj);
                    itr->m_input_flag = true;

                    auto itr2 = this->m_Buff.find(itr->m_ID);
                    if (itr2 != this->m_Buff.end())
                    {
                        switch (itr2->first)
                        {
                        case GameICODrawSystemID::begin:
                            itr2->second.m_buff = 0.0f;
                            itr2->second.m_key = false;
                            break;
                        case GameICODrawSystemID::AttackSpeed:
                            itr2->second.m_buff += __buff_buff;
                            break;
                        case GameICODrawSystemID::MoveSpeed:
                            itr2->second.m_buff += __buff_buff;
                            break;
                        case GameICODrawSystemID::HPRecovery:
                            itr2->second.m_buff += __recovery_buff;
                            break;
                        case GameICODrawSystemID::MPRecovery:
                            itr2->second.m_buff += __recovery_buff;
                            break;
                        case GameICODrawSystemID::SPRecovery:
                            itr2->second.m_buff += __recovery_buff;
                            break;
                        case GameICODrawSystemID::SuperArmor:
                            itr2->second.m_key = true;
                            break;
                        case GameICODrawSystemID::end:
                            itr2->second.m_buff = 0.0f;
                            itr2->second.m_key = false;
                            break;
                        default:
                            itr2->second.m_buff = 0.0f;
                            itr2->second.m_key = false;
                            break;
                        }
                    }
                }
            }
        }
    }

    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void GameICODrawObject::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//==========================================================================
// デバッグ
void GameICODrawObject::Debug(void)
{
    this->ImGui()->Text("AttackSpeed : %f", this->m_Buff[GameICODrawSystemID::AttackSpeed].m_buff);
    this->ImGui()->Text("MoveSpeed : %f", this->m_Buff[GameICODrawSystemID::MoveSpeed].m_buff);
    this->ImGui()->Text("HPRecovery : %f", this->m_Buff[GameICODrawSystemID::HPRecovery].m_buff);
    this->ImGui()->Text("MPRecovery : %f", this->m_Buff[GameICODrawSystemID::MPRecovery].m_buff);
    this->ImGui()->Text("SPRecovery : %f", this->m_Buff[GameICODrawSystemID::SPRecovery].m_buff);
    this->ImGui()->Text("SuperArmor : %d", this->m_Buff[GameICODrawSystemID::SuperArmor].m_key);
}

const GameICOBuff * GameICODrawObject::GetBuffData(GameICODrawSystemID id)
{
    auto itr = this->m_Buff.find(id);
    if (itr != this->m_Buff.end())
    {
        return &itr->second;
    }
    return nullptr;
}

GameICOBuff::GameICOBuff()
{
    this->m_key = false;
    this->m_buff = 0.0f;
}

GameICOBuff::GameICOBuff(bool key, float value)
{
    this->m_key = key;
    this->m_buff = value;
}

GameICOBuff::~GameICOBuff()
{
}
