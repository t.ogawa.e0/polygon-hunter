//==========================================================================
// バレットオブジェクト[BulletObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "DamageObject.h"

//==========================================================================
//
// class  : BulletObjectStruct
// Content: バレットの構成データ
//
//==========================================================================
class BulletObjectStruct : public DamageObjectParam
{
public:
    BulletObjectStruct();
    BulletObjectStruct(const mslib::DX9_3DObject * pobj, const mslib::CVector3<float> & speed, const mslib::CVector3<float> & rot, int limit_time, int label);
    ~BulletObjectStruct();
    // 更新
    void Update(void);
    // 移動力の取得
    const mslib::CVector3<float> * GetSpeed(void);
    // 回転力の取得
    const mslib::CVector3<float> * GetRot(void);
    // 生存時間の取得
    int GetSurvivalTime(void);
private:
    mslib::CVector3<float> m_speed; // 速度
    mslib::CVector3<float> m_rot; // 回転
    int m_survival_time_limit; // 生存時間
    int m_survival_time; // 生存時間
};

//==========================================================================
//
// class  : BulletInputSystem
// Content: BulletInputSystem
//
//=========================================================================
class BulletInputSystem
{
public:
    BulletInputSystem();
    virtual ~BulletInputSystem();
    // バレットデータの取得
    std::list<BulletObjectStruct> * GetBulletObjectStruct(void);
    // オブジェクトの生成
    virtual void Create(const mslib::DX9_3DObject * pobj, const mslib::CVector3<float> & speed, const mslib::CVector3<float> & rot, int limit_time) = 0;
protected:
    std::list<BulletObjectStruct> m_bullet_data; // バレットデータ
};

//==========================================================================
//
// class  : BulletObject
// Content: バレットオブジェクト
//
//==========================================================================
class BulletObject : public mslib::DX9_Object
{
public:
    BulletObject();
    ~BulletObject();

    // 初期化
    bool Init(void) override;

    // 解放
    void Uninit(void) override;

    // 更新
    void Update(void) override;

    // 描画
    void Draw(void) override;

    // デバッグ
    void Debug(void) override;

    // バレットデータ
    std::list<std::list<BulletObjectStruct>*> * GetBulletInputSystem(void);
private:
    std::list<std::list<BulletObjectStruct>*> m_object_data; // バレットデータ
};

