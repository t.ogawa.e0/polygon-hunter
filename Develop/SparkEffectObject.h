//==========================================================================
// 火花エフェクト[SparkEffectObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : SparkEffectData
// Content: 火花エフェクト
//
//=========================================================================
class SparkEffectData
{
public:
    SparkEffectData();
    SparkEffectData(const mslib::DX9_3DObject & obj);
    ~SparkEffectData();
public:
    mslib::DX9_3DObject m_obj; // オブジェクト
    bool m_delete; // 解放判定
};

//==========================================================================
//
// class  : SparkEffectObject
// Content: 火花エフェクト
//
//=========================================================================
class SparkEffectObject : public mslib::DX9_Object
{
public:
    SparkEffectObject();
    ~SparkEffectObject();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
    // 生成
    void Create(const mslib::DX9_3DObject & obj);
private:
    std::list<SparkEffectData> m_data; // データ
    void * m_CameraObject;
};

