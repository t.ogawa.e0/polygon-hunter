//==========================================================================
// テストオブジェクト[TestObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : TestObject
// Content: テストオブジェクト
//
//=========================================================================
class TestObject : public mslib::DX9_Object
{
public:
    TestObject();
    ~TestObject();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
private:
    void * m_LinearPolygon;
    void * m_EnclosurePolygon;
    void * m_PlayerObject;
    void * m_Game_Over_Clear;
};

