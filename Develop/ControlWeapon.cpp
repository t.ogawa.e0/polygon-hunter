//==========================================================================
// ウェポンコントロール[ControlWeapon.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ControlWeapon.h"
#include "InputSystem.h"
#include "KeyboardObject.h"
#include "GameICODrawObject.h"

constexpr int __change_time = 10;

ControlWeapon::ControlWeapon()
{
    this->m_serect = WeaponObject::EList::Sword;
    this->m_serect_old = WeaponObject::EList::Sword;
    this->m_MotionID = MotionName::Wait;
    this->m_MotionID_Old = "";
    this->m_input = nullptr;
    this->m_keyboard_object = nullptr;
    this->m_change_key = false;
    this->m_change_count = 0;
    this->SetObjectName("ControlWeapon");
}


ControlWeapon::~ControlWeapon()
{
    this->m_WeaponCase.clear();
}

//=========================================================================
// 初期化
bool ControlWeapon::Init(void)
{
    this->m_input = this->GetObjects(mslib::DX9_ObjectID::Default, "InputSystem");
    this->m_keyboard_object = this->GetObjects(mslib::DX9_ObjectID::Default, "KeyboardObject");
    this->m_WeaponCase.push_back(this->GetObjects(mslib::DX9_ObjectID::Xmodel, "GunObject"));
    this->m_WeaponCase.push_back(this->GetObjects(mslib::DX9_ObjectID::Xmodel, "SwordObject"));

    return false;
}

//=========================================================================
// 解放
void ControlWeapon::Uninit(void)
{
}

//=========================================================================
// 更新
void ControlWeapon::Update(void)
{
    if (this->GetPlayKey())
    {
        this->Change();
        this->Action();
    }

    // ウェポンオブジェクトの処理を行う
    for (auto itr = this->m_WeaponCase.begin(); itr != this->m_WeaponCase.end(); ++itr)
    {
        auto *pWeaponObject = (WeaponObject*)(*itr);

        // 処理ロックをかける
        pWeaponObject->Lock();

        // 操作したいウェポンのロックを解除する
        if (this->m_serect == pWeaponObject->GetWeaponID())
        {
            if (pWeaponObject->Check().key1 == true && pWeaponObject->Check().key2 == true)
            {
                if (pWeaponObject->GetMotion() == MotionName::Attack)
                {
                    this->m_MotionID = MotionName::Wait;
                }
            }

            if (this->m_MotionID_Old != this->m_MotionID)
            {
                pWeaponObject->Reset();
                this->m_MotionID_Old = this->m_MotionID;
                pWeaponObject->SetMotion(this->m_MotionID);
            }
            pWeaponObject->Unlock();
        }

        if (this->m_serect_old != this->m_serect)
        {
            if (this->m_serect_old == pWeaponObject->GetWeaponID())
            {
                this->m_MotionID = MotionName::Wait;
                pWeaponObject->Reset();
            }
            this->m_serect_old = this->m_serect;
        }
    }
}

//=========================================================================
// 描画
void ControlWeapon::Draw(void)
{
}

//=========================================================================
// デバッグ
void ControlWeapon::Debug(void)
{
}

//=========================================================================
// チェンジ
void ControlWeapon::Change(void)
{
    auto * p_keyborad = (KeyboardObject*)this->m_keyboard_object;
    auto * p_xinput = (CInputSystem*)this->m_input;

    //=========================================================================
    // 武器切り替え
    //=========================================================================
    if (p_keyborad->KEY_J(KeyboardObject::KeyboardType::Trigger))
    {
        this->m_change_count = 0;
    }
    if (p_xinput->GetXInput()->Trigger(mslib::XInputButton::RIGHT_RB, 0))
    {
        this->m_change_count = 0;
    }
    if (this->Dinput_Mouse()->Trigger(mslib::DirectInputMouseButton::Right))
    {
        this->m_change_count = 0;
    }

    //=========================================================================
    // 変更処理
    //=========================================================================
    if (this->m_change_key == false)
    {
        this->m_serect = WeaponObject::EList::Sword;
    }
    else if (this->m_change_key == true)
    {
        this->m_serect = WeaponObject::EList::Gun;
    }

    //=========================================================================
    // 変更タイミング処理
    //=========================================================================
    if (this->m_change_count<__change_time)
    {
        this->m_change_count++;

        if (p_keyborad->KEY_J(KeyboardObject::KeyboardType::Repeat))
        {
            mslib::bool_change(this->m_change_key);
            this->m_change_count = __change_time;
        }
        if (p_xinput->GetXInput()->Release(mslib::XInputButton::RIGHT_RB, 0))
        {
            mslib::bool_change(this->m_change_key);
            this->m_change_count = __change_time;
        }
        if (this->Dinput_Mouse()->Release(mslib::DirectInputMouseButton::Right))
        {
            mslib::bool_change(this->m_change_key);
            this->m_change_count = __change_time;
        }
    }
}

//=========================================================================
// アクション
void ControlWeapon::Action(void)
{
    auto * p_xinput = (CInputSystem*)this->m_input;
    auto * p_keyborad = (KeyboardObject*)this->m_keyboard_object;
    bool bkey = false;

    // コントロール(XInput)
    if (mslib::nullptr_check(p_xinput))
    {
        if (p_xinput->GetXInput()->Check(0))
        {
            if (p_xinput->GetXInput()->Press(mslib::XInputButton::X, 0))
            {
                bkey = true;
                if (this->m_MotionID != MotionName::Attack)
                {
                    this->m_MotionID = MotionName::Attack;
                }
            }

            if (this->m_MotionID != MotionName::Attack)
            {
                if (p_xinput->GetXInput()->AnalogL(0))
                {
                    bkey = true;
                    if (this->m_MotionID != MotionName::Moving)
                    {
                        this->m_MotionID = MotionName::Moving;
                    }
                }
                else if (bkey == false)
                {
                    if (this->m_MotionID != MotionName::Wait)
                    {
                        this->m_MotionID = MotionName::Wait;
                    }
                }
            }
        }
    }

    // コントロール(キーボード)
    if (mslib::nullptr_check(p_keyborad))
    {
        if (p_keyborad->KEY_U(KeyboardObject::KeyboardType::Press))
        {
            bkey = true;
            if (this->m_MotionID != MotionName::Attack)
            {
                this->m_MotionID = MotionName::Attack;
            }
        }

        if (this->m_MotionID != MotionName::Attack)
        {
            if (
                p_keyborad->KEY_W(KeyboardObject::KeyboardType::Press) ||
                p_keyborad->KEY_A(KeyboardObject::KeyboardType::Press) ||
                p_keyborad->KEY_S(KeyboardObject::KeyboardType::Press) ||
                p_keyborad->KEY_D(KeyboardObject::KeyboardType::Press)
                )
            {
                bkey = true;
                if (this->m_MotionID != MotionName::Moving)
                {
                    this->m_MotionID = MotionName::Moving;
                }
            }
            else if (bkey == false)
            {
                if (this->m_MotionID != MotionName::Wait)
                {
                    this->m_MotionID = MotionName::Wait;
                }
            }
        }
    }

    if (mslib::nullptr_check(this->Dinput_Mouse()))
    {
        if (this->Dinput_Mouse()->Press(mslib::DirectInputMouseButton::Left))
        {
            bkey = true;
            if (this->m_MotionID != MotionName::Attack)
            {
                this->m_MotionID = MotionName::Attack;
            }
        }
    }
}
