//==========================================================================
// 銃モード時の処理[GunModeObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : GunModeObject
// Content: 銃モード時の処理
//
//=========================================================================
class GunModeObject : public mslib::DX9_Object
{
public:
    GunModeObject();
    ~GunModeObject();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
private:
    void * m_WeaponCase; // 銃オブジェクト
};

