//==========================================================================
// 大型ボスLargeBoss.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "EnemyObject.h"
#include "PlayObject.h"

//==========================================================================
//
// class  : EnemyLargeBoss
// Content: 大型ボス
//
//=========================================================================
class EnemyLargeBoss : public CharacterObject, public EnemyInputSystem, public PlayObject
{
public:
    EnemyLargeBoss();
    ~EnemyLargeBoss();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    // 生成
    void Create(void)override;
private:
    // 大脳
    void Cerebrum(void);
    // 索敵
    void SearchEnemies(void);
    // 認識
    void Recognition(void);
    // フィールドとのコリジョン
    void FieldCollision(void)override;
    // ジャンプ
    void Jump(void)override;
    // 移動
    void Move(void)override;
    // ステップ
    bool Step(void)override;
    // アクション
    void Action(void)override;
    // 消滅処理
    void DeleteObject(void);
    // ベクトルセット
    void SetVector(void);
    // 古い座標のセット
    void OldPosSet(void);
private:
    void * m_Field; // フィールドへのアクセス権
    void * m_gravity; // 重力へのアクセス
    void * m_PlayerObject; // プレイヤー
    void * m_LinearPolygon;
    void * m_EnclosurePolygon;
};

