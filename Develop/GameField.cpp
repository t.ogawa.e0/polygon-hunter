//==========================================================================
// ゲームのフィールド[GameField.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameField.h"
#include "resource_list.h"

//=========================================================================
// 初期化
bool CGameField::Init(void)
{
    auto * p_obj = this->_3DObject()->Create();

    p_obj->Init(0);

    this->_3DField()->ObjectInput(p_obj);

    this->_3DField()->Init(RESOURCE_Asphalt_jpg, 1, 1);

    // Mapの読み込み
    this->_3DField()->Loat(RESOURCE_TestField_bin);

    return false;
}

//=========================================================================
// 解放
void CGameField::Uninit(void)
{

}

//=========================================================================
// 更新
void CGameField::Update(void)
{
    this->_3DField()->Update();
}

//=========================================================================
// 描画
void CGameField::Draw(void)
{
    this->_3DField()->Draw();
}

//=========================================================================
// デバッグ
void CGameField::Debug(void)
{
}
