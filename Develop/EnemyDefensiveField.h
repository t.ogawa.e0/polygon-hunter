//==========================================================================
// エネミー防御フィールド[EnemyDefensiveField.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "DefensiveFieldObject.h"

//==========================================================================
//
// class  : EnemyDefensiveField
// Content: エネミー防御フィールド
//
//=========================================================================
class EnemyDefensiveField : public DefensiveFieldObject
{
public:
    EnemyDefensiveField();
    ~EnemyDefensiveField();
    // 初期化
    bool Init(void) override;
    // 解放
    void Uninit(void) override;
    // 更新
    void Update(void) override;
    // 描画
    void Draw(void) override;
    // デバッグ
    void Debug(void) override;
private:
    // 生成
    void Create(void);
    // 処理
    void DefensiveFieldUpdate(void);
    // 破棄
    void DefensiveFieldDelete(void);
    // データのチェック
    void CheckData(void);
private:
    void * m_EnemyLargeBoss; // ボスエネミー
};

