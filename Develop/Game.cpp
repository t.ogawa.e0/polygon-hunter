//==========================================================================
// ゲームシーン[Game.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Screen.h" // デバッグ用

#include "TestObject.h"

#include "DamageObject.h"
#include "Game.h"
#include "GameManager.h"
#include "PlayerObject.h"
#include "GameCamera.h"
#include "GameField.h"
#include "Gravity.h"
#include "Game_Timer.h"
#include "Game_Skydome.h"
#include "EnemyObject.h"
#include "InputSystem.h"
#include "Game_Timer_Gage.h"
#include "GameLifeGage.h"
#include "StatusGage.h"
#include "BurstGage.h"
#include "PlayerAttackEffect_A.h"
#include "GunObject.h"
#include "ControlWeapon.h"
#include "KeyboardObject.h"
#include "SwordObject.h"
#include "GunModeObject.h"
#include "BulletNormalEffectObject.h"
#include "GameICODrawObject.h"
#include "EnemySmallSize.h"
#include "EnemyLargeBoss.h"
#include "LinearPolygon.h"
#include "EnclosurePolygon.h"
#include "ExplosiveEffect.h"
#include "DamagePolygonObject.h"
#include "SparkEffectObject.h"
#include "DamageBox.h"
#include "EnemyDefensiveField.h"
#include "HighGround.h"
#include "Game_Over_Clear.h"
#include "UserInformation.h"

CGame::CGame()
{
    CInputSystem * InputSystem = nullptr;
    CPlayerObject * PlayerObject = nullptr;
    CGameCamera * GameCamera = nullptr;
    CGameManager * managa = nullptr;
    CGameField * fild = nullptr;
    CGravity * Gravity = nullptr;
    CGame_Timer * Game_Timer = nullptr;
    CGame_Skydome * Game_Skydome = nullptr;
    CEnemyObject * EnemyObject = nullptr;
    Game_Timer_Gage * pGame_Timer_Gage = nullptr;
    GameLifeGage * pGameLifeGage = nullptr;
    StatusGage * pStatusGage = nullptr;
    BurstGage * pBurstGage = nullptr;
    CGunObject * pCGunObject = nullptr;
    SwordObject * pSwordObject = nullptr;
    ControlWeapon * pControlWeapon = nullptr;
    KeyboardObject * pKeyboardObject = nullptr;
    GunModeObject *pGunModeObject = nullptr;
    BulletNormalEffectObject * pBulletNormalEffectObject = nullptr;
    GameICODrawObject *pGameICODrawObject = nullptr;
    EnemySmallSize * pEnemySmallSize = nullptr;
    EnemyLargeBoss * pEnemyLargeBoss = nullptr;
    LinearPolygon * pLinearPolygon = nullptr;
    ExplosiveEffect * pExplosiveEffect = nullptr;
    EnclosurePolygon * pEnclosurePolygon = nullptr;
    TestObject * pTestObject = nullptr;
    DamageObject * pDamageObject = nullptr;
    BulletObject * pBulletObject = nullptr;
    DamagePolygonObject * pDamagePolygonObject = nullptr;
    SparkEffectObject * pSparkEffectObject = nullptr;
    EnemyDefensiveField * pEnemyDefensiveField = nullptr;
    HighGround * pHighGround = nullptr;
    Game_Over_Clear * pGame_Over_Clear = nullptr;
    UserInformation * pUserInformation = nullptr;

    //==========================================================================
    // ファイル指定
    //==========================================================================
    mslib::DX9_Object::SetFilePass("resource/data/level1.bin");

    //==========================================================================
    // システム
    //==========================================================================
    mslib::DX9_Object::NewObject(pTestObject);

    mslib::DX9_Object::NewObject(pUserInformation);
    mslib::DX9_Object::NewObject(EnemyObject);
    mslib::DX9_Object::NewObject(pBulletObject);
    mslib::DX9_Object::NewObject(pDamagePolygonObject);
    mslib::DX9_Object::NewObject(pDamageObject);
    mslib::DX9_Object::NewObject(InputSystem, 1);
    mslib::DX9_Object::NewObject(pKeyboardObject);
    mslib::DX9_Object::NewObject(managa);
    mslib::DX9_Object::NewObject(Gravity);
    mslib::DX9_Object::NewObject(pControlWeapon);

    //==========================================================================
    // 2D
    //==========================================================================
    mslib::DX9_Object::NewObject(Game_Timer);
    mslib::DX9_Object::NewObject(pGame_Timer_Gage);
    mslib::DX9_Object::NewObject(pGameLifeGage);
    mslib::DX9_Object::NewObject(pStatusGage);
    mslib::DX9_Object::NewObject(pBurstGage);
    mslib::DX9_Object::NewObject(pGunModeObject);
    mslib::DX9_Object::NewObject(pGameICODrawObject);
    mslib::DX9_Object::NewObject(pGame_Over_Clear);

    //==========================================================================
    // 3D
    //==========================================================================
    mslib::DX9_Object::NewObject(pHighGround);
    mslib::DX9_Object::NewObject(pEnemySmallSize);
    mslib::DX9_Object::NewObject(pEnemyLargeBoss);
    mslib::DX9_Object::NewObject(PlayerObject);
    mslib::DX9_Object::NewObject(pCGunObject);
    mslib::DX9_Object::NewObject(pSwordObject);
    mslib::DX9_Object::NewObject(pBulletNormalEffectObject);
    mslib::DX9_Object::NewObject(pLinearPolygon);
    mslib::DX9_Object::NewObject(pEnclosurePolygon);
    mslib::DX9_Object::NewObject(pExplosiveEffect);
    mslib::DX9_Object::NewObject(pSparkEffectObject);
    mslib::DX9_Object::NewObject(pEnemyDefensiveField);
    mslib::DX9_Object::NewObject(GameCamera);
    mslib::DX9_Object::NewObject(fild);
    mslib::DX9_Object::NewObject(Game_Skydome);
}

CGame::~CGame()
{
}

//==========================================================================
// 初期化
bool CGame::Init(void)
{
    return mslib::DX9_Object::InitAll();
}

//==========================================================================
// 解放
void CGame::Uninit(void)
{
    mslib::DX9_Object::ReleaseAll();
}

//==========================================================================
// 更新
void CGame::Update(void)
{
#if defined(_SETImGui_Dx9_) || defined(_DEBUG) || defined(DEBUG)
    auto *pKeyboard = mslib::DX9_DeviceManager::GetKeyboard();

    if (pKeyboard->Trigger(mslib::DirectInputKeyboardButton::KEY_F1))
    {
        CScreen::screenchange(SceneName::Result);
    }
#endif

    mslib::DX9_Object::UpdateAll();
}

//==========================================================================
// 描画
void CGame::Draw(void)
{
    mslib::DX9_Object::DrawAll();
}

