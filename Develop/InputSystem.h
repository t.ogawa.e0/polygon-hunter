//==========================================================================
// インプットライブラリ[InputSystem.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CInputSystem
// Content: インプットライブラリ操作
//
//==========================================================================
class CInputSystem :public mslib::DX9_Object
{
public:
    CInputSystem();
    CInputSystem(int NumXinput);
    ~CInputSystem();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    // コントローラーの数
    int GetNumControl(void);

    // XInputの取得
    mslib::XInput * GetXInput(void);
private:
    int m_num_xinput; // xInputコントローラー数
};

