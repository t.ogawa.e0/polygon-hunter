//==========================================================================
// リザルト仮置きオブジェクト[Result_TemporaryPlace.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Result_TemporaryPlace.h"
#include "resource_list.h"


CResult_TemporaryPlace::CResult_TemporaryPlace() : DX9_Object(mslib::DX9_ObjectID::Polygon2D)
{
    this->SetObjectName("Result_TemporaryPlace");
}


CResult_TemporaryPlace::~CResult_TemporaryPlace()
{
}

bool CResult_TemporaryPlace::Init(void)
{
    this->_2DPolygon()->Init(RESOURCE_リザルト仮置き_png, true);
    auto * p_obj = this->_2DObject()->Create();
    p_obj->Init(0);
    this->_2DPolygon()->ObjectInput(p_obj);

    return false;
}

void CResult_TemporaryPlace::Uninit(void)
{
}

void CResult_TemporaryPlace::Update(void)
{
    this->_2DPolygon()->Update();
}

void CResult_TemporaryPlace::Draw(void)
{
    this->_2DPolygon()->Draw();
}

void CResult_TemporaryPlace::Debug(void)
{
}
