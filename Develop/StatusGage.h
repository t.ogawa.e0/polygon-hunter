//==========================================================================
// ステータスのゲージ[StatusGage.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GageSystem.h"
#include "PlayObject.h"

class StatusData
{
public:
    StatusData();
    ~StatusData();
public:
    float m_HP; // hp
    float m_MP; // mp
    float m_SP; // sp
};

//==========================================================================
//
// class  : StatusGage
// Content: ステータスのゲージ
//
//=========================================================================
class StatusGage : public mslib::DX9_Object, public PlayObject
{
    enum class ObjectID
    {
        Begin,
        label_HP = Begin,
        label_MP,
        label_SP,
        label_END,

        label_HP_GageBegin = label_END,
        label_HP_GageEnd,
        label_MP_GageBegin,
        label_MP_GageEnd,
        label_SP_GageBegin,
        label_SP_GageEnd,
        label_HP_Gage_Sub,
        label_HP_Gage_Main,
        label_MP_Gage_Sub,
        label_MP_Gage_Main,
        label_SP_Gage_Sub,
        label_SP_Gage_Main,
        Gage_End,
        End = Gage_End,
    };
public:
    StatusGage();
    ~StatusGage();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;

    // HP操作
    // 戻り値 : 減らせない場合false 減らせる場合 true
    bool HP_Operation(float value);

    // MP操作
    // 戻り値 : 減らせない場合false 減らせる場合 true
    bool MP_Operation(float value);

    // SP操作
    // 戻り値 : 減らせない場合false 減らせる場合 true
    bool SP_Operation(float value);

    // ステータスの取得
    const StatusData * GetStatusData(void);
private:
    // ステータス共通処理
    bool StatusOperation(float & _this, float value);
private:
    mslib::vector_wrapper<GageSystem> m_gage; // ゲージ
    StatusData m_status; // status
    void * m_ICOObj; // アイコンオブジェクト
};

