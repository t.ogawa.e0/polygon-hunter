//==========================================================================
// シーン遷移[SceneChange.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// Include
//==========================================================================
#include <Windows.h>
#include "dxlib.h"

//==========================================================================
// シーンのリスト
//==========================================================================
enum class SceneName
{
    begin = -1, // 無し
    Title, // タイトル
    Tutorial, // タイトル
    Game, // ゲーム
    Result, // リザルト
    MapEditor, // マップエディタ
    Default, // default
    end
};

//==========================================================================
//
// class  : CBaseScene
// Content: ベースとなる継承用クラス
//
//==========================================================================
class CBaseScene
{
public:
    // 初期化
	virtual bool Init(void) = 0;
	// 解放
	virtual void Uninit(void) = 0;
	// 更新
	virtual void Update(void) = 0;
	// 描画
	virtual void Draw(void) = 0;
protected:
    SceneName m_scene_name;
};

//==========================================================================
//
// class  : CSceneManager
// Content: 全てのシーンの管理
//
//==========================================================================
class CSceneManager
{
public:
	CSceneManager()
	{
		this->m_pScene = nullptr;
	}
	~CSceneManager()
	{
		if (this->m_pScene != nullptr)
		{
			this->m_pScene->Uninit();
			delete[] this->m_pScene;
			this->m_pScene = nullptr;
		}
	}
    // 初期化終了時にtrue
    bool Init(void);
	// シーンの切り替え
    void ChangeScene(SceneName Name);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
	CBaseScene* m_pScene; // 今のシーン
};
