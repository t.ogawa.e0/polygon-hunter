//==========================================================================
// 防御フィールド(継承用)[DefensiveFieldObject.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "DamageObject.h"

//==========================================================================
//
// class  : DefensiveFieldObject
// Content: 防御フィールド(継承用)
//
//=========================================================================
class DefensiveFieldObject : public mslib::DX9_Object
{
private:
    class param : public DamageObjectParam
    {
    public:
        param();
        param(const mslib::DX9_3DObject & obj, int life);
        ~param();
        // 耐久値の取得
        int GetLife(void);
        // 耐久値操作
        void ControlLife(int value);
    private:
        int m_life; // 耐久値
    };
private:
    // コピー禁止 (C++11)
    DefensiveFieldObject(const DefensiveFieldObject &) = delete;
    DefensiveFieldObject &operator=(const DefensiveFieldObject &) = delete;
public:
    DefensiveFieldObject(mslib::DX9_ObjectID ObjectID = mslib::DX9_ObjectID::Default);
    virtual ~DefensiveFieldObject();
    // 初期化
    virtual bool Init(void) = 0;
    // 解放
    virtual void Uninit(void) = 0;
    // 更新
    virtual void Update(void) = 0;
    // 描画
    virtual void Draw(void) = 0;
    // デバッグ
    virtual void Debug(void) = 0;
    // 防御フィールドの取得
    std::unordered_map<mslib::DX9_3DObject*, param> * GetDefensiveField(void);
protected:
    // ベクトルのセット
    void SetVector(void);
    // デバッグInput
    void InputParamText(void);
protected:
    std::unordered_map<mslib::DX9_3DObject*, param> m_obj; // オブジェクト
    std::unordered_map<mslib::DX9_3DObject*, mslib::DX9_3DObject> m_obj_mirror; // ミラーオブジェクト
    std::list<mslib::DX9_3DObject*> m_create_log; // 生成ログ
};

