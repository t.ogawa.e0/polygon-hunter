//==========================================================================
// プレイヤーアタックエフェクト_A[PlayerAttackEffect_A.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "EffectObject.h"

//==========================================================================
//
// class  : PlayerAttackEffect_A
// Content: オブジェクト管理クラスの拡張版
//
//==========================================================================
class PlayerAttackEffect_A : public EffectObject
{
private:
    class EffectParam : public EffectObject::ModelEffectParam
    {
    public:
        EffectParam();
        EffectParam(const mslib::DX9_3DObject *pObj, float speed, int limit_time, float rotZ);
        ~EffectParam();
        // 更新
        void Update(void)override;
    };
public:
    PlayerAttackEffect_A();
    ~PlayerAttackEffect_A();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
    // デバッグ
    void Debug(void)override;
    // 生成
    void Create(const mslib::DX9_3DObject *pObj, float speed, int limit_time);
    // オブジェクトのゲッター
    const std::list<EffectParam> * GetEffectObject(void);
private:
    std::list<EffectParam>m_param; // エフェクトパラメータ
    mslib::rand_float m_rotZ_rand; // Z軸回転の乱数範囲
    mslib::rand_float m_scale_rand; // scaleの乱数範囲
};
